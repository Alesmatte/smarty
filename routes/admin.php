<?php

use App\Http\Middleware\CrezioneModificaFilmMiddleware;
use App\Http\Middleware\CrezioneModificaSerieTvMiddleware;
use App\Http\Middleware\CrezioneModificaFilmmakerMiddleware;
use App\Http\Middleware\CrezioneModificaGruppiMiddleware;
use App\Http\Middleware\CrezioneModificaUtenzaMiddleware;

use App\Http\Middleware\ListaFilmMiddleware;
use App\Http\Middleware\ListaSerieTvMiddleware;
use App\Http\Middleware\ListaFilmmakerMiddleware;
use App\Http\Middleware\ListaGruppiMiddleware;
use App\Http\Middleware\ListaUtenzaMiddleware;

use App\Http\Middleware\GestioneRecensioiMiddleware;

use App\Http\Middleware\ImpostazioniSitoMiddleware;
use App\Support\Route;

Route::get('', 'DashboardController@show');
Route::get('/recent_users', 'DashboardController@users');
Route::get('/recent_users_normali', 'DashboardController@usersNormale');
Route::get('/recent_users_critici', 'DashboardController@usersCritico');
Route::get('/recent_users_admin', 'DashboardController@usersAdmin');
Route::get('/recent_reviews', 'DashboardController@reviews');
Route::get('/recent_reviews_normali', 'DashboardController@reviewsNormale');
Route::get('/recent_reviews_critici', 'DashboardController@reviewsCritici');
Route::post('', 'WelcomeController@backend');

Route::get('/create_film', 'CreateFilmController@show')->add(CrezioneModificaFilmMiddleware::class);
Route::post('/create_film', 'CreateFilmController@store')->add(CrezioneModificaFilmMiddleware::class);

Route::get('/modify_film/{idFilm}', 'ModifyFilmController@show')->add(CrezioneModificaFilmMiddleware::class);
Route::post('/modify_film/{idFilm}', 'ModifyFilmController@store')->add(CrezioneModificaFilmMiddleware::class);

Route::get('/user_list', 'UserListController@show')->add(ListaUtenzaMiddleware::class);
Route::post('/create_user', 'CreateUserController@createUser')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/create_normale_user', 'CreateUserNormaleController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/create_normale_user', 'CreateUserNormaleController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/create_critico_user', 'CreateUserCriticoController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/create_critico_user', 'CreateUserCriticoController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/create_admin_user', 'CreateUserAdminController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/create_admin_user', 'CreateUserAdminController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/modify_normal_user/{idUser}', 'ModifyUserNormaleController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/modify_normal_user/{idUser}', 'ModifyUserNormaleController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/modify_critico_user/{idUser}', 'ModifyUserCriticoController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/modify_critico_user/{idUser}', 'ModifyUserCriticoController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/modify_admin_user/{idUser}', 'ModifyUserAdminController@show')->add(CrezioneModificaUtenzaMiddleware::class);
Route::post('/modify_admin_user/{idUser}', 'ModifyUserAdminController@store')->add(CrezioneModificaUtenzaMiddleware::class);

Route::get('/groups_services', 'GruppoServicesListsController@show')->add(ListaGruppiMiddleware::class);
Route::post('/groups_services', 'GruppoServicesListsController@matriceGruppiServizi')->add(CrezioneModificaGruppiMiddleware::class);

Route::get('/create_group', 'CreateGroupController@show')->add(CrezioneModificaGruppiMiddleware::class);
Route::post('/create_group', 'CreateGroupController@store')->add(CrezioneModificaGruppiMiddleware::class);

Route::get('/modify_group/{idGruppo}', 'ModifyGroupController@show')->add(CrezioneModificaGruppiMiddleware::class);
Route::post('/modify_group/{idGruppo}', 'ModifyGroupController@store')->add(CrezioneModificaGruppiMiddleware::class);

Route::get('/review_list/{entita}/{id}', 'ReviewListController@show')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_list/{entita}/{id}/{limit}/{offset}', 'ReviewListController@show')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_list/{entita}/{id}/cerca', 'ReviewListController@cerca')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_list/{entita}/{id}/cerca/{limit}/{offset}', 'ReviewListController@cerca')->add(GestioneRecensioiMiddleware::class);

Route::get('/review_grouped_by_opere', 'ReviewGroupedByOperaController@show')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_grouped_by_opere/{limit}/{offset}', 'ReviewGroupedByOperaController@show')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_grouped_by_opere/cerca', 'ReviewGroupedByOperaController@cerca')->add(GestioneRecensioiMiddleware::class);
Route::get('/review_grouped_by_opere/cerca/{limit}/{offset}', 'ReviewGroupedByOperaController@cerca')->add(GestioneRecensioiMiddleware::class);

Route::get('/modal/{modal}', 'AdminModalController@show');
Route::post('/modal/{modal}', 'AdminModalController@store');

Route::get('/film', 'ListFilmSerieTvFilmmakerController@showFilm')->add(ListaFilmMiddleware::class);

Route::get('/serietv', 'ListFilmSerieTvFilmmakerController@showSerieTv')->add(ListaSerieTvMiddleware::class);

Route::get('/filmmaker', 'ListFilmSerieTvFilmmakerController@showFilmmaker')->add(ListaSerieTvMiddleware::class)->add(ListaFilmMiddleware::class);
Route::get('/listfilmmaker', 'ListFilmSerieTvFilmmakerController@showFilmmaker')->add(ListaSerieTvMiddleware::class)->add(ListaFilmMiddleware::class);

Route::get('/create_serietv', 'CreateSerieTvController@show')->add(CrezioneModificaSerieTvMiddleware::class);
Route::post('/create_serietv', 'CreateSerieTvController@store')->add(CrezioneModificaSerieTvMiddleware::class);

Route::get('/modify_serietv/{idSerieTv}','ModifySerieTvController@show')->add(CrezioneModificaSerieTvMiddleware::class);
Route::post('/modify_serietv/{idSerieTv}','ModifySerieTvController@store')->add(CrezioneModificaSerieTvMiddleware::class);

Route::get('/create_filmmaker', 'CreateFilmmakerController@show')->add(CrezioneModificaFilmmakerMiddleware::class);
Route::post('/create_filmmaker', 'CreateFilmmakerController@store')->add(CrezioneModificaFilmmakerMiddleware::class);

Route::get('/modify__filmmaker/{idFilmmaker}', 'ModifyFilmmakerController@show')->add(CrezioneModificaFilmmakerMiddleware::class);
Route::post('/modify__filmmaker/{idFilmmaker}', 'ModifyFilmmakerController@store')->add(CrezioneModificaFilmmakerMiddleware::class);

Route::get('/impostazioni', 'ImpostazioniSitoController@show')->add(ImpostazioniSitoMiddleware::class);
Route::post('/impostazioni', 'ImpostazioniSitoController@store')->add(ImpostazioniSitoMiddleware::class);

Route::get('/generi', 'GenereController@showlist')->add(ListaSerieTvMiddleware::class)->add(ListaFilmMiddleware::class);