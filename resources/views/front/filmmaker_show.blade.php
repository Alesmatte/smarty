@extends('skeletons.front.app')

@section('content')
    <!-- COVER -->
    <section class="py-2">

        <div class="container min-h-50vh d-middle pt-3">

            <div class="row text-center-xs">

                <div class="col-12 col-md-6 pb-3" data-aos="fade-in" data-aos-delay="200">

                    @if($filmmaker->getImgProfilo()->getDato() != null)
                    <!-- image -->
                    <img width="450" height="250" class="ml-2 img-fluid rounded-xl" src="{{asset($filmmaker->getImgProfilo()->getDato())}}" alt="{{$filmmaker->getImgProfilo()->getNome()}}">
                    @else
                            <img width="450" height="250" class="ml-2 img-fluid rounded-xl" src="{{asset('assets/images/the_avengers/star_big.jpg')}}" alt="star_big.jpg">
                    @endif
                    <!-- lines, looks like through a glass -->
                    <div class="absolute-full w-100 overflow-hidden opacity-6">
                        <img class="img-fluid" width="1000" height="1000" src="{{asset('assets/images/masks/shape-line-lense.svg')}}" alt="shape-line-lense.svg">
                    </div>

                </div>

                <div class="col-12 col-md-6 d-middle">

                    <div class="my-4" data-aos="fade-in" data-aos-delay="300">

                        <div class="bg-light p-4 rounded-xl">

                            <!-- mobile : top arrow : documentation/util-misc.html -->
                            <i class="arrow arrow-lg arrow-top arrow-center border-light d-block d-md-none"></i>
                            <!-- desktop side arrow : documentation/util-misc.html -->
                            <i class="arrow arrow-lg arrow-start mt-3 border-light d-none d-md-inline-block d-lg-inline-block d-xl-inline-block"></i>

                            <h1 class="h2-xs font-weight-medium mb-0">
                                {{$filmmaker->getNome()}} {{$filmmaker->getCognome()}}
                            </h1>

                            <p class="text-primary h4 h6-xs font-weight-light mb-4">
                               {{decodifica_array_filmmakers_to_string($filmmaker->getTipologiaPrincipale())}}
                            </p>

                            <p class="lead max-w-600">
                                {{$filmmaker->getBreveDescrizione()}}
                            </p>

                        </div>

                    </div>

                </div>


            </div>


        </div>

    </section>
    <!-- /COVER -->




    <!-- QUOTE -->
    <section class="py-2">
        <div class="container">

            <blockquote class="blockquote text-center" data-aos="fade-in" data-aos-delay="250" data-aos-offset="0">
                <p class="fs--30 h1 h2-xs max-w-800 m-auto font-weight-light font-italic text-muted">
                    {{$filmmaker->getCitazione()}}
                </p>

                @if($filmmaker->getFilmSerieTvCitazione() != null)
                <footer class="blockquote-footer mt-4 fs--25 font-weight-light text-gray-500">
                    <cite title="Source Title">dal film {{$filmmaker->getFilmSerieTvCitazione()->getTitolo()}} ({{$filmmaker->getFilmSerieTvCitazione()->getDataUscita()}})</cite>
                </footer>
                @endif
            </blockquote>

        </div>
    </section>
    <!-- /QUOTE -->

    <!-- TEMPORARY SECTION -->
    <section class="pt--20 pb-0">
        <div class="container pl-0 pr-0 pt-0 pb-3">
            <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link active" id="pills-biography-tab" data-toggle="pill" href="#pills-biography" role="tab"
                       aria-controls="pills-biography" aria-selected="true">Biografia</a>
                </li>

                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link " id="pills-filmography-tab" data-toggle="pill" href="#pills-filmography" role="tab"
                       aria-controls="pills-filmography" aria-selected="false">Filmografia</a>
                </li>
                <li class="nav-item col pl--0 pr--3">
                    <a class="nav-link" id="pills-awards-tab" data-toggle="pill" href="#pills-awards" role="tab"
                       aria-controls="pills-awards" aria-selected="false">Premi</a>
                </li>
                <li class="nav-item col p--0">
                    <a class="nav-link" id="pills-photo-tab" data-toggle="pill" href="#pills-photo" role="tab"
                       aria-controls="pills-photo" aria-selected="false">Foto</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">

                <div class="js-ajax tab-pane fade show active" id="pills-biography" role="tabpanel" aria-labelledby="pills-biography-tab" data-ajax-url="{{asset('filmmaker/'.$filmmaker->getIdFilmMaker().'/biografia')}}" >
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-filmography" role="tabpanel" aria-labelledby="pills-filmography-tab"  data-ajax-url="{{asset('filmmaker/'.$filmmaker->getIdFilmMaker().'/filmografia')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-awards" role="tabpanel" aria-labelledby="pills-awards-tab" data-ajax-url="{{asset('filmmaker/'.$filmmaker->getIdFilmMaker().'/premi')}}">
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

                <div class="tab-pane fade show js-ajax" id="pills-photo" role="tabpanel" aria-labelledby="pills-photo-tab" data-ajax-url="{{asset('filmmaker/'.$filmmaker->getIdFilmMaker().'/foto')}}" >
                    <div class="text-center p--30">
                        <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /TEMPORARY SECTION -->
@if(count($sliders)>0)
    <!-- CAROSELLO FILM CORRELATI-->
    <section class="pt-0 pl-3 pr-3">
        <div class="container bg-c-6f3 mt--50"> <!-- style="color: #fff6f3" -->
            <div class="mt--25">
                <div class="row">
                    <div class="column col-2">
                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo"> <!--  style="color:#f3eed9;background-color:#BB3132" -->
                    </div>
                    <div class="column col-8">
                        <h1 class="fs&#45;&#45;30 text-center text-primary"> OPERE CORRELATE</h1>
                    </div>
                    <div class="column col-2">
                        <hr class="h&#45;&#45;1 bw&#45;&#45;0 linea-titolo">
                    </div>
                </div>
                <div class="swiper-container p--30" data-swiper='{
	"slidesPerView": 4,
	"spaceBetween": 0,
	"slidesPerGroup": 4,
	"loop": false,
	"autoplay": false
}'>

                    <div class="swiper-wrapper hide">
                        @foreach($sliders as $k=>$slider)
                            @if($slider instanceof \App\Model\Film)
                                <div class="swiper-slide">
                                    <div class="col-12">
                                        <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                            <div class="clearfix p--15">
                                                <img width="217.5" height="321.891"
                                                     src="{{ asset($slider->getImgCopertina()->getDato()) }}" alt="...">
                                            </div>
                                            <div class="card-body font-weight-light pt--0 pb--15">
                                                <div class="d-table">
                                                    <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($slider->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">{{$slider->mediaVotoRedCarpet}}</span>
                                    </span>
                                                        <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                           href="{{asset('film/' . $slider->getIdFilm())}}">
                                                            {{$slider->getTitolo()}}
                                                        </a>
                                                        <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                            {{$slider->getDescrizione()}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer bg-transparent b-0 pt--4">
                                                <hr class="mt--0 mb--15 bg-primary">
                                                <p class="text-center mb--4 fs--20 text-dark">
                                                    {{date_format(date_create($slider->getDataUscita()), 'd/m/Y') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @if($slider instanceof \App\Model\SerieTv)
                                    <div class="swiper-slide">
                                        <div class="col-12">
                                            <div class="card b-0 shadow-md shadow-lg-hover transition-all-ease-250 transition-hover-top h-100">
                                                <div class="clearfix p--15">
                                                    <img width="217.5" height="321.891"
                                                         src="{{ asset($slider->getImgCopertina()->getDato()) }}"
                                                         alt="...">
                                                </div>
                                                <div class="card-body font-weight-light pt--0 pb--15">
                                                    <div class="d-table">
                                                        <div class="d-table-cell align-bottom">
                                    <span class="d-block mb--5">
                                        <i class="rating-{{voto_stelle($slider->mediaVotoRedCarpet)}} text-warning fs--18"></i>
                                        <span class="fs--17 font-weight-bolder pl--5 text-dark">{{$slider->mediaVotoRedCarpet}}</span>
                                    </span>
                                                            <a class="h5 card-title mb--5 h--50 overflow-hidden text-truncate-2 text-decoration-none text-dark"
                                                               href="{{asset('serietv/' . $slider->getIdSerieTv())}}">
                                                                {{$slider->getTitolo()}}
                                                            </a>
                                                            <p class="lead overflow-hidden mb--0 h--105 fs--18 text-truncate-4">
                                                                {{$slider->getDescrizione()}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-footer bg-transparent b-0 pt--4">
                                                    <hr class="mt--0 mb--15 bg-primary">
                                                    <p class="text-center mb--4 fs--20 text-dark">
                                                        {{date_format(date_create($slider->getDataSviluppoSerieTv()), 'd/m/Y') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    </div> <!-- QUA FINISCE LO SWIPER-WRAPPER -->
                    <!-- FRECCIE -->
                    <div class="swiper-button-next swiper-button-redcarper-dx"></div>   <!-- BOTTONE DESTRO -->
                    <div class="swiper-button-prev swiper-button-redcarper-sx"></div>   <!-- BOTTONE SINISTRO -->
                </div>

            </div>
    </section>
    <!-- /CAROSELLO FILM CORRELATI-->
    @endif
@endsection