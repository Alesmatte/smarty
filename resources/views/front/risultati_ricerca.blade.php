@extends('skeletons.front.app')

@section('content')

    <!-- TITOLO-->
    <div class="container bg-c-6f3 mb--100 mt--20 pt--30 pb--30 rounded-xl">
        <div class="row pt--8">
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
            <div class="col-lg-8 text-center">
                <p class="font-weight-lighter fs--60 text-capitalize mb--0">
                    Risultati Ricerca
                </p>
            </div>
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
        </div>

        <div class="row d-flex justify-content-center">
            <p class="text-center fs--20 mt--16">
                Sono stati trovati <strong> {{ $elemTot }} elementi</strong>.
            </p>
        </div>

        <!-- ITEM FILM -->
        @foreach($filmList as $k=>$film)
            <div class="cart-item shadow-xs mb-4 rounded border bg-white cart-pd-8-16-16-16">
            <!-- riga parte 1 -->
            <div class="row">
                <div class="col-4 col-sm-4 col-md-2 col-lg-2 text-center pt--8">
                    <img class="img-fluid" src="{{ asset($film->getImgCopertina()->getDato()) }}" alt="...">
                </div>
                <div class="col-8 col-sm-8 col-md-10 col-lg-10">
                    <a class="cart-title-26-34 text-capitalize d-block text-primary mb--2 font-weight-bold text-decoration-none" href="{{asset('film/' . $film->getIdFilm())}}">
                        {{$film->getTitolo()}}
                    </a>
                    <h2 class="fs--18 opacity-6 mb--2">
                        {{ $film->getTramaBreve() }}
                    </h2>
                    <div>
                        @if($film->mediaVotoRedCarpet >= 3.50)
                        <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-verde fs--13 font-weight-bold">
                            <strong>{{$film->mediaVotoRedCarpet}} </strong>
                            - CONSIGLIATO: SI
                        </span>
                        @elseif($film->mediaVotoRedCarpet >= 2.00 && $film->mediaVotoRedCarpet < 3.50)
                            <span class="pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-giallo">
                                <strong>{{$film->mediaVotoRedCarpet}} </strong>
                                - CONSIGLIATO: NI
                            </span>
                        @elseif($film->mediaVotoRedCarpet > 0 && $film->mediaVotoRedCarpet < 2.00)
                            <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-grigio">
                                <strong>{{$film->mediaVotoRedCarpet}} </strong>
                                - CONSIGLIATO: NO
                            </span>
                        @elseif($film->mediaVotoRedCarpet == 0)
                            <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-grigio">
                                CONSIGLIATO: N.D.
                            </span>
                        @endif
                        <small class="pl--3 fs--12 opacity-9"> *media giudizi di pubblico e critica</small>
                    </div>
                    <div class="mt--12 mb--0">
                        <p class="fs--16 mb-0">
                            Un film di
                            @foreach($film->registi as $ke=>$reg)
                                    <a class="text-shadow-dark text-decoration-none font-weight-bold" href="{{asset('filmmaker/' . $reg[0])}}">{{$reg[1]}}</a>@if( $ke +1 == count($film->registi)). @else, @endif @endforeach
                                Con
                            @foreach($film->attori as $key=>$att)
                                <a class="text-shadow-dark text-decoration-none" href="{{asset('filmmaker/' . $att[0])}}">{{$att[1]}}</a>@if($key +1 == count($film->attori)). @else, @endif @endforeach
                    </div>
                    <div class="mt--12">
                        <p class="mb--0">
                            <strong>Genere:</strong> @foreach($film->getGeneri() as $keyy=>$genere)
                            <a href="{{asset('lista_film?genere='.$genere->getIdGenere())}}" class="text-shadow-dark text-decoration-none">{{$genere->getNome()}}</a>@if($keyy +1 == count($film->getGeneri())). @else, @endif @endforeach
                        </p>
                    </div>
                    <div>
                        <strong>Distributore:</strong> @foreach($film->getDistributori() as $keya=>$distr)
                        <a class="text-shadow-dark text-decoration-none" href="{{asset('lista_film?distributore='.$distr->getIdDistributore())}}"> {{$distr->getNome()}}</a>@if($keya +1 == count($film->getDistributori())). @else, @endif @endforeach
                        <strong class="ml--5">Uscita:</strong>
                        <span class="opacity-9 text-shadow-dark">{{italian_date_str($film->getDataUscita())}}</span>.
                        <strong class="ml--5">Durata:</strong>
                        <span class="opacity-9 text-shadow-dark">{{$film->getDurata()}}'</span>.
                    </div>
                    <!--  rating -->
                    <div class="row mt--12">
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($film->mediaVotoRedCarpet)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">SMARTY.IT <strong>@if($film->mediaVotoRedCarpet == 0) N.D. @else {{$film->mediaVotoRedCarpet}} @endif</strong></p>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($film->mediaVotoCritici)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">CRITICA <strong>@if($film->mediaVotoCritici == 0) N.D. @else {{$film->mediaVotoCritici}} @endif</strong></p>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($film->mediaVotoUtenti)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">PUBBLICO <strong>@if($film->mediaVotoUtenti == 0) N.D. @else {{$film->mediaVotoUtenti}} @endif</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <!-- ITEM SERIE TV -->
        @foreach($serieTvList as $ke=>$serieTv)
            <div class="cart-item shadow-xs mb-4 rounded border bg-white cart-pd-8-16-16-16">
            <!-- riga parte 1 -->
            <div class="row">
                <div class="col-4 col-sm-4 col-md-2 col-lg-2 text-center pt--8">
                    <img class="img-fluid" src="{{ asset($serieTv->getImgCopertina()->getDato()) }}" alt="...">
                </div>
                <div class="col-8 col-sm-8 col-md-10 col-lg-10">
                    <a class="cart-title-26-34 text-capitalize d-block text-primary mb--2 font-weight-bold text-decoration-none" href="{{asset('serietv/'. $serieTv->getIdSerieTv())}}">
                        {{$serieTv->getTitolo()}}
                    </a>
                    <h2 class="fs--18 opacity-6 mb--2">
                        {{$serieTv->getTramaBreve()}}
                    </h2>
                    <div>
                        @if($serieTv->mediaVotoRedCarpet >= 3.50)
                            <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-verde fs--13 font-weight-bold">
                            <strong>{{$serieTv->mediaVotoRedCarpet}} </strong>
                            - CONSIGLIATO: SI
                        </span>
                        @elseif($serieTv->mediaVotoRedCarpet >= 2.00 && $serieTv->mediaVotoRedCarpet < 3.50)
                            <span class="pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-giallo">
                                <strong>{{$serieTv->mediaVotoRedCarpet}} </strong>
                                - CONSIGLIATO: NI
                            </span>
                        @elseif($serieTv->mediaVotoRedCarpet > 0 && $serieTv->mediaVotoRedCarpet < 2.00)
                            <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-grigio">
                                <strong>{{$serieTv->mediaVotoRedCarpet}} </strong>
                                - CONSIGLIATO: NO
                            </span>
                        @elseif($serieTv->mediaVotoRedCarpet == 0)
                            <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl fs--13 font-weight-bold bg-color-grigio">
                                CONSIGLIATO: N.D.
                            </span>
                        @endif
                        <small class="pl--3 fs--12 opacity-9"> *media giudizi di pubblico e critica</small>
                    </div>
                    <div class="mt--12 mb--0">
                        <p class="fs--16 mb-0">
                            Un serie tv di
                            @foreach($serieTv->registi as $ke=>$reg)
                                <a class="text-shadow-dark text-decoration-none font-weight-bold" href="{{asset('filmmaker/' . $reg[0])}}">{{$reg[1]}}</a>@if( $ke +1 == count($serieTv->registi)). @else, @endif @endforeach
                            Con
                            @foreach($serieTv->attori as $key=>$att)
                                <a class="text-shadow-dark text-decoration-none" href="{{asset('filmmaker/' . $att[0])}}">{{$att[1]}}</a>@if($key +1 == count($serieTv->attori)). @else, @endif @endforeach
                    </div>
                    <div class="mt--12">
                        <p class="mb--0">
                            <strong>Genere:</strong> @foreach($serieTv->getGeneri() as $keyy=>$genere)
                                <a href="{{asset('lista_serietv?genere='.$genere->getIdGenere())}}" class="text-shadow-dark text-decoration-none">{{$genere->getNome()}}</a>@if($keyy +1 == count($serieTv->getGeneri())). @else, @endif @endforeach
                        </p>
                    </div>
                    <div>
                        <strong>Distributore:</strong> @foreach($serieTv->getDistributori() as $keya=>$distr)
                            <a class="text-shadow-dark text-decoration-none" href="{{asset('lista_serietv?distributore='.$distr->getIdDistributore())}}"> {{$distr->getNome()}}</a>@if($keya +1 == count($serieTv->getDistributori())). @else, @endif @endforeach
                        <strong class="ml--5">Uscita:</strong>
                        <span class="opacity-9 text-shadow-dark">{{italian_date_str($serieTv->getDataUscita())}}</span>.
                    </div>
                    <div class="mt--12">
                        <strong>Stagioni:</strong>
                        <span class="opacity-9 text-shadow-dark">{{$serieTv->stagioniTotali}}</span>.
                        <strong class="ml--5">Episodi:</strong>
                        <span class="opacity-9 text-shadow-dark">{{$serieTv->episodiTotali}}</span>.
                        <strong class="ml--5">Durata:</strong>
                        <span class="opacity-9 text-shadow-dark">{{$serieTv->durataEpisodio}}'</span>.
                    </div>
                    <!--  rating -->
                    <div class="row mt--12">
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($serieTv->mediaVotoRedCarpet)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">REDCARPET.IT <strong>@if($serieTv->mediaVotoRedCarpet == 0) N.D. @else {{$serieTv->mediaVotoRedCarpet}} @endif</strong></p>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($serieTv->mediaVotoCritici)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">CRITICA <strong>@if($serieTv->mediaVotoCritici == 0) N.D. @else {{$serieTv->mediaVotoCritici}} @endif</strong></p>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="pt-1">
											<span class="d-block">
												<i class="rating-{{voto_stelle($serieTv->mediaVotoRedCarpet)}} text-warning fs--18"></i>
											</span>
                                <p class="fs--13 mb--0">PUBBLICO <strong>@if($serieTv->mediaVotoRedCarpet == 0) N.D. @else {{$serieTv->mediaVotoRedCarpet}} @endif</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <!-- ITEM PERSONA -->
        @foreach($personaList as $key=>$persona)
            <div class="cart-item shadow-xs mb-4 rounded border bg-white cart-pd-8-16-16-16">
            <!-- riga parte 1 -->
            <div class="row">
                <div class="col-4 col-sm-4 col-md-2 col-lg-2 text-center pt--8 h--250">
                    <img class="img-fluid" src="{{asset($persona->getImgAvatar()->getDato())}}" alt="..." >
                </div>
                <div class="col-8 col-sm-8 col-md-10 col-lg-10">
                    <a class="cart-title-26-34 text-capitalize d-block text-primary mb--2 font-weight-bold text-decoration-none" href="{{asset('filmmaker/' . $persona->getIdFilmMaker())}}">
                        {{$persona->getNome()}} {{$persona->getCognome()}}
                    </a>
                    <h2 class="fs--18 opacity-6 mb--2">
                        {{$persona->getBreveDescrizione()}}
                    </h2>
                    <div class="mt--12">
                        <strong>Ruolo:</strong>
                        @foreach($persona->ruoli as $keyr=>$ruolo)
                            @if($keyr === array_key_last($persona->ruoli))
                                <a class="text-decoration-none text-shadow-dark" href="{{asset('filmmaker_list/'.$ruolo)}}">{{decodifica_array_filmmakers_to_string($ruolo)}}</a>.
                            @else
                        <a class="text-decoration-none text-shadow-dark" href="{{asset('filmmaker_list/'.$ruolo)}}">{{decodifica_array_filmmakers_to_string($ruolo)}}</a>,
                            @endif
                        @endforeach
                            <p> @if(count($persona->premi) != 0)
                            <strong>Premi vinti:</strong> @foreach($persona->premi as $keyp=>$prem)
                            <span class="text-shadow-dark opacity-9"> {{$prem}}({{$persona->premioCount[$keyp]}})</span>@if($keyp +1 == count($persona->premi)). @else, @endif @endforeach
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        @if(empty($filmList) && empty($serieTvList) && empty($personaList))
        @else
            <!-- pagination : center -->
            <nav aria-label="pagination" class="my-5 mt--100">
                <ul class="pagination pagination-pill justify-content-center">
                    @if($currentPage == 1)
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Precedente</a>
                    </li>
                    @else
                    <li class="page-item">
                        <a class="page-link" href="{{asset('risultati_ricerca/'.($currentPage-1).'?'.$queryString)}}">Precedente</a>
                    </li>
                    @endif

                    @foreach($pagine as $pagina)
                        <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                            @if($pagina === '...')
                                <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                            @else
                                <a class="page-link" @if($currentPage != $pagina) href="{{ asset('risultati_ricerca/' . $pagina.'?'.$queryString) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                            @endif
                        </li>
                    @endforeach

                    @if($currentPage == $numeroPagine)
                            <li class="page-item disabled">
                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                            </li>
                        @else
                            <li class="page-item">
                                <a class="page-link" href="{{ asset('risultati_ricerca/'.($currentPage+1).'?'.$queryString ) }}">Successiva</a>
                            </li>
                        @endif

                </ul>
            </nav>
            <!-- /pagination : center -->
        @endif

    </div>


@endsection