@extends('skeletons.front.app')

@section('content')

    <!-- TITOLO-->
    <div class="container bg-c-6f3 mb--100 mt--20 pt--30 pb--30 rounded-xl">
        <div class="row pt--8">
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
            <div class="col-lg-8 text-center">
                <p class="font-weight-lighter fs--60 text-capitalize mb--0">
                    {{$titolo}}
                </p>
            </div>
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
        </div>

        <div class="row d-flex justify-content-center">
            <p class="text-center fs--20 mt--16">
                Sono stati trovati <strong> {{ $totFilmmaker }} elementi</strong>.
            </p>
        </div>

    <!-- ITEM PERSONA -->
        @foreach($personaList as $key=>$persona)
            <div class="cart-item shadow-xs mb-4 rounded border bg-white cart-pd-8-16-16-16">
                <!-- riga parte 1 -->
                <div class="row">
                    <div class="col-4 col-sm-4 col-md-2 col-lg-2 text-center pt--8 h--250">
                        <img class="img-fluid" src="{{asset($persona->getImgAvatar()->getDato())}}" alt="..." >
                    </div>
                    <div class="col-8 col-sm-8 col-md-10 col-lg-10">
                        <a class="cart-title-26-34 text-capitalize d-block text-primary mb--2 font-weight-bold text-decoration-none" href="{{asset('filmmaker/' . $persona->getIdFilmMaker())}}">
                            {{$persona->getNome()}} {{$persona->getCognome()}}
                        </a>
                        <h2 class="fs--18 opacity-6 mb--2">
                            {{$persona->getBreveDescrizione()}}
                        </h2>
                        <div class="mt--12">
                            <strong>Ruolo:</strong>
                            @foreach($persona->ruoli as $keyr=>$ruolo)
                                @if($keyr === array_key_last($persona->ruoli))
                                    <a class="text-decoration-none text-shadow-dark" href="{{asset('filmmaker_list/'.$ruolo)}}">{{decodifica_array_filmmakers_to_string($ruolo)}}</a>.
                                @else
                                    <a class="text-decoration-none text-shadow-dark" href="{{asset('filmmaker_list/'.$ruolo)}}">{{decodifica_array_filmmakers_to_string($ruolo)}}</a>,
                                @endif
                            @endforeach
                            <p> @if(count($persona->premi) != 0)
                                    <strong>Premi vinti:</strong> @foreach($persona->premi as $keyp=>$prem)
                                        <span class="text-shadow-dark opacity-9"> {{$prem}}({{$persona->premioCount[$keyp]}})</span>@if($keyp +1 == count($persona->premi)). @else, @endif @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    @endforeach

    @if(!empty($personaList))
    <!-- pagination : center -->
        <nav aria-label="pagination" class="my-5 mt--100">
            <ul class="pagination pagination-pill justify-content-center">
                @if($currentPage == 1)
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Precedente</a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{asset('filmmaker_list/'.$tipologia.'/'.($currentPage-1))}}">Precedente</a>
                    </li>
                @endif

                @foreach($pagine as $pagina)
                    <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                        @if($pagina === '...')
                            <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                        @else
                            <a class="page-link" @if($currentPage != $pagina) href="{{ asset('filmmaker_list/'.$tipologia.'/'. $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                        @endif
                    </li>
                @endforeach

                @if($currentPage == $numeroPagine)
                    <li class="page-item disabled">
                        <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ asset('filmmaker_list/'.$tipologia.'/'.($currentPage+1)) }}">Successiva</a>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- /pagination : center -->
    @endif

    </div>


@endsection