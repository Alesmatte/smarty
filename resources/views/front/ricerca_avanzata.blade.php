@extends('skeletons.front.app')

@section('content')

    <!-- CONTENT -->
    <div class="container bg-c-6f3 mb--50 pt--30">
        <div class="row pt--8">
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
            <div class="col-lg-8 text-center">
                <p class="font-weight-lighter fs--60 text-capitalize mb--0">
                    Ricerca Avanzata
                </p>
            </div>
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines"></div>
            </div>
        </div>
        <form action="{{asset('risultati_ricerca')}}" method="POST" class="pt--20 pb--50" id="lista_ricerca">
                @csrf
            <div class="row mb--20">
                <div class="col-12 col-md">
                    <div class="form-label-group mb-3">
                        <input placeholder="Titolo Italiano / Titolo Originale" id="titolo_ita_orig" type="text" value="" name="titolo_ita_orig"
                               class="form-control">
                        <label for="titolo_ita_orig">Titolo Italiano / Titolo Originale</label>
                    </div>
                </div>
            </div>

            <div class="row mb--3">
                <div class="col-3 col-md">
                    <div class="form-label-group mb-3">
                        <input placeholder="(AAAA)" id="anno_distribuzione" name="anno_distribuzione" type="text" value=""
                               class="form-control"
                               pattern="[1-2][0-9]{3}">
                        <label for="anno_distribuzione">Anno di Distribuzione</label>
                        <small class="d-block text-muted fs--68perc">* deve iniziare con 1 o 2 e deve avere in totale 4
                            cifre</small>
                    </div>
                </div>

                <div class="col-3 col-md">
                    <div class="form-label-group mb-3">
                        <select id="select_cerc_anno_dis" name="select_cerc_anno_dis" class="form-control">
                            <option value="0"></option>
                            <option value="1">Minore di</option>
                            <option value="2">Minore e Uguale a</option>
                            <option value="3">Uguale a</option>
                            <option value="4">Maggiore e Uguale a</option>
                            <option value="5">Maggiore di</option>
                        </select>
                        <label for="select_cerc_anno_dis">Come cercare Anno di Distribuzione</label>
                    </div>
                </div>

                <div class="col-3 col-md">
                    <div class="form-label-group mb-3">
                        <select id="stelle" name="stelle" class="form-control">
                            <option value=""></option>
                            <option value="1">Nessuna Stella</option>
                            <option value="2">1 Stella</option>
                            <option value="3">2 Stelle</option>
                            <option value="4">3 Stelle</option>
                            <option value="5">4 Stelle</option>
                            <option value="6">5 Stelle</option>
                        </select>
                        <label for="stelle">Stelle</label>
                    </div>
                </div>
                <div class="col-3 col-md">
                    <div class="form-label-group mb-3">
                        <select id="select_cerc_stelle" name="select_cerc_stelle" class="form-control">
                            <option value="0"></option>
                            <option value="1">Minore di</option>
                            <option value="2">Minore e Uguale a</option>
                            <option value="3">Uguale a</option>
                            <option value="4">Maggiore e Uguale a</option>
                            <option value="5">Maggiore di</option>
                        </select>
                        <label for="select_cerc_stelle">Come cercare Stelle</label>
                    </div>
                </div>
            </div>

            <div class="row mb--20">
                <div class="col-4 col-md">
                    <div class="form-label-group mb-3">
                        <select id="premi" name="premi" class="form-control">
                            <option value=""></option>
                            @foreach($premioList as $premio)
                                <option value="{{$premio->getIdPremio()}}">{{$premio->getNome()}}</option>
                            @endforeach
                        </select>
                        <label for="premi">Premi</label>
                    </div>
                </div>

                <div class="col-4 col-md">
                    <div class="form-label-group mb-3">
                        <select id="genere" name="genere" class="form-control">
                            <option value=""></option>
                            @foreach($genereList as $genere)
                                <option value="{{$genere->getIdGenere()}}">{{$genere->getNome()}}</option>
                            @endforeach
                        </select>
                        <label for="genere">Genere</label>
                    </div>
                </div>

                <div class="col-4 col-md">
                    <div class="form-label-group mb-3">
                        <select id="distributore" name="distributore" class="form-control">
                            <option value=""></option>
                            @foreach($distributoreList as $distributore)
                                <option value="{{$distributore->getIdDistributore()}}">{{$distributore->getNome()}}</option>
                            @endforeach
                        </select>
                        <label for="distributore">Distributore</label>
                    </div>
                </div>
            </div>

                <div class="row mb--20">
                    <div class="col-6 col-md">
                        <div class="form-label-group mb-3">
                            <input placeholder="Persone" id="persone" type="text" value="" name="persone" class="form-control">
                            <label for="persone">Persone</label>
                        </div>
                    </div>
                    <div class="col-6 col-md">
                        <div class="form-label-group mb-3">
                            <select id="ruolo" class="form-control" name="ruolo">
                                <option value=""></option>
                                <option value="0">Altro</option>
                                <option value="1">Attore</option>
                                <option value="2">Regista</option>
                                <option value="3">Distributore</option>
                                <option value="4">Produttore</option>
                                <option value="5">Sceneggiatore</option>
                                <option value="6">Fotografia</option>
                                <option value="7">Montaggio</option>
                                <option value="8">Musica</option>
                                <option value="9">Scenografo</option>
                                <option value="10">Costumista</option>
                                <option value="11">Effetti</option>
                                <option value="12">Art Director</option>
                                <option value="13">Trucco</option>
                            </select>
                            <label for="ruolo">Ruolo</label>
                        </div>
                    </div>
                </div>

            <div class="row mb--20">
                <div class="col-6 col-md">
                    <div class="form-label-group mb-3">
                        <select id="match" name="match" class="form-control">
                            <option value="1">Film</option>
                            <option value="2">Serie TV</option>
                            <option value="3">Persone</option>
                            <option value="4">Film e Serie TV</option>
                            <option value="5">Film e Persone</option>
                            <option value="6">Serie TV e Persone</option>
                            <option selected value="7">Film, Serie TV e Persone</option>
                        </select>
                        <label for="match">Match</label>
                    </div>
                </div>
                <div class="col-6 col-md">
                    <div class="form-label-group mb-3">
                        <select id="ordina" name="ordina" class="form-control">
                            <option value="1">Titolo</option>
                            <option value="2">Anno (decrescente)</option>
                            <option value="3">Anno (crescente)</option>
                            <option value="4">Stelle (decrescente)</option>
                        </select>
                        <label for="ordina">Ordina tutto per</label>
                    </div>
                </div>
            </div>

            <div>
                <button type="submit" class="btn btn-primary pl--50 pr--50 mr--15 text-decoration-none">Cerca</button>
                <!--<button type="submit" class="btn btn-primary pl&#45;&#45;50 pr&#45;&#45;50 mr&#45;&#45;15">Cerca</button>-->
                <a href="#"
                   data-target-reset="#titolo_ita_orig,#regista,#attore,#doppiatore,#anno_distribuzione,#anno_premi,#premi,#select_cerc_stelle,#select_cerc_anno_dis,"
                   class="form-advanced-reset js-ignore btn pl--50 pr--50 text-primary reset-hover" id="reset">
                    Reset
                </a>
            </div>

        </form>

    </div>
    <!-- /CONTENT -->

@endsection
@section('script')
    @parent
    <script>
        <!-- SCRIPT SUL CAMBIAMENTO DELLE STELLE -->
        $(document).ready(function () {
            // siccome è impostato su "Nessuna Stella" allora mi da di base "Uguale a"
            $("#select_cerc_stelle").prop("disabled", true);
            $("#select_cerc_stelle").children().each(function () {
                if ($(this).attr("value") === '0') {
                    $(this).prop('selected', true);
                }
            })
            $('#stelle').change(function () {
                switch ($(this).val()) {
                    case "0":
                        $("#select_cerc_stelle").prop("disabled", true);
                        $("#select_cerc_stelle").children().each(function () {
                            if ($(this).attr("value") === '0') {
                                $(this).prop('selected', true);
                            }
                        });
                        break;
                    case "1":
                        $("#select_cerc_stelle").prop("disabled", false);
                        $("#select_cerc_stelle").children().each(function () {
                            if ($(this).attr("value") !== '3') {
                                $(this).hide();
                            } else {
                                $(this).show();
                            }
                            if ($(this).attr("value") === '3') {
                                $(this).prop('selected', true);
                            }
                            if ($(this).attr("value") === '0') {
                                $(this).prop('selected', false);
                            }
                        });
                        break;
                    case "2":
                        $("#select_cerc_stelle").prop("disabled", false);
                        $("#select_cerc_stelle").children().each(function () {
                            if ($(this).attr("value") !== '1' && $(this).attr("value") !== '2' && $(this).attr("value") !== '0') {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                            if ($(this).attr("value") === '3') {
                                $(this).prop('selected', true);
                            }
                            if ($(this).attr("value") === '0') {
                                $(this).prop('selected', false);
                            }
                        });
                        break;
                    case "6":
                        $("#select_cerc_stelle").prop("disabled", false);
                        $("#select_cerc_stelle").children().each(function () {
                            if ($(this).attr("value") !== '4' && $(this).attr("value") !== '5' && $(this).attr("value") !== '0') {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                            if ($(this).attr("value") === '3') {
                                $(this).prop('selected', true);
                            }
                            if ($(this).attr("value") === '0') {
                                $(this).prop('selected', false);
                            }
                        });
                        break;
                    default :
                        $("#select_cerc_stelle").prop("disabled", false);
                        $("#select_cerc_stelle").children().each(function () {
                            if ($(this).attr("value") !== '0') {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                            if ($(this).attr("value") === '3') {
                                $(this).prop('selected', true);
                            }
                            if ($(this).attr("value") === '0') {
                                $(this).prop('selected', false);
                            }
                        });
                }
            })
            // SCRIPT SULL'ANNO
            $("#select_cerc_anno_dis").prop("disabled", true);
            $("#select_cerc_anno_dis").children().each(function () {
                if ($(this).attr("value") === '0') {
                    $(this).prop('selected', true);
                }
            })
            $("#anno_distribuzione").blur(function(){
                if(!$(this).val()) {
                    $("#select_cerc_anno_dis").prop("disabled", true);
                    $("#select_cerc_anno_dis").children().each(function () {
                        if ($(this).attr("value") === '0') {
                            $(this).prop('selected', true);
                        }
                    });
                } else {
                    $("#select_cerc_anno_dis").prop("disabled", false);
                    $("#select_cerc_anno_dis").children().each(function () {
                        if ($(this).attr("value") === '0') {
                            $(this).prop('selected', false);
                            $(this).hide();
                        }
                        if ($(this).attr("value") === '3') {
                            $(this).prop('selected', true);
                        }
                    });
                }
            });
            //SCRIPT RESET
            $("#reset").click(function () {
                location.reload();
            });
        });
    </script>
@endsection