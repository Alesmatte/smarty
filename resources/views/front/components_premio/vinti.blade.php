<div class="container pt--10">
    @if(!empty($nominationList))
        @foreach($nominationList as $vinto)
            @if($vinto->getStato() == 1)
                <p>
                    <a href="{{asset('filmmaker/'.$vinto->getFilmMaker()->getIdFilmMaker())}}" class="text-decoration-none">{{ $vinto->getFilmMaker()->getNome() }} {{ $vinto->getFilmMaker()->getCognome() }}</a>
                    ({{ $vinto->getCategoriaPremio()->getTipologia()->getNome() }})
                </p>
            @endif
        @endforeach
    @else
        <div class="row d-flex justify-content-center">
            <h5 class="pt-3">"Mi dispiace, non ci sono premi per questa edizione"</h5>
        </div>
    @endif
</div>