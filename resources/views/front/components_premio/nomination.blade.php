<div class="container">
    @if(!empty($nominationList))
        @foreach($matriceNomination as $key1 => $nom)
            <div class="pb-0 border-bottom pt--20">
                <h4>{{$nom[0]}}</h4>
                <div class="pl-2 pt-2">
                    @foreach($nom[1] as $key2 => $nomination)
                        <p class="mb--12">
                            <a href="{{ asset('filmmaker/' . $nomination->getFilmMaker()->getIdFilmMaker()) }}" class="text-decoration-none">{{ $nomination->getFilmMaker()->getNome() }} {{ $nomination->getFilmMaker()->getCognome() }}</a>
                        </p>
                    @endforeach
                    @if(empty($matriceNomination[$key1][1]))
                        <p class="mb--12">N.D.</p>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <div class="row d-flex justify-content-center">
            <h5 class="pt-4">"Mi dispiace, non ci sono nomination per questa edizione"</h5>
        </div>
    @endif
</div>