@extends('skeletons.front.app')

@section('content')
    <!-- MIDDLE -->
    <!-- PAGE TITLE -->
    <section class="bg-light py-5">
        <div class="container">
            @if($tipo == 'normale')
                <h1 class="h3 text-gray-700 text-uppercase font-weight-light">Recensioni degli utenti</h1>
            @else
                <h1 class="h3 text-gray-700 text-uppercase font-weight-light">Recensioni della critica</h1>
            @endif
        </div>
    </section>
    <!-- /PAGE TITLE -->


    <!-- COVER -->
    <section class="pt--20 pb--60">

        <div class="container">
            <div class="border border-primary-soft bg-white rounded pl--0 pr--0 pb-0">
                <div class="d-flex justify-content-between border-bottom border-primary-soft mb--25">
                    <div class="mt-4 ml--35 mb-0">
                        <div class="row mt-n3 mb--8">
                            <div class="clearfix pl-3 d-flex">
                                <img width="80" src="{{ asset($immagineCopertina->getDato()) }}" alt="...">
                            </div>
                            <h2 class="text-center font-raleway font-weight-light text-gray-800 pl--35 pt--35">
                                Recensioni per <a @if($entita == 'film') href="{{ asset('film/' . $id) }}" @else href="{{ asset('serietv/' . $id) }}" @endif class="text-decoration-none text-primary font-weight-bold">{{ $nomeEntita }}</a> @isset($cerca) <i class="shadow-sm p-2"><i>Ricerca : "{{$cerca}}"</i> <a href="{{ asset('lista_recensioni/'. $entita .'/'. $id) }}"><i class="fi fi-close text-primary pl-2"></i></a></i> @endisset
                            </h2>
                        </div>
                    </div>
                    <div class="form-label-group mb-3 mt--25 mr-3">
                        <form action="{{ asset('lista_recensioni/cerca') }}">
                            <div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

                                <input placeholder="Cerca Recensioni" name="cerca_recensioni" type="text"
                                       class="form-control-sow-search form-control form-control form-control-lg "
                                       value="" autocomplete="off">

                                <span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit"
                                            class="btn btn-primary btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 b--0 bg-transparent text-muted">
										<i class="text-primary fi fi-search fs--20"></i>
									</button>

                                    <!-- close : mobile only (d-inline-block d-lg-none) -->
									<a class="btn-sow-search-toggler btn btn-light btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

                            </div>
                        </form>
                        <small class="d-block text-muted">* min. 3 characters. Titolo, nome film/serie Tv</small>
                    </div>
                </div>

                <div class="pl--15 pt-3 pr--15 pb-4">
                    @if(!empty($recensioniList))
                        @foreach($recensioniList as $key => $recensione)
                        <!-- review {{$key + 1}} -->
                        <div class="row mb-4 pl-0 pr-0 ml-0 mr-0 pb-1 border-bottom">
                            <div class="col-md-1 pl-0 pr-0  text-center min-w-120">

                                <!-- avatar -->
                            @if($recensione->getUtente()->getAvatar()->getDato()!= null)
                                <!-- avatar -->
                                    <span class="w--70 h--70 rounded-circle d-inline-block bg-cover"
                                          style="background-image:url('{{asset( $recensione->getUtente()->getAvatar()->getDato())}}')"></span>
                                @else
                                    <span class="w--70 h--70 rounded-circle d-inline-block bg-cover">
                                    <i class="fi fi-user-male fs--40"></i>
                                </span>
                                @endif
                                <div class="mt-2">
                                    <span class="text-primary">{{$recensione->getUtente()->getNome()}} {{$recensione->getUtente()->getCognome()}}</span>
                                    <p class="fs--14 font-weight-bold mb-1">
                                        @if($recensione->getUtente()->getTipologiaUtenza() == 1)
                                            {{$recensione->getUtente()->getNomeAzienda()}}
                                        @endif
                                    </p>
                                    <p class="fs--13 text-muted ">
                                        {{ date_format(date_create($recensione->getDataRecensione()), "d/m/Y H:i") }}

                                    </p>
                                </div>

                            </div>

                            <div class="col">
                                <div class="mb-1">
                                    <h5>
                                        <a href="#"
                                           data-href="{{asset('recensione/' . $recensione->getIdRecensione())}}"
                                           data-ajax-modal-size="modal-lg"
                                           data-ajax-modal-centered="false"
                                           data-ajax-modal-callback-function=""
                                           data-ajax-modal-backdrop=""
                                           class="js-ajax-modal text-decoration-none">
                                            {{ $recensione->getTitolo() }}
                                        </a>
                                    </h5>
                                    <i class="rating-{{voto_stelle($recensione->getVoto())}} text-warning"></i>
                                </div>
                                <p>
                                    {{strlen($recensione->getContenutoRecensione()) > 500  ? substr($recensione->getContenutoRecensione(), 0, strpos($recensione->getContenutoRecensione(), ' ', 500)) . ' ...' : $recensione->getContenutoRecensione() }}
                                </p>

                            </div>

                        </div>
                        <!-- /review {{$key + 1}} -->
                    @endforeach
                    @else
                        <div class="row d-flex justify-content-center">
                            <h5>Non ci sono ancora recensioni</h5>
                        </div>
                    @endif
                </div>

                <!-- pagination : center -->
                @if(!empty($recensioniList))
                    @if(isset($cerca))
                        <nav aria-label="pagination">
                            <ul class="pagination mt-5 pagination-pill justify-content-center">
                                @if($currentPage == 1)
                                    <li class="page-item disabled">
                                        <a class="page-link" tabindex="-1" aria-disabled="true">Precedente</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a class="page-link" href="{{ asset('lista_recensioni/'. $entita . '/' . $id . '/' . $tipo . '/cerca/' . ($currentPage-1)) . '?nome=' . $cerca }}">Precedente</a>
                                    </li>
                                @endif

                                @foreach($pagine as $pagina)
                                    <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                        @if($pagina === '...')
                                            <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                        @else
                                            <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                        @endif
                                    </li>
                                @endforeach

                                @if($currentPage == $numeroPagine)
                                    <li class="page-item disabled">
                                        <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a class="page-link" href="{{ asset('lista_recensioni/'. $entita . '/' . $id . '/' . $tipo . '/cerca/' . ($currentPage+1)) . '?nome=' . $cerca }}">Successiva</a>
                                    </li>
                                @endif

                            </ul>
                        </nav>
                    @else
                        <nav aria-label="pagination">
                            <ul class="pagination mt-5 pagination-pill justify-content-center">
                                @if($currentPage == 1)
                                    <li class="page-item disabled">
                                        <a class="page-link" tabindex="-1" aria-disabled="true" >Precedente</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a class="page-link" href="{{ asset('lista_recensioni/'. $entita . '/' . $id . '/' . $tipo . '/' . ($currentPage-1)) }}">Precedente</a>
                                    </li>
                                @endif

                                @foreach($pagine as $pagina)
                                    <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                        @if($pagina === '...')
                                            <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                        @else
                                            <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                        @endif
                                    </li>
                                @endforeach

                                @if($currentPage == $numeroPagine)
                                    <li class="page-item disabled">
                                        <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a class="page-link" href="{{ asset('lista_recensioni/'. $entita . '/' . $id . '/' . $tipo . '/' . ($currentPage+1)) }}">Successiva</a>
                                    </li>
                                @endif

                            </ul>
                        </nav>
                @endif
            @endif
            <!-- /pagination : center -->

            </div>
        </div>

    </section>
    <!-- /COVER -->
    <!-- /MIDDLE -->
    @if(session()->flash()->has('succesMessage'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif

    @if(session()->flash()->has('errorMessage'))
        <div class="hide toast-on-load"
             data-toast-type="danger"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif
    <!-- /MIDDLE -->

@endsection

@section('script')
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection