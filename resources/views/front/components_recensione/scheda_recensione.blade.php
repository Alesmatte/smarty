<div class="modal-header">
    <h5 class="modal-title m-0 text-primary">
        {{ $recensione->getTitolo() }}
    </h5>
    <button type="button" class="close pointer" data-dismiss="modal" aria-label="Close">
        <span class="fi fi-close fs--18" aria-hidden="true"></span>
    </button>
</div>
<!-- body -->
<div class="modal-body">
    <div class="row d-flex justify-content-center ">
        <div class="col-md-1 pl-0 pr-0 min-w-270 text-center pb-2">

            <!-- avatar -->
            <span class="w--70 h--70 rounded-circle d-inline-block bg-cover" style="background-image:url('{{asset( $recensione->getUtente()->getAvatar()->getDato())}}')"></span>
            <div class="mt-2">
                <span class="text-primary">{{$recensione->getUtente()->getNome()}} {{$recensione->getUtente()->getCognome()}}</span>
                <p class="fs--14 font-weight-bold mb-1">
                    @if($recensione->getUtente()->getTipologiaUtenza() == 1)
                        {{$recensione->getUtente()->getNomeAzienda()}}
                    @endif
                </p>
                <p class="fs--13 text-muted mb-1">
                    {{ date_format(date_create($recensione->getDataRecensione()), "d/m/Y H:i") }}
                </p>
            </div>
            <i class="fs--25 rating-{{voto_stelle($recensione->getIdRecensione())}} text-warning"></i>
        </div>
    </div>
    <div class="pl-3 pt-3 pr-3 pb-3 border">
        <div id="ajax_response_container"><!-- ajax response container --></div>
            {{ $recensione->getContenutoRecensione() }}
    </div>

</div>
