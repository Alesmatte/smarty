@extends('skeletons.front.app')

@section('content')

    <section class="pt--30">

    <div class="container">

        <div class="row pt--8">
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines" style=""></div>
            </div>
            <div class="col-lg-8 text-center">
                <p class="font-weight-lighter fs--60 text-capitalize mb--0">
                    {{ $award->getNome() }} {{ $awardEd }}
                </p>
            </div>
            <div class="col-lg-2 pt--45">
                <div class="border-title-lines" style=""></div>
            </div>
        </div>
        <p class="text-center fs--20">
            I <strong>23 Film, Serie TV e Personaggi</strong> premiati e le nomination durante l&apos;edizione<strong> del {{ $awardEd }}</strong>.
        </p>

    </div>

    <div class="container pb--20" data-swiper-parallax="-10">

        <div class="row">
            <!--SLIDER-->
            <div class="swiper-container swiper-preloader swiper-btn-group swiper-btn-group-end text-white rounded-top-video-slidser"
                 data-swiper='{
                        "slidesPerView": 1,
                        "spaceBetween": 0,
                        "autoplay": { "delay" : 3500, "disableOnInteraction": false },
                        "loop": true,
                        "pagination": { "type": "bullets" }}'>

                <div class="swiper-wrapper h--400" style="width: 1200px">
                    @foreach($immaginiSliderList as $imgSlider)
                        <!-- slide 1 -->
                            <div class="h-100 swiper-slide d-middle bg-white overlay-dark overlay-opacity-2 d-middle bg-cover">
                                <img src="{{ asset($imgSlider->getDato()) }}" alt="...">
                            </div>
                            <!-- /slide 1 -->
                    @endforeach
                </div>

                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>

                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>

            </div>
            <!-- /SLIDER SWIPER -->

        </div>

    </div>

    <!-- start :: blog content -->

    <div class="container">

        <div class="row">

            <div class="col-sm-9 col-md-9 col-lg-9 order-2 order-lg-1 pl--0">
                <!-- TEMPORARY SECTION -->
                <section class="pt--20">
                    <div class="container p--0">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item col pl--0 pr--3">
                                <a class="nav-link active" id="pills-awards-tab" data-toggle="pill" href="#pills-awards" role="tab"
                                   aria-controls="pills-awards" aria-selected="true">Vincitori {{ $award->getNome() }} {{ $awardEd }}</a>
                            </li>

                            <li class="nav-item col pl--0 pr--3">
                                <a class="nav-link " id="pills-nomination-tab" data-toggle="pill" href="#pills-nomination" role="tab"
                                   aria-controls="pills-nomination" aria-selected="false">Nomination {{ $award->getNome() }} {{ $awardEd }}</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="pills-tabContent">

                            <div class="js-ajax tab-pane fade show active" id="pills-awards" role="tabpanel" aria-labelledby="pills-awards-tab" data-ajax-url="{{asset('premio/' . $idPremio . '/'. $edizione .'/vinti')}}" >
                                <div class="text-center p--30">
                                    <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                                </div>
                            </div>

                            <div class="tab-pane fade show js-ajax" id="pills-nomination" role="tabpanel" aria-labelledby="pills-nomination-tab"  data-ajax-url="{{asset('premio/' . $idPremio . '/'. $edizione .'/nomination')}}">
                                <div class="text-center p--30">
                                    <i class="fi fi-circle-spin fi-spin fs--30 text-muted"></i>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <!-- /TEMPORARY SECTION -->



            </div>

            <div class="col-lg-3 order-1 order-lg-2 mb-5">

                <button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none"
                        data-target="#sidebar_premi_expand_mobile"
                        data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">
								<span class="group-icon px-2 py-2 float-start">
									<i class="fi fi-bars-2"></i>
									<i class="fi fi-close"></i>
								</span>

                    <span class="h5 py-2 m-0 float-start">
									Premi 2020
								</span>
                </button>


                <div id="sidebar_premi_expand_mobile" class="d-none d-lg-block">
                    <nav class="nav-deep nav-deep-light mb-2">
                        <!-- RECOMMENDED -->
                        <h3 class="h5 pt-3 pb-3 m-0 d-none d-lg-block">
                            Altri premi
                        </h3>
                        <ul id="nav_responsive" class="nav flex-column d-none d-lg-block">
                            @foreach($listaPremi as $premio)
                                <li class="nav-item">
                                    <a class="nav-link px-0" href="#">
                                        <span class="group-icon">
                                            <i class="fi fi-arrow-end"></i>
                                            <i class="fi fi-arrow-down"></i>
                                        </span>

                                        <span class="px-2 d-inline-block">
                                            {{$premio->getNome()}}
                                        </span>
                                    </a>

                                    <ul class="nav flex-column px-3">
                                        @for($i = date("Y"); $i > date("Y")-2; $i--)
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ asset('premio/' . $premio->getIdPremio() . '/' . $i) }}">
                                                    {{ $i }}
                                                </a>
                                            </li>
                                        @endfor
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                        <!-- /RECOMMENDED -->
                    </nav>
                </div>

                <div id="sidebar_expand_mobile" class="d-none d-lg-block">

                    <!-- RECOMMENDED -->
                    <h3 class="h5 mt-5 mt-0-xs">
                        Ultimi Film e Serie TV
                    </h3>
                    <ul class="list-unstyled">
                        @foreach($ultimiFilmAndSerieTv as $ultimo)
                            <li class="list-item border-bottom py-1">
                                @if($ultimo instanceof \App\Model\Film)
                                    <a class="text-dark font-weight-light fs--15"
                                       href="{{ asset('film/' . $ultimo->getIdFilm()) }}">{{ $ultimo->getTitolo() }}</a>
                                @else
                                    <a class="text-dark font-weight-light fs--15"
                                       href="{{ asset('serietv/' . $ultimo->getIdSerieTv()) }}">{{ $ultimo->getTitolo() }}</a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    <!-- /RECOMMENDED -->

                    @if($entitaPiuVotata !== null)
                        <!-- BANNER -->
                        <h3 class="h5 mt-5 mt-0-xs">
                            Il più votato
                        </h3>
                        <!-- BANNER -->
                        <a @if($entitaPiuVotata instanceof \App\Model\Film) href="{{ asset('film/' . $entitaPiuVotata->getIdFilm()) }}" @else href="{{ asset('serietv/' . $entitaPiuVotata->getIdSerieTv()) }}" @endif
                        class="mt-3 d-block text-center overlay-dark-hover overlay-opacity-2 rounded overflow-hidden">
                            <img class="w-100 img-fluid rounded" src="{{asset($entitaPiuVotata->getImgCopertina()->getDato())}}"
                                 alt="...">
                        </a>
                        <!-- /BANNER -->
                    @endif
                </div>

            </div>

        </div>

    </div>
</section>

@endsection