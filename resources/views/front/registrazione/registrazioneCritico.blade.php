@extends('front.registrazione.appRegistrazione')

@section('wrapper')
    <div id="wrapper">

        <!-- go back button -->
        <a href="{{asset('')}}"
           class="btn btn-primary rounded-circle position-absolute top-0 start-0 mx-4 my-4 z-index-2"
           aria-label="go back" title="go back" data-toggle="tooltip">
            <i class="fi fi-arrow-start"></i>
        </a>


        <div class="d-lg-flex min-h-100vh">

            <div class="col-12 col-lg-5 d-lg-flex bg-gradient-primary text-white d-none d-lg-block">
                <div class="w-100 align-self-center text-center">


                    <div class="d-inline-block max-w-600">

                        <!-- icon -->
                        <div class="mb-5" data-aos="fade-in" data-aos-delay="50" data-aos-offset="0">
								<span class="badge badge-primary fs--45 w--100 h--100 badge-pill rounded-circle">
									<i class="fi fi-users mt-1"></i>
								</span>
                        </div>


                        <!-- light logo : desktop only -->
                        <a href="{{asset('')}}" class="text-decoration-none" data-aos="fade-in" data-aos-delay="150"
                           data-aos-offset="0">
                            <img class="" src="assets/images/logo/redcarpet.png" height="85" width="242" alt="...">
                        </a>


                        <!-- quote / slogan -->
                        <blockquote class="blockquote text-center mt-5" data-aos="fade-in" data-aos-delay="250"
                                    data-aos-offset="0">
                            <h2 class="h4 m-auto font-weight-light font-italic text-white">
                                Istriviti a Smarty!<br>
                                La comiunity di cinefili più grande d&apos;Italia!
                            </h2>

                        </blockquote>


                    </div>

                </div>
            </div>


            <div class="col-12 col-lg-7 d-lg-flex">
                <div class="w-100 align-self-center text-center py-7">


                    <div class="max-w-600 w-100 d-inline-block text-align-start p-4">

                        <!-- title -->
                        <h1 class="text-primary text-center mb-5 font-weight-normal">
                            Iscrizione <span class="font-weight-medium">Critico </span>
                        </h1>

                    @if(session()->flash()->has('errors'))
                        @php
                            $errors = session()->flash()->get('errors');
                        @endphp
                    @endif

                    <!-- optional class: .form-control-pill -->
                        <form novalidate action="{{asset('registrazione_critico')}}" method="POST"
                              class="bs-validate p-5 rounded shadow-xs">


                            @csrf

                            <div class="bs-validate-info hide alert alert-danger" data-error-alert-delay="4000">
                                <i class="fi fi-circle-spin fi-spin float-start"></i>
                                Perfavore completa i campi!
                            </div>

                            @if(isset($errors['nome']))
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Nome" id="account_first_name" name="nome" type="text" class="form-control invalidato">
                                    <label for="account_first_name">Nome</label>
                                </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['nome'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Nome" id="account_first_name" name="nome" type="text" class="form-control" value="{{old('nome')}}">
                                    <label for="account_first_name">Nome</label>
                                </div>
                            @endif


                            @if(isset($errors['cognome']))
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Cognome" id="account_last_name" name="cognome" type="text" class="form-control invalidato">
                                    <label for="account_last_name">Cognome</label>
                                </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['cognome'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Cognome" id="account_last_name" name="cognome" type="text" class="form-control" value="{{old('cognome')}}">
                                    <label for="account_last_name">Cognome</label>
                                </div>
                            @endif

                            <div class="form-label-group mb-3">
                                <input required id="account_birthday" name="birthday" type="date" class="form-control invalidato" max="{{date('o-m-d', mktime(23, 59, 59, date("m")  , date("d"), date("Y") -18))}}" value="{{date('o-m-d', mktime(23, 59, 59, date("m")  , date("d"), date("Y") -18))}}">
                                <label for="account_birthday">Data di Nascita</label>
                            </div>

                            @if(isset($errors['codice_identificativo_albo']))
                            <div class="form-label-group mb-3">
                                <input required placeholder="Codice Identificativo nell'Albo"
                                       id="codice_identificativo_albo" name="codice_identificativo_albo" type="text"
                                       class="form-control invalidato">
                                <label for="codice_identificativo_albo">Codice Identificativo nell'Albo</label>
                            </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['codice_identificativo_albo'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Codice Identificativo nell'Albo"
                                           id="codice_identificativo_albo" name="codice_identificativo_albo" type="text"
                                           class="form-control" value="{{old('codice_identificativo_albo')}}">
                                    <label for="codice_identificativo_albo">Codice Identificativo nell'Albo</label>
                                </div>
                            @endif

                            @if(isset($errors['azienda']))
                            <div class="form-label-group mb-3">
                                <input required placeholder="Rivista / Azienda" id="azienda" name="azienda" type="text"
                                       class="form-control invalidato">
                                <label for="azienda">Rivista / Azienda</label>
                            </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['azienda'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Rivista / Azienda" id="azienda" name="azienda" type="text"
                                           class="form-control" value="{{old('azienda')}}">
                                    <label for="azienda">Rivista / Azienda</label>
                                </div>
                            @endif

                            @if(isset($errors['email']))
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Email" id="account_email" name="email" type="email" class="form-control invalidato">
                                    <label for="account_email">Email</label>
                                </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['email'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="form-label-group mb-3">
                                    <input required placeholder="Email" id="account_email" name="email" type="email" class="form-control" value="{{old('email')}}">
                                    <label for="account_email">Email</label>
                                </div>
                            @endif

                            @if(isset($errors['account_password']))
                                <div class="input-group-over">
                                    <div class="form-label-group mb-3">
                                        <input required placeholder="Password" id="account_password" name="account_password" type="password" class="form-control invalidato">
                                        <label for="account_password">Password</label>
                                    </div>

                                    <!-- `SOW : Form Advanced` plugin used -->
                                    <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                    </a>

                                </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['account_password'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="input-group-over">
                                    <div class="form-label-group mb-3">
                                        <input required placeholder="Password" id="account_password" name="account_password" type="password" class="form-control">
                                        <label for="account_password">Password</label>
                                    </div>

                                    <!-- `SOW : Form Advanced` plugin used -->
                                    <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                    </a>

                                </div>

                            @endif

                            @if(isset($errors['account_password_same']))
                                <div class="input-group-over">
                                    <div class="form-label-group mb-4">
                                        <input required placeholder="Password" id="account_password_same" name="account_password_same" type="password" class="form-control invalidato">
                                        <label for="account_password_same">Resinserisci La Password</label>
                                    </div>

                                    <!-- `SOW : Form Advanced` plugin used -->
                                    <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password_same">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                    </a>

                                </div>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['account_password_same'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @else
                                <div class="input-group-over">
                                    <div class="form-label-group mb-4">
                                        <input required placeholder="Password" id="account_password_same" name="account_password_same" type="password" class="form-control">
                                        <label for="account_password_same">Resinserisci La Password</label>
                                    </div>

                                    <!-- `SOW : Form Advanced` plugin used -->
                                    <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password_same">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                    </a>

                                </div>
                            @endif

                            <div class="row">

                                <div class="col-12 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        ISCRIVITI !
                                    </button>
                                </div>

                                <div class="col-12 col-md-12 mb-4 text-align-end text-center-xs">
                                    <!-- empty -->
                                </div>

                            </div>

                        </form>

                    </div>

                </div>
            </div>

        </div>

    </div><!-- /#wrapper -->

@endsection