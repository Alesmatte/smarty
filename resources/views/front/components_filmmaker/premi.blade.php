@if(count($array_by_genere) > 0)
    <section class="pt--20 pb-0">
        <div class="container">
            @foreach($array_by_genere as $genere)
                @foreach($genere as $anno)
                    <div class="text-primary fs--20 pt--5">
                        <a href="{{asset("premio/".$anno[0]->getCategoriaPremio()->getPremio()->getIdPremio()."/".date("Y",strtotime($anno[0]->getDataPremiazione())))}}"
                           class="text-decoration-none"><strong>{{$anno[0]->getCategoriaPremio()->getPremio()->getNome()}} {{date("Y",strtotime($anno[0]->getDataPremiazione()))}}</strong></a>
                    </div>
                    @foreach($anno as $nomination)

                        <p class="mt--5 mb--0">
                            @if($nomination->getStato())Vittoria @else Nominatio @endif
                            per {{$nomination->getCategoriaPremio()->getTipologia()->getNome()}} {{$nomination->getDescrizione()}}
                        </p>
                    @endforeach
                @endforeach
            @endforeach
        </div>

    </section>
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center col">Questo Filmmaker non è stato mai in lizza per un premio</h1>
    </div>
@endif