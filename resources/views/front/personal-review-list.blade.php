@extends('skeletons.front.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!-- PAGE TITLE -->
        <section class="bg-light pt--50 pb--40">
            <div class="container pt--5 pb--8">

                <h1 class="h3 text-gray-700 text-uppercase font-weight-light">
                    Storico Recensioni Pubblicate
                </h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb fs--14">
                        <li class="breadcrumb-item "><a href="{{ asset('/') }}">Home</a></li>
                        <li class="breadcrumb-item  active" aria-current="page">Storico Recensioni Pubblicate</li>
                    </ol>
                </nav>

            </div>
        </section>
        <!-- /PAGE TITLE -->

        <!-- COVER -->
        <section class="pt--20">

            <div class="container">
                <div class="border border-primary-soft bg-white rounded pl--0 pr--0 pb-0">
                    <div class="d-flex justify-content-between border-bottom border-primary-soft mb--25">
                        <div class="mt-4 ml-3 mb-0">
                            <h2 class="text-center font-raleway font-weight-light text-gray-800 p--15">
                                Ultime Recensioni @isset($cerca) <span class="fs--20"><i class="shadow-sm p-2"><i>Ricerca : "{{$cerca}}"</i> <a href="{{ asset('lista_recensioni_personali') }}"><i class="fi fi-close text-primary pl-2"></i></a></i></span> @endisset
                            </h2>
                        </div>
                        <div class="form-label-group mb-3 mt-3 mr-3">
                            <form action="{{ asset('lista_recensioni_personal/cerca') }}" method="GET">
                                <div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

                                    <input placeholder="Cerca Recensioni"
                                           id="ricerca"
                                           name="ricerca"
                                           type="text"
                                           class="form-control-sow-search form-control form-control form-control-lg "
                                           value="">
                                    <span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit"
                                            class="btn btn-primary btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 b--0 bg-transparent text-muted">
										<i class="text-primary fi fi-search fs--20"></i>
									</button>

                                        <!-- close : mobile only (d-inline-block d-lg-none) -->
									<a class="btn-sow-search-toggler btn btn-light btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

                                </div>
                            </form>
                            <small class="d-block text-muted">* min. 3 characters. Titolo, nome film/serie Tv</small>
                        </div>
                    </div>

                    <div class="pl--15 pt-4 pr--15 pb-4">
                        @if(!empty($recensioniList))
                            @foreach($recensioniList as $key => $recensione)
                                <!-- review {{ $key+1 }} -->
                                <div class="row mb-4 pl-0 pr-0 ml-0 mr-0 pb-1 border-bottom">

                                    <div class="col-md-1 text-center pl-0 pr-0" style="min-width: 120px">

                                        <a @if($entitaList[$key] instanceof \App\Model\Film) href="{{ asset("film/" . $entitaList[$key]->getIdFilm()) }}" @else href="{{ asset("serietv/" . $entitaList[$key]->getIdSerieTv()) }}" @endif class="m-0 w--90 rounded-circle d-inline-flex justify-content-center align-items-center text-decoration-none">
                                            <img class=" img-fluid rounded" src="{{ asset($entitaList[$key]->getImgCopertina()->getDato()) }}"
                                                 alt="...">
                                        </a>

                                        <div class="mt-0">
                                            <a @if($entitaList[$key] instanceof \App\Model\Film) href="{{ asset("film/" . $entitaList[$key]->getIdFilm()) }}" @else href="{{ asset("serietv/" . $entitaList[$key]->getIdSerieTv()) }}" @endif class="text-decoration-none text-primary">{{ $entitaList[$key]->getTitolo() }}</a>
                                            <p class="fs--12 text-muted">
                                                {{ date_format(date_create($recensione->getDataRecensione()), "d/m/Y H:i") }}
                                            </p>
                                        </div>

                                    </div>

                                    <div class="col">
                                        <div class="mb-2 d-flex justify-content-between">

                                            <div class="">
                                                <h5>{{ $recensione->getTitolo() }}</h5>
                                                <i class="rating-{{voto_stelle($recensione->getVoto())}} text-warning"></i>
                                            </div>

                                            <div class="">
                                                <a href="#" class="js-ajax-modal mr-2 text-decoration-none"
                                                   data-href="{{asset('components/recenzione_modal/' . $recensione->getIdRecensione())}}"
                                                   data-ajax-modal-size="modal-xl"
                                                   data-ajax-modal-centered="false"
                                                   data-ajax-modal-callback-function=""
                                                   data-ajax-modal-backdrop="">
                                                    <i class=" fs--25 fi fi-pencil text-primary"></i>
                                                </a>
                                                <a data-href="{{ asset('api/review/' . $recensione->getIdRecensione()) }}"
                                                   class="js-ajax-confirm ml--15" href="#"

                                                   data-ajax-confirm-mode="ajax"
                                                   data-ajax-confirm-method="DELETE"

                                                   data-ajax-confirm-size="modal-md"
                                                   data-ajax-confirm-centered="false"
                                                   data-ajax-confirm-callback-function="myFunction"

                                                   data-ajax-confirm-title="Per favore conferma"
                                                   data-ajax-confirm-body="Sei sicuro di voler Eliminare la recensione per The Avengers?"

                                                   data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                   data-ajax-confirm-btn-yes-text="Conferma"
                                                   data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                   data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                   data-ajax-confirm-btn-no-text="Annulla"
                                                   data-ajax-confirm-btn-no-icon="fi fi-close">
                                                    <i class="fs--25 fi fi-thrash text-primary"></i>
                                                </a>

                                            </div>
                                            <!-- /dropdown options -->

                                        </div>
                                        <p>
                                            {{strlen($recensione->getContenutoRecensione()) > 500  ? substr($recensione->getContenutoRecensione(), 0, strpos($recensione->getContenutoRecensione(), ' ', 500)) . ' ...' : $recensione->getContenutoRecensione() }}
                                        </p>

                                    </div>

                                </div>
                                <!-- /review {{ $key+1 }} -->
                            @endforeach
                        @else
                            <div class="row d-flex justify-content-center">
                                <h5>Non hai ancora recensito nulla. Scrivi subito la Tua Prima recensione!</h5>
                            </div>
                        @endif
                    </div>

                    <!-- pagination : center -->
                    @if(!empty($recensioniList))
                        @if(isset($cerca))
                            <nav aria-label="pagination">
                                <ul class="pagination mt-5 pagination-pill justify-content-center">
                                    @if($currentPage == 1)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('lista_recensioni_personal/cerca/' . ($currentPage-1)) . '?nome=' . $cerca }}">Precedente</a>
                                        </li>
                                    @endif

                                    @foreach($pagine as $pagina)
                                        <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                            @if($pagina === '...')
                                                <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                            @else
                                                <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                            @endif
                                        </li>
                                    @endforeach

                                    @if($currentPage == $numeroPagine)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('lista_recensioni_personal/cerca/' . ($currentPage+1)) . '?nome=' . $cerca }}">Successiva</a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        @else
                            <nav aria-label="pagination">
                                <ul class="pagination mt-5 pagination-pill justify-content-center">
                                    @if($currentPage == 1)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('lista_recensioni_personali/' . ($currentPage-1)) }}">Precedente</a>
                                        </li>
                                    @endif

                                    @foreach($pagine as $pagina)
                                        <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                            @if($pagina === '...')
                                                <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                            @else
                                                <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                            @endif
                                        </li>
                                    @endforeach

                                    @if($currentPage == $numeroPagine)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('lista_recensioni_personali/' . ($currentPage+1)) }}">Successiva</a>
                                        </li>
                                    @endif

                                </ul>
                            </nav>
                        @endif
                    @endif
                    <!-- /pagination : center -->

                </div>
            </div>

        </section>
        <!-- /COVER -->

    </div>
    @if(session()->flash()->has('succesMessage'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true">
        </div>
    @endif

    @if(session()->flash()->has('errorMessage'))
        <div class="hide toast-on-load"
             data-toast-type="danger"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif
    <!-- /MIDDLE -->

@endsection

@section('script')
    @parent
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection