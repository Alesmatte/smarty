<div class="row">
    <div class="col-12 col-lg-4">

        <div class="">


            <div class="clearfix">
                <img class="img-fluid pb--20 pt--5" src="{{asset($film->getImgCopertina()->getDato())}}"
                     alt="{{$film->getImgCopertina()->getDescrizione()}}">

            </div>

        </div>

    </div>

    <div class="col-12 col-lg-8">
        {!!$film->getTrama()!!}
    </div>
</div>