@if(count($film->getRecensioni()) < 5)
    {{-- Sezione quando non ci sono rencezioni sufficienti --}}
    <div class="p--15 rounded bg-white border border-primary mb--15">
        <h1 class="font-weight-light"> Risultati</h1>
        <div class="progress mb-3 h--40">
            <div class="progress-bar bg-primary fs--14" role="progressbar" style="width: 100%" aria-valuenow="25"
                 aria-valuemin="0"
                 aria-valuemax="100">
                RECENSIONI DELLA CRITICA 0.0 (non ci sono voti)
            </div>
        </div>


        <div class="progress mb-3 h--40">
            <div class="progress-bar bg-secondary-palette fs--14" role="progressbar" style="width: 100%"
                 aria-valuenow="25"
                 aria-valuemin="0" aria-valuemax="100">
                RECENSIONI DEGLI UTENTI 0.0 (non ci sono voti)
            </div>
        </div>

        @if(!auth())
            <a href="#"
               data-href="{{asset('components/accedi_modal')}}"
               data-ajax-modal-size="modal-xl"
               data-ajax-modal-centered="false"
               data-ajax-modal-callback-function=""
               data-ajax-modal-backdrop=""
               class="js-ajax-modal btn btn-primary text-uppercase mb-1">
                Accedi Per Scrivere Una Recenzione
            </a>
        @endif
        @if(can("Creazione Recensione"))
            <a href="#"
               @if($idRecenzioneSua!=0)
               data-href="{{asset('components/recenzione_modal/'.$idRecenzioneSua)}}"
               @else
               data-href="{{asset('components/recenzione_modal')}}"
               @endif
               data-ajax-modal-size="modal-xl"
               data-ajax-modal-centered="false"
               data-ajax-modal-callback-function=""
               data-ajax-modal-backdrop=""
               class="js-ajax-modal btn btn-primary text-uppercase mb-1">
                @if($harecensito) Modifica recenzione @else Scrivi Recenzione @endif
            </a>
            @if($harecensito)
                @if($idRecenzioneSua!=0)
                    <a data-href="{{ asset('api/review/' . $idRecenzioneSua) }}"
                       class="js-ajax-confirm btn btn-primary text-uppercase mb-1" href="#"

                       data-ajax-confirm-mode="ajax"
                       data-ajax-confirm-method="DELETE"

                       data-ajax-confirm-size="modal-md"
                       data-ajax-confirm-centered="false"
                       data-ajax-confirm-callback-function="myFunction"

                       data-ajax-confirm-title="Per favore conferma"
                       data-ajax-confirm-body="Sei sicuro di voler Eliminare la recensione per The Avengers?"

                       data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                       data-ajax-confirm-btn-yes-text="Conferma"
                       data-ajax-confirm-btn-yes-icon="fi fi-check"

                       data-ajax-confirm-btn-no-class="btn-sm btn-light"
                       data-ajax-confirm-btn-no-text="Annulla"
                       data-ajax-confirm-btn-no-icon="fi fi-close">
                        Elimina Recenzione<i class="fi fi-thrash ml--25 "></i>
                    </a>
                @endif
            @endif
        @endif
    </div>

    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="rounded bg-primary">
                <h3 class="text-center text-secondary-palette pt--10  pb--10">RECENSIONI DELLA CRITICA</h3>
            </div>

            <div class="row">
                <div class=" col mt--10 ">


                    <h2 class="font-weight-light text-center">Non ci sono recenzioni per questa serie tv da parte della
                        Critica!</h2>

                </div>

            </div>

        </div>

        <div class="col-12 col-lg-6">
            <div class="rounded bg-primary">
                <h3 class="text-center text-secondary-palette pt--10 pb--10">RECENSIONI DEGLI UTENTI</h3>
            </div>

            <div class="row">
                <div class=" col mt--10 ">
                    <h2 class="font-weight-light text-center">Non ci sono recenzioni per questa serie tv da parte degli
                        utenti! <br>scrivine una tu!</h2>
                </div>
            </div>
        </div>
    </div>
@else

    <div class="p--15 rounded bg-white border border-primary mb--15">
        <h1 class="font-weight-light"> Risultati</h1>
        <div class="progress mb-3 h--40">
            <div class="progress-bar bg-primary fs--14" role="progressbar"
                 style="width: {{rapporto_a_cento($valutazione_critica)}}%"
                 aria-valuenow="{{rapporto_a_cento($valutazione_critica)}}"
                 aria-valuemin="0"
                 aria-valuemax="100">
                RECENSIONI DELLA CRITICA {{number_format($valutazione_critica, 2)}}
            </div>
        </div>


        <div class="progress mb-3 h--40">
            <div class="progress-bar bg-secondary-palette fs--14" role="progressbar"
                 style="width: {{rapporto_a_cento($valutazione_user)}}%"
                 aria-valuenow="{{rapporto_a_cento($valutazione_user)}}"
                 aria-valuemin="0" aria-valuemax="100">
                RECENSIONI DEGLI UTENTI {{number_format($valutazione_user, 2)}}
            </div>
        </div>
        <div class="d-flex justify-content-between">
            @if(!auth())
                <a href="#"
                   data-href="{{asset('components/accedi_modal')}}"
                   data-ajax-modal-size="modal-xl"
                   data-ajax-modal-centered="false"
                   data-ajax-modal-callback-function=""
                   data-ajax-modal-backdrop=""
                   class="js-ajax-modal btn btn-primary text-uppercase mb-1">
                    Accedi Per Scrivere Una Recenzione
                </a>
            @endif

            @if(can("Creazione Recensione"))
                <a href="#"
                   @if($idRecenzioneSua!=0)
                   data-href="{{asset('components/recenzione_modal/'.$idRecenzioneSua)}}"
                   @else
                   data-href="{{asset('components/recenzione_modal')}}"
                   @endif
                   data-ajax-modal-size="modal-xl"
                   data-ajax-modal-centered="false"
                   data-ajax-modal-callback-function=""
                   data-ajax-modal-backdrop=""
                   class="js-ajax-modal btn btn-primary text-uppercase mb-1">
                    @if($harecensito) Modifica recenzione @else Scrivi Recenzione @endif
                </a>
                @if($harecensito)
                    @if($idRecenzioneSua!=0)
                        <a data-href="{{ asset('api/review/' . $idRecenzioneSua) }}"
                           class="js-ajax-confirm btn btn-primary text-uppercase mb-1" href="#"

                           data-ajax-confirm-mode="ajax"
                           data-ajax-confirm-method="DELETE"

                           data-ajax-confirm-size="modal-md"
                           data-ajax-confirm-centered="false"
                           data-ajax-confirm-callback-function="myFunction"

                           data-ajax-confirm-title="Per favore conferma"
                           data-ajax-confirm-body="Sei sicuro di voler Eliminare la recensione?"

                           data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                           data-ajax-confirm-btn-yes-text="Conferma"
                           data-ajax-confirm-btn-yes-icon="fi fi-check"

                           data-ajax-confirm-btn-no-class="btn-sm btn-light"
                           data-ajax-confirm-btn-no-text="Annulla"
                           data-ajax-confirm-btn-no-icon="fi fi-close">
                            Elimina Recenzione<i class="fi fi-thrash ml--25"></i>
                        </a>
                    @endif
                @endif
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="rounded bg-primary">
                <h3 class="text-center text-secondary-palette pt--10  pb--10">ULTIME RECENSIONI DELLA CRITICA</h3>
            </div>

            @foreach($recenzioniCritico as $recenzione)
                <div class="row">
                    <div class=" col mt--10 media mb-4">

                        <!-- avatar : as background -->
                        <div class="w--100 text-center">
                            @if($recenzione->getUtente()->getAvatar()->getDato() != null)
                                <div class="w--70 h--70 mb-1 rounded-circle d-inline-block bg-light bg-cover"
                                     style="background-image:url('{{asset($recenzione->getUtente()->getAvatar()->getDato())}}')"></div>
                            @else
                                <div class="d-inline-block">
                                    <a href="#"
                                       class="w--70 h--70 fs--25 btn bg-light bg-cover rounded-circle float-start">
                                        <i class=" fs--25 fi fi-user-male"></i>
                                    </a></div>
                            @endif
                            <span class="d-block text-dark">{{$recenzione->getUtente()->getNome()}} {{$recenzione->getUtente()->getCognome()}}</span>
                            <small class="d-block text-dark fs--12">{{tempo_fa(date($recenzione->getDataRecensione()))}}</small>
                        </div>

                        <div class="media-body">
                            <h5>{{$recenzione->getTitolo()}}</h5>
                            <i class="rating-{{voto_stelle(floatval($recenzione->getVoto()))}} text-warning"></i>
                            <p>{{$recenzione->getContenutoRecensione()}}</p>
                        </div>
                    </div>
                    <!-- /Comment -->
                </div>
            @endforeach
            <div class="d-flex justify-content-center">
                @if($film instanceof \App\Model\Film)
                    <a class="btn btn-primary" href="{{asset('lista_recensioni/film/'.$film->getIdFilm().'/critico')}}">Visualizza Tutte le recenzioni</a>
                @else()
                    <a class="btn btn-primary" href="{{asset('lista_recensioni/serietv/'.$film->getIdSerieTv().'/critico')}}">Visualizza Tutte le recenzioni</a>
                @endisset
            </div>

        </div>

        <div class="col-12 col-lg-6">
            <div class="rounded bg-primary">
                <h3 class="text-center text-secondary-palette pt--10 pb--10">ULTIME RECENSIONI DEGLI UTENTI</h3>
            </div>
            @foreach($recenzioniUtente as $recenzione)
                <div class="row">
                    <div class=" col mt--10 media mb-4">

                        <!-- avatar : as background -->
                        <div class="w--100 text-center">
                            @if($recenzione->getUtente()->getAvatar()->getDato() != null)
                                <div class="w--70 h--70 mb-1 rounded-circle d-inline-block bg-light bg-cover"
                                     style="background-image:url('{{asset($recenzione->getUtente()->getAvatar()->getDato())}}')"></div>
                            @else
                                <div class="d-inline-block">
                                    <a href="#"
                                       class="w--70 h--70 fs--25 btn bg-light bg-cover rounded-circle float-start">
                                        <i class=" fs--25 fi fi-user-male"></i>
                                    </a></div>
                            @endif
                            <span class="d-block text-dark">{{$recenzione->getUtente()->getNome()}} {{$recenzione->getUtente()->getCognome()}}</span>
                            <small
                                    class="d-block text-dark fs--12">{{tempo_fa(date($recenzione->getDataRecensione()))}}</small>
                        </div>

                        <div class="media-body">
                            <h5>{{$recenzione->getTitolo()}}</h5>
                            <i class="rating-{{voto_stelle(floatval($recenzione->getVoto()))}} text-warning"></i>
                            <p>{{$recenzione->getContenutoRecensione()}}</p>
                        </div>
                    </div>
                    <!-- /Comment -->
                </div>
            @endforeach
            <div class="d-flex justify-content-center">
                @if($film instanceof \App\Model\Film)
                    <a class="btn btn-primary" href="{{asset('lista_recensioni/film/'.$film->getIdFilm().'/normale')}}">Visualizza Tutte le recenzioni</a>
                @else()
                    <a class="btn btn-primary" href="{{asset('lista_recensioni/serietv/'.$film->getIdSerieTv().'/normale')}}">Visualizza Tutte le recenzioni</a>
                @endisset
            </div>
        </div>
    </div>
@endif
<script>
    function myFunction() {
        window.location.reload();
    }
</script>