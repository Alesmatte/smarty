@if(count($array_nomination[array_key_first($array_nomination)]) > 0)

    {{--<div class="row">
        @foreach($array_nomination as $kn => $an)
        <div class="col-12 mt--10 mb--5">
            <div class="rounded bg-primary">
                <div class="row">
                    <div class="col-10">
                        <h4 class="text-secondary-palette pt--10 pl--15">{{$an[key($an)]->getCategoriaPremio()->getPremio()->getNome()}} {{date("Y", strtotime($an[key($an)]->getDataPremiazione()))}}</h4>
                    </div>
                    <div class="col-2">
                        <a href="{{asset('premio/').$kn.'/'.date("Y", strtotime($an[key($an)]->getDataPremiazione()))}}" class="a-text-secondary-palette"><h6 class="text-right pr--15 pt--5">tutti i premi <br> del cinema</h6></a>
                    </div>
                </div>
            </div>
            @foreach($an as $nomination)
            <p class="pl--5 pt--5 mb--5 mt--5">
                @if($nomination->getStato())[Vittoria]@else[Nomination]@endif {{$nomination->getCategoriaPremio()->getTipologia()->getNome()}} a {{ $nomination->getFilmMaker()->getNome()}} {{$nomination->getFilmMaker()->getCognome()}}
            </p>
            @endforeach
        </div>
        @endforeach

    </div>--}}
    <div class="row">
        @foreach($array_nomination as $genere)
            @foreach($genere as $anno)
                <div class="col-12 mt--10 mb--5">
                    <div class="rounded bg-primary">
                        <div class="row">
                            <div class="col-10">
                                <h4 class="text-secondary-palette pt--10 pl--15">{{$anno[0]->getCategoriaPremio()->getPremio()->getNome()}} {{date("Y",strtotime($anno[0]->getDataPremiazione()))}}</h4>
                            </div>
                            <div class="col-2">
                                <a href="{{asset("premio/".$anno[0]->getCategoriaPremio()->getPremio()->getIdPremio()."/".date("Y",strtotime($anno[0]->getDataPremiazione())))}}"
                                   class="a-text-secondary-palette"><h6 class="text-right pr--15 pt--5">tutti i premi
                                        <br> del cinema</h6></a>
                            </div>
                        </div>
                    </div>
                    @foreach($anno as $nomination)
                        <p class="pl--5 pt--5 mb--5 mt--5">
                            @if($nomination->getStato())[Vittoria]@else
                                [Nomination]@endif {{$nomination->getCategoriaPremio()->getTipologia()->getNome()}}
                            a {{ $nomination->getFilmMaker()->getNome()}} {{$nomination->getFilmMaker()->getCognome()}}
                        </p>
                    @endforeach
                </div>
            @endforeach
        @endforeach
    </div>
@else
    <div class="h--300 d-flex align-items-center">
        <h1 class="font-weight-100 text-justify text-center">Questo Titolo non è stato candidato e non
            ha vinto nessun premio</h1>
    </div>
@endif