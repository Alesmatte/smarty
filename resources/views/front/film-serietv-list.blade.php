@extends('skeletons.front.app')

@section('content')
    <!-- MIDDLE -->
    <section class="pt--30">

        <div class="container">

            <div class="row pt--6">
                <div class="col-lg-2 pt--45">
                    <div class="border-title-lines"></div>
                </div>
                <div class="col-lg-8 text-center">
                    <p class="font-weight-lighter fs--40 mt--12">
                        {{$infoPagina}}
                    </p>
                </div>
                <div class="col-lg-2 pt--45">
                    <div class="border-title-lines"></div>
                </div>
            </div>
            <p class="text-center fs--20">
                Elenco aggiornato di <strong>tutte le opere della storia del cinema e della tv</strong>
            </p>
        </div>

        <div class="container pb--20">

            <div class="row">
                <!--SLIDER-->
                <!--
                    .swiper-btn-group.swiper-btn-group-start
                    .swiper-btn-group.swiper-btn-group-end
                    -->
                <div class="swiper-container swiper-preloader swiper-btn-group swiper-btn-group-end text-white rounded-top-video-slidser"
                     data-swiper='{
                "slidesPerView": 1,
                            "spaceBetween": 0,
                            "autoplay": { "delay" : 3500, "disableOnInteraction": false },
                            "loop": true,
                            "pagination": { "type": "bullets" }
                        }'>
                    <div class="swiper-wrapper h--400">
                    @foreach($immaginiSliderList as $imgSlider)
                        <!-- slide 1 -->
                            <div class="h-100 swiper-slide d-middle bg-white overlay-dark overlay-opacity-2 d-middle bg-cover">
                                <img src="{{ asset($imgSlider->getDato()) }}" alt="...">
                            </div>
                            <!-- /slide 1 -->
                        @endforeach
                    </div>

                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>

                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>

                </div>
                <!-- /SLIDER SWIPER -->

            </div>

        </div>

        <!-- start :: blog content -->

        <div class="container">

            <div class="row">

                <div class="col-sm-9 col-md-9 col-lg-9 order-2 order-lg-1 pl--0">

                    <!-- posts -->
                    <div class="row pr--20 @if(empty($operaListAppo)) d-flex justify-content-center @endif ">
                    @if(!empty($operaListAppo))
                        @foreach($operaListAppo as $key=>$opera)
                            <!-- post 1 -->
                                <div class="col-12 mt-4 mb-4 position-relative text-dark clearfix text-decoration-none">
                                    <div class="bg-light">
                                        <!--<div class="position-absolute top-0 start-0 mt&#45;&#45;400 ml&#45;&#45;35 z-index-1 border-2 border-solid border-white">
                                            <a class="text-decoration-none fancybox fancybox-secondary" data-fancybox="gallery" href="assets/images/the_avengers/locandina.jpg">
                                                <img width="110" height="160" src="assets/images/the_avengers/locandina.jpg" alt="...">
                                            </a>
                                        </div>-->
                                        <!-- Local Video -->
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <video preload="auto" controls="controls"
                                                   @if($opera instanceof \App\Model\Film)
                                                   poster="{{ asset($coverVideoList[$opera->getIdFilm()]->getDato()) }}"
                                                   @else
                                                   poster="{{ asset($coverVideoList[$opera->getIdSerieTv()]->getDato()) }}"
                                                   @endif
                                                   class="rounded-top-video-slidser">
                                                <source src="{{ asset($opera->getVideos()[0]->getDato()) }}"
                                                        type="video/mp4;">
                                            </video>
                                        </div>
                                        <div class="text-center pt-2 text-dark">
                                            <span class="d-block">
                                                    <i class="rating-{{ voto_stelle($opera->mediaRedCarpet) }} text-warning fs--25"></i>
                                            </span>
                                        </div>
                                        <!-- /Local Video -->

                                        <!-- Post content -->
                                        <div class="pl--15 pr--15">
                                            <!-- Post avarage rating -->
                                            <div class="text-center">
                                                <strong>SMARTOMETRO </strong>
                                                - GIUDIZIO MEDIO
                                            </div>
                                            <div class="text-center fs--15">
                                                @if($opera->mediaRedCarpet == 0 || !$opera->maggiorediCinqueVoti)
                                                    <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-grigio">
                                                    CONSIGLIATO N.D.!!!
                                                </span>
                                                @elseif($opera->mediaRedCarpet < 2)
                                                    <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-grigio">
                                                    <strong>{{ number_format($opera->mediaRedCarpet, 2, ',', ' ') }} </strong>
                                                    - CONSIGLIATO NO!!!
                                                </span>
                                                @elseif($opera->mediaRedCarpet >= 2 && $opera->mediaRedCarpet < 3.5)
                                                    <span class="text-dark pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-giallo">
                                                    <strong>{{ number_format($opera->mediaRedCarpet, 2, ',', ' ') }} </strong>
                                                    - CONSIGLIATO NI!!!
                                                </span>
                                                @elseif($opera->mediaRedCarpet >= 3.5 && $opera->mediaRedCarpet <= 5)
                                                    <span class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-color-verde">
                                                    <strong>{{ number_format($opera->mediaRedCarpet, 2, ',', ' ') }} </strong>
                                                    - CONSIGLIATO SI!!!
                                                </span>
                                                @endif
                                            </div>
                                            <!-- /Post avarage rating -->

                                            <!-- Post title -->
                                            <p class="mt-3 fs--25 mb-2 pl--10">
                                                <a @if($opera instanceof \App\Model\Film) href="{{asset('film/' . $opera->getIdFilm())}}"
                                                   @else href="{{asset('serietv/' . $opera->getIdSerieTv())}}"
                                                   @endif class="h6-xs d-block shadow-3d-hover text-decoration-none">
                                                    <strong>
                                                        {{ $opera->getTitolo() }}
                                                    </strong>
                                                </a>
                                            </p>
                                            <!-- /Post title -->

                                            <!-- /Post subtitle -->
                                            <div class="pl--15">
                                                <p class="mb-0">
                                                    {{ $opera->getDescrizione() }}
                                                    @foreach($opera->getGeneri() as $genere)
                                                        <a class=" text-decoration-none"
                                                           @if($opera instanceof \App\Model\Film)
                                                           href="{{asset('lista_film?genere=' . $genere->getIdGenere())}}">{{ $genere->getNome() }}
                                                            @else
                                                                href="{{asset('lista_serietv?genere=' . $genere->getIdGenere())}}
                                                                ">{{ $genere->getNome()}}
                                                            @endif
                                                        </a>,
                                                    @endforeach
                                                    @foreach($opera->getNazionalita() as $keyNazionalita => $nazionalita)
                                                        <a class=" text-decoration-none"
                                                           href="{{asset('lista_film/' . $keyNazionalita)}}">{{ $nazionalita }}</a>
                                                        ,
                                                    @endforeach
                                                    <a class=" text-decoration-none"
                                                       @if($opera instanceof \App\Model\Film)
                                                       href="{{asset('lista_film/' . date("Y", strtotime($opera->getDataSviluppoFilm())))}}">{{ date("Y", strtotime($opera->getDataSviluppoFilm())) }}</a>,
                                                    @else
                                                        href="{{asset('lista_serietv/' . date("Y", strtotime($opera->getDataSviluppoSerieTv())))}}
                                                        ">{{ date("Y", strtotime($opera->getDataSviluppoSerieTv())) }}</a>
                                                        ,
                                                    @endif
                                                    @if($opera instanceof \App\Model\Film)
                                                        <strong>Durata {{ $opera->getDurata() }} Minuti. </strong>
                                                    @endif
                                                    Consigliato per la visione:
                                                    <span class="bg-primary text-white pt--2 pb--2 pr--6 pl--6 rounded-xl">Ragazzi&nbsp;+{{$opera->getAgeRating()}}</span>
                                                </p>

                                            </div>
                                            <!-- /Post subtitle -->

                                            <!-- Post rating -->
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="text-center pt-1">
                                                    <span class="d-block">
                                                        <i class="rating-{{voto_stelle($opera->mediaRedCarpet)}} text-warning fs--18"></i>
                                                    </span>
                                                        @if($opera->mediaRedCarpet == 0 || !$opera->maggiorediCinqueVoti)
                                                            <p class="fs--13">PUBBLICO <strong>N.D.</strong></p>
                                                        @else
                                                            <p class="fs--13">REDCARPET.IT
                                                                <strong>{{ number_format($opera->mediaRedCarpet, 2, ',', ' ') }}</strong>
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="text-center pt-1">
                                                    <span class="d-block">
                                                        <i class="rating-{{voto_stelle($opera->mediaCritica)}} text-warning fs--18"></i>
                                                    </span>
                                                        @if($opera->mediaCritica == 0 || !$opera->maggiorediCinqueVoti)
                                                            <p class="fs--13">PUBBLICO <strong>N.D.</strong></p>
                                                        @else
                                                            <p class="fs--13">CRITICA
                                                                <strong>{{ number_format($opera->mediaCritica, 2, ',', ' ') }}</strong>
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="text-center pt-1">
                                                    <span class="d-block">
                                                        <i class="rating-{{voto_stelle($opera->mediaPubblico)}} text-warning fs--18"></i>
                                                    </span>
                                                        @if($opera->mediaPubblico == 0 || !$opera->maggiorediCinqueVoti)
                                                            <p class="fs--13">PUBBLICO <strong>N.D.</strong></p>
                                                        @else
                                                            <p class="fs--13">PUBBLICO
                                                                <strong>{{ number_format($opera->mediaPubblico, 2, ',', ' ') }}</strong>
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Post rating -->

                                            <!-- Post description -->
                                            <div class="pl--15 pr--15">
                                                @if($opera instanceof \App\Model\Film)
                                                    <p class="fs--16 mb-0 pb-2">
                                                        Un film di
                                                        @foreach($matriceRegisti[$opera->getIdFilm()] as $keyRegista => $regista)
                                                            <a class="text-decoration-none text-decoration-none"
                                                               href="{{asset('filmmaker/' . $regista->getIdFilmMaker())}}">{{ $regista->getNome() }} {{ $regista->getCognome() }}</a>@if($keyRegista + 1 == count($matriceRegisti[$opera->getIdFilm()]))
                                                                . @else, @endif
                                                        @endforeach
                                                        Con
                                                        @foreach($matriceAttori[$opera->getIdFilm()] as $keyAttore => $attore)
                                                            <a class="text-decoration-none"
                                                               href="{{asset('filmmaker/' . $attore->getIdFilmMaker())}}"> {{ $attore->getNome() }} {{ $attore->getCognome()}}</a>@if($keyAttore > 3 || $keyAttore + 1 == count($matriceAttori[$opera->getIdFilm()]))
                                                                .@else,@endif
                                                        @endforeach
                                                        <strong>
                                                            Uscita {{ italian_date_str($opera->getDataUscita()) }}</strong>.
                                                        Distribuzione
                                                        @foreach($opera->getDistributori() as $keyDist => $distributore)
                                                            <a class="text-decoration-none"
                                                               href="{{asset('filmmaker?distributore=/' . $distributore->getIdDistributore())}}"> {{ $distributore->getNome() }}</a>@if($keyDist + 1 == count($opera->getDistributori()))
                                                                . @else, @endif
                                                        @endforeach
                                                    </p>
                                                @else
                                                    <p class="fs--16 mb-0 pb-2">
                                                        Una serie tv di
                                                        @foreach($matriceRegisti[$opera->getIdSerieTv()] as $keyRegista => $regista)
                                                            <a class="text-decoration-none text-decoration-none"
                                                               href="{{asset('filmmaker/' . $regista->getIdFilmMaker())}}">{{ $regista->getNome() }} {{ $regista->getCognome() }}</a>@if($keyRegista + 1 == count($matriceRegisti[$opera->getIdSerieTv()]))
                                                                . @else, @endif
                                                        @endforeach
                                                        Con
                                                        @foreach($matriceAttori[$opera->getIdSerieTv()] as $keyAttore => $attore)
                                                            <a class="text-decoration-none"
                                                               href="{{asset('filmmaker/' . $attore->getIdFilmMaker())}}"> {{ $attore->getNome() }} {{ $attore->getCognome()}}</a>@if($keyAttore > 3 || $keyAttore + 1 == count($matriceAttori[$opera->getIdSerieTv()]))
                                                                .@else,@endif
                                                        @endforeach
                                                        <strong>
                                                            Uscita {{ italian_date_str($opera->getDataUscita()) }}</strong>.
                                                        Distribuzione
                                                        @foreach($opera->getDistributori() as $keyDist => $distributore)
                                                            <a class="text-decoration-none"
                                                               href="{{asset('filmmaker?distributore=/' . $distributore->getIdDistributore())}}"> {{ $distributore->getNome() }}</a>@if($keyDist + 1 == count($opera->getDistributori()))
                                                                . @else, @endif
                                                        @endforeach
                                                    </p>
                                                @endif

                                                <p class="fs--16 mb-0 pb-1">
                                                    {{ (strlen($opera->getTramaBreve()) > 200) ? substr($opera->getTramaBreve(),0,200).'...' : $opera->getTramaBreve() }}
                                                    <span role="button" onClick="espandi(event);"><strong
                                                                class="fs--22 text-primary" id="espandi{{$key}}">Espandi trama&nbsp;▷</strong></span>
                                                </p>
                                                <div id="espandi{{$key}}trama" class="d-none">

                                                    <div class="ml--15 mr--15 w-inherit align-items-center bg-primary-soft border-bottom h--1 w-auto mb-2 mt-3"></div>

                                                    <p class=" fs--16 pt-3 mb-2">
                                                        {{$opera->getTramaBreve()}}
                                                        <a @if($opera instanceof \App\Model\Film) href="{{asset('film/' . $opera->getIdFilm())}}"
                                                           @else href="{{asset('serietv/' . $opera->getIdSerieTv())}}"
                                                           @endif class=" text-decoration-none">
                                                            <span type="button"
                                                                  class="text-white pt--2 pb--2 pr--6 pl--6 rounded-xl bg-primary">Scheda</span>
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- /Post description -->
                                        @if(auth())
                                            <!-- Rewiew -->
                                                <div id="ajax_response_container-{{$key}}"></div>
                                                <div @if($opera instanceof \App\Model\Film) id="review{{$opera->getIdFilm()}}"
                                                     @else id="review{{$opera->getIdSerieTv()}}"
                                                     @endif class="collapse pl--10 pr--10 pt-1">

                                                    <div class="ml--15 mr--15 w-inherit align-items-center bg-primary-soft border-bottom h--1 w-auto mb-2 mt--2"></div>

                                                    <h3 class="text-center text-primary pt-3">Rcrivi una recensione</h3>

                                                    <form class="js-ajax bs-validate" novalidate
                                                          @if($opera instanceof \App\Model\Film)
                                                          action="{{ asset('api/recensione/film/' . $opera->getIdFilm()) }}"
                                                          @else
                                                          action="{{ asset('api/recensione/serietv/' . $opera->getIdSerieTv()) }}"
                                                          @endif
                                                          method="POST"
                                                          data-ajax-container="#ajax_response_container-{{$key}}"
                                                          data-ajax-update-url="false"
                                                          data-ajax-show-loading-icon="true"
                                                          data-error-scroll-up="false"
                                                          data-ajax-callback-function="">

                                                        <div class="form-label-group m-3">
                                                            <div class="row m-2 d-flex justify-content-center">
                                                                <i @if($opera instanceof \App\Model\Film) id="star_rating{{$opera->getIdFilm()}}"
                                                                   @else id="star_rating{{$opera->getIdSerieTv()}}"
                                                                   @endif class="rating-1_0 text-warning fs--50"></i>
                                                            </div>
                                                            <div class="row mt-2 mb-3 d-flex justify-content-center">
                                                                <input type="range" class="max-w-280 custom-range"
                                                                       min="1" max="5" step="0.5" value="1.0"
                                                                       @if($opera instanceof \App\Model\Film) id="formControlRange{{$opera->getIdFilm()}}"
                                                                       onchange="updateRating({{$opera->getIdFilm()}})"
                                                                       @else id="formControlRange{{$opera->getIdSerieTv()}}"
                                                                       onchange="updateRating({{$opera->getIdSerieTv()}})"
                                                                       @endif name="valutazione" required>
                                                            </div>
                                                        </div>

                                                        <input type="text" name="titolo" value=""
                                                               placeholder="Il titolo della tua recensione"
                                                               class="form-control mb-4 rounded-xl bg-secondary-soft">
                                                        <textarea type="text" name="contenuto"
                                                                  placeholder="La tua recensione"
                                                                  class="form-control mb-4 rounded-xl bg-secondary-soft h--200 max-h-300"></textarea>
                                                        <div class="text-center">
                                                            <button type="submit" class="btn btn-primary">
                                                                Invia la recensione
                                                            </button>
                                                        </div>
                                                        @csrf
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                        <!-- Post content -->

                                        <!-- /Rewiew -->

                                        <!-- Footer post with button review and rating -->

                                        <div class="mt-3 rounded-bottom d-flex justify-content-end bg-primary h--41">
                                            @if(auth())
                                                @if($opera instanceof \App\Model\Film)
                                                    <a class="btn btn-primary pt--0 pb--0" href=""
                                                       data-toggle="collapse"
                                                       data-target="#review{{$opera->getIdFilm()}}"
                                                       onclick="rating({{$opera->getIdFilm()}})">
                                                        <span class="text-white fs--20 pt--5">Scrivi una Recensione</span>
                                                        <i class="fi fi-pencil pb-1 fs--25 text-white pl--5"></i>
                                                    </a>
                                                @else
                                                    <a class="btn btn-primary pt--0 pb--0" href=""
                                                       data-toggle="collapse"
                                                       data-target="#review{{$opera->getIdSerieTv()}}"
                                                       onclick="rating({{$opera->getIdSerieTv()}})">
                                                        <span class="text-white fs--20 pt--5">Scrivi una Recensione</span>
                                                        <i class="fi fi-pencil pb-1 fs--25 text-white pl--5"></i>
                                                    </a>
                                                @endif
                                            @else
                                                <a href="#"
                                                   data-href="{{asset('components/accedi_modal')}}"
                                                   data-ajax-modal-size="modal-xl"
                                                   data-ajax-modal-centered="false"
                                                   data-ajax-modal-callback-function=""
                                                   data-ajax-modal-backdrop=""
                                                   class="js-ajax-modal btn btn-primary pt--0 pb--0">
                                                    <span class="text-white fs--20 pt--5">Accedi per Recensire</span>
                                                    <i class="fi fi-pencil pb-1 fs--25 text-white pl--5"></i>
                                                </a>
                                            @endif
                                        </div>
                                        <!-- /Footer post with button review and rating -->
                                    </div>
                                </div>
                                <!-- /post 1 -->
                                @if($key+1 < count($operaListAppo))
                                    <div class="col-lg-5 col-md-5 col-sm-5"></div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 align-items-center border-title-lines max-w-250"></div>
                                    <div class="col-lg-5 col-md-5 col-sm-5"></div>
                                @endif
                            @endforeach
                        @else
                            <div class="d-flex justify-content-center pt-4">
                                @if($titlepage == 'Film')
                                    <div class="text-center">
                                        <h5>Mi dispiace, non ci sono Film disponibili. </h5>
                                        <h5 class="pt-2">Torna alla <a class="text-decoration-none text-primary" href="{{ asset('') }}">Home</a>!</h5>
                                    </div>
                                @else
                                    <div class="text-center">
                                        <h5>Mi dispiace, non ci sono Serie Tv disponibili. </h5>
                                        <h5 class="pt-2">Torna alla <a class="text-decoration-none text-primary" href="{{ asset('') }}">Home</a>!</h5>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                    <!-- /posts -->

                    <!-- pagination : center -->
                    @if(!empty($operaListAppo))
                        @if($titlepage == 'Film')
                            @if($tipo != '')
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film_list/' . $tipo . '/5/' . ($currentPage-1)) }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film_list/' . $tipo . '/5/' . ($currentPage+1)) }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                            @elseif($genere != '')
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage-1) . '?genere=' . $genere) }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage+1 . '?genere=' . $genere)) }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                            @elseif($distributore != '')
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage-1) . '?distributore=' . $distributore) }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage+1 . '?distributore=' . $distributore)) }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                            @else
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage-1)) }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="{{ asset('film/5/' . ($currentPage+1)) }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                        @endif()
                    @endif
                @endif
                <!-- /pagination : center -->

                </div>

                <div class="col-lg-3 order-1 order-lg-2 mb-5">


                    <!-- CATEGORIES -->
                    <nav class="nav-deep nav-deep-light mb-2">

                        <!-- desktop only -->
                        <h3 class="h5 pt-3 pb-3 m-0 d-none d-lg-block">
                            Categorie Principali
                        </h3>

                        <!-- navigation -->
                        <ul id="nav_responsive" class="nav flex-column d-none d-lg-block">

                            <li class="nav-item">
                                <a class="nav-link px-0" href="{{ asset('lista_film/oscar') }}">
                                    <span class="px-2 ml-3 d-inline-block">
                                        Premiati agli Oscar
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link px-0" href="{{ asset('lista_film/cinema') }}">
                                    <span class="px-2 ml-3 d-inline-block">
                                        In Sala
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link px-0" href="{{ asset('lista_film/'. date("Y")) }}">
                                    <span class="px-2 ml-3 d-inline-block">
                                        {{ date("Y") }}
                                    </span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link px-0" href="#">
                                    <span class="group-icon">
                                        <i class="fi fi-arrow-end"></i>
                                        <i class="fi fi-arrow-down"></i>
                                    </span>

                                    <span class="px-2 d-inline-block">
                                        Anni precedenti
                                    </span>
                                </a>

                                <ul class="nav flex-column px-3">
                                    @for($i = date("Y")-1; $i > date("Y")-5; $i--)
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ asset('lista_film/'. $i) }}">
                                                {{ $i }}
                                            </a>
                                        </li>
                                    @endfor
                                </ul>

                            </li>

                            <li class="nav-item">
                                <a class="nav-link px-0" href="#">
                                    <span class="group-icon">
                                        <i class="fi fi-arrow-end"></i>
                                        <i class="fi fi-arrow-down"></i>
                                    </span>

                                    <span class="px-2 d-inline-block">
                                        Generi
                                    </span>
                                </a>

                                <ul class="nav flex-column px-3">
                                    @foreach($generiList as $genere)
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="{{ asset('lista_film?genere=' . $genere->getIdGenere()) }}">
                                                {{ $genere->getNome() }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                            </li>

                        </ul>

                    </nav>
                    <!-- /CATEGORIES -->

                    <div id="sidebar_expand_mobile" class="d-none d-lg-block">

                        <!-- RECOMMENDED -->
                        <h3 class="h5 mt-5 mt-0-xs">
                            Ultimi Film e Serie TV
                        </h3>
                        <ul class="list-unstyled">
                            @foreach($ultimiFilmAndSerieTv as $ultimo)
                                <li class="list-item border-bottom py-1">
                                    @if($ultimo instanceof \App\Model\Film)
                                        <a class="text-dark font-weight-light fs--15"
                                           href="{{ asset('film/' . $ultimo->getIdFilm()) }}">{{ $ultimo->getTitolo() }}</a>
                                    @else
                                        <a class="text-dark font-weight-light fs--15"
                                           href="{{ asset('serietv/' . $ultimo->getIdSerieTv()) }}">{{ $ultimo->getTitolo() }}</a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        @if($entitaPiuVotata !== null)
                            <!-- /RECOMMENDED -->
                            <h3 class="h5 mt-5 mt-0-xs">
                                Il più votato
                            </h3>
                            <!-- BANNER -->
                            <a @if($entitaPiuVotata instanceof \App\Model\Film) href="{{ asset('film/' . $entitaPiuVotata->getIdFilm()) }}"
                               @else href="{{ asset('serietv/' . $entitaPiuVotata->getIdSerieTv()) }}" @endif
                               class="mt-3 d-block text-center overlay-dark-hover overlay-opacity-2 rounded overflow-hidden">
                                <img class="w-100 img-fluid rounded"
                                     src="{{ asset($entitaPiuVotata->getImgCopertina()->getDato()) }}"
                                     alt="...">
                            </a>
                            <!-- /BANNER -->
                        @endisset
                    </div>

                </div>

            </div>

        </div>
    </section>
    <!-- /MIDDLE -->
@endsection

@section('script')
    @parent
    <script>
        function espandi(e) {
            let esp = e.target.id;
            let collass = "trama";
            let espcollass = esp.concat(collass);
            let elem = document.getElementById(espcollass);
            if (elem.classList.contains("d-none")) {
                elem.classList.toggle("d-none");
                document.getElementById(esp).innerHTML = "Riduci trama&nbsp;▽";
            } else {
                document.getElementById(espcollass).classList.toggle("d-none");
                document.getElementById(esp).innerHTML = "Espandi trama&nbsp;▷";
            }
        }

        function rating(identifier) {
            const star_rating = document.getElementById('star_rating' + identifier.toString());
            const $value = document.getElementById('formControlRange' + identifier.toString());
            star_rating.classList.remove("rating-0_0", "rating-0_5", "rating-1_0", "rating-1_5", "rating-2_0", "rating-2_5", "rating-3_0", "rating-3_5", "rating-4_0", "rating-4_5", "rating-5_0");
            switch ($value.value) {
                case "0":
                    star_rating.classList.add("rating-0_0");
                    break;
                case "0.5":
                    star_rating.classList.add("rating-0_5");
                    break;
                case "1":
                    star_rating.classList.add("rating-1_0");
                    break;
                case "1.5":
                    star_rating.classList.add("rating-1_5");
                    break;
                case "2":
                    star_rating.classList.add("rating-2_0");
                    break;
                case "2.5":
                    star_rating.classList.add("rating-2_5");
                    break;
                case "3":
                    star_rating.classList.add("rating-3_0");
                    break;
                case "3.5":
                    star_rating.classList.add("rating-3_5");
                    break;
                case "4":
                    star_rating.classList.add("rating-4_0");
                    break;
                case "4.5":
                    star_rating.classList.add("rating-4_5");
                    break;
                case "5":
                    star_rating.classList.add("rating-5_0");
                    break;
            }
        }

        function updateRating(identifier) {
            const star_rating = document.getElementById('star_rating' + identifier.toString());
            const $value = document.getElementById('formControlRange' + identifier.toString());
            star_rating.classList.remove("rating-0_0", "rating-0_5", "rating-1_0", "rating-1_5", "rating-2_0", "rating-2_5", "rating-3_0", "rating-3_5", "rating-4_0", "rating-4_5", "rating-5_0");
            switch ($value.value) {
                case "0":
                    star_rating.classList.add("rating-0_0");
                    break;
                case "0.5":
                    star_rating.classList.add("rating-0_5");
                    break;
                case "1":
                    star_rating.classList.add("rating-1_0");
                    break;
                case "1.5":
                    star_rating.classList.add("rating-1_5");
                    break;
                case "2":
                    star_rating.classList.add("rating-2_0");
                    break;
                case "2.5":
                    star_rating.classList.add("rating-2_5");
                    break;
                case "3":
                    star_rating.classList.add("rating-3_0");
                    break;
                case "3.5":
                    star_rating.classList.add("rating-3_5");
                    break;
                case "4":
                    star_rating.classList.add("rating-4_0");
                    break;
                case "4.5":
                    star_rating.classList.add("rating-4_5");
                    break;
                case "5":
                    star_rating.classList.add("rating-5_0");
                    break;
            }
        }


    </script>
@endsection