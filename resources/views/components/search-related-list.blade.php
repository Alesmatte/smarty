<h3 class="fs--18 p--20 m-0 text-primary">
    In primo piano
</h3>
<ul class="list-unstyled list-suggestion">
    @if(!empty($resultOpera))
        @foreach($resultOpera as $key => $opera)
            <li class="list-item">
                <a href="{{ $opera->link }}">
                    <div class="row">
                        <div class="col-1">
                            <img src="{{ $opera->datoImg }}" height="85" alt="...">
                        </div>
                        @if($opera->tipo == 'FILM' || $opera->tipo == 'SERIE TV')
                            <div class="col-11 pt-3">
                                <span class="text-muted fs--16 float-end p--3">{{ $opera->testoTipo }}</span>
                                <h3 class="fs--20 text-dark">{{ $opera->titolo }}&nbsp;&nbsp;&nbsp;&nbsp;- {{ strtoupper($opera->tipo) }} -</h3>
                                {{ $opera->annoUscita }} -
                                @foreach($opera->registi as $key => $regista) {{$regista}}@if($key + 1 !== count($opera->registi)),@endif
                                @endforeach
                            </div>
                        @else
                            <div class="col-11 pt-3">
                                {{--                                <span class="text-muted fs--16 float-end p--3">ricercato 5 volte</span>--}}
                                <h3 class="fs--20 text-dark">{{ $opera->titolo }}</h3>
                                {{ $opera->tipo }}
                            </div>
                        @endif
                    </div>
                </a>
            </li>
        @endforeach
    @else
        <h4 class="fs--16 p--20 m-0">
            Nessun risultato trovato. Cerca ancora!!!
        </h4>
    @endif
</ul>

<div class="text-muted fs--12 p--15 border-top">
    <a href="{{ asset('ricerca_avanzata') }}"><h5 class="py-2 m-0 ">Ricerca Avanzata</h5></a>
</div>