@if(session()->flash()->has('errors'))
    @php
        $errors = session()->flash()->get('errors');
    @endphp
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="fi fi-close" aria-hidden="true"></span>
        </button>
        @foreach($errors as $frerror)
            @foreach($frerror as $ernome)
                {{$ernome}}<br>
            @endforeach
        @endforeach
    </div>
@endif

@isset($err)
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="fi fi-close" aria-hidden="true"></span>
        </button>

        {{$err}}<br>

    </div>
@endisset
<script type="application/javascript">
    $('#accedi_form input[type=hidden]').remove();
    $("#accedi_form").append($('@csrf'));
</script>

@isset($redirect)
    @if($redirect)
        <script language="javascript">
            window.location = '{{$url_referer}}';
        </script>
    @endif
@endisset