@if(session()->flash()->has('errors'))
    @php
        $errors = session()->flash()->get('errors');
    @endphp
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="fi fi-close" aria-hidden="true"></span>
        </button>
        @foreach($errors as $frerror)
            @foreach($frerror as $ernome)
                {{$ernome}}<br>
            @endforeach
        @endforeach
    </div>
    <script>
        @foreach($errors as $k =>$frerror)
        $('input[name={{$k}}]').addClass('invalidato');
        $('textarea[name={{$k}}]').addClass('invalidato');
        @endforeach
        $("input[type=range]").val(0);
    </script>
@endif

@isset($error_mex)
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="fi fi-close" aria-hidden="true"></span>
        </button>
        {{$error_mex}}
    </div>
@endisset

@isset($success_mex)
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="fi fi-close" aria-hidden="true"></span>
        </button>

        <h4 class="alert-heading">{{$success_mex['title']}}</h4>
        <p>{{$success_mex['mex']}}</p>
        <hr>
        <p class="mb-0">{{$success_mex['mex_1']}}</p>

    </div>
    <script>
        $('.invalidato').removeClass('invalidato');
    </script>
    @isset($success_mex['type'])
        @if($success_mex['type']==="distributore")
            <script type="application/javascript">
                $('#distrib_form input[type=hidden]').remove();
                $("#distrib_form").append($('@csrf'));
                $("#distributore").append(new Option("{{$success_mex['text']}}", "{{$success_mex['value']}}"));
                $("#distributore").selectpicker("refresh");
            </script>
        @elseif($success_mex['type']==="genere")
            <script type="application/javascript">
                $('#generi_form input[type=hidden]').remove();
                $("#generi_form").append($('@csrf'));
                $("#genere").append(new Option("{{$success_mex['text']}}", "{{$success_mex['value']}}"));
                $("#genere").selectpicker("refresh");
            </script>
            <script>
                if (window.location.pathname.includes('/admin/generi')) {
                    $('.table-datatable tr:last').after('<tr role="row" class="odd"> <td class="dtr-control" tabindex="0">{{$success_mex['text']}}</td> <td>{{$success_mex['descrione']}}</td> <td class="d-flex justify-content-center"> <a data-href="{{asset('api/admin/delete/genere/'. $success_mex['value'])}}?action=delete"class="js-ajax-confirm-{{$success_mex['value']}} btn btn-sm rounded-circle-xs btn-primary btn-pill text-white"href="#" data-ajax-confirm-mode="ajax" data-ajax-confirm-method="DELETE" data-ajax-confirm-size="modal-md" data-ajax-confirm-centered="false" data-ajax-confirm-callback-function="myFunction" data-ajax-confirm-title="Per favore conferma" data-ajax-confirm-body="Sei sicuro di voler Eliminare il Genere <strong>{{$success_mex['text']}}</strong>?" data-ajax-confirm-btn-yes-class="btn-sm btn-danger" data-ajax-confirm-btn-yes-text="Conferma" data-ajax-confirm-btn-yes-icon="fi fi-check" data-ajax-confirm-btn-no-class="btn-sm btn-light" data-ajax-confirm-btn-no-text="Annulla" data-ajax-confirm-btn-no-icon="fi fi-close"> <i class="fi fi-thrash text-white pr--10"></i>Elimina</a></td></tr>');
                    $.SOW.core.ajax_confirm.init('.js-ajax-confirm-{{$success_mex['value']}}');
                }
            </script>
        @elseif($success_mex['type']==="filmmaker")
            <script type="application/javascript">
                $("#filmmakers_form").remove();
                $("#filmmakers_form_sub").remove();
                $("#filmmakers_form_an").text("Chiudi").addClass("w-100");
            </script>
        @endif
    @endisset
@endisset

@isset($redirect)
    @if($redirect)
        <script language="javascript">
            window.location = '{{$url_referer}}';
        </script>
    @endif
@endisset

@isset($error_recensione)
    @if($error_recensione)
        @isset($recensito)
            @if($recensito)
                <div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span class="fi fi-close" aria-hidden="true"></span>
                    </button>
                    <div class="text-center">
                        <h2 class="h6">Hai gia recensito {{$mexOpera1}}</h2>
                        <p class="m-0">
                            @if(str_contains($mexOpera2, 'Film'))
                                Vai nella <a class="text-decoration-none" href="{{asset('film/' . $idOpera)}}"> scheda {{$mexOpera2}}</a> per modificare la tua recensione!
                            @else
                                Vai nella <a class="text-decoration-none" href="{{asset('serietv/' . $idOpera)}}"> scheda {{$mexOpera2}}</a> per modificare la tua recensione!
                            @endif
                        </p>
                    </div>
                </div>
            @endif
        @endisset
    @else
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span class="fi fi-close" aria-hidden="true"></span>
            </button>
            <div class="text-center">
                <h2 class="h6">Grazie mille!!!</h2>
                <p class="m-0">
                    La tua recensione è stata inviata.
                </p>
            </div>
        </div>
    @endif
    <script type="application/javascript">
        @if($error_recensione)
            const star_rating = document.getElementById('star_rating' + {{$idOpera}});
            const $value = document.getElementById('formControlRange' + {{$idOpera}});
            star_rating.classList.remove("rating-0_0", "rating-0_5", "rating-1_0", "rating-1_5", "rating-2_0", "rating-2_5", "rating-3_0", "rating-3_5", "rating-4_0", "rating-4_5", "rating-5_0");
            switch ($value.value) {
                case "0":
                    star_rating.classList.add("rating-0_0");
                    break;
                case "0.5":
                    star_rating.classList.add("rating-0_5");
                    break;
                case "1":
                    star_rating.classList.add("rating-1_0");
                    break;
                case "1.5":
                    star_rating.classList.add("rating-1_5");
                    break;
                case "2":
                    star_rating.classList.add("rating-2_0");
                    break;
                case "2.5":
                    star_rating.classList.add("rating-2_5");
                    break;
                case "3":
                    star_rating.classList.add("rating-3_0");
                    break;
                case "3.5":
                    star_rating.classList.add("rating-3_5");
                    break;
                case "4":
                    star_rating.classList.add("rating-4_0");
                    break;
                case "4.5":
                    star_rating.classList.add("rating-4_5");
                    break;
                case "5":
                    star_rating.classList.add("rating-5_0");
                    break;
                default:
                    star_rating.classList.add("rating-0_0");
                    break;
            }
        @else
            $("#review{{$idOpera}}").collapse('toggle');
        @endif
    </script>
@endisset

