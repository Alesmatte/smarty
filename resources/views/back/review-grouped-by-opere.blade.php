@extends('skeletons.back.app')
@section('content')
<!-- MIDDLE -->
<div id="middle" class="flex-fill">

    <!-- PAGE TITLE -->

    <div class="page-title bg-transparent b-0">

        <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
            Recensioni della stampa
        </h1>

    </div>

    <div class="row d-flex flex-fill align-items-start">

        <div class="col align-self-start">

            <section id="rounded mb-3">

                <div class="bg-light rounded pt-2 pl-3 pr-3 pb-1 mb-4 fs--20 border-bottom d-flex justify-content-between">
                    <div class="mt-3 ml-2 mr-2 mb-2 fs--25">
                        Ordinate per Film e Serie Tv @isset($cerca) <i class=" ml-3 shadow-sm p-2"><i>Ricerca : "{{$cerca}}"</i> <a href="{{ asset('admin/review_grouped_by_opere') }}"><i class="fi fi-close text-primary pl-2"></i></a></i> @endisset
                    </div>
                    @if(!empty($operaList))
                        <div class="form-label-group">
                        <form action="{{ asset('admin/review_grouped_by_opere/cerca') }}" method="GET">
                            <div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

                                <input placeholder="Cerca Recensioni"
                                       id="ricerca"
                                       name="ricerca"
                                       type="text"
                                       class="form-control-sow-search form-control form-control form-control-lg "
                                       value="">
                                <span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit"
                                            class="btn btn-primary btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 b--0 bg-transparent text-muted">
										<i class="text-primary fi fi-search fs--20"></i>
									</button>

                                    <!-- close : mobile only (d-inline-block d-lg-none) -->
									<a class="btn-sow-search-toggler btn btn-light btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

                            </div>
                        </form>
                        <small class="d-block text-muted">* cerca per Titolo, Nome o Cognome utente</small>
                    </div>
                    @endif
                </div>

                <div class="tab-pane fade show active" id="tab-users" role="tabpanel" aria-labelledby="tab-users-tab">
                    @if(!empty($operaList))
                        @foreach($operaList as $key=>$opera)
                            <!-- item -->
                            @if($opera instanceof \App\Model\Film)
                                <div onclick="location.href='{{ asset('admin/review_list/film/' . $opera->getIdFilm()) }}'" class="shadow-sm-hover position-relative bg-secondary-soft-hover border-bottom pt-2 pb-2 cursor-pointer">
                            @else
                                <div onclick="location.href='{{ asset('admin/review_list/serietv/' . $opera->getIdSerieTv()) }}'" class="shadow-sm-hover position-relative bg-secondary-soft-hover border-bottom pt-2 pb-2 cursor-pointer">
                            @endif

                                <div class="row">
                                    <div class="col">

                                        <div class="d-flex">

                                            <div class="w--50 d-flex align-items-center">
                                                <!-- film image -->
                                                <a href="#" class="m-0 rounded-circle d-inline-flex justify-content-center align-items-center text-decoration-none">
                                                    <img class=" img-fluid rounded" src="{{ asset($opera->getImgCopertina()->getDato()) }}"
                                                         alt="...">
                                                </a>
                                            </div>

                                            <div class="col pt--10">
                                                <a class="h5 mb-0 text-primary text-decoration-none">
                                                    {{$opera->getTitolo()}}
                                                </a>
                                                <ul class="list-inline mt-2 text-muted fs--14">
                                                    <li class="list-inline-item">
                                                        <i class="fi fi-user-male float-start"></i> Ultima recensione di <strong>{{ $listaQuantitaRecensioni[$key][2]->getUtente()->getNome() }} {{ $listaQuantitaRecensioni[$key][2]->getUtente()->getCognome() }}</strong> per <strong>{{ $listaQuantitaRecensioni[$key][2]->getUtente()->getNomeAzienda() }}</strong>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fi fi-time float-start"></i> {{ date_format(date_create($listaQuantitaRecensioni[$key][2]->getDataRecensione()), "d/m/Y") }}
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-12 col-md-3">
                                        <div class="row no-gutters col-border text-center d-flex justify-content-end">
                                            <div class="col-6 pt-2">
                                                {{ $listaQuantitaRecensioni[$key][0] }}
                                                <span class="d-lg-block pt-2">
                                                    Recensioni
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /item -->
                        @endforeach
                    @else
                        <span class="pl-4">Non ci sono recensioni.</span>
                    @endif
                    <!-- pagination : center -->
                        @if(!empty($operaList))
                            @if(isset($cerca))
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link" href="{{ asset('admin/review_grouped_by_opere/cerca/10/' . ($currentPage-1)) . '?nome=' . $cerca }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('admin/review_grouped_by_opere/cerca/10/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link" href="{{ asset('admin/review_grouped_by_opere/cerca/10/' . ($currentPage+1)) . '?nome=' . $cerca }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                            @else
                                <nav aria-label="pagination">
                                    <ul class="pagination mt-5 pagination-pill justify-content-center">
                                        @if($currentPage == 1)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link" href="{{ asset('admin/review_grouped_by_opere/10/' . ($currentPage-1)) }}">Precedente</a>
                                            </li>
                                        @endif

                                        @foreach($pagine as $pagina)
                                            <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                                @if($pagina === '...')
                                                    <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                                @else
                                                    <a class="page-link" @if($currentPage != $pagina) href="{{ asset('admin/review_grouped_by_opere/10/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                                @endif
                                            </li>
                                        @endforeach

                                        @if($currentPage == $numeroPagine)
                                            <li class="page-item disabled">
                                                <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link" href="{{ asset('admin/review_grouped_by_opere/10/' . ($currentPage+1)) }}">Successiva</a>
                                            </li>
                                        @endif

                                    </ul>
                                </nav>
                        @endif
                    @endif
                    <!-- /pagination : center -->

                </div>

            </section>

        </div>


    </div>


</div>
<!-- /MIDDLE -->

@endsection