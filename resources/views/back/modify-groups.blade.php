@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Inserisci un nuovo Gruppo
            </h1>

        </div>
        @if(session()->flash()->has('errors'))
            @php
                $errors = session()->flash()->get('errors');
            @endphp
        @endif

        <form action="{{ asset('admin/modify_group/' . $gruppo->getIdGruppo()) }}" method="POST">

        @csrf
            <!-- Primary -->
            <section class="rounded mb-3 pb--50">

                <!-- section header -->
                <div class="bg-light rounded clearfix p-3 mb-4">
                    <div class="d-flex justify-content-between">
                        <div>
                            Informazioni Gruppo
                        </div>
                        <a class="pt--5">
                                <span class="group-icon">
                                </span>
                        </a>
                    </div>
                </div>
                <!-- /section header -->

                <div class="row d-flex justify-content-center">
                    <div class="col-4">
                        <div class="form-label-group mb-3">
                            <input placeholder="Nome" id="name" type="text"
                                   value="{{ $gruppo->getNome() }}"
                                   name="nome"
                                   class="form-control @isset($errors) @if(array_key_exists ('nome', $errors)) invalidato @endif @endisset">
                            <label for="name">Nome</label>
                        </div>
                        @isset($errors)
                            @if(array_key_exists ('nome', $errors))
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['nome'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @endif
                        @endisset
                    </div>

                    <div class="col-8">
                        <div class="form-label-group mb-3">
                            <input placeholder="Descrizione" id="descrizione" type="text"
                                   value="{{ $gruppo->getDescrizione() }}"
                                   name="descrizione"
                                   class="form-control @isset($errors) @if(array_key_exists ('descrizione', $errors)) invalidato @endif @endisset">
                            <label for="descrizione">Descrizione</label>
                        </div>
                        @isset($errors)
                            @if(array_key_exists ('descrizione', $errors))
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span class="fi fi-close" aria-hidden="true"></span>
                                    </button>
                                    @foreach($errors['descrizione'] as $ernome)
                                        {{$ernome}}<br>
                                    @endforeach
                                </div>
                            @endif
                        @endisset
                    </div>

                </div>
                <div class="row pr--7 d-flex justify-content-center">
                    <div class="col-12 col-md pr-md-2">
                        <div class="form-label-group mb-3">
                            <select id="service" class="form-control bs-select" name="service[]" multiple>
                                <option class="d-none" value="0" disabled></option>
                                @foreach($serviceList as $servizio)
                                    <option value="{{$servizio->getIdServizio()}}" @if(!empty($idServizi) && in_array($servizio->getIdServizio(), $idServizi)) selected @endif >{{$servizio->getNome()}}</option>
                                @endforeach
                            </select>
                            <label for="service">Scegli i Servizi</label>
                        </div>
                    </div>
                </div>
            </section>

            <a type="button" class="btn btn-secondary float-start" href="{{asset('admin/groups_services')}}">
                <i class="fi fi-close"></i>
                Annulla
            </a>
            <button type="submit" class="ml-0 btn btn-success float-end">
                <i class="fi fi-check"></i>
                Salva
            </button>
        </form>
    </div>
    <!-- /MIDDLE -->
@endsection