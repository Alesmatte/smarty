@extends('skeletons.back.app')

@section('content')
<!-- MIDDLE -->
<div id="middle" class="flex-fill">

    <!-- PAGE TITLE -->

    <div class="page-title bg-transparent b-0">

        <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
            Gestione Utenza
        </h1>

    </div>

    <div class="row d-flex flex-fill align-items-start">

        <div class="col align-self-start">

            <section id="rounded mb-3">

                <div class="bg-light rounded pt-2 pl-3 pr-3 mb-4 fs--20 d-flex justify-content-between border-bottom">
                    <div class="pr-2 pl-2 pb-2 pt-3">
                        Lista utenti
                    </div>
                    <div class="p-2">
                        <a type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#insertUserModal">
                            Aggiungi utente
                            <i class="fi fi-plus"></i>
                        </a>
                        <!-- Modal -->
                        <div class="modal fade" id="insertUserModal" tabindex="-1" role="dialog" aria-labelledby="insertUserModal" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="{{asset('admin/create_user')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <!-- Header -->
                                        <div class="modal-header d-flex justify-content-center pr-4 pl-4">
                                            <h4 class="modal-title " id="exampleModalLabelMd">Scegli la Tipologia </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span class="fi fi-close fs--18" aria-hidden="true"></span>
                                            </button>
                                        </div>

                                        <!-- Content -->
                                            <div class="col p-4">
                                                <div class="form-label-group">
                                                    <select class="form-control" id="type" name="tipologia">
                                                        <option value="0">Normale</option>
                                                        <option value="1">Critico</option>
                                                        <option value="2">Amministratore</option>

                                                    </select>
                                                    <label for="type">Tipologia Utente</label>
                                                </div>
                                            </div>

                                        <!-- Footer -->
                                        <div class="modal-footer d-flex justify-content-between pl-4 pr-4">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fi fi-close"></i>
                                                Annulla
                                            </button>
                                            <button type="submit" class="ml-0 btn btn-primary">
                                                <i class="fi fi-check"></i>
                                                Procedi
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt--30 mb--60">

                    <table class="table-datatable table-responsive-lg table table-bordered table-hover table-striped"
                           data-lng-empty="Nessun dato disponibile"
                           data-lng-page-info="Mostrati _START_ a _END_ di _TOTAL_ elementi"
                           data-lng-filtered="(filtered from _MAX_ total entries)"
                           data-lng-loading="Caricamento..."
                           data-lng-processing="Inizializzazione..."
                           data-lng-search="Cerca..."
                           data-lng-norecords="Nessun risultato per la ricerca"
                           data-lng-sort-ascending=": activate to sort column ascending"
                           data-lng-sort-descending=": activate to sort column descending"

                           data-lng-column-visibility="Mostra/nascondi campi"
                           data-lng-all="All"

                           data-main-search="true"
                           data-column-search="false"
                           data-row-reorder="false"
                           data-col-reorder="true"
                           data-responsive="true"
                           data-header-fixed="true"
                           data-select-onclick="false"
                           data-enable-paging="true"
                           data-enable-col-sorting="true"
                           data-autofill="false"
                           data-group="false"

                           data-lng-export="<i class='fi fi-squared-dots fs--18 line-height-1'></i>"
                           data-export-pdf-disable-mobile="true">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Cognome</th>
                            <th>Email</th>
                            <th>Data Iscrizione</th>
                            <th>Gruppi</th>
                            <th>Stato</th>
                            <th class=" text-center">Cambia stato</th>
                            <th class=" text-center">Azioni</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($userList as $k=>$user)
                                <tr>
                                <td>{{ $user->getNome() }}</td>
                                <td>{{ $user->getCognome() }}</td>
                                <td>{{ $user->getEmail() }}</td>
                                <td>{{ date_format(date_create($user->getDataNascita()), "d/m/Y")}}</td>
                                    <td>
                                        @isset($groupListStr[$user->getIdUser()])
                                            @if(strlen($groupListStr[$user->getIdUser()]) > 20)
                                                <a href="#" class="mb-1 text-dark text-decoration-none" data-toggle="tooltip" data-placement="right" data-html="true" title="<div class='scrollable-vertical max-h-200 pl--15 pr--10 pt--10 pb--10 text-align-start'><h6 class='p-0 fs--15 text-warning mb-2'>Servizi</h6><ul class='p-0'>@foreach($groupListOver[$user->getIdUser()] as $gru)<li>{{ $gru->getNome() }}</li>@endforeach</ul></div>">
                                            @endif
                                                {{strlen($groupListStr[$user->getIdUser()]) > 20  ? substr($groupListStr[$user->getIdUser()],0,17) . '...' : $groupListStr[$user->getIdUser()] }}
                                            @if(strlen($groupListStr[$user->getIdUser()]) > 20)
                                                </a>
                                            @endif
                                        @endisset
                                    </td>
                                @if($user->getStato() == 1)
                                    <td>Attivo</td>
                                @else
                                    <td>Disattivo</td>
                                @endif
                                <td class=" text-center">

                                    @if($user->getStato() == 0)
                                    <button data-href="{{asset('api/admin/user_list/' . $user->getIdUser())}}"
                                            type="button"
                                            href="#"
                                            class="js-ajax-confirm btn btn-sm rounded-circle-xs btn-indigo btn-pill pl--30 pr--30"

                                            data-ajax-confirm-mode="ajax"
                                            data-ajax-confirm-method="PUT"

                                            data-ajax-confirm-size="modal-md"
                                            data-ajax-confirm-centered="false"

                                            data-ajax-confirm-title="Per favore conferma"
                                            data-ajax-confirm-body="Sei sicuro di voler Attivare l'utente di tipo
                                                    @if($user->getTipologiaUtenza() == 0)
                                                    User normale
                                                    @elseif($user->getTipologiaUtenza() == 1)
                                                    Critico
                                                    @elseif($user->getTipologiaUtenza() == 2)
                                                    Amministratore
                                                    @endif
                                                    <strong>{{ $user->getNome() }} {{ $user->getCognome() }}</strong>?"
                                            data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                            data-ajax-confirm-btn-yes-text="Conferma"
                                            data-ajax-confirm-btn-yes-icon="fi fi-check"

                                            data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                            data-ajax-confirm-btn-no-text="Annulla"
                                            data-ajax-confirm-btn-no-icon="fi fi-close"

                                            data-ajax-confirm-callback-function="myFunction">
                                        Attiva
                                    </button>

                                    @elseif($user->getStato() == 1)
                                    <button data-href="{{asset('api/admin/user_list/' . $user->getIdUser())}}"
                                            type="button"
                                            class="js-ajax-confirm btn btn-sm rounded-circle-xs btn-secondary btn-pill pl--20 pr--20"

                                            data-ajax-confirm-mode="ajax"
                                            data-ajax-confirm-method="PUT"

                                            data-ajax-confirm-size="modal-md"
                                            data-ajax-confirm-centered="false"


                                            data-ajax-confirm-title="Per favore conferma"
                                            data-ajax-confirm-body="Sei sicuro di voler Disattivare l'utente di tipo
                                                    @if($user->getTipologiaUtenza() == 0)
                                                    User normale
                                                    @elseif($user->getTipologiaUtenza() == 1)
                                                    Critico
                                                    @elseif($user->getTipologiaUtenza() == 2)
                                                    Amministratore
                                                    @endif
                                                    <strong>{{ $user->getNome() }} {{ $user->getCognome() }}</strong>?"

                                            data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                            data-ajax-confirm-btn-yes-text="Conferma"
                                            data-ajax-confirm-btn-yes-icon="fi fi-check"

                                            data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                            data-ajax-confirm-btn-no-text="Annulla"
                                            data-ajax-confirm-btn-no-icon="fi fi-close"

                                            data-ajax-confirm-callback-function="myFunction">
                                            Disattiva
                                    </button>
                                    @endif
                                </td>
                                <td class=" text-center">
                                    <button class="btn btn-sm rounded-circle-xs btn-primary btn-pill dropdown-toggle"
                                            type="button" id="drop_actions_user_1" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                                    <span class="group-icon">
                                                        <i class="fi fi-menu-dots"></i>
                                                        <i class="fi fi-arrow-down"></i>
                                                    </span>
                                        <span>Azioni</span>
                                    </button>

                                    <div aria-labelledby="drop_actions_user_1"
                                         class="dropdown-menu dropdown-menu-right shadow-3d  p-0 mt-0  fs--15 w--190">
                                        <div class="ml--0 mt--10 bg-white rounded">
                                            <h6 class="dropdown-header">Azioni</h6>

                                            <div class="dropdown-divider"></div>

                                            <div class="scrollable-vertical max-h-50vh">

                                                <a class="dropdown-item text-dark pl--15"
                                                   @if($user->getTipologiaUtenza() == 0)
                                                   href="{{asset('admin/modify_normal_user/' . $user->getIdUser())}}"
                                                   @elseif($user->getTipologiaUtenza() == 1)
                                                   href="{{asset('admin/modify_critico_user/' . $user->getIdUser())}}"
                                                   @elseif($user->getTipologiaUtenza() == 2)
                                                   href="{{asset('admin/modify_admin_user/' . $user->getIdUser())}}"
                                                   @endif>
                                                    <i class="fi fi-pencil text-primary"></i>
                                                    Modifica
                                                </a>
                                                <a data-href="{{asset('api/admin/user_list/' . $user->getIdUser())}}?action=delete"
                                                   class="js-ajax-confirm dropdown-item text-dark pl--15" href="#"

                                                   data-ajax-confirm-mode="ajax"
                                                   data-ajax-confirm-method="DELETE"

                                                   data-ajax-confirm-size="modal-md"
                                                   data-ajax-confirm-centered="false"
                                                   data-ajax-confirm-callback-function="myFunction"
                                                   data-ajax-confirm-title="Per favore conferma"
                                                   data-ajax-confirm-body="Sei sicuro di voler Eliminare l'utente di tipo
                                                            @if($user->getTipologiaUtenza() == 0)
                                                            User normale
                                                            @elseif($user->getTipologiaUtenza() == 1)
                                                            Critico
                                                            @elseif($user->getTipologiaUtenza() == 2)
                                                            Amministratore
                                                            @endif
                                                            <strong>{{ $user->getNome() }} {{ $user->getCognome() }}</strong>?"

                                                   data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                   data-ajax-confirm-btn-yes-text="Conferma"
                                                   data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                   data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                   data-ajax-confirm-btn-no-text="Annulla"
                                                   data-ajax-confirm-btn-no-icon="fi fi-close">

                                                    <i class="fi fi-thrash text-primary"></i>
                                                    Elimina
                                                </a>
                                                <a  data-href="plugins-sow-ajax-confirm.html?action=delete&amp;item_id=1"
                                                    class="js-ajax-confirm dropdown-item text-dark pl--15" href="#"

                                                    data-ajax-confirm-mode="ajax"
                                                    data-ajax-confirm-method="POST"

                                                    data-ajax-confirm-size="modal-md"
                                                    data-ajax-confirm-centered="false"

                                                    data-ajax-confirm-title="Per favore conferma"
                                                    data-ajax-confirm-body="Sei sicuro di voler Resettare la Password alll'utente di tipo
                                                            @if($user->getTipologiaUtenza() == 0)
                                                            User normale
                                                            @elseif($user->getTipologiaUtenza() == 1)
                                                            Critico @elseif($user->getTipologiaUtenza() == 2)
                                                            Amministratore
                                                            @endif
                                                            <strong>{{ $user->getNome() }} {{ $user->getCognome() }}</strong>?"

                                                    data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                    data-ajax-confirm-btn-yes-text="Conferma"
                                                    data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                    data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                    data-ajax-confirm-btn-no-text="Annulla"
                                                    data-ajax-confirm-btn-no-icon="fi fi-close">
                                                    <i class="fi fi-round-lightning text-primary"></i>
                                                    Resetta password
                                                </a>

                                            </div>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Cognome</th>
                            <th>Email</th>
                            <th>Data Iscrizione</th>
                            <th>Tipologia Utente</th>
                            <th>Stato</th>
                            <th class=" text-center">Cambia stato</th>
                            <th class=" text-center">Azioni</th>
                        </tr>
                        </tfoot>
                    </table>

                </div>

            </section>
        </div>
    </div>
</div>
<!-- /MIDDLE -->

@if(session()->flash()->has('succesMessage'))
<div class="hide toast-on-load"
     data-toast-type="success"
     data-toast-title="Informazione"
     data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
     data-toast-pos="top-center"
{{--     data-toast-delay="4000"--}}
     data-toast-fill="true"
     data-
></div>
@endif

@if(session()->flash()->has('errorMessage'))
    <div class="hide toast-on-load"
         data-toast-type="danger"
         data-toast-title="Informazione"
         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
         data-toast-pos="top-center"
         {{--     data-toast-delay="4000"--}}
         data-toast-fill="true"
         data-
    ></div>
@endif
@endsection

@section('script')
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection