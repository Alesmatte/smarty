@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Inserisci un nuovo film
            </h1>

        </div>

        <form class="" action="{{asset('admin/create_film')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <!-- Primary -->
            <section class="rounded ">


                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse1"
                   role="button" aria-expanded="true" aria-controls="collapse1">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Scheda Film
                                <small class="fs--11 text-muted d-block mt-1">Titolo, Trama e Copertina</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->

                <div class="collapse mb-3 show" id="collapse1">
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_originale_film" name="titolo_originale_film" type="text"
                                               placeholder="Titolo Originale"
                                               value=""
                                               class="form-control"
                                        >
                                        <label for="titolo_originale_film">Titolo Originale Film</label>
                                    </div>
                                </div>
                                <div class="d-none d-sm-block fs--35 font-weight-100">
                                    <div>/</div>
                                </div>
                                <div class="col-12 col-md pl-md-2">
                                    <div class="form-label-group mb-3">
                                        <input id="titolo_film" name="titolo_film" type="text" placeholder="Titolo"
                                               value=""
                                               class="form-control">
                                        <label for="titolo_film">Titolo Film</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="description" class="">Trama:</label>
                                    <textarea id="description" name="trama" class="summernote-editor w-100"
                                              data-placeholder="Trama"
                                              data-min-height="285"
                                              data-max-height="285"
                                              data-lang="it-IT"
                                              data-disable-resize-rditor="true"
                                              data-ajax-url="_ajax/demo.summernote.php"
                                              data-ajax-params="['action','upload']['param2','value2']"

                                              data-toolbar='[
			["style", ["style"]],
			["font", ["bold", "italic", "underline", "clear"]],
			["para", ["ul", "ol", "paragraph"]],
			["insert", ["link", "hr"]],
			["view", ["fullscreen", "codeview"]],
			["help", ["help"]]
		]'
                                    ></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <div class="position-relative">
                                        <label for="descrizione_breve">Trama in riassunto: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		500 caratteri massimi
	</span>

                                        <textarea id="descrizione_breve"
                                                  name="trama_breve"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="500"></textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">500</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-auto">

                            <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed h--565 w--382">

                                <!-- remove button -->
                                <a href="#"
                                   class="js-file-input-showcase-remove hide position-absolute absolute-top text-align-start w-100 z-index-3">
		                            <span class="d-inline-block btn btn-sm bg-secondary text-white pt--4 pb--4 pl--10 pr--10 m--1"
                                          title="remove image" data-tooltip="tooltip">
			<i class="fi fi-close m-0"></i>
		</span>
                                </a>

                                <span class="z-index-2 js-file-input-showcase-container d-block absolute-full z-index-1 hide-empty"><!-- image container --></span>

                                <input name="immagine_copertina"
                                       type="file"
                                       data-file-ext="jpg, jpeg, png"
                                       data-file-max-size-kb-per-file="10240"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="top-center"
                                       data-file-preview-container=".js-file-input-showcase-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="m-0 p-0 rounded"
                                       data-file-preview-img-height="auto"
                                       data-file-btn-clear="a.js-file-input-showcase-remove"
                                       data-file-preview-img-cover="true"
                                       class="custom-file-input absolute-full">

                                <div class="absolute-full">
                                    <div class="d-table">
                                        <div class="d-table-cell align-middle text-center">

                                            <i class="fi fi-image fs--50 text-muted"></i>
                                            <small class="d-block text-muted">
                                                <b>IMMAGINE COPERTINA</b>
                                                <span class="d-block mt-1">
						Perfafore, 380x563 px preferibile.
					</span>
                                            </small>

                                        </div>
                                    </div>
                                </div>

                                <!-- ratio maintained using a `blank` image -->
                                <img class="w-100"
                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     alt="...">

                            </label>

                        </div>
                    </div>

                </div>

                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapseDati"
                   role="button" aria-expanded="false" aria-controls="collapseDati">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Dati del Film
                                <small class="fs--11 text-muted d-block mt-1">Descrizioni, Dati vari </small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapseDati">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-12 col-md-8" style="padding-right: 26px;">
                                    <div class="position-relative">
                                        <label for="descrizione_film">Descrizione film: </label>
                                        <span class="js-form-advanced-limit-info badge badge-warning hide animate-bouncein position-absolute absolute-top m--2">
		250 caratteri massimi
	</span>

                                        <textarea id="descrizione_film"
                                                  name="descrizione_film"
                                                  class="js-form-advanced-char-count-down form-control"
                                                  data-output-target=".js-form-advanced-char-left2"
                                                  maxlength="250"></textarea>

                                        <div class="fs--12 text-muted text-align-end mt--3">
                                            Caratteri rimanenti: <span
                                                    class="js-form-advanced-char-left2">250</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4 pl--5" style="padding-top: 33px;">
                                    <div class="form-label-group w-100">
                                        <select id="film_in_sala"
                                                name="film_in_sala"
                                                class="form-control">
                                            <option value="0">Film non disponibile nei cinema</option>
                                            <option value="1">Film nei cinema</option>
                                        </select>
                                        <label for="film_in_sala">Film in sala</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_sviluppo" type="date"
                                       name="data_sviluppo"
                                       value="{{date('Y-m-d')}}"
                                       class="form-control">
                                <label for="data_sviluppo">Data sviluppo film</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <input id="data_uscita_italia" type="date"
                                       name="data_uscita_italia"
                                       value="{{date('Y-m-d')}}"
                                       class="form-control">
                                <label for="data_uscita_italia">Data di uscita</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-label-group mb-3">
                                <select id="eta_consigliata"
                                        name="eta_consigliata"
                                        class="form-control">
                                    <option value="0">0+ anni</option>
                                    <option value="5">5+ anni</option>
                                    <option value="13">13+ anni</option>
                                    <option value="15">15+ anni</option>
                                    <option value="18">18+ anni</option>
                                </select>
                                <label for="eta_consigliata">Età consigliata</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-auto">
                            <div class="form-label-group mb-3">
                                <input placeholder="Titolo Originale" id="durata_film" type="number"
                                       name="durata_film"
                                       value=""
                                       class="form-control">
                                <label for="durata_film">Durata Film</label>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="form-label-group mb-3">
                                <select id="nazione" class="form-control bs-select" name="nazione[]" multiple>
                                    <option value="US">United States</option>
                                    <option value="CA">Canada</option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia and Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo (Democratic Republic)</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="TP">East Timor</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="FX">France (European Territory)</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard and McDonald Islands</option>
                                    <option value="VA">Holy See (Vatican)</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT" selected="selected">Italy</option>
                                    <option value="CI">Cote D&rsquo;Ivoire</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="KP">North Korea</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territory</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PF">Polynesia</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="GS">S. Georgia &amp; S. Sandwich Isls.</option>
                                    <option value="SH">Saint Helena</option>
                                    <option value="KN">Saint Kitts &amp; Nevis Anguilla</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="PM">Saint Pierre and Miquelon</option>
                                    <option value="VC">Saint Vincent &amp; Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome and Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="KR">South Korea</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UM">USA Minor Outlying Islands</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="VG">Virgin Islands (British)</option>
                                    <option value="VI">Virgin Islands (USA)</option>
                                    <option value="WF">Wallis and Futuna Islands</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZR">Zaire</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                                <label for="nazione">Nazionalit&agrave;</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="distributore" class="form-control bs-select"
                                                name="distributore[]" multiple>
                                            <option value="0"></option>
                                            @isset($distributori)
                                                @if(count($distributori)>0)
                                                    @foreach($distributori as $distributore)
                                                        <option value="{{$distributore->getIdDistributore()}}">{{$distributore->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="distributore">Distributore film</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#" class="js-ajax-modal btn btn-primary"
                                       data-href="{{asset('admin/modal/create_distributore')}}"
                                       data-ajax-modal-size="modal-md"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop="">
                                        Aggiungi nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md">
                            <div class="row">
                                <div class="col-12 col-md pr-md-2">
                                    <div class="form-label-group mb-3">
                                        <select id="genere" class="form-control bs-select" name="genere[]" multiple>
                                            <option value="0"></option>
                                            @isset($generi)
                                                @if(count($generi)>0)
                                                    @foreach($generi as $genere)
                                                        <option value="{{$genere->getIdGenere()}}">{{$genere->getNome()}}</option>
                                                    @endforeach
                                                @endif
                                            @endisset
                                        </select>
                                        <label for="genere">Genere film</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-auto pl-md-2 mb-3">
                                    <a href="#"
                                       data-href="{{asset('admin/modal/create_genere')}}"
                                       data-ajax-modal-size="modal-lg"
                                       data-ajax-modal-centered="false"
                                       data-ajax-modal-callback-function=""
                                       data-ajax-modal-backdrop=""
                                       class="js-ajax-modal btn btn-primary">
                                        Aggiungi Nuovo
                                        <i class="fi fi-plus ml--5 mr--0"></i>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Primary -->

            <!-- CAST -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse2"
                   role="button" aria-expanded="false" aria-controls="collapse2">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                Cast
                                <small class="fs--11 text-muted d-block mt-1">Cast & Filmmakers</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <!-- /section header -->
                <div class="collapse mb-3" id="collapse2">
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->

                    <div class="js-form-advanced-table"
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-row-limit="50"
                         data-table-row-method="append">


                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th>Filmmakers</th>
                                <th>Tipologia</th>
                                <th>Ruolo</th>
                                <th class="w--80"></th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr>
                                <!-- SKU -->
                                <td>
                                        <div class="col pr--0 pl--0">
                                            <div class="form-label-group">
                                                <select class="form-control select2-plugin ajax-method"
                                                        name="cast[select_filmmakers][]"
                                                        data-input-min-length="1" data-ajax-method="GET"
                                                        data-ajax-url="{{asset('api/cast')}}"
                                                        data-ajax-cache="true"
                                                        data-input-delay="250">
                                                </select>
                                                <label
                                                        class="label-select2">Filmmakers</label>
                                            </div>
                                        </div>
                                </td>

                                <!-- Barcode -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="cast[tipologia][]">
                                            <option value="0">Altro</option>
                                            <option value="1">Attore</option>
                                            <option value="2">Regista</option>
                                            <option value="4">Produttore</option>
                                            <option value="5">Sceneggiatore</option>
                                            <option value="6">Fotografia</option>
                                            <option value="7">Montaggio</option>
                                            <option value="8">Musica</option>
                                            <option value="9">Scenografo</option>
                                            <option value="10">Costumista</option>
                                            <option value="11">Effetti</option>
                                            <option value="12">Art Director</option>
                                            <option value="13">Trucco</option>
                                        </select>
                                        <label>Tipologia</label>
                                    </div>
                                </td>

                                <!-- Price -->
                                <td>
                                    <div class="form-label-group">
                                        <input placeholder="ruolo" type="text" class="form-control"
                                               name="cast[ruolo][]"
                                               value="">
                                        <label>Ruolo</label>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="text-center">

                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
						<span class="group-icon">
							<i class="fi fi-plus"></i>
							<i class="fi fi-thrash"></i>
						</span>
                                    </a>

                                </td>

                            </tr>

                            </tbody>
                        </table>
                        <div class="d-flex justify-content-start">
                            <div class="col-auto pl--0">
                                Se non trovi il filmmaker aggiungilo tramite questo bottone:
                                <a href="#" class="js-ajax-modal btn btn-sm btn-primary ml--15"
                                   data-href="{{asset('admin/modal/create_filmmakers')}}"
                                   data-ajax-modal-size="modal-xl"
                                   data-ajax-modal-centered="false"
                                   data-ajax-modal-callback-function=""
                                   data-ajax-modal-backdrop="">
                                    Aggiungi Filmmaker
                                    <i class="fi fi-plus ml--5 mr--0"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
            <!-- /CAST-->

            <!-- MEDIA -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse3"
                   role="button" aria-expanded="false" aria-controls="collapse3">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                MEDIA
                                <small class="fs--11 text-muted d-block mt-1">Foto, Poster & Trailer</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse3">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>SCHEDA</th>
                                <th class="w--90">MEDIA</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Scheda -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="immagini[scheda][]">
                                            <option value="6">Poster</option>
                                            <option value="5">Foto</option>
                                        </select>
                                        <label>Visualizzabile in</label>
                                    </div>
                                </td>

                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="immagini[file][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>

                            {{--

                            <!-- preadded -->
                            <tr>
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- scheda -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="media[scheda][]">
                                            <option value="0"></option>
                                            <option value="1">Trailer</option>
                                            <option value="2">Poster</option>
                                            <option value="3" selected>Foto</option>
                                        </select>
                                        <label>Visualizzabile in</label>
                                    </div>
                                </td>
                                <!-- Image -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty">
							<span data-id="0"
                                  data-file-name="mutzii-fmDCrqPQKog-unsplash-min.jpg"
                                  style="background-image:url({{asset('assets/images/mutzii-fmDCrqPQKog-unsplash-min.jpg')}})"
                                  class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
							</span>
						</span>

                                        <!-- hidden file input -->
                                        <input name="media[file][]"
                                               type="file"

                                               data-file-ext="jpg, png, gif, mp4"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               data-file-ajax-upload-enable="true"
                                               data-file-ajax-upload-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-upload-params="['action','upload']['param2','value2']"

                                               data-file-ajax-delete-enable="true"
                                               data-file-ajax-delete-url="../demo.files/php/demo.ajax_file_upload.php"
                                               data-file-ajax-delete-params="['action','delete_file']"

                                               data-file-ajax-toast-success-txt="Successfully Uploaded!"
                                               data-file-ajax-toast-error-txt="One or more files not uploaded!"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>

                                <!-- tipologia file -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="media[tipologia_file][]">
                                            <option value="0"></option>
                                            <option value="1" selected>Jpeg</option>
                                            <option value="2">Png</option>
                                            <option value="3">Gif</option>
                                            <option value="4">Mp4</option>
                                        </select>
                                        <label>Tipologia file</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"> la descrizione 1</textarea>
                                    </div>
                                </td>

                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                        --}}

                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>

                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="append">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>VIDEO</th>
                                <th class="w--80">COVER</th>
                                <th>DESCRIZIONE</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>


                                <!-- video -->
                                <td>
                                    <div class="input-group">
                                        <input type="file" name="media[video][]">

                                    </div>
                                </td>


                                <!-- file -->
                                <td>

                                    <!--

                                        AJAX IMAGE UPLOAD

                                    -->
                                    <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                        <!-- remove button -->
                                        <a href="#!"
                                           class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                            <i class="fi fi-close m-0"></i>
                                        </a>

                                        <!-- image container -->
                                        <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                        <!-- hidden file input -->
                                        <input name="media[cover][]"
                                               type="file"
                                               data-file-ext="jpg, png, gif, jpeg"
                                               data-file-max-size-kb-per-file="50000"
                                               data-file-ext-err-msg="Allowed:"
                                               data-file-size-err-item-msg="File too large!"
                                               data-file-size-err-total-msg="Total allowed size exceeded!"
                                               data-file-toast-position="bottom-center"
                                               data-file-preview-container=".js-table-file-preview-1"
                                               data-file-preview-show-info="false"
                                               data-file-preview-class="m-0 p-0 rounded"
                                               data-file-preview-img-height="auto"
                                               data-file-btn-clear=".js-table-file-remove-1"
                                               data-file-preview-img-cover="true"

                                               class="custom-file-input absolute-full">

                                        <!-- icon -->
                                        <span class="absolute-full d-middle">
							<i class="fi fi-image fs--30 text-muted"></i>
						</span>

                                    </label>

                                </td>



                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="media[descrizione][]"></textarea>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /MEDIA-->

            <!-- PREMI -->
            <section class="rounded shadow-3d">
                <!-- section header -->
                <a class="text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse4"
                   role="button" aria-expanded="false" aria-controls="collapse4">
                    <div class="bg-light rounded clearfix p-3 mb-4">
                        <div class="d-flex justify-content-between">
                            <div>
                                PREMI
                                <small class="fs--11 text-muted d-block mt-1">Premi & Nomination</small>
                            </div>

                            <span class="group-icon pt--5">
			<i class="fi fi-arrow-end-slim"></i>
			<i class="fi fi-arrow-down-slim"></i>
		</span>

                        </div>
                    </div>
                </a>
                <div class="collapse mb-3" id="collapse4">
                    <!-- /section header -->
                    <!--
     default
         data-table-clone-method="append|prepend"
 -->
                    <div class="js-form-advanced-table mb-6"
                         data-table-column-insert-before=".js-clone-before"
                         data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                         data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                         data-table-column-limit="4"
                         data-table-row-limit="50"
                         data-table-row-method="prepend">

                        <table class="table table-bordered table-align-middle table-sm">
                            <thead>
                            <tr>
                                <th class="w--30">&nbsp;</th>
                                <th>GENERE</th>
                                <th>TIPOLOGIA</th>
                                <th>STATO</th>
                                <th>ASSEGNATO A</th>
                                <th>DESCRIZIONE</th>
                                <th>DATA</th>
                                <th class="w--80 text-center">
                                    <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                        <i class="fi fi-plus"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>

                            <!--
                                See Sortable doumentation if ajax reorder is needed
                            -->
                            <tbody class="sortable">

                            <!--
                                OPTIONAL
                                ACTING AS A TEMPLATE TO CLONE
                                IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                Else, the first TR is used by default!
                                .js-ignore = optional, used by sortable to igore from drag/drop reorder
                            -->
                            <tr class="js-ignore hide">
                                <!-- sortable handler -->
                                <td class="px-0 text-center">
                                    <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                </td>
                                <!-- Genere -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[genere][]">
                                            @foreach($premi as $k => $premio)
                                                <option value="{{$premio->getIdPremio()}}" >{{$premio->getNome()}}</option>
                                            @endforeach
                                        </select>
                                        <label>Premio</label>
                                    </div>
                                </td>

                                <!-- Tipologia -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[tipologia][]">
                                            @foreach($tipologiaPremi as $k => $tipologiaPremio)
                                                @if($tipologiaPremio->getidTipologiaPremio() != 1 && $tipologiaPremio->getidTipologiaPremio() != 2)
                                                <option value="{{$tipologiaPremio->getidTipologiaPremio()}}" >{{$tipologiaPremio->getNome()}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <label>Premio per</label>
                                    </div>
                                </td>

                                <!-- Stato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control bs-select" name="premio[stato][]">
                                            <option value="0">Nomination</option>
                                            <option value="1">Vittoria</option>
                                        </select>
                                        <label>Stato:</label>
                                    </div>
                                </td>

                                <!-- Assegnato -->
                                <td>
                                    <div class="form-label-group">
                                        <select class="form-control select2-plugin ajax-method"
                                                name="premio[select_filmmakers][]"
                                                data-input-min-length="1" data-ajax-method="GET"
                                                data-ajax-url="{{asset('api/cast')}}"
                                                data-input-delay="250">
                                        </select>
                                        <label
                                                class="label-select2">Assegnato a</label>
                                    </div>
                                </td>


                                <!-- Descrizione -->
                                <td>
                                    <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="premio[descrizione][]"></textarea>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Data premiazione" type="date" value="{{date('Y-m-d')}}"
                                               class="form-control" name="premio[data_premiazione][]">
                                        <label>Data Premiazione</label>
                                    </div>
                                </td>


                                <!-- Option -->
                                <td class="position-relative text-center">

                                    <!-- direct delete -->
                                    <!--
                                    <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>
                                    -->

                                    <!-- remove button (confirm trigger) -->
                                    <a href="#"
                                       class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                        <i class="fi fi-thrash"></i>
                                    </a>

                                    <!-- confirm -->
                                    <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                        <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                        <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                        <a href="#!"
                                           class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                    </div>

                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <small class="d-block text-muted mt-1">Se vuoi puoi trascinare e rilasciare le righe per
                            riordinarle</small>

                    </div>
                </div>
            </section>
            <!-- /PREMI-->
            <button type="submit" class="btn btn-lg btn-success float-end"> Salva <i
                        class="fi fi-check mr--0 ml--15"></i>
            </button>
        </form>
    </div>
    <!-- /MIDDLE -->
@endsection