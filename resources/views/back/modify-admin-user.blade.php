@extends('skeletons.back.app')

@section('content')
<!-- MIDDLE -->
<div id="middle" class="flex-fill">

    <!--
        PAGE TITLE
    -->
    <div class="page-title bg-transparent b-0">

        <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
            Inserisci un nuovo Utente
        </h1>

    </div>

    @if(session()->flash()->has('errors'))
        @php
            $errors = session()->flash()->get('errors');
        @endphp
    @endif

    <form id="create_user" action="{{asset('admin/modify_admin_user/' . $user->getIdUser())}}" method="POST" enctype="multipart/form-data">

    @csrf
        <!-- Primary -->
        <section class="rounded mb-3 pb--50">

            <!-- section header -->
            <div class="bg-light rounded clearfix p-3 mb-4">
                <div class="d-flex justify-content-between">
                    <div>
                        Informazioni Utente e Account
                    </div>
                    <a class="pt--5">
                    </a>
                </div>
            </div>
            <!-- /section header -->

            <div class="row">
                @if($user->getAvatar()->getIdImmagione() != null)
                    <div class="col-2  pt--40 min-w-270 pl-4">
                        <div class="d-flex justify-content-center">
                            <div>
                                <label class="rounded-circle text-center position-relative d-inline-block cursor-pointer border border-dashed w--250 h--250 bg-primary-soft border-primary shadow-danger-xs">

                                    <span class="z-index-2 js-file-input-avatar-ajax-circle-container-preadded d-block absolute-full z-index-1">
                                        <span data-id="{{$user->getAvatar()->getIdImmagione()}}"
                                              data-file-name="{{$user->getAvatar()->getNome()}}"
                                              style="background-image:url('{{asset($user->getAvatar()->getDato())}}')"
                                              class="js-file-input-item d-inline-block position-relative overflow-hidden text-center rounded-circle m-0 p-0 animate-bouncein bg-cover w-100 h-100">
                                        </span>
                                    </span>

                                     <input name="avatar_file"
                                            type="file"

                                            data-file-ext="jpg, jpeg, png"
                                            data-file-max-size-kb-per-file="11500"
                                            data-file-ext-err-msg="Allowed:"
                                            data-file-size-err-item-msg="File too large!"
                                            data-file-size-err-total-msg="Total allowed size exceeded!"
                                            data-file-toast-position="bottom-center"
                                            data-file-preview-container=".js-file-input-avatar-ajax-circle-container-preadded"
                                            data-file-preview-show-info="false"
                                            data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                            data-file-preview-img-height="248"
                                            data-file-preview-img-cover="true"

                                            class="custom-file-input absolute-full min-w-270">

                                    <svg class="rounded-circle m-4 z-index-0 fill-gray-300 m--50 fill-primary"
                                         viewBox="0 0 60 60">
                                        <path d="M41.014,45.389l-9.553-4.776C30.56,40.162,30,39.256,30,38.248v-3.381c0.229-0.28,0.47-0.599,0.719-0.951c1.239-1.75,2.232-3.698,2.954-5.799C35.084,27.47,36,26.075,36,24.5v-4c0-0.963-0.36-1.896-1-2.625v-5.319c0.056-0.55,0.276-3.824-2.092-6.525C30.854,3.688,27.521,2.5,23,2.5s-7.854,1.188-9.908,3.53c-2.368,2.701-2.148,5.976-2.092,6.525v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C1.801,46.924,0,49.958,0,53.262V57.5h46v-4.043C46,50.018,44.089,46.927,41.014,45.389z"/>
                                        <path d="M55.467,46.526l-9.723-4.21c-0.23-0.115-0.485-0.396-0.704-0.771l6.525-0.005c0,0,0.377,0.037,0.962,0.037c1.073,0,2.638-0.122,4-0.707c0.817-0.352,1.425-1.047,1.669-1.907c0.246-0.868,0.09-1.787-0.426-2.523c-1.865-2.654-6.218-9.589-6.354-16.623c-0.003-0.121-0.397-12.083-12.21-12.18c-1.187,0.01-2.309,0.156-3.372,0.413c0.792,2.094,0.719,3.968,0.665,4.576v4.733c0.648,0.922,1,2.017,1,3.141v4c0,1.907-1.004,3.672-2.607,4.662c-0.748,2.022-1.738,3.911-2.949,5.621c-0.15,0.213-0.298,0.414-0.443,0.604v2.86c0,0.442,0.236,0.825,0.631,1.022l9.553,4.776c3.587,1.794,5.815,5.399,5.815,9.41V57.5H60v-3.697C60,50.711,58.282,47.933,55.467,46.526z"/>
                                    </svg>
                                 </label>
                                 <h4 class="font-weight-normal fs&#45;&#45;17 text-center">Immagine profilo</h4>
                            </div>
                        </div>
                        <div class="pt--10 d-flex justify-content-center">
                            <a  data-method="delete" href="{{asset('api/admin/modify_user/' . $user->getIdUser())}}" class="btn btn-primary">Cancella
                                Immagine profilo</a>
                        </div>
                    </div>
                @else
                    <div class="col-2 d-flex justify-content-center pt--40 min-w-270 pl-4">
                        <div>
                            <label class="rounded-circle text-center position-relative d-inline-block cursor-pointer border border-dashed w--250 h--250 bg-primary-soft border-primary shadow-danger-xs">

                                <!-- remove button -->
                                <a href="#"
                                   class="js-file-upload-avatar-circle-remove hide position-absolute absolute-top w-100 z-index-3">
                                 <span class="d-inline-block btn btn-sm btn-pill bg-secondary text-white pt&#45;&#45;4 pb&#45;&#45;4 pl&#45;&#45;10 pr&#45;&#45;10 m&#45;&#45;1 mt&#45;&#45;n15"
                                       title="remove avatar" data-tooltip="tooltip">
                                     <i class="fi fi-close m-0"></i>
                                 </span>
                                </a>
                                <span class="z-index-2 js-file-input-avatar-circle-container d-block absolute-full z-index-1 hide-empty"></span>

                                <input name="avatar_file"
                                       type="file"

                                       data-file-ext="jpg, jpeg, png"
                                       data-file-max-size-kb-per-file="10240"
                                       data-file-ext-err-msg="Allowed:"
                                       data-file-size-err-item-msg="File too large!"
                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                       data-file-toast-position="bottom-center"
                                       data-file-preview-container=".js-file-input-avatar-circle-container"
                                       data-file-preview-show-info="false"
                                       data-file-preview-class="rounded-circle m-0 p-0 animate-bouncein"
                                       data-file-preview-img-height="248"
                                       data-file-btn-clear="a.js-file-upload-avatar-circle-remove"
                                       data-file-preview-img-cover="true"

                                       class="custom-file-input absolute-full min-w-270 cursor-pointer">

                                <svg class="rounded-circle m-4 z-index-0 fill-gray-300 m&#45;&#45;50 fill-primary" viewBox="0 0 60 60">
                                    <path d="M41.014,45.389l-9.553-4.776C30.56,40.162,30,39.256,30,38.248v-3.381c0.229-0.28,0.47-0.599,0.719-0.951c1.239-1.75,2.232-3.698,2.954-5.799C35.084,27.47,36,26.075,36,24.5v-4c0-0.963-0.36-1.896-1-2.625v-5.319c0.056-0.55,0.276-3.824-2.092-6.525C30.854,3.688,27.521,2.5,23,2.5s-7.854,1.188-9.908,3.53c-2.368,2.701-2.148,5.976-2.092,6.525v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C1.801,46.924,0,49.958,0,53.262V57.5h46v-4.043C46,50.018,44.089,46.927,41.014,45.389z"/>
                                    <path d="M55.467,46.526l-9.723-4.21c-0.23-0.115-0.485-0.396-0.704-0.771l6.525-0.005c0,0,0.377,0.037,0.962,0.037c1.073,0,2.638-0.122,4-0.707c0.817-0.352,1.425-1.047,1.669-1.907c0.246-0.868,0.09-1.787-0.426-2.523c-1.865-2.654-6.218-9.589-6.354-16.623c-0.003-0.121-0.397-12.083-12.21-12.18c-1.187,0.01-2.309,0.156-3.372,0.413c0.792,2.094,0.719,3.968,0.665,4.576v4.733c0.648,0.922,1,2.017,1,3.141v4c0,1.907-1.004,3.672-2.607,4.662c-0.748,2.022-1.738,3.911-2.949,5.621c-0.15,0.213-0.298,0.414-0.443,0.604v2.86c0,0.442,0.236,0.825,0.631,1.022l9.553,4.776c3.587,1.794,5.815,5.399,5.815,9.41V57.5H60v-3.697C60,50.711,58.282,47.933,55.467,46.526z"/>
                                </svg>
                            </label>
                            <h4 class="font-weight-normal fs&#45;&#45;17 text-center">Immagine profilo</h4>
                        </div>
                    </div>
                @endif
                <div class="col">
                    <div class="row">
                        <div class="col-12 pb--10">
                            <div class="form-label-group mb-3">
                                <input placeholder="Nome" id="name"
                                       type="text"
                                       name="nome"
                                       @if(isset($errors))
                                       value="{{old('nome')}}"
                                       @else
                                       value="{{$user->getNome()}}"
                                       @endif
                                       class="form-control @isset($errors) @if(array_key_exists ('nome', $errors)) invalidato @endif @endisset">
                                <label for="name">Nome</label>
                            </div>
                            @isset($errors)
                                @if(array_key_exists ('nome', $errors))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span class="fi fi-close" aria-hidden="true"></span>
                                        </button>
                                        @foreach($errors['nome'] as $ernome)
                                            {{$ernome}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            @endisset
                        </div>
                        <div class="col-12 pb--10">
                            <div class="form-label-group mb-3">
                                <input placeholder="Cognome" id="surname"
                                       type="text"
                                       name="cognome"
                                       @if(isset($errors))
                                       value="{{old('cognome')}}"
                                       @else
                                       value="{{$user->getCognome()}}"
                                       @endif

                                       class="form-control @isset($errors) @if(array_key_exists ('cognome', $errors)) invalidato @endif @endisset">
                                <label for="surname">Cognome</label>
                            </div>
                            @isset($errors)
                                @if(array_key_exists ('cognome', $errors))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span class="fi fi-close" aria-hidden="true"></span>
                                        </button>
                                        @foreach($errors['cognome'] as $ernome)
                                            {{$ernome}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            @endisset
                        </div>

                        <div class="col-12 pr--15 pb--10">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Email" id="email" type="email"
                                               name="email"
                                               @if(isset($errors))
                                               value="{{old('email')}}"
                                               @else
                                               value="{{$user->getEmail()}}"
                                               @endif
                                               class="form-control @isset($errors) @if(array_key_exists ('email', $errors)) invalidato @endif @endisset">
                                        <label for="email">Email</label>
                                    </div>
                                    @isset($errors)
                                        @if(array_key_exists ('email', $errors))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span class="fi fi-close" aria-hidden="true"></span>
                                                </button>
                                                @foreach($errors['email'] as $ernome)
                                                    {{$ernome}}<br>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endisset
                                </div>

                                <div class="col-4">
                                    <div class="form-label-group mb-3">
                                        <input placeholder="Data di Nascita" id="birthday" type="date"
                                               name="birthday"
                                               @if(isset($errors))
                                               value="{{old('birthday')}}"
                                               @else
                                               value="{{ $user->getDataNascita()}}"
                                               @endif
                                               class="form-control @isset($errors) @if(array_key_exists ('birthday', $errors)) invalidato @endif @endisset">
                                        <label for="birthday">Data di nascita</label>
                                    </div>
                                    @isset($errors)
                                        @if(array_key_exists ('birthday', $errors))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span class="fi fi-close" aria-hidden="true"></span>
                                                </button>
                                                @foreach($errors['birthday'] as $ernome)
                                                    {{$ernome}}<br>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row pb--10">
                        <div class="col-6">
                            <div class="input-group-over">
                                <div class="form-label-group mb-3">
                                    <input placeholder="Password" id="account_password" name="account_password" type="password" class="form-control @isset($errors) @if(array_key_exists ('account_password', $errors)) invalidato @endif @endisset">
                                    <label for="account_password">Password</label>
                                </div>
                                <!-- `SOW : Form Advanced` plugin used -->
                                <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password">
										<span class="group-icon">
											<i class="fi fi-eye m-0"></i>
											<i class="fi fi-close m-0"></i>
										</span>
                                </a>
                            </div>
                            @isset($errors)
                                @if(array_key_exists ('account_password', $errors))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span class="fi fi-close" aria-hidden="true"></span>
                                        </button>
                                        @foreach($errors['account_password'] as $ernome)
                                            {{$ernome}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            @endisset
                        </div>

                        <div class="col-6 pr--15">
                            <div class="input-group-over">
                                <div class="form-label-group mb-4">
                                    <input placeholder="Password" id="account_password_same" name="account_password_same" type="password" class="form-control @isset($errors) @if(array_key_exists ('account_password_same', $errors)) invalidato @endif @endisset">
                                    <label for="account_password_same">Resinserisci La Password</label>
                                </div>
                                <!-- `SOW : Form Advanced` plugin used -->
                                <a href="#" class="btn fs--12 btn-password-type-toggle" data-target="#account_password_same">
                                    <span class="group-icon">
                                        <i class="fi fi-eye m-0"></i>
                                        <i class="fi fi-close m-0"></i>
                                    </span>
                                </a>
                            </div>
                            @isset($errors)
                                @if(array_key_exists ('account_password_same', $errors))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span class="fi fi-close" aria-hidden="true"></span>
                                        </button>
                                        @foreach($errors['account_password_same'] as $ernome)
                                            {{$ernome}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            @endisset
                        </div>
                    </div>
                    <div class="row pb--35 pr--10">
                        <div class="col">
                            <div class="form-label-group">
                                <select id="group" class="form-control bs-select" name="group[]" multiple>
                                    <option class="d-none" value="0" disabled></option>
                                        @foreach($grouplist as $gruppo)
                                            <option value="{{ $gruppo->getIdGruppo() }}" @if(in_array($gruppo->getIdGruppo(), $idGruppi)) selected @endif >{{ $gruppo->getNome() }}</option>
                                        @endforeach
                                </select>
                                <label for="group">Gruppo</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <a type="button" class="btn btn-secondary float-start" href="{{asset('admin/user_list')}}">
            <i class="fi fi-close"></i>
            Annulla
        </a>
        <button type="submit" class="ml-0 btn btn-success float-end">
            <i class="fi fi-check"></i>
            Salva
        </button>
        <!-- /Primary -->
    </form>
</div>
<!-- /MIDDLE -->
@endsection

@section('script')
    <script>
        $("a[data-method='delete']").click(function(){
            $.ajax({
                url: this.getAttribute('href'),
                type: 'DELETE'
            })
            location.reload();
            return false;
        })
    </script>
@endsection