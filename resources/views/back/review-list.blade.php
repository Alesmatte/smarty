@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!-- PAGE TITLE -->

        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Lista recensioni
            </h1>

        </div>

        <div class="row d-flex flex-fill align-items-start">

            <div class="col align-self-start">

                <section id="rounded mb-1">
                    <div class="bg-light rounded pt-3 pl-3 pr-3 pb-1 mb-4 border-bottom d-flex justify-content-between">
                        <div class="mt-3 ml-2 mr-2 mb-2 fs--25">
                            Recensioni per <strong class="text-primary @isset($cerca) pr-3 @endisset ">{{$nomeEntita}}</strong>@isset($cerca) <i class="shadow-sm p-2"><i>Ricerca : "{{$cerca}}"</i> <a href="{{ asset('admin/review_list/'. $entita .'/'. $id) }}"><i class="fi fi-close text-primary pl-2"></i></a></i> @endisset
                        </div>
                        <div class="form-label-group">
                            <form action="{{ asset('admin/review_list/'. $entita .'/'. $id .'/cerca') }}" method="GET">
                                <div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

                                    <input placeholder="Cerca Recensioni"
                                           id="ricerca"
                                           name="ricerca"
                                           type="text"
                                           class="form-control-sow-search form-control form-control form-control-lg "
                                           value="">
                                    <span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit"
                                            class="btn btn-primary btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 b--0 bg-transparent text-muted">
										<i class="text-primary fi fi-search fs--20"></i>
									</button>

                                        <!-- close : mobile only (d-inline-block d-lg-none) -->
									<a class="btn-sow-search-toggler btn btn-light btn-noshadow m--0 pl--8 pr--8 pt--3 pb--3 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

                                </div>
                            </form>
                            <small class="d-block text-muted">* cerca per Titolo, Nome o Cognome utente</small>
                        </div>
                    </div>
                    @if(!empty($recensioniList))
                        @foreach($recensioniList as $recensione)
                            <!-- review 1 -->
                            <div class="row mb-4 pl-0 pr-0 ml-0 mr-0 pb-1 border-bottom">

                                <div class="col-md-1 text-center min-w-120">

                                    <!-- avatar -->
                                @if($recensione->getUtente()->getAvatar()->getDato()!= null)
                                    <!-- avatar -->
                                        <span class="w--70 h--70 rounded-circle d-inline-block bg-cover"
                                              style="background-image:url('{{asset( $recensione->getUtente()->getAvatar()->getDato())}}')"></span>
                                    @else
                                        <span class="w--70 h--70 rounded-circle d-inline-block bg-cover">
                                    <i class="fi fi-user-male fs--40"></i>
                                </span>
                                    @endif
                                    <div class="mt-2">
                                        <span class="text-primary">{{$recensione->getUtente()->getNome()}} {{$recensione->getUtente()->getCognome()}}</span>
                                        <p class="fs--14 font-weight-bold mb-1">
                                            @if($recensione->getUtente()->getTipologiaUtenza() == 1)
                                                {{$recensione->getUtente()->getNomeAzienda()}}
                                            @else
                                                User normale
                                            @endif
                                        </p>
                                        <p class="fs--12 text-muted ">
                                            {{ date_format(date_create($recensione->getDataRecensione()), "d/m/Y H:i") }}

                                        </p>
                                    </div>

                                </div>

                                <div class="col">

                                    <div class="mb-2">

                                        <!-- dropdown options -->
                                        <div class="position-absolute top-0 end-0 dropdown">

                                            <!-- dropdown -->
                                            <button class="btn btn-sm rounded-circle-xs btn-primary btn-pill dropdown-toggle"
                                                    type="button" id="drop_actions_review_1" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                        <span class="group-icon">
                                                            <i class="fi fi-menu-dots"></i>
                                                            <i class="fi fi-arrow-down"></i>
                                                        </span>
                                                <span>Azioni</span>
                                            </button>
                                            <div aria-labelledby="drop_actions_review_1"
                                                 class="dropdown-menu dropdown-menu-right shadow-3d  p-0 mt-0  fs--15 w--190">
                                                <div class="ml--0 mt--10 bg-white rounded">
                                                    <h6 class="dropdown-header">Azioni</h6>

                                                    <div class="dropdown-divider"></div>

                                                    <div class="scrollable-vertical max-h-50vh">

                                                        <a data-href="{{asset('api/admin/review_list/' . $recensione->getIdRecensione())}}"
                                                           class="js-ajax-confirm dropdown-item text-dark pl--15" href="#"

                                                           data-ajax-confirm-mode="ajax"
                                                           data-ajax-confirm-method="DELETE"

                                                           data-ajax-confirm-size="modal-md"
                                                           data-ajax-confirm-centered="false"

                                                           data-ajax-confirm-title="Per favore conferma"
                                                           data-ajax-confirm-body="Sei sicuro di voler Eliminare la recensione?"

                                                           data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                           data-ajax-confirm-btn-yes-text="Conferma"
                                                           data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                           data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                           data-ajax-confirm-btn-no-text="Annulla"
                                                           data-ajax-confirm-btn-no-icon="fi fi-close"
                                                           data-ajax-confirm-callback-function="myFunction">

                                                            <i class="fi fi-thrash text-primary"></i>
                                                            Elimina
                                                        </a>
                                                        @if($recensione->getUtente()->getStato() == 1)
                                                            <a  data-href="{{asset('api/admin/user_list/' . $recensione->getUtente()->getIdUser())}}"
                                                                class="js-ajax-confirm dropdown-item text-dark pl--15" href="#"

                                                                data-ajax-confirm-mode="ajax"
                                                                data-ajax-confirm-method="PUT"

                                                                data-ajax-confirm-size="modal-md"
                                                                data-ajax-confirm-centered="false"


                                                                data-ajax-confirm-title="Per favore conferma"
                                                                data-ajax-confirm-body="Sei sicuro di voler Bloccare l'utente di tipo
                                                                        @if($recensione->getUtente()->getTipologiaUtenza() == 0)
                                                                        User normale
                                                                        @elseif($recensione->getUtente()->getTipologiaUtenza() == 1)
                                                                        Critico
                                                                        @elseif($recensione->getUtente()->getTipologiaUtenza() == 2)
                                                                        Amministratore
                                                                        @endif
                                                                        <strong>{{ $recensione->getUtente()->getNome() }} {{ $recensione->getUtente()->getCognome() }}</strong>?"

                                                                data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                                data-ajax-confirm-btn-yes-text="Conferma"
                                                                data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                                data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                                data-ajax-confirm-btn-no-text="Annulla"
                                                                data-ajax-confirm-btn-no-icon="fi fi-close"

                                                                data-ajax-confirm-callback-function="myFunction">
                                                                <i class="fi fi-round-lightning text-primary"></i>
                                                                Blocca Utente
                                                            </a>
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- /dropdown -->

                                        </div>
                                        <!-- /dropdown options -->

                                        <h5>{{ $recensione->getTitolo() }} @if($recensione->getUtente()->getStato() == 0) <i class="fi fi-round-lightning text-primary pl-3"></i> <span class="text-primary"> Utente Bloccato</span> @endif </h5>
                                        <i class="rating-{{voto_stelle($recensione->getVoto())}} text-warning"></i>
                                    </div>

                                    <p>
                                        {{ $recensione->getContenutoRecensione() }}
                                    </p>

                                </div>

                            </div>
                            <!-- /review 1 -->
                        @endforeach
                    @else
                        <div class="row d-flex justify-content-center">
                            <h5>Non ci sono ancora recensioni</h5>
                        </div>
                    @endif
                    <!-- pagination : center -->
                    @if(!empty($recensioniList))
                        @if(isset($cerca))
                            <nav aria-label="pagination">
                                <ul class="pagination mt-5 pagination-pill justify-content-center">
                                    @if($currentPage == 1)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('admin/review_list/'. $entita . '/' . $id . '/cerca/10/' . ($currentPage-1)) . '?nome=' . $cerca }}">Precedente</a>
                                        </li>
                                    @endif

                                    @foreach($pagine as $pagina)
                                        <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                            @if($pagina === '...')
                                                <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                            @else
                                                <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                            @endif
                                        </li>
                                    @endforeach

                                    @if($currentPage == $numeroPagine)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('admin/review_list/'. $entita . '/' . $id . '/cerca/10/' . ($currentPage+1)) . '?nome=' . $cerca }}">Successiva</a>
                                        </li>
                                    @endif

                                </ul>
                            </nav>
                        @else
                            <nav aria-label="pagination">
                                <ul class="pagination mt-5 pagination-pill justify-content-center">
                                    @if($currentPage == 1)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true"  >Precedente</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('admin/review_list/'. $entita . '/' . $id . '/10/' . ($currentPage-1)) }}">Precedente</a>
                                        </li>
                                    @endif

                                    @foreach($pagine as $pagina)
                                        <li class="page-item @if($currentPage == $pagina) active @endif " @if($currentPage == $pagina) aria-current="page" @endif >
                                            @if($pagina === '...')
                                                <span class="mb-2 fs--25 pl-3 pr-3 ">{{ $pagina }}</span>
                                            @else
                                                <a class="page-link" @if($currentPage != $pagina) href="{{ asset('lista_ultime_recensioni/' . $pagina) }}" @endif >{{ $pagina }}<span class="sr-only">(Corrente)</span></a>
                                            @endif
                                        </li>
                                    @endforeach

                                    @if($currentPage == $numeroPagine)
                                        <li class="page-item disabled">
                                            <a class="page-link" tabindex="-1" aria-disabled="true">Successiva</a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link" href="{{ asset('admin/review_list/'. $entita . '/' . $id . '/10/' . ($currentPage+1)) }}">Successiva</a>
                                        </li>
                                    @endif

                                </ul>
                            </nav>
                        @endif
                    @endif
                    <!-- /pagination : center -->

                </section>
            </div>

        </div>


    </div>
    @if(session()->flash()->has('succesMessage'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif

    @if(session()->flash()->has('errorMessage'))
        <div class="hide toast-on-load"
             data-toast-type="danger"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif
    <!-- /MIDDLE -->

@endsection

@section('script')
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection