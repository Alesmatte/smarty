@extends('skeletons.back.app')

@section('content')
<!-- MIDDLE -->
<div id="middle" class="flex-fill">

    <!-- PAGE TITLE -->

    <div class="page-title bg-transparent b-0">

        <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
            Gestione Gruppi e Servizi
        </h1>

    </div>

    <div class="row d-flex flex-fill align-items-start">

        <div class="col align-self-start">

            <section id="section_1">
                <ul class="nav nav-tabs bg-light d-flex" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link @if(!session()->flash()->has('modMatrice')) active @endif pb-3 pt-3" id="tab-roles-tab" data-toggle="tab" href="#tab-roles" role="tab" aria-controls="tab-roles" aria-selected="true">
                            Lista Gruppi
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link pb-3 pt-3" id="tab-permissions-tab" data-toggle="tab" href="#tab-permissions" role="tab" aria-controls="tab-permissions" aria-selected="false">
                            Lista Servizi
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if(session()->flash()->has('modMatrice')) active @endif pb-3 pt-3" id="tab-gruppiservizi-tab" data-toggle="tab" href="#tab-gruppiservizi" role="tab" aria-controls="tab-gruppiservizi" aria-selected="false">
                            Matrice Gruppi/Servizi
                        </a>
                    </li>

                    <li class="ml-4 nav-item ml-auto">
                        <a href="{{asset('admin/create_group')}}" class="btn btn-sm btn-primary mt-2 mr-4">
                            <span>Nuovo Gruppo</span>
                            <i class="fi fi-plus"></i>
                        </a>
                    </li>


                </ul>

                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade @if(!session()->flash()->has('modMatrice')) show active @endif" id="tab-roles" role="tabpanel" aria-labelledby="tab-roles-tab">

                        <div class="mt--20 mb--60">
                            <div class="table-responsive">

                                <table class="table table-lg table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Descrizione</th>
                                        <th class="text-center" scope="col">Servizi collegati</th>
                                        <th class="text-center" scope="col">Modifica Gruppo</th>
                                        <th class="text-center" scope="col">Elimina Gruppo</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(!empty($groupsList))
                                        @foreach($groupsList as $Key=>$group)
                                        <tr>
                                            <th class="pt--10 pb--10" scope="row">{{ $Key+1 }}</th>
                                            <td class="pt--10 pb--10">{{ $group->getNome() }}</td>
                                            <td class="pt--10 pb--10">{{ $group->getDescrizione() }}</td>
                                            <td class="text-center pt--10 pb--10">
                                                <div class="d-flex align-items-center justify-content-center">
                                                    <a href="#" class="mb-1 text-decoration-none" data-toggle="tooltip" data-placement="left" data-html="true" title="<div class='scrollable-vertical max-h-200 pl--15 pr--10 pt--10 pb--10 text-align-start'><h6 class='p-0 fs--15 text-warning mb-2'>Servizi</h6><ul class='p-0'>@if(!empty($group->getServizi())) @foreach($group->getServizi() as $servizio)<li>{{ $servizio->getNome() }}</li>@endforeach @endif</ul></div>">
                                                        <span><i class="fi fi-round-info-full fs--20 pr-1 mb--1"></i></span>
                                                        <span class="hidden-sm-down">Servizi</span>
                                                    </a>
                                                </div>
                                            </td>
                                            <td class="text-center pt--10 pb--10">
                                                <a class="btn btn-sm rounded-circle-xs btn-indigo btn-pill pl--20 pr--20 mb--0 mt--0 @if($group->getIdGruppo() == 1 || $group->getIdGruppo() == 2 || $group->getIdGruppo() == 3) disabled @endif"
                                                   @if($group->getIdGruppo() != 1 && $group->getIdGruppo() != 2 && $group->getIdGruppo() != 3)
                                                        href="{{asset('admin/modify_group/' . $group->getIdGruppo())}}"
                                                   @else
                                                   data-toggle="tooltip" data-placement="left" title="Questo gruppo non può essere modificato"
                                                   @endif
                                                >
                                                    <i class="fi fi-pencil"></i>
                                                    <span class="hidden-sm-down">Modifica</span>
                                                </a>
                                            </td>
                                            <td class="text-center pt--10 pb--10">
                                                <button
                                                        @if($group->getIdGruppo() != 1 && $group->getIdGruppo() != 2 && $group->getIdGruppo() != 3)
                                                        data-href="{{asset('api/admin/groups_services/' . $group->getIdGruppo())}}"
                                                        @else
                                                        data-toggle="tooltip" data-placement="left" title="Questo gruppo non può essere eliminato"
                                                        @endif
                                                        type="button"
                                                        @if($group->getIdGruppo() == 1 || $group->getIdGruppo() == 2 || $group->getIdGruppo() == 3) disabled @endif
                                                        class="js-ajax-confirm btn btn-sm rounded-circle-xs btn-secondary text-white btn-pill pl--20 pr--20 mb--0 mt--0"

                                                        data-ajax-confirm-mode="ajax"
                                                        data-ajax-confirm-method="DELETE"

                                                        data-ajax-confirm-size="modal-md"
                                                        data-ajax-confirm-centered="false"

                                                        data-ajax-confirm-title="Per favore conferma"
                                                        data-ajax-confirm-body="Sei sicuro di voler eliminare il gruppo <strong>{{ $group->getNome() }}</strong> dal sistema?"

                                                        data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                                        data-ajax-confirm-btn-yes-text="Conferma"
                                                        data-ajax-confirm-btn-yes-icon="fi fi-check"

                                                        data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                                        data-ajax-confirm-btn-no-text="Annulla"
                                                        data-ajax-confirm-btn-no-icon="fi fi-close"

                                                        data-ajax-confirm-callback-function="myFunction">
                                                    <i class="fi fi-thrash"></i>
                                                    <span class="hidden-sm-down">Elimina</span>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-end pt-4 pr-4">
                                    <small>
                                        <strong>* </strong>I Gruppi <strong> User base</strong>, <strong>Critico</strong> e <strong>Amministratore</strong> sono fondamentali e non possono essere modificati o eliminati.
                                    </small>
                                </div>
                                <div class="d-flex justify-content-end pr-4">
                                    <small>
                                        <strong>* </strong> Passa il mouse sopra <strong> Servizi</strong> per avere informazioni sui Servizi collegati al gruppo.
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab-permissions" role="tabpanel" aria-labelledby="tab-permissions-tab">

                        <div class="mt--20 mb--60">
                            <div class="table-responsive">

                                <table class="table table-lg table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Descrizione</th>
                                        <th class="text-center" scope="col">Gruppi collegati</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(!empty($servicesList))
                                        @foreach($servicesList as $key=>$service)
                                        <tr>
                                            <th class="pt--10 pb--10" scope="row">{{ $key+1 }}</th>
                                            <td class="pt--10 pb--10">{{ $service->getNome() }}</td>
                                            <td class="pt--10 pb--10">{{ $service->getDescrizione() }}</td>
                                            <td class="text-center pt--10 pb--10">
                                                <div class="d-flex align-items-center justify-content-center">
                                                    <a href="#" class="mb-1 text-decoration-none" data-toggle="tooltip" data-placement="left" data-html="true" title="<div class='scrollable-vertical max-h-200 pl--15 pr--10 pt--10 pb--10 text-align-start'> <h6 class='p-0 fs--15 text-warning mb-2'>Gruppi</h6> <ul class='p-0'> @if(!empty($matriceServicesGroups)) @foreach($matriceServicesGroups[$service->getIdServizio()] as $gruppo) <li>{{ $gruppo->getNome() }}</li> @endforeach @endif </ul></div>">
                                                        <span><i class="fi fi-round-info-full fs--20 pr-1 mb--1"></i></span>
                                                        <span class="hidden-sm-down">Gruppi</span>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-end pt-4 pr-4">
                                    <small>
                                        <strong>* </strong> Passa il mouse sopra <strong> Gruppi</strong> per avere informazioni sui Gruppi che hanno quel servizio.
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade @if(session()->flash()->has('modMatrice')) show active @endif" id="tab-gruppiservizi" role="tabpanel" aria-labelledby="tab-gruppiservizi-tab">

                        <div class="mt--40 mb--20">
                            <div class="table-responsive">
                                <form action="{{asset('admin/groups_services')}}" method="POST" enctype="multipart/form-data">

                                    <table class="table table-lg table-bordered">
                                        <thead>
                                        <tr class="text-center">
                                            <th class="w--350"><strong class="fs--20">Servizi \ Gruppi</strong></th>
                                            @foreach($groupsList as $gruppo)
                                                <th scope="col">{{ $gruppo->getNome() }}</th>
                                            @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($servicesList as $servizio)
                                            <tr>
                                                <th class="pt--10 pb--10 w--350" scope="row">{{ $servizio->getNome() }}</th>
                                                @foreach($groupsList as $gruppo)
                                                    <td class="pt--10 pb--10 text-center">
                                                        @csrf
                                                        <label class="form-checkbox form-checkbox-primary">
                                                            <input @if($gruppo->getIdGruppo() == 1 || $gruppo->getIdGruppo() == 2 || $gruppo->getIdGruppo() == 3 ) disabled @endif  type="checkbox" name="{{$gruppo->getIdGruppo()}}-{{$servizio->getIdServizio()}}" value="{{$gruppo->getIdGruppo()}}-{{$servizio->getIdServizio()}}" @if(!empty($gruppo->getServizi())) @foreach($gruppo->getServizi() as $serv) @if($servizio->getIdServizio() == $serv->getIdServizio() ) checked @endif @endforeach @endif>
                                                            <i class="text-center"></i>
                                                        </label>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                    <div class="d-flex justify-content-end pt-1 pr-4">
                                        <small>
                                            <strong>* </strong>Le caselle dei Gruppi <strong> User base</strong>, <strong>Critico</strong> e <strong>Amministratore</strong> sono disabilitate perchè i rispettivi gruppi sono fondamentali e non possono essere modificati.
                                        </small>
                                    </div>

                                    <div class="d-flex justify-content-end pt-3 pr-4">
                                        <button type="submit" class="ml-0 btn btn-sm btn-success float-end">
                                            <i class="fi fi-check"></i>
                                            Salva
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- /MIDDLE -->

@if(session()->flash()->has('succesMessage'))
    <div class="hide toast-on-load"
         data-toast-type="success"
         data-toast-title="Informazione"
         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
         data-toast-pos="top-center"
         {{--     data-toast-delay="4000"--}}
         data-toast-fill="true"
         data-
    ></div>
@endif

@if(session()->flash()->has('errorMessage'))
    <div class="hide toast-on-load"
         data-toast-type="danger"
         data-toast-title="Informazione"
         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
         data-toast-pos="top-center"
         {{--     data-toast-delay="4000"--}}
         data-toast-fill="true"
         data-
    ></div>
@endif

@if(session()->flash()->has('modMatrice'))
    <div class="hide toast-on-load"
         data-toast-type="success"
         data-toast-title="Informazione"
         data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('modMatrice'))[0] }}</span></div></div>"
         data-toast-pos="top-center"
         {{--     data-toast-delay="4000"--}}
         data-toast-fill="true"
         data-
    ></div>
@endif
@endsection

@section('script')
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection