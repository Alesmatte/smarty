@extends('skeletons.back.app')

@section('content')
    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">
        <!--
            PAGE TITLE
        -->
        <div class="page-title bg-transparent b-0">
            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Gestione Pubblicità
            </h1>
        </div>
        <!-- Primary -->
            <section class="rounded">

                <!-- section header -->
                <div class="bg-light rounded clearfix p-3 mb-2">
                    <div class="d-flex justify-content-between">
                        <div>
                            Slider Pubblicitari
                        </div>
                        <a class="pt--5">
                            <span class="group-icon">
                            </span>
                        </a>
                    </div>
                </div>
                <!-- /section header -->
                <section class="rounded shadow-3d">
                    <form action="{{ asset('admin/impostazioni') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- section header -->
                    {{--<a class=" text-decoration-none text-gray-900" data-toggle="collapse" href="#collapse3"
                       role="button" aria-expanded="false" aria-controls="collapse3">
                        <div class="bg-light rounded clearfix p-3 mb-4">
                            <div class="d-flex justify-content-between">
                                <div>
                                    MEDIA
                                    <small class="fs--11 text-muted d-block mt-1">Foto, Poster & Trailer</small>
                                </div>

                                <span class="group-icon pt--5">
                                    <i class="fi fi-arrow-end-slim"></i>
                                    <i class="fi fi-arrow-down-slim"></i>
                                </span>

                            </div>
                        </div>
                    </a>--}}
{{--                    <div class="collapse mb-3" id="collapse3">--}}
                        <!-- /section header -->
                        <!--default data-table-clone-method="append|prepend" -->
                        <div class="js-form-advanced-table mb-6"
                             data-table-column-insert-before=".js-clone-before"
                             data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">'
                             data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>'
                             data-table-column-limit="4"
                             data-table-row-limit="50"
                             data-table-row-method="prepend">

                            <table class="table table-bordered table-align-middle table-sm">
                                <thead>
                                <tr>
                                    <th class="w--30">&nbsp;</th>
                                    <th>SCHEDA</th>
                                    <th class="w--90">MEDIA</th>
                                    <th>DESCRIZIONE</th>
                                    <th class="w--80 text-center">
                                        <a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
                                            <i class="fi fi-plus"></i>
                                        </a>
                                    </th>
                                </tr>
                                </thead>

                                <!--
                                    See Sortable doumentation if ajax reorder is needed
                                -->
                                <tbody class="sortable">

                                <!--
                                    OPTIONAL
                                    ACTING AS A TEMPLATE TO CLONE
                                    IS REMOVED ON LOAD IF .hide CLASS IS PRESENT

                                    Else, the first TR is used by default!
                                    .js-ignore = optional, used by sortable to igore from drag/drop reorder
                                -->
                                <tr class="js-ignore hide">
                                    <!-- sortable handler -->
                                    <td class="px-0 text-center">
                                        <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                    </td>
                                    <!-- Scheda -->
                                    <td>
                                        <input type="hidden" name="immagini[Id_immagini][]" value="">
                                        <div class="form-label-group">
                                            <select class="form-control bs-select" name="immagini[scheda][]">
                                                <option value="4" selected="selected">Slider Lista Film</option>
                                                <option value="8">Slider Lista Serie Tv</option>
                                                <option value="9" >Slider Premi</option>
                                            </select>
                                            <label>Visualizzabile in</label>
                                        </div>
                                    </td>

                                    <!-- file -->
                                    <td>
                                        <!-- AJAX IMAGE UPLOAD -->
                                        <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">

                                            <!-- remove button -->
                                            <a href="#!"
                                               class="js-table-file-remove-1 js-file-item-del position-absolute absolute-top start-0 z-index-3 btn btn-sm btn-secondary p-0 w--20 h--20 m--1 line-height-1 text-center hide">
                                                <i class="fi fi-close m-0"></i>
                                            </a>

                                            <!-- image container -->
                                            <span class="js-table-file-preview-1 z-index-2 d-block absolute-full z-index-1 hide-empty"></span>

                                            <!-- hidden file input -->
                                            <input name="immagini[file][]"
                                                   type="file"
                                                   data-file-ext="jpg, png, gif, mp4"
                                                   data-file-max-size-kb-per-file="50000"
                                                   data-file-ext-err-msg="Allowed:"
                                                   data-file-size-err-item-msg="File too large!"
                                                   data-file-size-err-total-msg="Total allowed size exceeded!"
                                                   data-file-toast-position="bottom-center"
                                                   data-file-preview-container=".js-table-file-preview-1"
                                                   data-file-preview-show-info="false"
                                                   data-file-preview-class="m-0 p-0 rounded"
                                                   data-file-preview-img-height="auto"
                                                   data-file-btn-clear=".js-table-file-remove-1"
                                                   data-file-preview-img-cover="true"

                                                   class="custom-file-input absolute-full">

                                            <!-- icon -->
                                            <span class="absolute-full d-middle">
                                                <i class="fi fi-image fs--30 text-muted"></i>
                                            </span>
                                        </label>

                                    </td>
                                    <!-- Descrizione -->
                                    <td>
                                        <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]"></textarea>
                                        </div>
                                    </td>
                                    <!-- Option -->
                                    <td class="position-relative text-center">

                                        <!-- direct delete -->
                                        <!--
                                        <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>
                                        -->

                                        <!-- remove button (confirm trigger) -->
                                        <a href="#"
                                           class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                            <i class="fi fi-thrash"></i>
                                        </a>

                                        <!-- confirm -->
                                        <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                            <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                            <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                            <a href="#!"
                                               class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                        </div>

                                    </td>

                                </tr>

                                @if(!empty($sliderOriginalList))
                                    @foreach($sliderOriginalList as $k => $immagine)
                                    <!-- preadded -->
                                    <tr>
                                        <!-- sortable handler -->
                                        <td class="px-0 text-center">
                                            <div class="sortable-handle line-height-1 py-2 fi fi-dots-vertical"></div>
                                        </td>
                                        <!-- scheda -->
                                        <td>
                                            <input type="hidden" name="immagini[Id_immagini][]"
                                                   value="{{$immagine->getIdImmagione()}}">
                                            <div class="form-label-group">
                                                <select class="form-control bs-select" name="immagini[scheda][]">
                                                    <option value="4" @if($immagine->getTipo() == 4) selected="selected" @endif>Slider Lista Film</option>
                                                    <option value="8" @if($immagine->getTipo() == 8) selected="selected" @endif>Slider Lista Serie Tv</option>
                                                    <option value="9" @if($immagine->getTipo() == 9) selected="selected" @endif>Slider Premi</option>
                                                    </option>
                                                </select>
                                                <label>Visualizzabile in</label>
                                            </div>
                                        </td>
                                        <!-- Image -->
                                        <td>
                                            <!-- AJAX IMAGE UPLOAD -->
                                            <label class="rounded text-center position-relative d-block cursor-pointer border border-secondary border-dashed mb-0 h--50">
                                                <!-- image container -->
                                                <span class="js-table-file-preview-{{$immagine->getIdImmagione()}} z-index-2 d-block absolute-full z-index-1 hide-empty">
                                                    <span data-id="{{$immagine->getIdImmagione()}}"
                                                          data-file-name="{{$immagine->getNome()}}"
                                                          style="background-image:url({{asset($immagine->getDato())}})"
                                                          class="js-file-input-item d-inline-block position-relative overflow-hidden text-center m-0 p-0 animate-bouncein bg-cover w-100 h-100">
                                                    </span>
                                                </span>

                                                <!-- hidden file input -->
                                                <input name="immagini[file][]"
                                                       type="file"

                                                       data-file-ext="jpg, png, gif"
                                                       data-file-max-size-kb-per-file="50000"
                                                       data-file-ext-err-msg="Allowed:"
                                                       data-file-size-err-item-msg="File too large!"
                                                       data-file-size-err-total-msg="Total allowed size exceeded!"
                                                       data-file-toast-position="bottom-center"
                                                       data-file-preview-container=".js-table-file-preview-{{$immagine->getIdImmagione()}}"
                                                       data-file-preview-show-info="false"
                                                       data-file-preview-class="shadow-md mb-2 rounded"
                                                       data-file-preview-img-height="auto"

                                                       data-file-preview-img-cover="true"
                                                       class="custom-file-input absolute-full">

                                                <!-- icon -->
                                                <span class="absolute-full d-middle">
                                                    <i class="fi fi-image fs--30 text-muted"></i>
                                                </span>

                                            </label>

                                        </td>
                                        <!-- Descrizione -->
                                        <td>
                                            <div class="form-label-group">
                                        <textarea placeholder="Descrizione" class="form-control" rows="2"
                                                  name="immagini[descrizione][]">{{$immagine->getDescrizione()}}</textarea>
                                            </div>
                                        </td>

                                        <!-- Option -->
                                        <td class="position-relative text-center">

                                            <!-- direct delete -->
                                            <!--
                                            <a href="#" class="btn btn-sm btn-light btn-table-clone-remove rounded-circle">
                                                <i class="fi fi-thrash"></i>
                                            </a>
                                            -->

                                            <!-- remove button (confirm trigger) -->
                                            <a href="#"
                                               class="btn btn-table-clone-remove-confirm btn-sm btn-light rounded-circle">
                                                <i class="fi fi-thrash"></i>
                                            </a>

                                            <!-- confirm -->
                                            <div class="position-absolute bg-warning shadow top-0 bottom-0 end-0 w--200 z-index-10 hide">
                                                <i class="arrow arrow-lg arrow-start arrow-center border-warning"></i>
                                                <label class="d-block fs--13 mb--2">Sei sicuro?</label>
                                                <a href="#!"
                                                   class="btn btn-table-clone-remove btn-danger btn-sm px-2 pt--2 pb--2">Cancella</a>
                                                <a href="#!"
                                                   class="btn btn-table-clone-remove-cancel btn-secondary btn-sm px-2 pt--2 pb--2">Annulla</a>
                                            </div>

                                        </td>

                                    </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                            <small class="d-block text-muted mt-1 mb-3">Se vuoi puoi trascinare e rilasciare le righe per
                                riordinarle</small>
                            <a type="button" class="btn btn-secondary float-start" href="{{asset('admin/impostazioni')}}">
                                <i class="fi fi-close"></i>
                                Annulla
                            </a>
                            <button type="submit" class="ml-0 btn btn-success float-end">
                                <i class="fi fi-check"></i>
                                Salva
                            </button>

                        </div>
{{--                    </div>--}}

                    </form>
                </section>
            </section>
    </div>
    <!-- /MIDDLE -->
@endsection