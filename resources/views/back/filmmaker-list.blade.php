@extends('skeletons.back.app')

@section('content')
    @if(session()->flash()->has('successCreationFilmmaker'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('successCreationFilmmaker'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif
    @if(session()->flash()->has('successModifyFilmmaker'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('successModifyFilmmaker'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             {{--     data-toast-delay="4000"--}}
             data-toast-fill="true"
             data-
        ></div>
    @endif

    <!-- MIDDLE -->
    <div id="middle" class="flex-fill">

        <!-- PAGE TITLE -->

        <div class="page-title bg-transparent b-0">

            <h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
                Gestione Filmmaker
            </h1>

        </div>

        <div class="row d-flex flex-fill align-items-start">

            <div class="col align-self-start">

                <section id="rounded mb-3">

                    <div class="bg-light rounded pt-3 pl-3 pr-3 fs--20 d-flex justify-content-between border-bottom">
                        <div class="p-2 max-h-60">
                            Lista Filmmaker
                        </div>
                        <div class="p-2">
                            <a type="button" class="btn btn-sm btn-primary mb-3"
                               href="{{asset('admin/create_filmmaker')}}">
                                <i class="fi fi-plus"></i>
                                Aggiungi Filmmaker
                            </a>
                        </div>
                    </div>

                    <div class="mt--30 mb--60">

                        <table class="table-datatable table-responsive-lg table table-bordered table-hover table-striped"
                               data-lng-empty="Nessun dato disponibile"
                               data-lng-page-info="Mostrati _START_ a _END_ di _TOTAL_ elementi"
                               data-lng-filtered="(filtered from _MAX_ total entries)"
                               data-lng-loading="Caricamento..."
                               data-lng-processing="Inizializzazione..."
                               data-lng-search="Cerca..."
                               data-lng-norecords="Nessun risultato per la ricerca"
                               data-lng-sort-ascending=": activate to sort column ascending"
                               data-lng-sort-descending=": activate to sort column descending"

                               data-lng-column-visibility="Mostra/nascondi campi"
                               data-lng-all="All"

                               data-main-search="true"
                               data-column-search="false"
                               data-row-reorder="false"
                               data-col-reorder="true"
                               data-responsive="true"
                               data-header-fixed="true"
                               data-select-onclick="false"
                               data-enable-paging="true"
                               data-enable-col-sorting="false"
                               data-autofill="false"
                               data-group="false"

                               data-lng-export="<i class='fi fi-squared-dots fs--18 line-height-1'></i>"
                               data-export-pdf-disable-mobile="true">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Tipologia principale</th>
                                <th class=" text-center">Modifica / Visualizza</th>
                                <th class="text-center">Elimina</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($filmmakerList as $key=>$filmmaker)
                                <tr>
                                    <td>{{ $filmmaker->getNome() }}</td>
                                    <td>{{ $filmmaker->getCognome() }}</td>
                                    <td>{{ decodifica_array_filmmakers_to_string($filmmaker->getTipologiaPrincipale()) }}</td>
                                    <td class=" text-center">
                                        <a class="btn btn-sm rounded-circle-xs btn-indigo btn-pill text-white"
                                           href="{{asset('admin/modify__filmmaker/' . $filmmaker->getIdFilmMaker())}}">
                                            Visualizza / Modifica
                                        </a>
                                    </td>
                                    <td class=" text-center">
                                        <a data-href="{{asset('api/admin/filmmaker/' . $filmmaker->getIdFilmMaker())}}?action=delete"
                                           class="js-ajax-confirm btn btn-sm rounded-circle-xs btn-primary btn-pill text-white"
                                           href="#"

                                           data-ajax-confirm-mode="ajax"
                                           data-ajax-confirm-method="DELETE"

                                           data-ajax-confirm-size="modal-md"
                                           data-ajax-confirm-centered="false"
                                           data-ajax-confirm-callback-function="myFunction"
                                           data-ajax-confirm-title="Per favore conferma"
                                           data-ajax-confirm-body="Sei sicuro di voler Eliminare il Filmmaker <strong>{{ $filmmaker->getNome() }}</strong>?"

                                           data-ajax-confirm-btn-yes-class="btn-sm btn-danger"
                                           data-ajax-confirm-btn-yes-text="Conferma"
                                           data-ajax-confirm-btn-yes-icon="fi fi-check"

                                           data-ajax-confirm-btn-no-class="btn-sm btn-light"
                                           data-ajax-confirm-btn-no-text="Annulla"
                                           data-ajax-confirm-btn-no-icon="fi fi-close">

                                            <i class="fi fi-thrash text-white pr--10"></i>
                                            Elimina
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Tipologia principale</th>
                                <th class=" text-center">Modifica / Visualizza</th>
                                <th class="text-center">Elimina</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </section>
            </div>
        </div>
    </div>
    <!-- /MIDDLE -->

    @if(session()->flash()->has('succesMessage'))
        <div class="hide toast-on-load"
             data-toast-type="success"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('succesMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             data-toast-delay="4000"
             data-toast-fill="true"
             data-
        ></div>
    @endif

    @if(session()->flash()->has('errorMessage'))
        <div class="hide toast-on-load"
             data-toast-type="danger"
             data-toast-title="Informazione"
             data-toast-body="<div class='row'><div class='col-2 d-flex align-items-center'><i class='fi fi-like float-start fs--40 pl-1'></i></div><div class='col-10 d-flex align-items-center'><span class='fs--20 mb-0 text-center'>{{ array_values(session()->flash()->get('errorMessage'))[0] }}</span></div></div>"
             data-toast-pos="top-center"
             data-toast-delay="4000"
             data-toast-fill="true"
             data-
        ></div>
    @endif

@endsection

@section('script')
    <script>
        function myFunction() {
            window.location.reload();
        }
    </script>
@endsection