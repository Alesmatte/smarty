
@section('menu')
<!-- Menu -->

<div class="collapse navbar-collapse navbar-animate-fadein" id="navbarMainNav">


    <!-- MOBILE MENU NAVBAR -->
    <div class="navbar-xs d-none bg-primary b-b">

        <!-- mobile menu button : close -->
        <button class="navbar-toggler pt-0" type="button" data-toggle="collapse"
                data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false"
                aria-label="Toggle navigation">
            <svg width="20" viewBox="0 0 20 20">
                <path d="M 20.7895 0.977 L 19.3752 -0.4364 L 10.081 8.8522 L 0.7869 -0.4364 L -0.6274 0.977 L 8.6668 10.2656 L -0.6274 19.5542 L 0.7869 20.9676 L 10.081 11.679 L 19.3752 20.9676 L 20.7895 19.5542 L 11.4953 10.2656 L 20.7895 0.977 Z"></path>
            </svg>
        </button>

        <!--
            Mobile Menu Logo
            Logo : height: 70px max
        -->
        <a class="navbar-brand" href="/">
            <img src="{{asset('assets/images/logo/redcarpet.png')}}" alt="...">
        </a>
    </div>
    <!-- /MOBILE MENU NAVBAR -->


    <!-- NAVIGATION -->
    <ul class="navbar-nav">

        <!-- mobile only image + simple search (d-block d-sm-none) -->
        <li class="nav-item d-block d-sm-none">

            <!-- search -->
            <form method="get" action="#"
                  class="input-group-over mb-5 bg-light p-2 form-control-pill">
                <input type="text" name="keyword" value="" placeholder="Ricerca veloce"
                       class="form-control border-dashed">
                <button class="btn btn-primary btn-ghost fi fi-search p-0 ml-2 mr-2 w--50 h--50"></button>
            </form>

        </li>


        <!-- home -->
        <li class="nav-item active b-b">
            <a class="nav-link" href="/">
                Home
            </a>
        </li>

        <!-- dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link c-link" href="!#" data-toggle="collapse" data-target="#collapseExample1"
               aria-expanded="false" id="mainNavPages">
                <span> Film </span>
                <span class="group-icon">
			        <i class="fi fi-arrow-down-slim fs--12 pl--5"></i>
		        </span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link c-link" href="!#" data-toggle="collapse" data-target="#collapseExample2"
               aria-expanded="false" id="mainNavPages">
                <span> Serie TV </span>
                <span class="group-icon">
			        <i class="fi fi-arrow-down-slim fs--12 pl--5"></i>
		        </span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link c-link" href="!#" data-toggle="collapse" data-target="#collapseExample3"
               aria-expanded="false" id="mainNavPages">
                <span> Filmmaker </span>
                <span class="group-icon">
			        <i class="fi fi-arrow-down-slim fs--12 pl--5"></i>
		        </span>
            </a>
        </li>

        <!-- Ricerca Avanzata -->
        <li class="nav-item active b-b">
            <a class="nav-link" href="{{asset('ricerca_avanzata')}}">
               <span> Ricerca Avanzata </span>
            </a>
        </li>

    </ul>
    <!-- /NAVIGATION -->
</div>
@endsection
