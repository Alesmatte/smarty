<?php

/* Global Helper Functions */

use App\Model\Services;
use App\Services\GenereService;
use App\Services\RuoloService;
use App\Services\ServicesService;
use App\Services\UserService;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Psr\Http\Message\UploadedFileInterface;

/*
 * menu
 * is_active_link
 * italian_date_str
 * rapporto_a_cento
 * tempo_fa
 * voto_stelle
 * decodifica_array_filmmakers_to_string
 * codifica_array
 * decodifica_array
 * deleteFile
 * movefiles
 * old
 * back
 * session
 * validator
 * asset
 * redirect
 * collect
 * factory
 * env
 * base_path
 * config_path
 * resources_path
 * public_path
 * routes_path
 * storage_path
 * app_path
 * dd (die and dump)
 * throw_when
 * class_basename
 * config
 * data_get
 * data_set
 */

if(!function_exists('menu')){
    function menu(): array {
        $genereService = new GenereService();
        $ruoloService = new RuoloService();
        try {
            $genereList = $genereService->getIDGenereNomeGenere();
        } catch (\Exception $exception) {
            $genereList = array();
        }
        try {
            $tipologiaRuoloList = $ruoloService->getTipologiaRuolo();
        } catch (\Exception $exception) {
            $tipologiaRuoloList = array();
        }
        return [$genereList, $tipologiaRuoloList];
    }
}

if(!function_exists('is_active_link')){
    function is_active_link(string $uri, bool $absulutUri = false): bool {
        $requestInpunt =  app()->resolve(\App\Support\RequestInput::class);
        if($absulutUri){
            return $uri == $requestInpunt->getCurrentUri()->getPath();
        }else{
            return str_contains($requestInpunt->getCurrentUri()->getPath(), $uri);
        }
    }
}

if(!function_exists('italian_date_str')){
    function italian_date_str(string $date):string{
        // setto il locale
        setlocale(LC_TIME, 'it_IT.utf8');
        date_default_timezone_set("Europe/Rome");
        $date_real = strtotime($date);
        $str = strftime("%A %d %B %Y",$date_real);
        return $str;
    }
}

if(!function_exists('rapporto_a_cento')){
    function rapporto_a_cento(float $valutazione): int{
        $val = floatval(number_format($valutazione, 2 ));
        return intval(($val*100)/5);
    }
}

if(!function_exists('tempo_fa')){
    function tempo_fa($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'anni',
            'm' => 'mesi',
            'w' => 'settimane',
            'd' => 'gioni',
            'h' => 'ore',
            'i' => 'minuti',
            's' => 'secondi',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v ;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' fa' : ' poco fa';
    }
}

if (!function_exists('voto_stelle')) {
    function voto_stelle(float $voto): string {
       if ($voto < 0.25){
           return '0_0';
       } elseif ($voto >= 0.25 && $voto < 0.75){
           return '0_5';
       } elseif ($voto >= 0.75 && $voto < 1.25){
           return '1_0';
       } elseif ($voto >= 1.25 && $voto < 1.75){
           return '1_5';
       } elseif ($voto >= 1.75 && $voto < 2.25){
           return '2_0';
       } elseif ($voto >= 2.25 && $voto < 2.75){
           return '2_5';
       } elseif ($voto >= 2.75 && $voto < 3.25){
           return '3_0';
       } elseif ($voto >= 3.25 && $voto < 3.75){
           return '3_5';
       } elseif ($voto >= 3.75 && $voto < 4.25){
           return '4_0';
       } elseif ($voto >= 4.25 && $voto < 4.75){
           return '4_5';
       } elseif ($voto >= 4.75){
           return '5_0';
       }
       else return '0_0';
    }
}

if (!function_exists('decodifica_array_filmmakers_to_string')) {
    function decodifica_array_filmmakers_to_string(string $codifica): string
    {
        $array = explode("/", $codifica);
        $decodifica = '';
        $i = 0;
        if (isset($array)) {
            foreach (array_values($array) as $value) {
                switch ($value) {
                    case '0': {
                        $value = "Altro";
                        break;
                    }
                    case '1': {
                        $value = "Attore";
                        break;}
                    case '2': {
                        $value = "Regista";
                        break;
                    }
                    case '3': {
                        $value = "Distributore";
                        break;
                    }
                    case '4': {
                        $value = "Produttore";
                        break;
                    }
                    case '5': {
                        $value = "Sceneggiatore";
                        break;
                    }
                    case '6': {
                        $value = "Fotografia";
                        break;
                    }
                    case '7': {
                        $value = "Montaggio";
                        break;
                    }
                    case '8': {
                        $value = "Musica";
                        break;
                    }
                    case '9': {
                        $value = "Scenografo";
                        break;
                    }
                    case '10': {
                        $value = "Costumista";
                        break;
                    }
                    case '11': {
                        $value = "Effetti";
                        break;
                    }
                    case '12': {
                        $value = "Art Director";
                        break;
                    }
                    case '13': {
                        $value = "Trucco";
                        break;
                    }
                    default:

                        break;
                }
                if ($i < 1) {
                    $decodifica = $value;
                    $i++;
                } else {
                    $decodifica = $decodifica . ' / ' . $value;
                }
            }
        }
        return $decodifica;
    }
}

if (!function_exists('codifica_array')) {
    function codifica_array(array $array): string
    {
        $codifica = '';
        $i = 0;
        if (isset($array)) {
            foreach ($array as $key) {
                if ($i < 1) {
                    $codifica = $key;
                    $i++;
                } else {
                    $codifica = $codifica . '/' . $key;
                }
            }
        }
        return $codifica;
    }
}

if (!function_exists('decodifica_array')) {
    function decodifica_array($codifica): array
    {
        if(is_string($codifica)){
            return explode("/", $codifica);
        } else{
            dd("NON e una strigha", $codifica);
        }

    }
}

if (!function_exists('deleteFile')){
    function deleteFile(string $filePath): bool{
        if (file_exists($filePath)){
            $deleted = unlink($filePath);
            return $deleted;
        } else return true;
    }
}

if (!function_exists('movefiles')) {
    function movefiles(string $directory, UploadedFileInterface $uploadedFile): string
    {
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(24));
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        chmod($directory . DIRECTORY_SEPARATOR . $filename, 0777);

        return $filename;
    }

}

if (!function_exists('auth')) {
    /**
     * ritorna true se l'user è loggato false altrimenti
     * da richiamare principalmente nelle pagine blade e nei middleware
     * @return bool
     */
    function auth(): bool
    {
        $userService = new \App\Services\UserService();
        return $userService->check();
    }
}

if (!function_exists('can')) {
    /**
     * ritorna true se il servizio è collegato all'user false altrimenti
     * da richiamare principalmente nelle pagine blade e nei middleware
     * @param $serviceName
     * @return bool
     */
    function can(string $serviceName): bool {
        if (auth()) {
            $serviceService = new ServicesService();
            $userService = new UserService();
            $user = $userService->getUser();
            if ($serviceService->ifCanByNomeServizioAndUser($serviceName, $user)) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('old')) {
    function old($key)
    {
        $input = app()->resolve('old_input');

        $field = collect($input)->filter(fn($value, $field) => $key == $field);

        if (isset($field[$key])) {
            return $field[$key];
        }
    }
}

if (!function_exists('back')) {
    function back()
    {
        $route = app()->resolve(\App\Support\RequestInput::class);

        $back = $route->getCurrentUri();

        return redirect($back);
    }
}

if (!function_exists('session')) {
    function session($key = false, $value = false)
    {
        $session = app()->resolve(\Boot\Foundation\Http\Session::class);

        if (!$key) {
            return $session;
        }

        if (!$value) {
            return $session->get($key);
        }

        $session->set($key, $value);

        return $session;
    }
}

if (!function_exists('validator')) {
    function validator(array $input, array $rules, array $messages = [])
    {
        $factory = app()->resolve(\Boot\Foundation\Http\ValidatorFactory::class);

        return $factory->make($input, $rules, $messages);
    }
}

if (!function_exists('asset')) {
    function asset($path)
    {
        return env('APP_URL') . "/{$path}";
    }
}
if (!function_exists('redirect')) {
    function redirect(string $to)
    {
        $redirect = app()->resolve(\App\Support\Redirect::class);

        return $redirect($to);
    }
}
if (!function_exists('collect')) {
    function collect($items)
    {
        return new Collection($items);
    }
}

if (!function_exists('env')) {
    function env($key, $default = false)
    {
        $value = getenv($key);

        throw_when(!$value and !$default, "{$key} is not a defined .env variable and has not default value");

        return $value or $default;
    }
}


if (!function_exists('base_path')) {
    function base_path($path = '')
    {
        return __DIR__ . "/../{$path}";
    }
}


if (!function_exists('database_path')) {
    function database_path($path = '')
    {
        return base_path("database/{$path}");
    }
}

if (!function_exists('config_path')) {
    function config_path($path = '')
    {
        return base_path("config/{$path}");
    }
}

if (!function_exists('storage_path')) {
    function storage_path($path = '')
    {
        return base_path("storage/{$path}");
    }
}

if (!function_exists('public_path')) {
    function public_path($path = '')
    {
        return base_path("public/{$path}");
    }
}

if (!function_exists('resources_path')) {
    function resources_path($path = '')
    {
        return base_path("resources/{$path}");
    }
}

if (!function_exists('routes_path')) {
    function routes_path($path = '')
    {
        return base_path("routes/{$path}");
    }
}

if (!function_exists('app_path')) {
    function app_path($path = '')
    {
        return base_path("app/{$path}");
    }
}

if (!function_exists('dd')) {
    function dd()
    {
        array_map(function ($content) {
            echo "<pre>";
            var_dump($content);
            echo "</pre>";
            echo "<hr>";
        }, func_get_args());

        die;
    }
}

if (!function_exists('throw_when')) {
    function throw_when(bool $fails, string $message, string $exception = Exception::class)
    {
        if (!$fails) return;
        $req = app()->resolve(App\Support\RequestInput::class);
        switch ($exception) {
            case 'Slim\Exception\HttpNotImplementedException':
            case 'Slim\Exception\HttpUnauthorizedException':
            case 'Slim\Exception\HttpNotFoundException':
            case 'Slim\Exception\HttpInternalServerErrorException':
            case 'Slim\Exception\HttpForbiddenException':
            case 'Slim\Exception\HttpBadRequestException':
                throw new $exception($req->getRequest(), $message);
                break;
            default:
                throw new $exception($message);
        }

    }
}

if (!function_exists('class_basename')) {
    function class_basename($class)
    {
        $class = is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }
}

if (!function_exists('config')) {
    function config($path = null)
    {
        $config = [];
        $folder = scandir(config_path());
        $config_files = array_slice($folder, 2, count($folder));

        foreach ($config_files as $file) {
            throw_when(
                Str::after($file, '.') !== 'php',
                'Config files must be .php files'
            );


            data_set($config, Str::before($file, '.php'), require config_path($file));
        }

        return data_get($config, $path);
    }
}

if (!function_exists('data_get')) {
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param mixed $target
     * @param string|array|int|null $key
     * @param mixed $default
     * @return mixed
     */
    function data_get($target, $key, $default = null)
    {
        if (is_null($key)) {
            return $target;
        }

        $key = is_array($key) ? $key : explode('.', $key);

        while (!is_null($segment = array_shift($key))) {
            if ($segment === '*') {
                if ($target instanceof Collection) {
                    $target = $target->all();
                } elseif (!is_array($target)) {
                    return value($default);
                }

                $result = [];

                foreach ($target as $item) {
                    $result[] = data_get($item, $key);
                }

                return in_array('*', $key) ? Arr::collapse($result) : $result;
            }

            if (Arr::accessible($target) && Arr::exists($target, $segment)) {
                $target = $target[$segment];
            } elseif (is_object($target) && isset($target->{$segment})) {
                $target = $target->{$segment};
            } else {
                return value($default);
            }
        }

        return $target;
    }
}

if (!function_exists('data_set')) {
    /**
     * Set an item on an array or object using dot notation.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     * @param bool $overwrite
     * @return mixed
     */
    function data_set(&$target, $key, $value, $overwrite = true)
    {
        $segments = is_array($key) ? $key : explode('.', $key);

        if (($segment = array_shift($segments)) === '*') {
            if (!Arr::accessible($target)) {
                $target = [];
            }

            if ($segments) {
                foreach ($target as &$inner) {
                    data_set($inner, $segments, $value, $overwrite);
                }
            } elseif ($overwrite) {
                foreach ($target as &$inner) {
                    $inner = $value;
                }
            }
        } elseif (Arr::accessible($target)) {
            if ($segments) {
                if (!Arr::exists($target, $segment)) {
                    $target[$segment] = [];
                }

                data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite || !Arr::exists($target, $segment)) {
                $target[$segment] = $value;
            }
        } elseif (is_object($target)) {
            if ($segments) {
                if (!isset($target->{$segment})) {
                    $target->{$segment} = [];
                }

                data_set($target->{$segment}, $segments, $value, $overwrite);
            } elseif ($overwrite || !isset($target->{$segment})) {
                $target->{$segment} = $value;
            }
        } else {
            $target = [];

            if ($segments) {
                data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite) {
                $target[$segment] = $value;
            }
        }

        return $target;
    }
}