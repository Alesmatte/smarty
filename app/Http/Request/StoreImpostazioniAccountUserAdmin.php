<?php


namespace App\Http\Request;


use App\Http\Request\Rules\BirthdayRules;
use App\Http\Request\Rules\EmailRules;
use App\Http\Request\Rules\PasswordRules;

class StoreImpostazioniAccountUserAdmin extends \App\Support\FormRequest
{
    protected function afterValidationPasses()
    {
        $this->forget('csrf_value');
        $this->forget('csrf_name');
        $this->forget('account_password_same');
        $this->account_password = sha1($this->account_password);
    }

    public function messages()
    {
        return [
            'nome.required' => 'Il Nome è richiesto',
            'nome.string' => 'Il Nome deve essere un testo',
            'cognome.required' => 'Il Cogmome è richiesto',
            'cognome.string' => 'Il Cogmome deve essere un testo',
            'email.email' => 'L\'email deve essere un email valida',
            'email.required' => 'L\'email è richiesto',
            'birthday.required' => 'La data di nascita è richiesta',
            'birthday.data' => 'La data di nascita deve essere una data',
            'account_password.string' => 'La password deve essere un testo avente: una lettera maiuscola, una minuscola, un numero e un carattere speciale',
            'account_password.required' => 'La password è richiesta',
            'account_password_same.same' => 'Le password non sono uguali',
            'account_password.required_with' => 'riempi entrambi i campi password',
            'account_password_same.required' => 'La password è richiesta',
            'account_password_same.string' => 'La password deve essere un testo avente: una lettera maiuscola, una minuscola, un numero e un carattere speciale'
        ];
    }

    public function rules(){
        // regole di validazione

        return [
            'nome' => 'required|string',
            'cognome' => 'required|string',
            'email' => ['required', 'email', new EmailRules(session('user')['email'])],
            'birthday' => ['required', 'date', new BirthdayRules()],
            'oldPassword' => ['string', new PasswordRules(), 'required_with:account_password, newPassword2'],
            'account_password' => ['string', new PasswordRules(), 'required_with:oldPassword, newPassword2'],
            'newPassword2' => ['string', new PasswordRules(),'required_with:oldPassword, account_password', 'same:account_password']
        ];
    }
}