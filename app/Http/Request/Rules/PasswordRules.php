<?php


namespace App\Http\Request\Rules;


use Illuminate\Contracts\Validation\Rule;

class PasswordRules implements Rule
{

    /**
     * PasswordRules constructor.
     */
    public function __construct()
    {
    }

    public function passes($attribute, $value){
        if($value != ''){
            $pattern = '/^(?=.*[A-Z])(?=.*[\W])(?=.*[0-9])(?=.*[a-z]).{8,128}$/';
            return preg_match($pattern, $value);
        }
        return true;
    }

    public function message()
    {
        return 'La password non rispetta i requisiti: Almeno una lettera maiscula, una minuscola, un numero e un carattere speciale, minima 8 caratteri.';
    }
}