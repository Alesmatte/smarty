<?php


namespace App\Http\Request\Rules;


use Illuminate\Contracts\Validation\Rule;
use DB;

class EmailRules implements Rule
{
    private $email;
    /**
     * EmailRules constructor.
     */
    public function __construct(string $email='')
    {
        $this->email = $email;
    }

    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {

        $result = DB::select('select email from users where email = ?', [$value]);
        if(empty($result)){
            return true;
        } else{
            return $this->email === $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return 'l\'email è già presente nel sistema';
    }
}