<?php


namespace App\Http\Request;


use App\Http\Request\Rules\CustomRules;
use App\Http\Request\Rules\EmailRules;
use App\Http\Request\Rules\PasswordRules;
use App\Support\FormRequest;

class StoreGroup extends FormRequest
{
    protected function afterValidationPasses()
    {
        $this->forget('csrf_value');
        $this->forget('csrf_name');
    }

    public function messages()
    {
        return [
            'nome.required' => 'Il Nome è richiesto',
            'nome.string' => 'Il Nome deve essere un testo',
            'descrizione.required' => 'La Descrizione è richiesta',
            'descrizione.string' => 'La Descrizione deve essere un testo',
        ];
    }

    public function rules(){
        // regole di validazione
        return [
            'nome' => 'required|string',
            'descrizione' => 'required|string',
        ];
    }
}