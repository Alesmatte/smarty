<?php


namespace App\Http\Middleware;


use App\Services\ServicesService;
use App\Services\UserService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handle;

class ListaUtenzaMiddleware
{
    public function __invoke(Request $request, Handle $handler)
    {
        $serviziService = new ServicesService();
        $userService = new UserService();
        $user = $userService->getUser();
        if (!$serviziService->ifPermissByNomeServizioAndUser('Lista Utenza', $user)) {
            return redirect(asset('admin'));
        }
        return $handler->handle($request);
    }

}