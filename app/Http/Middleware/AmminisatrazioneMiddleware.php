<?php


namespace App\Http\Middleware;


use App\Services\UserService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handle;

class AmminisatrazioneMiddleware
{
    /*
     * CreazioneFilm ModificaFilm               Creazione e Modifica Film
     * CancellazioneFilm                        Cancellazione Film
     * ListaSerieTv                             Lista Film
     *
     * CreazioneSerieTv ModificaSerieTv         Creazione e Modifica Serie Tv
     * CancellazioneSerieTv                     Cancellazione Serie Tv
     * ListaSerieTv                             Lista Serie Tv
     *
     * CreazioneFilmmaker ModificaFilmmaker     Creazione e Modifica Filmmaker
     * CancellazioneFilmmaker                   Cancellazione Filmmaker
     * ListaFilmmaker                           Lista Filmmaker
     *
     * CreazioneUtenza ModifiocaUtenza          Creazione e Modifica User
     * CancellazioneUtenza                      Cancellazione Utenza
     * ListaUtenza                              Lista Utenza
     *
     * CreazioneGruppo ModificaGruppo           Creazione e Modifica Gruppi
     * CancellazioneGruppo                      Cancellazione Gruppi
     * ListaGruppiEServizi                      Lista Gruppi
     *
     * GestioneRecensioni                       Gestione Recensioni
     *
     * ImpostazioniSito                         Impostazioni Sito
     */
    public function __invoke(Request $request, Handle $handler)
    {
        $userService = new UserService();
        if ($userService->getUser()->getTipologiaUtenza() != 2) {
            session()->flash()->set('errorPermission', 'Non hai il permesso di eseguire questa operazione!');
            return redirect('/');
        }
        return $handler->handle($request);
    }
}