<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\FilmService;
use App\Services\GenereService;
use App\Services\RecensioneService;
use App\Services\RuoloService;
use App\Services\SerieTvService;
use App\Services\UserService;
use App\Support\View;
use DB;

class IndexFrontController
{
    public function show(View $view)
    {
        $titlepage = 'Red Carpet';
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $recensioneService = new RecensioneService();
        $userService = new UserService();


        try {
            $filmInSala = $filmService->getFilmsInSala();
        } catch (\Exception $exception) {
            $filmInSala = array();
        }
        try {
            $serieTvInCorso = $serieTvService->getSerieTvInCorso();
        } catch (\Exception $exception) {
            $serieTvInCorso = array();
        }
        try {
            $filmPiuVotati = $filmService->getFilmsPiuVotati();
        } catch (\Exception $exception) {
            $filmPiuVotati = array();
        }

        try {
            $serieTVPiuVate = $serieTvService->getSerieTvPiuVotate();
        } catch (\Exception $exception) {
            $serieTVPiuVate = array();
        }

        try {
            $filmSerieTvTot = $filmService->countFilm() + $serieTvService->countSerieTv();
        } catch (\Exception $exception) {
            $filmSerieTvTot = 0;
        }

        $totRec = $recensioneService->countRecensioni();
        $totUtenti = $userService->countUser();

        return $view('front.index', compact('titlepage', 'filmInSala', 'serieTvInCorso', 'filmPiuVotati', 'serieTVPiuVate', 'filmSerieTvTot', 'totRec', 'totUtenti'));
    }
}