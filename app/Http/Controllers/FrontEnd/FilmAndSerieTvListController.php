<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\DistributoreService;
use App\Services\ImmagineService;
use App\Services\SerieTvService;
use App\Services\GenereService;
use App\Services\FilmService;
use App\Support\RequestInput;
use App\Support\View;

class FilmAndSerieTvListController
{
    public function showFilm(RequestInput $requestInput, View $view)
    {
        $queryStrParameters = $requestInput->getQueryString();
        $parameters = $requestInput->getArguments();
        $distributoreService = new DistributoreService();
        $immagineService = new ImmagineService();
        $serieTvService = new SerieTvService();
        $genereService = new GenereService();
        $filmService = new FilmService();
        $titlepage = 'Film';
        $distributore = '';
        $genere = '';
        $tipo = '';
        $ricerca = '';
        $infoPagina = 'Lista Film';
        if (isset($parameters['limit']) && isset($parameters['offset'])) {
            if (isset($parameters['tipo'])) {
                $tipo = $parameters['tipo'];
                if ($parameters['tipo'] == 'cinema') {
                    $ricerca = 'film in sala';
                    $infoPagina = 'Lista Film in sala';
                    $operaList = $filmService->getFilmInSalaPaginato($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmInSala();
                } elseif ($parameters['tipo'] == 'oscar') {
                    $ricerca = 'film premiati';
                    $infoPagina = 'Lista Film premiati agli Oscar';
                    $operaList = $filmService->getFilmByPremioPaginato(1, $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmByPremio(1);
                } elseif ($parameters['tipo'] == 'inuscita') {
                    $ricerca = 'film in uscita';
                    $infoPagina = 'Lista Film in uscita';
                    $operaList = $filmService->getFilmInUscitaPaginato($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmInUscita();
                } elseif ($parameters['tipo'] == 'mese') {
                    $ricerca = 'film di questo mese';
                    $infoPagina = 'Lista Film di questo mese';
                    $operaList = $filmService->getFilmDelMesePaginato($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmDelMese();
                } elseif (is_numeric($parameters['tipo']) && strlen($parameters['tipo']) == 4) {
                    $ricerca = 'film prodotti nell\'anno' . $parameters['tipo'];
                    $infoPagina = 'Lista Film del ' . $parameters['tipo'];
                    $operaList = $filmService->getFilmByAnnoPaginato(intval($parameters['tipo']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmByAnno($parameters['tipo']);
                } else {
                    $ricerca = 'film';
                    $operaList = $filmService->getFilmByNazionePaginato($parameters['tipo'], $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $filmService->countFilmByNazione($parameters['tipo']);
                }
            } elseif (isset($queryStrParameters['genere'])) {
                $genere = $queryStrParameters['genere'];
                $infoPagina = 'Lista Film di genere ' . $genereService->getGenereById(intval($genere))->getNome();
                $ricerca = 'film';
                $operaList = $filmService->getFilmByIdGenerePaginato(intval($queryStrParameters['genere']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $filmService->countFilmByIdGenere((int)$queryStrParameters['genere']);
            } elseif (isset($queryStrParameters['distributore'])) {
                $ricerca = 'film';
                $distributore = $queryStrParameters['distributore'];
                $infoPagina = 'Lista Film distribuiti da ' . $distributoreService->getDistrucutoreById(intval($distributore))->getNome();
                $operaList = $filmService->getFilmByDistributorePaginato(intval($queryStrParameters['distributore']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $filmService->countFilmByDistributore((int)$queryStrParameters['distributore']);
            }
            else {
                $ricerca = 'film';
                $operaList = $filmService->getAllFilmParzialiPerListaPaginati($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $filmService->countAllFilmParzialiPerLista();
            }
            $numeroPagine = ceil($countRisultati / $parameters['limit']);
            $currentPage = $parameters['offset'];
        } else {
            if (isset($parameters['tipo'])) {
                $tipo = $parameters['tipo'];
                if ($parameters['tipo'] == 'cinema') {
                    $ricerca = 'film in sala';
                    $infoPagina = 'Lista Film in sala';
                    $operaList = $filmService->getFilmInSalaPaginato(5, 0);
                    $countRisultati = $filmService->countFilmInSala();
                } elseif ($parameters['tipo'] == 'oscar') {
                    $ricerca = 'film premiati';
                    $infoPagina = 'Lista Film premiati agli Oscar';
                    $operaList = $filmService->getFilmByPremioPaginato(1, 5, 0);
                    $countRisultati = $filmService->countFilmByPremio(1);
                } elseif ($parameters['tipo'] == 'inuscita') {
                    $ricerca = 'film in uscita';
                    $infoPagina = 'Lista Film in uscita';
                    $operaList = $filmService->getFilmInUscitaPaginato(5, 0);
                    $countRisultati = $filmService->countFilmInUscita();
                } elseif ($parameters['tipo'] == 'mese') {
                    $ricerca = 'film di questo mese';
                    $infoPagina = 'Lista Film di questo mese';
                    $operaList = $filmService->getFilmDelMesePaginato(5, 0);
                    $countRisultati = $filmService->countFilmDelMese();
                } elseif (is_numeric($parameters['tipo']) && strlen($parameters['tipo']) == 4) {
                    $ricerca = 'film prodotti nell\'anno' . $parameters['tipo'];
                    $infoPagina = 'Lista Film del ' . $parameters['tipo'];
                    $operaList = $filmService->getFilmByAnnoPaginato(intval($parameters['tipo']), 5, 0);
                    $countRisultati = $filmService->countFilmByAnno($parameters['tipo']);
                } else {
                    $ricerca = 'film';
                    $operaList = $filmService->getFilmByNazionePaginato($parameters['tipo'], 5, 0);
                    $countRisultati = $filmService->countFilmByNazione($parameters['tipo']);
                }
            } elseif (isset($queryStrParameters['genere'])) {
                $ricerca = 'film';
                $genere = $queryStrParameters['genere'];
                $infoPagina = 'Lista Film di genere ' . $genereService->getGenereById(intval($genere))->getNome();
                $operaList = $filmService->getFilmByIdGenerePaginato(intval($queryStrParameters['genere']), 5, 0);
                $countRisultati = $filmService->countFilmByIdGenere((int)$queryStrParameters['genere']);
            } elseif (isset($queryStrParameters['distributore'])) {
                $ricerca = 'film';
                $distributore = $queryStrParameters['distributore'];
                $infoPagina = 'Lista Film distribuiti da ' . $distributoreService->getDistrucutoreById(intval($distributore))->getNome();
                $operaList = $filmService->getFilmByDistributorePaginato(intval($queryStrParameters['distributore']), 5, 0);
                $countRisultati = $filmService->countFilmByDistributore((int)$queryStrParameters['distributore']);
            }
            else {
                $ricerca = 'film';
                $operaList = $filmService->getAllFilmParzialiPerListaPaginati(5, 0);
                $countRisultati = $filmService->countAllFilmParzialiPerLista();
            }
            $numeroPagine = ceil($countRisultati / 5);
            $currentPage = 1;
        }
        $immaginiSliderList = $immagineService->getImgByTipologia(4);
        $coverVideoList = array();
        $matriceRegisti = array();
        $matriceAttori = array();
        foreach ($operaList as $film) {
            if (!empty($film->getVideos())) {
                $coverVideoList[$film->getIdFilm()] = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('video', $film->getVideos()[0]->getIdVideo(), 3)[0];
                $matriceRegisti[$film->getIdFilm()] = array();
                $matriceAttori[$film->getIdFilm()] = array();
                foreach ($film->getRuoli() as $ruolo) {
                    if ($ruolo->getTipologia() == 2)
                        array_push($matriceRegisti[$film->getIdFilm()], $ruolo->getFilmmaker());
                    if ($ruolo->getTipologia() == 1)
                        array_push($matriceAttori[$film->getIdFilm()], $ruolo->getFilmmaker());
                }
                $matriceAttori[$film->getIdFilm()] = array_slice($matriceAttori[$film->getIdFilm()], 0, 5);
            }
        }

        $ultimiFilmAndSerieTv = $filmService->getUltimicinqueFilm();
        foreach ($serieTvService->getUltimecinqueSerieTv() as $serieTv) {
            array_push($ultimiFilmAndSerieTv, $serieTv);
        }
        uasort($ultimiFilmAndSerieTv, fn($a, $b) => (date($a->getDataUscita()) > date($b->getDataUscita())) ? -1 : 1);
        $ultimiFilmAndSerieTv = array_slice($ultimiFilmAndSerieTv, 0, 5);

        $generiList = $genereService->getAllGenereFilm();
        $filmPiuVotato = $filmService->getSingoloFilmPiuVotato();
        $serieTvPiuVotata = $serieTvService->getSingolaSerieTvPiuVotata();
        if ($filmPiuVotato->getIdFilm() === null) {
            $entitaPiuVotata = $serieTvPiuVotata;
        } elseif ($serieTvPiuVotata->getIdSerieTv() === null) {
            $entitaPiuVotata = $filmPiuVotato;
        } elseif ($filmPiuVotato->getIdFilm() === null && $serieTvPiuVotata->getIdSerieTv() === null) {
            $entitaPiuVotata = null;
        } else {
            if ($filmPiuVotato->mediaVoto > $serieTvPiuVotata->mediaVoto) {
                $entitaPiuVotata = $filmPiuVotato;
            } else {
                $entitaPiuVotata = $serieTvPiuVotata;
            }
        }

        $operaListAppo = $operaList;
        $pagine = self::paginazione($currentPage, $numeroPagine);
        $p = compact('operaListAppo', 'immaginiSliderList', 'coverVideoList', 'matriceRegisti', 'matriceAttori', 'ricerca', 'currentPage', 'numeroPagine', 'countRisultati', 'ultimiFilmAndSerieTv', 'entitaPiuVotata', 'generiList', 'genere', 'distributore', 'infoPagina', 'tipo', 'titlepage', 'pagine');
        //dd($p);
        return $view('front.film-serietv-list', compact('operaListAppo', 'immaginiSliderList', 'coverVideoList', 'matriceRegisti', 'matriceAttori', 'ricerca', 'currentPage', 'numeroPagine', 'countRisultati', 'ultimiFilmAndSerieTv', 'entitaPiuVotata', 'generiList', 'genere', 'distributore', 'infoPagina', 'tipo', 'titlepage', 'pagine'));
    }

    public function showSerieTv(RequestInput $requestInput, View $view)
    {
        $queryStrParameters = $requestInput->getQueryString();
        $parameters = $requestInput->getArguments();
        $distributoreService = new DistributoreService();
        $immagineService = new ImmagineService();
        $serieTvService = new SerieTvService();
        $genereService = new GenereService();
        $filmService = new FilmService();
        $titlepage = 'Serie Tv';
        $distributore = '';
        $genere = '';
        $tipo = '';
        $ricerca = '';
        $infoPagina = 'Lista Serie Tv';
        if (isset($parameters['limit']) && isset($parameters['offset'])) {
            if (isset($parameters['tipo'])) {
                $tipo = $parameters['tipo'];
                if ($parameters['tipo'] == 'inonda') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv in onda';
                    $operaList = $serieTvService->getSerieTvInCorsoPerListaPaginato($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $serieTvService->countSerieTvInCorso();
                } elseif ($parameters['tipo'] == 'oscar') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv premiati agli Oscar';
                    $operaList = $serieTvService->getSerieTvByPremioPerListaPaginato(1, $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $serieTvService->countSerieTvByPremio(1);
                } elseif ($parameters['tipo'] == 'inuscita') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv in uscita';
                    $operaList = $serieTvService->getSerieTvInUscitaPaginato($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $serieTvService->countSerieTvInUscita();
                } elseif (is_numeric($parameters['tipo']) && strlen($parameters['tipo']) == 4) {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv del ' . $parameters['tipo'];
                    $operaList = $serieTvService->getSerieTvByAnnoPerListaPaginato(intval($parameters['tipo']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $serieTvService->countSerieTvByAnno($parameters['tipo']);
                } else {
                    $ricerca = 'serie tv';
                    $operaList = $serieTvService->getSerieTvByNazionePerListaPaginato($parameters['tipo'], $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                    $countRisultati = $serieTvService->countSerieTvByNazione($parameters['tipo']);
                }
            } elseif (isset($queryStrParameters['genere'])) {
                $ricerca = 'serie tv';
                $genere = $queryStrParameters['genere'];
                $infoPagina = 'Lista Serie Tv di genere ' . $genereService->getGenereById(intval($genere))->getNome();
                $operaList = $serieTvService->getSerieTvByIdGenerePerListaPaginato(intval($queryStrParameters['genere']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $serieTvService->countSerieTvByIdGenere($queryStrParameters['genere']);
            } elseif (isset($queryStrParameters['distributore'])) {
                $ricerca = 'serie tv';
                $distributore = $queryStrParameters['distributore'];
                $infoPagina = 'Lista Serie Tv distribuiti da ' . $distributoreService->getDistrucutoreById(intval($distributore))->getNome();
                $operaList = $serieTvService->getSerieTvByDistributorePerListaPaginato(intval($queryStrParameters['distributore']), $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $serieTvService->countSerieTvByDistributore($queryStrParameters['genere']);
            }
            else {
                $ricerca = 'serie tv';
                $operaList = $serieTvService->getAllSerieTvParzialiPerListaPaginati($parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $countRisultati = $serieTvService->countAllSerieTvParziali();
            }
            $numeroPagine = ceil($countRisultati / $parameters['limit']);
            $currentPage = $parameters['offset'];
        } else {
            if (isset($parameters['tipo'])) {
                $tipo = $parameters['tipo'];
                if ($parameters['tipo'] == 'inonda') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv in onda';
                    $operaList = $serieTvService->getSerieTvInCorsoPerListaPaginato(5, 0);
                    $countRisultati = $serieTvService->countSerieTvInCorso();
                } elseif ($parameters['tipo'] == 'oscar') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv premiati agli Oscar';
                    $operaList = $serieTvService->getSerieTvByPremioPerListaPaginato(1, 5, 0);
                    $countRisultati = $serieTvService->countSerieTvByPremio(1);
                } elseif ($parameters['tipo'] == 'inuscita') {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv in uscita';
                    $operaList = $serieTvService->getSerieTvInUscitaPaginato(5, 0);
                    $countRisultati = $serieTvService->countSerieTvInUscita();
                } elseif (is_numeric($parameters['tipo']) && strlen($parameters['tipo']) == 4) {
                    $ricerca = 'serie tv';
                    $infoPagina = 'Lista Serie Tv del ' . $parameters['tipo'];
                    $operaList = $serieTvService->getSerieTvByAnnoPerListaPaginato(intval($parameters['tipo']), 5, 0);
                    $countRisultati = $serieTvService->countSerieTvByAnno($parameters['tipo']);
                } else {
                    $ricerca = 'serie tv';
                    $operaList = $serieTvService->getSerieTvByNazionePerListaPaginato($parameters['tipo'], 5, 0);
                    $countRisultati = $serieTvService->countSerieTvByNazione($parameters['tipo']);
                }
            } elseif (isset($queryStrParameters['genere'])) {
                $ricerca = 'serie tv';
                $genere = $queryStrParameters['genere'];
                $infoPagina = 'Lista Serie Tv di genere ' . $genereService->getGenereById(intval($genere))->getNome();
                $operaList = $serieTvService->getSerieTvByIdGenerePerListaPaginato(intval($queryStrParameters['genere']), 5, 0);
                $countRisultati = $serieTvService->countSerieTvByIdGenere($queryStrParameters['genere']);
            } elseif (isset($queryStrParameters['distributore'])) {
                $ricerca = 'serie tv';
                $distributore = $queryStrParameters['distributore'];
                $operaList = $serieTvService->getSerieTvByDistributorePerListaPaginato(intval($queryStrParameters['distributore']), 5, 0);
                $countRisultati = $serieTvService->countSerieTvByDistributore($queryStrParameters['genere']);
            }
            else {
                $operaList = $serieTvService->getAllSerieTvParzialiPerListaPaginati(5, 0);
                $countRisultati = $serieTvService->countAllSerieTvParziali();
            }
            $numeroPagine = ceil($countRisultati / 5);
            $currentPage = 1;
        }
        $immaginiSliderList = $immagineService->getImgByTipologia(8);
        $coverVideoList = array();
        $matriceRegisti = array();
        $matriceAttori = array();
        foreach ($operaList as $serieTv) {
            if (!empty($serieTv->getVideos())) {
                $coverVideoList[$serieTv->getIdSerieTv()] = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('video', $serieTv->getVideos()[0]->getIdVideo(), 3)[0];
                $matriceRegisti[$serieTv->getIdSerieTv()] = array();
                $matriceAttori[$serieTv->getIdSerieTv()] = array();
                foreach ($serieTv->getRuoli() as $ruolo) {
                    if ($ruolo->getTipologia() == 2)
                        array_push($matriceRegisti[$serieTv->getIdSerieTv()], $ruolo->getFilmmaker());
                    if ($ruolo->getTipologia() == 1)
                        array_push($matriceAttori[$serieTv->getIdSerieTv()], $ruolo->getFilmmaker());
                }
                $matriceAttori[$serieTv->getIdSerieTv()] = array_slice($matriceAttori[$serieTv->getIdSerieTv()], 0, 5);
            }
        }

        $ultimiFilmAndSerieTv = $filmService->getUltimicinqueFilm();
        foreach ($serieTvService->getUltimecinqueSerieTv() as $serieTv) {
            array_push($ultimiFilmAndSerieTv, $serieTv);
        }
        uasort($ultimiFilmAndSerieTv, fn($a, $b) => (date($a->getDataUscita()) > date($b->getDataUscita())) ? -1 : 1);
        $ultimiFilmAndSerieTv = array_slice($ultimiFilmAndSerieTv, 0, 5);

        $generiList = $genereService->getAllGenereSerieTv();
        $filmPiuVotato = $filmService->getSingoloFilmPiuVotato();
        $serieTvPiuVotata = $serieTvService->getSingolaSerieTvPiuVotata();
        if ($filmPiuVotato->mediaVoto > $serieTvPiuVotata->mediaVoto) {
            $entitaPiuVotata = $filmPiuVotato;
        } else {
            $entitaPiuVotata = $serieTvPiuVotata;
        }
        $operaListAppo = $operaList;
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('front.film-serietv-list', compact('operaListAppo', 'immaginiSliderList', 'coverVideoList', 'matriceRegisti', 'matriceAttori', 'ricerca', 'currentPage', 'numeroPagine', 'countRisultati', 'ultimiFilmAndSerieTv', 'entitaPiuVotata', 'generiList', 'genere', 'distributore', 'infoPagina', 'tipo', 'titlepage', 'pagine'));
    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}