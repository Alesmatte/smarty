<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\RecensioneService;
use App\Services\ImmagineService;
use App\Services\SerieTvService;
use App\Services\FilmService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class ReviewListFrontController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $immagineService = new ImmagineService();
        $parameters = $requestInput->getArguments();
        $entita = $parameters['entita'];
        $tipo = $parameters['tipo'];
        $id = $parameters['id'];
        if ($entita == 'film') {
            $filmService = new FilmService();
            $nomeEntita = $filmService->getFilmByIdPerRecensioni($id)->getTitolo();
            if (isset($parameters['offset'])) {
                if ($tipo == 'normale') {
                    $totRecensioni = $recensioneService->countGetRecensioniPrTipoUtenteByIdFilm($id, 0);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, (($parameters['offset']) - 1) * 10, 0);
                } elseif ($tipo == 'critico') {
                    $totRecensioni = $recensioneService->countGetRecensioniPrTipoUtenteByIdFilm($id, 1);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, (($parameters['offset']) - 1) * 10, 1);
                } else {
                    $totRecensioni = $recensioneService->countGetRecensioniByIdFilm($id);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, (($parameters['offset']) - 1) * 10);
                }
                $currentPage = $parameters['offset'];
                $numeroPagine = ceil($totRecensioni / 10);
            } else {
                if ($tipo == 'normale') {
                    $totRecensioni = $recensioneService->countGetRecensioniPrTipoUtenteByIdFilm($id, 0);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, 0, 0);
                } elseif ($tipo == 'critico') {
                    $totRecensioni = $recensioneService->countGetRecensioniPrTipoUtenteByIdFilm($id, 1);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, 0, 1);
                } else {
                    $totRecensioni = $recensioneService->countGetRecensioniByIdFilm($id);
                    $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10);
                }
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } elseif ($entita == 'serietv') {
            $serieTvService = new SerieTvService();
            $nomeEntita = $serieTvService->getSerieTvByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniByIdSerieTv($id);
            if (isset($parameters['offset'])) {
                if ($tipo == 'normale') {
                    $totRecensioni = $recensioneService->countGetRecensioniPerTipoUserByIdSerieTv($id, 0);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, (($parameters['offset']) - 1) * 10, 0);
                } elseif ($tipo == 'critico') {
                    $totRecensioni = $recensioneService->countGetRecensioniPerTipoUserByIdSerieTv($id, 1);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, (($parameters['offset']) - 1) * 10, 1);
                } else {
                    $totRecensioni = $recensioneService->countGetRecensioniByIdSerieTv($id);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, (($parameters['offset']) - 1) * 10);
                }
                $currentPage = $parameters['offset'];
                $numeroPagine = ceil($totRecensioni / 10);
            } else {
                if ($tipo == 'normale') {
                    $totRecensioni = $recensioneService->countGetRecensioniPerTipoUserByIdSerieTv($id, 0);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, 0, 0);
                } elseif ($tipo == 'critico') {
                    $totRecensioni = $recensioneService->countGetRecensioniPerTipoUserByIdSerieTv($id, 1);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, 0, 1);
                } else {
                    $totRecensioni = $recensioneService->countGetRecensioniByIdSerieTv($id);
                    $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10);
                }
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } else {
            return back();
        }
        $titlepage = 'Recensioni' . $nomeEntita;
        $pagine = self::paginazione($currentPage, $numeroPagine);
        $immagineCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia($entita, $id, 2)[0];
        return $view('front.review-list', compact('titlepage', 'recensioniList', 'entita', 'nomeEntita', 'id', 'tipo', 'immagineCopertina', 'totRecensioni', 'numeroPagine', 'currentPage', 'pagine'));
    }

    public function cerca(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $immagineService = new ImmagineService();
        $parameters = $requestInput->getArguments();
        $qstr = $requestInput->getQueryString();
        $entita = $parameters['entita'];
        $tipo = $parameters['tipo'];
        $cerca = $qstr['ricerca'];
        $id = $parameters['id'];

        if ($entita == 'film') {
            $filmService = new FilmService();
            $nomeEntita = $filmService->getFilmByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniSearchByIdFilm($id, $cerca);
            if (isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniBySearchIdFilm($cerca, $id, 10, (($parameters['offset']) - 1) * 10);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = $parameters['offset'];
            } else {
                $recensioniList = $recensioneService->getRecensioniBySearchIdFilm($cerca, $id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } elseif ($entita == 'serietv') {
            $serieTvService = new SerieTvService();
            $nomeEntita = $serieTvService->getSerieTvByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniSearchByIdSerieTv($id, $cerca);
            if (isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniBySearchIdSerieTv($cerca, $id, 10, (($parameters['offset']) - 1) * 10);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = $parameters['offset'];
            } else {
                $recensioniList = $recensioneService->getRecensioniBySearchIdSerieTv($cerca, $id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } else {
            return back();
        }
        $titlepage = 'Recensioni' . $nomeEntita;
        $pagine = self::paginazione($currentPage, $numeroPagine);
        $immagineCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia($entita, $id, 2)[0];
        return $view('front.review-list', compact('titlepage', 'recensioniList', 'entita', 'nomeEntita', 'id', 'tipo', 'immagineCopertina', 'totRecensioni', 'numeroPagine', 'currentPage', 'cerca', 'pagine'));
    }

    public function showModal(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $recensione = $recensioneService->getRecensioneById($requestInput->getArguments()['idRecensione']);
        return $view('front.components_recensione.scheda_recensione', ["recensione" => $recensione]);
    }

    public function showPersonal(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $serieTvService = new SerieTvService();
        $userService = new UserService();
        $filmService = new FilmService();
        $parameters = $requestInput->getArguments();
        $totRecensioni = $recensioneService->countRecensioniPersonalByUser($userService->getUser());
        if (isset($parameters['offset'])) {
            $recensioniList = $recensioneService->getRecensioniPersonalByUserPaginato($userService->getUser(), 10, (($parameters['offset']) - 1) * 10);
            $currentPage = $parameters['offset'];
            $numeroPagine = ceil($totRecensioni / 10);
        } else {
            $recensioniList = $recensioneService->getRecensioniPersonalByUserPaginato($userService->getUser(), 10, 0);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = 1;
        }

        $entitaList = array();
        foreach ($recensioniList as $recensione) {
            $film = $filmService->getFilmByIdRecensioni($recensione->getIdRecensione());
            $idFilm = $film->getIdFilm();
            if (isset($idFilm)) {
                array_push($entitaList, $film);
            } else {
                array_push($entitaList, $serieTvService->getSerieTvByIdRecensioni($recensione->getIdRecensione()));
            }
        }

        $titlepage = 'Recensioni';
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('front.personal-review-list', compact('titlepage', 'recensioniList', 'entitaList', 'totRecensioni', 'numeroPagine', 'currentPage', 'pagine'));
    }

    public function cercaPersonal(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $serieTvService = new SerieTvService();
        $userService = new UserService();
        $filmService = new FilmService();
        $parameters = $requestInput->getArguments();
        $qstr = $requestInput->getQueryString();
        $cerca = $qstr['ricerca'];

        $totRecensioni = $recensioneService->countRecensioniPersonalSearchByUser($userService->getUser(), $cerca);
        if (isset($parameters['offset'])) {
            $recensioniList = $recensioneService->getRecensioniPersonalSearchByUserPaginato($cerca, $userService->getUser(), 10, (($parameters['offset']) - 1) * 10);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = $parameters['offset'];
        } else {
            $recensioniList = $recensioneService->getRecensioniPersonalSearchByUserPaginato($cerca, $userService->getUser(), 10, 0);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = 1;
        }

        $entitaList = array();
        foreach ($recensioniList as $recensione) {
            $film = $filmService->getFilmByIdRecensioni($recensione->getIdRecensione());
            $idFilm = $film->getIdFilm();
            if (isset($idFilm)) {
                array_push($entitaList, $film);
            } else {
                array_push($entitaList, $serieTvService->getSerieTvByIdRecensioni($recensione->getIdRecensione()));
            }
        }
        $titlepage = 'Recensioni';
        $pagine = self::paginazione($currentPage, $numeroPagine);

        return $view('front.personal-review-list', compact('titlepage', 'recensioniList', 'entitaList', 'numeroPagine', 'currentPage', 'cerca', 'pagine'));
    }

    public function showLast(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $serieTvService = new SerieTvService();
        $filmService = new FilmService();
        $entita = 'entrambe';
        $tipo = 'ultime';
        $parameters = $requestInput->getArguments();
        $totRecensioni = $recensioneService->countRecensioni();
        if (isset($parameters['offset'])) {
            $recensioniList = $recensioneService->getLastRecensioniPaginato(10, (($parameters['offset']) - 1) * 10);
            $currentPage = $parameters['offset'];
            $numeroPagine = ceil($totRecensioni / 10);
        } else {
            $recensioniList = $recensioneService->getLastRecensioniPaginato(10, 0);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = 1;
        }

        $entitaList = array();
        foreach ($recensioniList as $recensione) {
            $film = $filmService->getFilmByIdRecensioni($recensione->getIdRecensione());
            $idFilm = $film->getIdFilm();
            if (isset($idFilm)) {
                array_push($entitaList, $film);
            } else {
                array_push($entitaList, $serieTvService->getSerieTvByIdRecensioni($recensione->getIdRecensione()));
            }
        }
        $titlepage = 'Recensioni';
        $pagine = self::paginazione($currentPage, $numeroPagine);

        return $view('front.last-review-list', compact('titlepage', 'recensioniList', 'entitaList', 'tipo', 'entita', 'totRecensioni', 'numeroPagine', 'currentPage', 'pagine'));
    }

    public function cercaLast(RequestInput $requestInput, View $view)
    {
        $recensioneService = new RecensioneService();
        $serieTvService = new SerieTvService();
        $filmService = new FilmService();
        $parameters = $requestInput->getArguments();
        $qstr = $requestInput->getQueryString();
        $cerca = $qstr['cerca_recensioni'];
        $totRecensioni = $recensioneService->countLastRecensioniBySearch($cerca);
        if (isset($parameters['offset'])) {
            $recensioniList = $recensioneService->getLastRecensioniBySearchPaginato($cerca, 10, (($parameters['offset']) - 1) * 10);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = $parameters['offset'];
        } else {
            $recensioniList = $recensioneService->getLastRecensioniBySearchPaginato($cerca, 10, 0);
            $numeroPagine = ceil($totRecensioni / 10);
            $currentPage = 1;
        }

        $entitaList = array();
        foreach ($recensioniList as $recensione) {
            $film = $filmService->getFilmByIdRecensioni($recensione->getIdRecensione());
            $idFilm = $film->getIdFilm();
            if (isset($idFilm)) {
                array_push($entitaList, $film);
            } else {
                array_push($entitaList, $serieTvService->getSerieTvByIdRecensioni($recensione->getIdRecensione()));
            }
        }

        $titlepage = 'Recensioni';
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('front.last-review-list', compact('titlepage', 'recensioniList', 'entitaList', 'numeroPagine', 'currentPage', 'cerca', 'pagine'));
    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}