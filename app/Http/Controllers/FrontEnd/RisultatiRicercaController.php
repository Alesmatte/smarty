<?php


namespace App\Http\Controllers\FrontEnd;


use App\Model\Film;
use App\Model\SerieTv;
use App\Services\FilmMakerService;
use App\Services\FilmService;
use App\Services\NominationService;
use App\Services\PremioService;
use App\Services\RuoloService;
use App\Support\RequestInput;
use App\Support\View;
use App\Services\SerieTvService;

class RisultatiRicercaController
{
    public function show(RequestInput $requestInput, View $view)
    {
        if(isset($requestInput->getArguments()['offset'])){
            $data = $requestInput->getQueryString();
        } else {
            $data = $requestInput->all();
        }
        $titlepage = 'Risultati ricerca';
        $filmList = array();
        $serieTvList = array();
        $personaList = array();
        switch ($data['match']) {
            case "1":
                /** FILM */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $filmList = $this->bloccoFilm($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                break;
            }
            case "2":
                /** SERIE TV */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $serieTvList = $this->bloccoSerieTv($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                break;
            }
            case "3":
                /** PERSONE */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $filmList = $this->bloccoFilm($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                $serieTvList = $this->bloccoSerieTv($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                if ($data['persone'] != "") {
                    $personaFilmList = $this->bloccoPersoneFilm($data['persone'], $data['ruolo'], $filmList);
                    $personaSerieTvList = $this->bloccoPersoneSerieTv($data['persone'], $data['ruolo'], $serieTvList);
                    if (empty($personaFilmList)) {
                        $personaList = $personaSerieTvList;
                        $filmList = array();
                        $serieTvList = array();
                        break;
                    } elseif (empty($personaSerieTvList)) {
                        $personaList = $personaFilmList;
                        $filmList = array();
                        $serieTvList = array();
                        break;
                    }
                    $personaList = $personaFilmList;
                    $perApp = array();
                    foreach ($personaFilmList as $k => $pe) {
                        array_push($perApp, $pe->getIdFilmMaker());
                    }
                    foreach ($personaSerieTvList as $ke => $per) {
                        if (!in_array($per->getIdFilmMaker(), $perApp)) {
                            array_push($perApp, $per->getIdFilmMaker());
                            array_push($personaList, $per);
                        }
                    }
                    uasort($personaList, fn($a, $b) => strcmp($a->getNome(), $b->getNome()));
                    $ruoloService = new RuoloService();
                    foreach ($personaList as $persona) {
                        $ruoli = array();
                        $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
                        foreach ($ru as $ruolo){
                            if(!in_array($ruolo->getTipologia(), $ruoli)){
                                array_push($ruoli, $ruolo->getTipologia());
                            }
                        }
                        $persona->ruoli = $ruoli;
                    }
                }
                $filmList = array();
                $serieTvList = array();
                break;
            }
            case "4":
                /** FILM e SERIE TV */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $filmList = $this->bloccoFilm($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                $serieTvList = $this->bloccoSerieTv($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                break;
            }
            case "5":
                /** FILM e PERSONE */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $filmList = $this->bloccoFilm($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                if (!empty($filmList) && $data['persone'] != "") {
                    $personaList = $this->bloccoPersoneFilm($data['persone'], $data['ruolo'], $filmList);
                    $ruoloService = new RuoloService();
                    foreach ($personaList as $persona) {
                        $ruoli = array();
                        $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
                        foreach ($ru as $ruolo){
                            if(!in_array($ruolo->getTipologia(), $ruoli)){
                                array_push($ruoli, $ruolo->getTipologia());
                            }
                        }
                        $persona->ruoli = $ruoli;
                    }
                }
                break;
            }
            case "6":
                /** SERIE TV e PERSONE */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $serieTvList = $this->bloccoSerieTv($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                if (!empty($serieTvList) && $data['persone'] != "") {
                    $personaList = $this->bloccoPersoneSerieTv($data['persone'], $data['ruolo'], $serieTvList);
                    $ruoloService = new RuoloService();
                    foreach ($personaList as $persona) {
                        $ruoli = array();
                        $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
                        foreach ($ru as $ruolo){
                            if(!in_array($ruolo->getTipologia(), $ruoli)){
                                array_push($ruoli, $ruolo->getTipologia());
                            }
                        }
                        $persona->ruoli = $ruoli;
                    }
                }
                break;
            }
            case "7":
                /** FILM, SERIE TV e PERSONE */
            {
                if (isset($data['select_cerc_anno_dis'])) {
                    $selectCercAnnoDis = $data['select_cerc_anno_dis'];
                } else {
                    $selectCercAnnoDis = null;
                }
                if (isset($data['select_cerc_stelle'])) {
                    $selectCercStelle = $data['select_cerc_stelle'];
                } else {
                    $selectCercStelle = null;
                }
                $filmList = $this->bloccoFilm($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                $serieTvList = $this->bloccoSerieTv($data['titolo_ita_orig'], $data['anno_distribuzione'], $selectCercAnnoDis, $data['stelle'],
                    $selectCercStelle, $data['premi'], $data['genere'], $data['distributore'], $data['persone'],
                    $data['ruolo'], $data['ordina']);
                if ($data['persone'] != "") {
                    $personaFilmList = $this->bloccoPersoneFilm($data['persone'], $data['ruolo'], $filmList);
                    $personaSerieTvList = $this->bloccoPersoneSerieTv($data['persone'], $data['ruolo'], $serieTvList);
                    if (empty($personaFilmList)) {
                        $personaList = $personaSerieTvList;
                        break;
                    } elseif (empty($personaSerieTvList)) {
                        $personaList = $personaFilmList;
                        break;
                    }
                    $personaList = $personaFilmList;
                    $perApp = array();
                    foreach ($personaFilmList as $k => $pe) {
                        array_push($perApp, $pe->getIdFilmMaker());
                    }
                    foreach ($personaSerieTvList as $ke => $per) {
                        if (!in_array($per->getIdFilmMaker(), $perApp)) {
                            array_push($perApp, $per->getIdFilmMaker());
                            array_push($personaList, $per);
                        }
                    }
                    uasort($personaList, fn($a, $b) => strcmp($a->getNome(), $b->getNome()));
                    $ruoloService = new RuoloService();
                    foreach ($personaList as $persona) {
                        $ruoli = array();
                        $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
                        foreach ($ru as $ruolo){
                            if(!in_array($ruolo->getTipologia(), $ruoli)){
                                array_push($ruoli, $ruolo->getTipologia());
                            }
                        }
                        $persona->ruoli = $ruoli;
                    }
                }
                break;
            }
        }
        $elemTot = count($filmList)+count($serieTvList)+count($personaList);
        $numeroPagine = ceil($elemTot / 10);

        /** BLOCCO PAGINAZIONE */
        if (isset($requestInput->getArguments()['offset'])) {
            $currentPage = $requestInput->getArguments()['offset'];
            $totElem = array();
            foreach ($filmList as $film){
                array_push($totElem, $film);
            }
            foreach ($serieTvList as $serieTv){
                array_push($totElem, $serieTv);
            }
            foreach ($personaList as $persona){
                array_push($totElem, $persona);
            }
            $totElem = array_slice($totElem, 10 * ((int)$currentPage - 1), 10);
            $filmList = array();
            $serieTvList = array();
            $personaList = array();
            foreach ($totElem as $k=>$elem){
                if($elem instanceof Film){
                    array_push($filmList, $elem);
                } elseif ($elem instanceof SerieTv){
                    array_push($serieTvList, $elem);
                } else {
                    array_push($personaList, $elem);
                }
            }
        } else {
            $currentPage = 1;
            if (!empty($filmList)) {
                if (count($filmList) >= 10) {
                    $filmList = array_slice($filmList, 0, 10);
                    $serieTvList = array();
                    $personaList = array();
                } elseif (!empty($serieTvList)) {
                    $nfilm = count($filmList);
                    if (count($serieTvList) >= (10 - $nfilm)) {
                        $serieTvList = array_slice($serieTvList, 0, 10 - $nfilm);
                        $personaList = array();
                    } elseif (!empty($personaList)) {
                        $nserietv = count($serieTvList);
                        if (count($personaList) >= (10 - ($nserietv + $nfilm))) {
                            $personaList = array_slice($personaList, 0, 10 - ($nserietv + $nfilm));
                        }
                    }
                } elseif (!empty($personaList)) {
                    $nfilm = count($filmList);
                    if (count($personaList) >= (10 - $nfilm)) {
                        $personaList = array_slice($personaList, 0, 10 - $nfilm);
                    }
                }
            } elseif (!empty($serieTvList)) {
                if (count($serieTvList) >= 10 ) {
                    $serieTvList = array_slice($serieTvList, 0, 10);
                    $personaList = array();
                } elseif (!empty($personaList)) {
                    $nserietv = count($serieTvList);
                    if (count($personaList) >= (10 - $nserietv)) {
                        $personaList = array_slice($personaList, 0, 10 - $nserietv);
                    }
                }
            } elseif (!empty($personaList)) {
                if (count($personaList) >= 10) {
                    $personaList = array_slice($personaList, 0, 10);
                }
            }
        }

        /** Costruzione QueryString */
        if(isset($data['select_cerc_anno_dis'])){
            if(isset($data['select_cerc_stelle'])){
                $queryString = 'titolo_ita_orig='.$data['titolo_ita_orig'].'&anno_distribuzione='.$data['anno_distribuzione'].'&select_cerc_anno_dis='.$data['select_cerc_anno_dis'].'&stelle='.$data['stelle'].'&select_cerc_stelle='.$data['select_cerc_stelle'].'&premi='.$data['premi'].'&genere='.$data['genere'].'&distributore='.$data['distributore'].'&persone='.$data['persone'].'&ruolo='.$data['ruolo'].'&match='.$data['match'].'&ordina='.$data['ordina'];
            } else {
                $queryString = 'titolo_ita_orig='.$data['titolo_ita_orig'].'&anno_distribuzione='.$data['anno_distribuzione'].'&select_cerc_anno_dis='.$data['select_cerc_anno_dis'].'&stelle='.$data['stelle'].'&premi='.$data['premi'].'&genere='.$data['genere'].'&distributore='.$data['distributore'].'&persone='.$data['persone'].'&ruolo='.$data['ruolo'].'&match='.$data['match'].'&ordina='.$data['ordina'];
            }
        } else {
            if (isset($data['select_cerc_stelle'])){
                $queryString = 'titolo_ita_orig='.$data['titolo_ita_orig'].'&anno_distribuzione='.$data['anno_distribuzione'].'&stelle='.$data['stelle'].'&select_cerc_stelle='.$data['select_cerc_stelle'].'&premi='.$data['premi'].'&genere='.$data['genere'].'&distributore='.$data['distributore'].'&persone='.$data['persone'].'&ruolo='.$data['ruolo'].'&match='.$data['match'].'&ordina='.$data['ordina'];
            } else {
                $queryString = 'titolo_ita_orig='.$data['titolo_ita_orig'].'&anno_distribuzione='.$data['anno_distribuzione'].'&stelle='.$data['stelle'].'&premi='.$data['premi'].'&genere='.$data['genere'].'&distributore='.$data['distributore'].'&persone='.$data['persone'].'&ruolo='.$data['ruolo'].'&match='.$data['match'].'&ordina='.$data['ordina'];
            }
        }

        $pagine = self::paginazione($currentPage, $numeroPagine);

        return $view('front.risultati_ricerca', compact('titlepage', 'filmList', 'serieTvList', 'personaList', 'elemTot', 'numeroPagine', 'currentPage', 'queryString', 'pagine'));
    }

    private
    function bloccoFilm(string $titoloItaOrig, string $annoDistribuzione, $selectCercAnnoDistr,
                        string $stelle, $selectCercStelle, string $premi, string $genere,
                        string $distributore, string $persone, string $ruolo, string $ordina): array
    {
        $filmService = new FilmService();
        $filmList = array();
        if ($titoloItaOrig != "") {
            $filmList = $this->titoloItaOrig($titoloItaOrig, 0);
            if (empty($filmList)) {
                return $filmList;
            }
        }
        if ($annoDistribuzione != "") {
            if (isset($selectCercAnnoDistr)) {
                $filmList = $this->annoDistribuzione($annoDistribuzione, $selectCercAnnoDistr, 0, $filmList);
                if (empty($filmList)) {
                    return $filmList;
                }
            }
        }
        if ($stelle != "") {
            if (isset($selectCercStelle)) {
                $filmList = $this->valutazioneStelle($stelle, $selectCercStelle, 0, $filmList);
                if (empty($filmList)) {
                    return $filmList;
                }
            }
        }
        if ($premi != "") {
            $filmList = $this->vittoriaPremio($premi, 0, $filmList);
            if (empty($filmList)) {
                return $filmList;
            }
        }
        if ($genere != "") {
            $filmList = $this->forGenere($genere, 0, $filmList);
            if (empty($filmList)) {
                return $filmList;
            }
        }
        if ($distributore != "") {
            $filmList = $this->forDistributore($distributore, 0, $filmList);
            if (empty($filmList)) {
                return $filmList;
            }
        }
        if ($persone != "") {
            $filmList = $this->forPersone($persone, 0, $filmList);
            if (empty($filmList)) {
                return $filmList;
            }
            if ($ruolo != "") {
                $filmList = $this->forRuolo($persone, $ruolo, 0, $filmList);
            }
            if (empty($filmList)) {
                return $filmList;
            }
        }
        /** AGGIUNTA votoUtenti, votoCritici, votoRedCarpet */
        foreach ($filmList as $k => $fl) {
            if ($filmService->maggiorediCinqueVoti($fl)) {
                $fl->mediaVotoUtenti = number_format($filmService->getMediaVotiUtenti($fl), 2, '.', '');
                $fl->mediaVotoCritici = number_format($filmService->getMediaVotiCritici($fl), 2, '.', '');
                $fl->mediaVotoRedCarpet = number_format($filmService->getMediaVotiFilm($fl), 2, '.', '');
            } else {
                $fl->mediaVotoUtenti = 0;
                $fl->mediaVotoCritici = 0;
                $fl->mediaVotoRedCarpet = 0;
            }
            /** BLOCCO regista, attori */
            $registi = array();
            $attori = array();
            foreach ($fl->getRuoli() as $ke => $ruolo) {
                if ($ruolo->getTipologia() == 2) {
                    array_push($registi, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                } elseif ($ruolo->getTipologia() == 1) {
                    array_push($attori, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                }
            }
            $fl->registi = $registi;
            $fl->attori = array_slice($attori, 0, 5);
        }
        /** ORDINA PER */
        $filmList = $this->ordinaPer($ordina, $filmList);
        return $filmList;
    }

    private
    function bloccoSerieTv(string $titoloItaOrig, string $annoDistribuzione, $selectCercAnnoDistr,
                           string $stelle, $selectCercStelle, string $premi, string $genere,
                           string $distributore, string $persone, string $ruolo, string $ordina): array
    {
        $serieTvService = new SerieTvService();
        $serieTvList = array();
        if ($titoloItaOrig != "") {
            $serieTvList = $this->titoloItaOrig($titoloItaOrig, 1);
            if (empty($serieTvList)) {
                return $serieTvList;
            }
        }
        if ($annoDistribuzione != "") {
            if (isset($selectCercAnnoDistr)) {
                $serieTvList = $this->annoDistribuzione($annoDistribuzione, $selectCercAnnoDistr, 1, $serieTvList);
                if (empty($serieTvList)) {
                    return $serieTvList;
                }
            }
        }
        if ($stelle != "") {
            if (isset($selectCercStelle)) {
                $serieTvList = $this->valutazioneStelle($stelle, $selectCercStelle, 1, $serieTvList);
                if (empty($serieTvList)) {
                    return $serieTvList;
                }
            }
        }
        if ($premi != "") {
            $serieTvList = $this->vittoriaPremio($premi, 1, $serieTvList);
            if (empty($serieTvList)) {
                return $serieTvList;
            }
        }
        if ($genere != "") {
            $serieTvList = $this->forGenere($genere, 1, $serieTvList);
            if (empty($serieTvList)) {
                return $serieTvList;
            }
        }
        if ($distributore != "") {
            $serieTvList = $this->forDistributore($distributore, 1, $serieTvList);
            if (empty($serieTvList)) {
                return $serieTvList;
            }
        }
        if ($persone != "") {
            $serieTvList = $this->forPersone($persone, 1, $serieTvList);
            if (empty($serieTvList)) {
                return $serieTvList;
            }
            if ($ruolo != "") {
                $serieTvList = $this->forRuolo($persone, $ruolo, 1, $serieTvList);
            }
            if (empty($serieTvList)) {
                return $serieTvList;
            }
        }
        /** AGGIUNTA votoUtenti, votoCritici, votoRedCarpet, totStagioni, totEpisodi, durataEpisodi */
        foreach ($serieTvList as $k => $stv) {
            if ($serieTvService->maggiorediCinqueVoti($stv)) {
                $stv->mediaVotoUtenti = number_format($serieTvService->mediaVotiUtenti($stv), 2, '.', '');
                $stv->mediaVotoCritici = number_format($serieTvService->mediaVotiCritici($stv), 2, '.', '');
                $stv->mediaVotoRedCarpet = number_format($serieTvService->getMediaVotiSerieTv($stv), 2, '.', '');
            } else {
                $stv->mediaVotoUtenti = 0;
                $stv->mediaVotoCritici = 0;
                $stv->mediaVotoRedCarpet = 0;
            }
            $stv->stagioniTotali = $serieTvService->getCountStagioniBySerieTv($stv);
            $stv->episodiTotali = $serieTvService->getCountEpisodiBySerieTv($stv);
            $stv->durataEpisodio = $serieTvService->getDurataEpisodioBySerieTv($stv);
            /** BLOCCO regista, attori */
            $registi = array();
            $attori = array();
            foreach ($stv->getRuoli() as $ke => $ruolo) {
                if ($ruolo->getTipologia() == 2) {
                    array_push($registi, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                } elseif ($ruolo->getTipologia() == 1) {
                    array_push($attori, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                }
            }
            $stv->registi = $registi;
            $stv->attori = array_slice($attori, 0, 5);
        }
        /** ORDINA PER */
        $serieTvList = $this->ordinaPer($ordina, $serieTvList);
        return $serieTvList;
    }

    private
    function bloccoPersoneFilm(string $persone, string $ruolo, array $filmList): array
    {
        if ($ruolo != "") {
            $personaList = $this->forPersonaRuolo($persone, $ruolo, 0, $filmList);
            if (empty($personaList)) {
                return $personaList;
            }
        } else {
            $personaList = $this->forPersonaPersona($persone, 0, $filmList);
            if (empty($personaList)) {
                return $personaList;
            }
        }
        /** AGGIUNTA PREMI->QUANTITA' */
        $nominationService = new NominationService();
        foreach ($personaList as $k => $persona) {
            $premioCount = array();
            $premi = array();
            $nominationsPremi = $nominationService->getNominationByIdFilmmaker($persona->getIdFilmMaker(), false);
            foreach ($nominationsPremi as $ke => $nomination) {
                if ($nomination->getStato() == 1 && !in_array($nomination->getCategoriaPremio()->getPremio()->getNome(), $premi)) {
                    array_push($premi, $nomination->getCategoriaPremio()->getPremio()->getNome());
                }
            }
            foreach ($premi as $key => $premio) {
                $cont = 0;
                foreach ($nominationsPremi as $ke => $nomination) {
                    if ($nomination->getStato() == 1 && $nomination->getCategoriaPremio()->getPremio()->getNome() == $premio) {
                        $cont++;
                    }
                }
                array_push($premioCount, $cont);
            }
            $persona->premi = $premi;
            $persona->premioCount = $premioCount;
        }
        /** ORDINA PER NOME */
        uasort($personaList, fn($a, $b) => strcmp($a->getNome(), $b->getNome()));
        return $personaList;
    }

    private
    function bloccoPersoneSerieTv(string $persone, string $ruolo, array $serieTvList): array
    {
        if ($ruolo != "") {
            $personaList = $this->forPersonaRuolo($persone, $ruolo, 1, $serieTvList);
            if (empty($personaList)) {
                return $personaList;
            }
        } else {
            $personaList = $this->forPersonaPersona($persone, 1, $serieTvList);
            if (empty($personaList)) {
                return $personaList;
            }
        }
        /** AGGIUNTA PREMI->QUANTITA' */
        $nominationService = new NominationService();
        foreach ($personaList as $k => $persona) {
            $premioCount = array();
            $premi = array();
            $nominationsPremi = $nominationService->getNominationByIdFilmmaker($persona->getIdFilmMaker(), false);
            foreach ($nominationsPremi as $ke => $nomination) {
                if ($nomination->getStato() == 1 && !in_array($nomination->getCategoriaPremio()->getPremio()->getNome(), $premi)) {
                    array_push($premi, $nomination->getCategoriaPremio()->getPremio()->getNome());
                }
            }
            foreach ($premi as $key => $premio) {
                $cont = 0;
                foreach ($nominationsPremi as $ke => $nomination) {
                    if ($nomination->getStato() == 1 && $nomination->getCategoriaPremio()->getPremio()->getNome() == $premio) {
                        $cont++;
                    }
                }
                array_push($premioCount, $cont);
            }
            $persona->premi = $premi;
            $persona->premioCount = $premioCount;
        }
        /** ORDINA PER NOME */
        uasort($personaList, fn($a, $b) => strcmp($a->getNome(), $b->getNome()));
        return $personaList;
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function titoloItaOrig(string $titolo, int $flag)
    {
        if ($flag == 1) {
            $serieTvService = new SerieTvService();
            $serieTvList = $serieTvService->getSerieTvByTitoloOrTitoloOrig($titolo, false);
            return $serieTvList;
        } elseif ($flag == 0) {
            $filmService = new FilmService();
            $filmList = $filmService->getFilmByTitoloOrTitoloOrig($titolo, false);
            return $filmList;
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function annoDistribuzione(string $anno, string $comeCercare, int $flag, array $list)
    {
        switch ($comeCercare) {
            case "1":
                /** MINORE DI */
            {
                if ($flag == 1) {
                    if (empty($list)) {
                        $serieTvService = new SerieTvService();
                        $serieTvList = $serieTvService->getSerieTvByAnnoMinoreDi($anno, false);
                        return $serieTvList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $stvl) {
                            if (date_create($stvl->getDataUscita()) < date_create($anno . "/1/01")) {
                                array_push($stvAnno, $stvl);
                            }
                        }
                        return $stvAnno;
                    }
                } elseif ($flag == 0) {
                    if (empty($list)) {
                        $filmService = new FilmService();
                        $filmList = $filmService->getFilmByAnnoMinoreDi($anno, false);
                        return $filmList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $fl) {
                            if (date_create($fl->getDataUscita()) < date_create($anno . "/1/01")) {
                                array_push($stvAnno, $fl);
                            }
                        }
                        return $stvAnno;
                    }
                }
            }
            case "2":
                /** MINORE UGUALE DI */
            {
                if ($flag == 1) {
                    if (empty($list)) {
                        $serieTvService = new SerieTvService();
                        $serieTvList = $serieTvService->getSerieTvByAnnoMinoreUgualeDi($anno, false);
                        return $serieTvList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $stvl) {
                            if (date_create($stvl->getDataUscita()) <= date_create($anno . "/12/31")) {
                                array_push($stvAnno, $stvl);
                            }
                        }
                        return $stvAnno;
                    }
                } elseif ($flag == 0) {
                    if (empty($list)) {
                        $filmService = new FilmService();
                        $filmList = $filmService->getFilmByAnnoMinoreUgualeDi($anno, false);
                        return $filmList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $fl) {
                            if (date_create($fl->getDataUscita()) <= date_create($anno . "/12/31")) {
                                array_push($stvAnno, $fl);
                            }
                        }
                        return $stvAnno;
                    }
                }
            }
            case "3":
                /** UGUALE A */
            {
                if ($flag == 1) {
                    if (empty($list)) {
                        $serieTvService = new SerieTvService();
                        $serieTvList = $serieTvService->getSerieTvByAnno($anno, false);
                        return $serieTvList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $stvl) {
                            if ((date_create($stvl->getDataUscita()) >= date_create($anno . "/1/01")) && (date_create($stvl->getDataUscita()) <= date_create($anno . "/12/31"))) {
                                array_push($stvAnno, $stvl);
                            }
                        }
                        return $stvAnno;
                    }
                } elseif ($flag == 0) {
                    if (empty($list)) {
                        $filmService = new FilmService();
                        $filmList = $filmService->getFilmByAnno($anno, false);
                        return $filmList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $fl) {
                            if ((date_create($fl->getDataUscita()) >= date_create($anno . "/1/01")) && (date_create($fl->getDataUscita()) <= date_create($anno . "/12/31"))) {
                                array_push($stvAnno, $fl);
                            }
                        }
                        return $stvAnno;
                    }
                }
            }
            case "4":
                /** MAGGIORE UGUALE DI */
            {
                if ($flag == 1) {
                    if (empty($list)) {
                        $serieTvService = new SerieTvService();
                        $serieTvList = $serieTvService->getSerieTvByAnnoMaggioreUgualeDi($anno, false);
                        return $serieTvList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $stvl) {
                            if (date_create($stvl->getDataUscita()) >= date_create($anno . "/1/01")) {
                                array_push($stvAnno, $stvl);
                            }
                        }
                        return $stvAnno;
                    }
                } elseif ($flag == 0) {
                    if (empty($list)) {
                        $filmService = new FilmService();
                        $filmList = $filmService->getFilmByAnnoMaggioreUgualeDi($anno, false);
                        return $filmList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $fl) {
                            if (date_create($fl->getDataUscita()) >= date_create($anno . "/1/01")) {
                                array_push($stvAnno, $fl);
                            }
                        }
                        return $stvAnno;
                    }
                }
            }
            case "5":
                /** MAGGIORE DI */
            {
                if ($flag == 1) {
                    if (empty($list)) {
                        $serieTvService = new SerieTvService();
                        $serieTvList = $serieTvService->getSerieTvByAnnoMaggioreDi($anno, false);
                        return $serieTvList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $stvl) {
                            if (date_create($stvl->getDataUscita()) > date_create($anno . "/12/31")) {
                                array_push($stvAnno, $stvl);
                            }
                        }
                        return $stvAnno;
                    }
                } elseif ($flag == 0) {
                    if (empty($list)) {
                        $filmService = new FilmService();
                        $filmList = $filmService->getFilmByAnnoMaggioreDi($anno, false);
                        return $filmList;
                    } else {
                        $stvAnno = array();
                        foreach ($list as $k => $fl) {
                            if (date_create($fl->getDataUscita()) > date_create($anno . "/12/31")) {
                                array_push($stvAnno, $fl);
                            }
                        }
                        return $stvAnno;
                    }
                }
            }
        } //chiusa switch
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function valutazioneStelle(string $stelle, string $comeCercare, int $flag, array $list)
    {
        switch ($comeCercare) {
            case "1":
                /** MINORE DI */
            {
                if ($flag == 1) {
                    $serieTvService = new SerieTvService();
                    if (empty($list)) {
                        $sTvList = $serieTvService->getAllSerieTv(false);
                        $serieTvList = array();
                        foreach ($sTvList as $k => $stv) {
                            if ($serieTvService->getMediaVotiSerieTv($stv) < (((int)$stelle) - 1)) {
                                array_push($serieTvList, $stv);
                            }
                        }
                        return $serieTvList;
                    } else {
                        $stvStelle = array();
                        foreach ($list as $ke => $stvl) {
                            if ($serieTvService->getMediaVotiSerieTv($stvl) < (((int)$stelle) - 1)) {
                                array_push($stvStelle, $stvl);
                            }
                        }
                        return $stvStelle;
                    }
                } elseif ($flag == 0) {
                    $filmService = new FilmService();
                    if (empty($list)) {
                        $fList = $filmService->getAllFilm(false);
                        $filmList = array();
                        foreach ($fList as $k => $f) {
                            if ($filmService->getMediaVotiFilm($f) < (((int)$stelle) - 1)) {
                                array_push($filmList, $f);
                            }
                        }
                        return $filmList;
                    } else {
                        $filmStelle = array();
                        foreach ($list as $ke => $fl) {
                            if ($filmService->getMediaVotiFilm($fl) < (((int)$stelle) - 1)) {
                                array_push($filmStelle, $fl);
                            }
                        }
                        return $filmStelle;
                    }
                }
            }
            case "2":
                /** MINORE E UGUALE A */
            {
                if ($flag == 1) {
                    $serieTvService = new SerieTvService();
                    if (empty($list)) {
                        $sTvList = $serieTvService->getAllSerieTv(false);
                        $serieTvList = array();
                        foreach ($sTvList as $k => $stv) {
                            if ($serieTvService->getMediaVotiSerieTv($stv) <= (((int)$stelle) - 1)) {
                                array_push($serieTvList, $stv);
                            }
                        }
                        return $serieTvList;
                    } else {
                        $stvStelle = array();
                        foreach ($list as $ke => $stvl) {
                            if ($serieTvService->getMediaVotiSerieTv($stvl) <= (((int)$stelle) - 1)) {
                                array_push($stvStelle, $stvl);
                            }
                        }
                        return $stvStelle;
                    }
                } elseif ($flag == 0) {
                    $filmService = new FilmService();
                    if (empty($list)) {
                        $fList = $filmService->getAllFilm(false);
                        $filmList = array();
                        foreach ($fList as $k => $f) {
                            if ($filmService->getMediaVotiFilm($f) <= (((int)$stelle) - 1)) {
                                array_push($filmList, $f);
                            }
                        }
                        return $filmList;
                    } else {
                        $filmStelle = array();
                        foreach ($list as $ke => $fl) {
                            if ($filmService->getMediaVotiFilm($fl) <= (((int)$stelle) - 1)) {
                                array_push($filmStelle, $fl);
                            }
                        }
                        return $filmStelle;
                    }
                }
            }
            case "3":
                /** UGUALE A */
            {
                if ($flag == 1) {
                    $serieTvService = new SerieTvService();
                    if (empty($list)) {
                        $sTvList = $serieTvService->getAllSerieTv(false);
                        $serieTvList = array();
                        foreach ($sTvList as $k => $stv) {
                            if ($serieTvService->getMediaVotiSerieTv($stv) == (((int)$stelle) - 1)) {
                                array_push($serieTvList, $stv);
                            }
                        }
                        return $serieTvList;
                    } else {
                        $stvStelle = array();
                        foreach ($list as $ke => $stvl) {
                            if ($serieTvService->getMediaVotiSerieTv($stvl) == (((int)$stelle) - 1)) {
                                array_push($stvStelle, $stvl);
                            }
                        }
                        return $stvStelle;
                    }
                } elseif ($flag == 0) {
                    $filmService = new FilmService();
                    if (empty($list)) {
                        $fList = $filmService->getAllFilm(false);
                        $filmList = array();
                        foreach ($fList as $k => $f) {
                            if ($filmService->getMediaVotiFilm($f) == (((int)$stelle) - 1)) {
                                array_push($filmList, $f);
                            }
                        }
                        return $filmList;
                    } else {
                        $filmStelle = array();
                        foreach ($list as $ke => $fl) {
                            if ($filmService->getMediaVotiFilm($fl) == (((int)$stelle) - 1)) {
                                array_push($filmStelle, $fl);
                            }
                        }
                        return $filmStelle;
                    }
                }
            }
            case "4":
                /** MAGGIORE E UGUALE A */
            {
                if ($flag == 1) {
                    $serieTvService = new SerieTvService();
                    if (empty($list)) {
                        $sTvList = $serieTvService->getAllSerieTv(false);
                        $serieTvList = array();
                        foreach ($sTvList as $k => $stv) {
                            if ($serieTvService->getMediaVotiSerieTv($stv) >= (((int)$stelle) - 1)) {
                                array_push($serieTvList, $stv);
                            }
                        }
                        return $serieTvList;
                    } else {
                        $stvStelle = array();
                        foreach ($list as $ke => $stvl) {
                            if ($serieTvService->getMediaVotiSerieTv($stvl) >= (((int)$stelle) - 1)) {
                                array_push($stvStelle, $stvl);
                            }
                        }
                        return $stvStelle;
                    }
                } elseif ($flag == 0) {
                    $filmService = new FilmService();
                    if (empty($list)) {
                        $fList = $filmService->getAllFilm(false);
                        $filmList = array();
                        foreach ($fList as $k => $f) {
                            if ($filmService->getMediaVotiFilm($f) >= (((int)$stelle) - 1)) {
                                array_push($filmList, $f);
                            }
                        }
                        return $filmList;
                    } else {
                        $filmStelle = array();
                        foreach ($list as $ke => $fl) {
                            if ($filmService->getMediaVotiFilm($fl) >= (((int)$stelle) - 1)) {
                                array_push($filmStelle, $fl);
                            }
                        }
                        return $filmStelle;
                    }
                }
            }
            case "5":
                /** MAGGIORE DI */
            {
                if ($flag == 1) {
                    $serieTvService = new SerieTvService();
                    if (empty($list)) {
                        $sTvList = $serieTvService->getAllSerieTv(false);
                        $serieTvList = array();
                        foreach ($sTvList as $k => $stv) {
                            if ($serieTvService->getMediaVotiSerieTv($stv) > (((int)$stelle) - 1)) {
                                array_push($serieTvList, $stv);
                            }
                        }
                        return $serieTvList;
                    } else {
                        $stvStelle = array();
                        foreach ($list as $ke => $stvl) {
                            if ($serieTvService->getMediaVotiSerieTv($stvl) > (((int)$stelle) - 1)) {
                                array_push($stvStelle, $stvl);
                            }
                        }
                        return $stvStelle;
                    }
                } elseif ($flag == 0) {
                    $filmService = new FilmService();
                    if (empty($list)) {
                        $fList = $filmService->getAllFilm(false);
                        $filmList = array();
                        foreach ($fList as $k => $f) {
                            if ($filmService->getMediaVotiFilm($f) > (((int)$stelle) - 1)) {
                                array_push($filmList, $f);
                            }
                        }
                        return $filmList;
                    } else {
                        $filmStelle = array();
                        foreach ($list as $ke => $fl) {
                            if ($filmService->getMediaVotiFilm($fl) > (((int)$stelle) - 1)) {
                                array_push($filmStelle, $fl);
                            }
                        }
                        return $filmStelle;
                    }
                }
            }
        } // FINE SWITCH
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function vittoriaPremio(string $premio, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $serieTvService = new SerieTvService();
                $serieTvList = $serieTvService->getSerieTvByPremio((int)$premio, false);
                return $serieTvList;
            } else {
                $serieTvList = array();
                foreach ($list as $k => $stv) {
                    foreach ($stv->getNominations() as $ke => $nomination) {
                        if ($nomination->getCategoriaPremio()->getPremio()->getIdPremio() == $premio) {
                            array_push($serieTvList, $stv);
                        }
                    }
                }
                return $serieTvList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmService = new FilmService();
                $filmList = $filmService->getFilmByPremio((int)$premio, false);
                return $filmList;
            } else {
                $filmList = array();
                foreach ($list as $k => $f) {
                    foreach ($f->getNominations() as $ke => $nomination) {
                        if ($nomination->getCategoriaPremio()->getPremio()->getIdPremio() == $premio) {
                            array_push($filmList, $f);
                        }
                    }
                }
                return $filmList;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function forGenere(string $genere, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $serieTvService = new SerieTvService();
                $serieTvList = $serieTvService->getSerieTvByGenere((int)$genere, false);
                return $serieTvList;
            } else {
                $serieTvList = array();
                foreach ($list as $k => $stv) {
                    foreach ($stv->getGeneri() as $ke => $gen) {
                        if ($gen->getIdGenere() == $genere) {
                            array_push($serieTvList, $stv);
                        }
                    }
                }
                return $serieTvList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmService = new FilmService();
                $filmList = $filmService->getFilmByGenere((int)$genere, false);
                return $filmList;
            } else {
                $filmList = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getGeneri() as $ke => $gen) {
                        if ($gen->getIdGenere() == $genere) {
                            array_push($filmList, $fi);
                        }
                    }
                }
                return $filmList;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function forDistributore(string $distributore, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $serieTvService = new SerieTvService();
                $serieTvList = $serieTvService->getSerieTvByDistributore((int)$distributore, false);
                return $serieTvList;
            } else {
                $serieTvList = array();
                foreach ($list as $k => $stv) {
                    foreach ($stv->getDistributori() as $ke => $dis) {
                        if ($dis->getIdDistributore() == $distributore) {
                            array_push($serieTvList, $stv);
                        }
                    }
                }
                return $serieTvList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmService = new FilmService();
                $filmList = $filmService->getFilmByDistributore((int)$distributore, false);
                return $filmList;
            } else {
                $filmList = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getDistributori() as $ke => $dis) {
                        if ($dis->getIdDistributore() == $distributore) {
                            array_push($filmList, $fi);
                        }
                    }
                }
                return $filmList;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function forPersone(string $persona, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $serieTvService = new SerieTvService();
                $serieTvList = $serieTvService->getSerieTvByNomeCognomeFilmmaker($persona, false);
                return $serieTvList;
            } else {
                $serieTvList = array();
                $a = -1;
                foreach ($list as $k => $stv) {
                    foreach ($stv->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if ($a != $k) {
                                $a = $k;
                                array_push($serieTvList, $stv);
                            }
                        }
                    }
                }
                return $serieTvList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmService = new FilmService();
                $filmList = $filmService->getFilmByNomeCognomeFilmmaker($persona, false);
                return $filmList;
            } else {
                $filmList = array();
                $a = -1;
                foreach ($list as $k => $fi) {
                    foreach ($fi->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if ($a != $k) {
                                $a = $k;
                                array_push($filmList, $fi);
                            }
                        }
                    }
                }
                return $filmList;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV */
    private
    function forRuolo(string $persona, string $ruolo, int $flag, array $list)
    {
        if ($flag == 1) {
            $serieTvList = array();
            foreach ($list as $k => $stv) {
                foreach ($stv->getRuoli() as $ke => $pe) {
                    if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                        if ($pe->getTipologia() == $ruolo) {
                            array_push($serieTvList, $stv);
                        }
                    }
                }
            }
            return $serieTvList;
        } elseif ($flag == 0) {
            $filmList = array();
            foreach ($list as $k => $fi) {
                foreach ($fi->getRuoli() as $ke => $pe) {
                    if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                        if ($pe->getTipologia() == $ruolo) {
                            array_push($filmList, $fi);
                        }
                    }
                }
            }
            return $filmList;
        }
    }

    private
    function ordinaPer(string $ordina, array $list)
    {
        switch ($ordina) {
            case "1":
                /** TITOLO */
            {
                uasort($list, fn($a, $b) => strcmp($a->getTitolo(), $b->getTitolo()));
                return $list;
            }
            case "2":
                /** Anno (decrescente) */
            {
                return $list;
            }
            case "3":
                /** Anno (crescente) */
            {
                $reverseList = array_reverse($list);
                return $reverseList;
            }
            case "4":
                /** Stelle (decrescente) */
            {
                uasort($list, fn($a, $b) => ($a->mediaVotoRedCarpet > $b->mediaVotoRedCarpet) ? -1 : 1);
                return $list;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV - RICERCA PERSONE */
    private
    function forPersonaPersona(string $persona, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $filmmakerService = new FilmMakerService();
                $personaList = $filmmakerService->getFilmmakerSerieTvByNomeCognome($persona);
                return $personaList;
            } else {
                $personaList = array();
                $a = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if (!in_array($pe->getFilmMaker()->getIdFilmMaker(), $a)) {
                                array_push($a, $pe->getFilmMaker()->getIdFilmMaker());
                                array_push($personaList, $pe->getFilmMaker());
                            }
                        }
                    }
                }
                return $personaList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmmakerService = new FilmMakerService();
                $personaList = $filmmakerService->getFilmmakerFilmByNomeCognome($persona);
                return $personaList;
            } else {
                $personaList = array();
                $a = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if (!in_array($pe->getFilmMaker()->getIdFilmMaker(), $a)) {
                                array_push($a, $pe->getFilmMaker()->getIdFilmMaker());
                                array_push($personaList, $pe->getFilmMaker());
                            }
                        }
                    }
                }
                return $personaList;
            }
        }
    }

    /** FLAG: 0 FILM, 1 SERIE TV - RICERCA PERSONE, RUOLO */
    private
    function forPersonaRuolo(string $persona, string $ruolo, int $flag, array $list)
    {
        if ($flag == 1) {
            if (empty($list)) {
                $filmmakerService = new FilmMakerService();
                $personaList = $filmmakerService->getFilmmakerSerieTvByNomeCognomeRuolo($persona, (int)$ruolo);
                return $personaList;
            } else {
                $personaList = array();
                $a = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if ($pe->getTipologia() == $ruolo) {
                                if (!in_array($pe->getFilmMaker()->getIdFilmMaker(), $a)) {
                                    array_push($a, $pe->getFilmMaker()->getIdFilmMaker());
                                    array_push($personaList, $pe->getFilmMaker());
                                }
                            }
                        }
                    }
                }
                return $personaList;
            }
        } elseif ($flag == 0) {
            if (empty($list)) {
                $filmmakerService = new FilmMakerService();
                $personaList = $filmmakerService->getFilmmakerFilmByNomeCognomeRuolo($persona, (int)$ruolo);
                return $personaList;
            } else {
                $personaList = array();
                $a = array();
                foreach ($list as $k => $fi) {
                    foreach ($fi->getRuoli() as $ke => $pe) {
                        if ((str_contains(strtoupper($pe->getFilmMaker()->getNome()), strtoupper($persona))) || (str_contains(strtoupper($pe->getFilmMaker()->getCognome()), strtoupper($persona)))) {
                            if ($pe->getTipologia() == $ruolo) {
                                if (!in_array($pe->getFilmMaker()->getIdFilmMaker(), $a)) {
                                    array_push($a, $pe->getFilmMaker()->getIdFilmMaker());
                                    array_push($personaList, $pe->getFilmMaker());
                                }
                            }
                        }
                    }
                }
                return $personaList;
            }
        }
    }

    public function showRisultatiRicerca(RequestInput $requestInput, View $view)
    {
        $data = $requestInput->getQueryString();
        $titlepage = 'Risultati ricerca';
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $filmmakerService = new FilmMakerService();
        $filmList = $filmService->getFilmByTitoloOrTitoloOrig($data['search'], false);
        $serieTvList = $serieTvService->getSerieTvByTitoloOrTitoloOrig($data['search'], false);
        $personaList = $filmmakerService->getFilmmakerByNomeOrCognome($data['search']);


        /**  paginazione */
        $elemTot = count($filmList)+count($serieTvList)+count($personaList);
        $numeroPagine = ceil($elemTot / 10);
        $currentPage = 1;
        if (!empty($filmList)) {
            if (count($filmList) >= 10) {
                $filmList = array_slice($filmList, 0, 10);
                $serieTvList = array();
                $personaList = array();
            } elseif (!empty($serieTvList)) {
                $nfilm = count($filmList);
                if (count($serieTvList) >= (10 - $nfilm)) {
                    $serieTvList = array_slice($serieTvList, 0, 10 - $nfilm);
                    $personaList = array();
                } elseif (!empty($personaList)) {
                    $nserietv = count($serieTvList);
                    if (count($personaList) >= (10 - ($nserietv + $nfilm))) {
                        $personaList = array_slice($personaList, 0, 10 - ($nserietv + $nfilm));
                    }
                }
            } elseif (!empty($personaList)) {
                $nfilm = count($filmList);
                if (count($personaList) >= (10 - $nfilm)) {
                    $personaList = array_slice($personaList, 0, 10 - $nfilm);
                }
            }
        } elseif (!empty($serieTvList)) {
            if (count($serieTvList) >= 10 ) {
                $serieTvList = array_slice($serieTvList, 0, 10);
                $personaList = array();
            } elseif (!empty($personaList)) {
                $nserietv = count($serieTvList);
                if (count($personaList) >= (10 - $nserietv)) {
                    $personaList = array_slice($personaList, 0, 10 - $nserietv);
                }
            }
        } elseif (!empty($personaList)) {
            if (count($personaList) >= 10) {
                $personaList = array_slice($personaList, 0, 10);
            }
        }

        foreach ($filmList as $k => $fl) {
            if ($filmService->maggiorediCinqueVoti($fl)) {
                $fl->mediaVotoUtenti = number_format($filmService->getMediaVotiUtenti($fl), 2, '.', '');
                $fl->mediaVotoCritici = number_format($filmService->getMediaVotiCritici($fl), 2, '.', '');
                $fl->mediaVotoRedCarpet = number_format($filmService->getMediaVotiFilm($fl), 2, '.', '');
            } else {
                $fl->mediaVotoUtenti = 0;
                $fl->mediaVotoCritici = 0;
                $fl->mediaVotoRedCarpet = 0;
            }
            /** BLOCCO regista, attori */
            $registi = array();
            $attori = array();
            foreach ($fl->getRuoli() as $ke => $ruolo) {
                if ($ruolo->getTipologia() == 2) {
                    array_push($registi, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                } elseif ($ruolo->getTipologia() == 1) {
                    array_push($attori, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                }
            }
            $fl->registi = $registi;
            $fl->attori = array_slice($attori, 0, 5);
        }
        /** ORDINA PER */
        $filmList = $this->ordinaPer('1', $filmList);

        foreach ($serieTvList as $k => $stv) {
            if ($serieTvService->maggiorediCinqueVoti($stv)) {
                $stv->mediaVotoUtenti = number_format($serieTvService->mediaVotiUtenti($stv), 2, '.', '');
                $stv->mediaVotoCritici = number_format($serieTvService->mediaVotiCritici($stv), 2, '.', '');
                $stv->mediaVotoRedCarpet = number_format($serieTvService->getMediaVotiSerieTv($stv), 2, '.', '');
            } else {
                $stv->mediaVotoUtenti = 0;
                $stv->mediaVotoCritici = 0;
                $stv->mediaVotoRedCarpet = 0;
            }
            $stv->stagioniTotali = $serieTvService->getCountStagioniBySerieTv($stv);
            $stv->episodiTotali = $serieTvService->getCountEpisodiBySerieTv($stv);
            $stv->durataEpisodio = $serieTvService->getDurataEpisodioBySerieTv($stv);
            /** BLOCCO regista, attori */
            $registi = array();
            $attori = array();
            foreach ($stv->getRuoli() as $ke => $ruolo) {
                if ($ruolo->getTipologia() == 2) {
                    array_push($registi, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                } elseif ($ruolo->getTipologia() == 1) {
                    array_push($attori, [$ruolo->getFilmMaker()->getIdFilmMaker(), $ruolo->getFilmMaker()->getNome() . " " . $ruolo->getFilmMaker()->getCognome()]);
                }
            }
            $stv->registi = $registi;
            $stv->attori = array_slice($attori, 0, 5);
        }
        /** ORDINA PER */
        $serieTvList = $this->ordinaPer('1', $serieTvList);

        $ruoloService = new RuoloService();
        foreach ($personaList as $persona) {
            $ruoli = array();
            $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
            foreach ($ru as $ruolo){
                if(!in_array($ruolo->getTipologia(), $ruoli)){
                    array_push($ruoli, $ruolo->getTipologia());
                }
            }
            $persona->ruoli = $ruoli;
        }

        $nominationService = new NominationService();
        foreach ($personaList as $k => $persona) {
            $premioCount = array();
            $premi = array();
            $nominationsPremi = $nominationService->getNominationByIdFilmmaker($persona->getIdFilmMaker(), false);
            foreach ($nominationsPremi as $ke => $nomination) {
                if ($nomination->getStato() == 1 && !in_array($nomination->getCategoriaPremio()->getPremio()->getNome(), $premi)) {
                    array_push($premi, $nomination->getCategoriaPremio()->getPremio()->getNome());
                }
            }
            foreach ($premi as $key => $premio) {
                $cont = 0;
                foreach ($nominationsPremi as $ke => $nomination) {
                    if ($nomination->getStato() == 1 && $nomination->getCategoriaPremio()->getPremio()->getNome() == $premio) {
                        $cont++;
                    }
                }
                array_push($premioCount, $cont);
            }
            $persona->premi = $premi;
            $persona->premioCount = $premioCount;
        }
        /** ORDINA PER NOME */
        uasort($personaList, fn($a, $b) => strcmp($a->getNome(), $b->getNome()));

        $queryString = 'titolo_ita_orig='.$data['search'].'&anno_distribuzione='.''.'&stelle='.''.'&premi='.''.'&genere='.''.'&distributore='.''.'&persone='.$data['search'].'&ruolo='.''.'&match='.'7'.'&ordina='.'1';

        $pagine = self::paginazione($currentPage, $numeroPagine);

        return $view('front.risultati_ricerca', compact('titlepage', 'filmList', 'serieTvList', 'personaList', 'elemTot', 'numeroPagine', 'currentPage', 'queryString', 'pagine'));
    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}