<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\FilmMakerService;
use App\Services\FilmService;
use App\Services\NominationService;
use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class FilmmakerShowController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $args = $requestInput->getArguments();
        $filmmakerService = new FilmMakerService();
        $filmmaker = $filmmakerService->getFilmakerById($args['idFilmmaker']);
        $filmService = new FilmService();
        try {
            $film_sliders = $filmService->getFilmByFilmmakerCarosello($filmmaker);
        } catch (\Exception $ex) {
            $film_sliders = array();
        }

        $serieTvService = new SerieTvService();
        try {
            $serieTv_sliders = $serieTvService->getSerieTvByFilmmakerCarosello($filmmaker);
        } catch (\Exception $ex) {
            $serieTv_sliders = array();
        }
        $sliders = array_merge($film_sliders, $serieTv_sliders);
        uasort($sliders, fn($a, $b) => (date($a->getDataUscita()) > date($b->getDataUscita())) ? -1 : 1);
        return $view('front.filmmaker_show', ['titlepage' => $filmmaker->getNome() . ' ' . $filmmaker->getCognome(), "filmmaker" => $filmmaker, "sliders" => $sliders]);
    }

    public function components(RequestInput $requestInput, View $view)
    {
        $args = $requestInput->getArguments();
        $filmmakerService = new FilmMakerService();
        $filmmaker = $filmmakerService->getFilmakerById($args['idFilmmaker']);
        switch ($args['components']) {
            case "biografia":
                return $view('front.components_filmmaker.biografia', ["filmmaker" => $filmmaker]);
            case "filmografia":
            {
                $filmService = new FilmService();
                $serieTvService = new SerieTvService();
                $listFilm = $filmService->getFilmByFilmmaker($filmmaker);
                $listSerietv = $serieTvService->getSerieTvByFilmmaker($filmmaker);
                foreach ($listFilm as $film) {
                    $cast = array();
                    $regista = array();
                    foreach ($film->getRuoli() as $ruolo) {
                        if ($ruolo->getTipologia() == '1') {
                            if (count($cast) < 5) {
                                array_push($cast, $ruolo);
                            }
                        }
                        if ($ruolo->getTipologia() == '2') {
                            array_push($regista, $ruolo);
                        }
                        if (count($regista) === 2 && count($cast) === 4) {
                            break;
                        }
                    }
                    $film->cast = $cast;
                    $film->regista = $regista;
                    $film->valutazione = $filmService->getMediaVotiFilm($film);
                }
                foreach ($listSerietv as $serietv) {
                    $cast = array();
                    $regista = array();
                    foreach ($serietv->getRuoli() as $ruolo) {
                        if ($ruolo->getTipologia() == '1') {
                            if (count($cast) < 5) {
                                array_push($cast, $ruolo);
                            }
                        }
                        if ($ruolo->getTipologia() == '2') {
                            array_push($regista, $ruolo);
                        }
                        if (count($regista) === 2 && count($cast) === 4) {
                            break;
                        }
                    }
                    $serietv->cast = $cast;
                    $serietv->regista = $regista;
                    $serietv->valutazione = $serieTvService->getMediaVotiSerieTv($serietv);
                }
                return $view('front.components_filmmaker.filmografia', ["listFilm" => $listFilm, "listSerietv" => $listSerietv]);
            }
            case "premi":
            {
                $nominationService = new NominationService();
                $nominations = $nominationService->getNominationByIdFilmmaker($filmmaker->getIdFilmMaker());

                $array_by_genere = array();//$nominations = array(new Nomination());
                foreach ($nominations as $kn => $nomination) {
                    if (key_exists($nomination->getCategoriaPremio()->getPremio()->getIdPremio(), $array_by_genere)) {
                        if (key_exists(date("Y", strtotime($nomination->getDataPremiazione())), $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()])) {
                            array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                        } else {
                            $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))] = array();
                            array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                        }
                    } else {
                        $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()] = array();
                        $array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))] = array();
                        array_push($array_by_genere[$nomination->getCategoriaPremio()->getPremio()->getIdPremio()][date("Y", strtotime($nomination->getDataPremiazione()))], $nomination);
                    }
                }
                return $view('front.components_filmmaker.premi', ["array_by_genere" => $array_by_genere]);
            }
            case "foto":
            {
                $listFoto = array();
                $row = array();
                foreach ($filmmaker->getImmaginiFilmMakers() as $img) {
                    if (count($row) < 3) {
                        array_push($row, $img);
                    } else {
                        array_push($row, $img);
                        array_push($listFoto, $row);
                        $row = array();
                    }
                }
                if (count($row) <= 3) {
                    array_push($listFoto, $row);
                }
                return $view('front.components_filmmaker.foto', ["listFoto" => $listFoto]);
            }
            default:
                break;
        }
        return $view('front.components_filmmaker.biografia', ["filmmaker" => $filmmaker]);
    }

}