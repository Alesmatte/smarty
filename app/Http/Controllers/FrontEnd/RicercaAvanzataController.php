<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\DistributoreService;
use App\Services\GenereService;
use App\Services\PremioService;
use App\Support\RequestInput;
use App\Support\View;

class RicercaAvanzataController
{
    public function show(RequestInput $requestInput, View $view){
        $titlepage = 'Ricerca avanzata';
        $distributoreService = new DistributoreService();
        $genereService = new GenereService();
        $premioService = new PremioService();
        $distributoreList = $distributoreService->getAllDistrucutore();
        $genereList = $genereService->getAllGenere();
        $premioList = $premioService->getAllPremio();
        return $view('front.ricerca_avanzata', compact('titlepage', 'distributoreList', 'genereList', 'premioList'));
    }
}