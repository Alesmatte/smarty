<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\TipologiaPremioService;
use App\Services\NominationService;
use App\Services\ImmagineService;
use App\Services\SerieTvService;
use App\Services\PremioService;
use App\Services\FilmService;
use App\Support\RequestInput;
use App\Support\View;

class PremioController
{
    public function show(RequestInput $requestInput, View $view)
    {

        $serieTvService = new SerieTvService();
        $premioService = new PremioService();
        $filmService = new FilmService();
        $immagineService = new ImmagineService();
        $idPremio = $requestInput->getArguments()['idPremio'];
        $titlepage = $premioService->getPremioById($idPremio)->getNome();
        $edizione = $requestInput->getArguments()['edizione'];
        $awardEd = $requestInput->getArguments()['edizione'];
        $award = $premioService->getPremioById($idPremio);
        $immaginiSliderList = $immagineService->getImgByTipologia(9);

        $listaPremi = $premioService->getAllPremio();

        $ultimiFilmAndSerieTv = $filmService->getUltimicinqueFilm();
        foreach ($serieTvService->getUltimecinqueSerieTv() as $serieTv) {
            array_push($ultimiFilmAndSerieTv, $serieTv);
        }
        uasort($ultimiFilmAndSerieTv, fn($a, $b) => (date($a->getDataUscita()) > date($b->getDataUscita())) ? -1 : 1);
        $ultimiFilmAndSerieTv = array_slice($ultimiFilmAndSerieTv, 0, 5);

        $filmPiuVotato = $filmService->getSingoloFilmPiuVotato();
        $serieTvPiuVotata = $serieTvService->getSingolaSerieTvPiuVotata();
        if ($filmPiuVotato->getIdFilm() === null) {
            $entitaPiuVotata = $serieTvPiuVotata;
        } elseif ($serieTvPiuVotata->getIdSerieTv() === null) {
            $entitaPiuVotata = $filmPiuVotato;
        } elseif ($filmPiuVotato->getIdFilm() === null && $serieTvPiuVotata->getIdSerieTv() === null) {
            $entitaPiuVotata = null;
        } else {
            if ($filmPiuVotato->mediaVoto > $serieTvPiuVotata->mediaVoto) {
                $entitaPiuVotata = $filmPiuVotato;
            } else {
                $entitaPiuVotata = $serieTvPiuVotata;
            }
        }

        return $view('front.premio', compact('award', 'awardEd', 'idPremio', 'edizione', 'immaginiSliderList', 'titlepage', 'listaPremi', 'ultimiFilmAndSerieTv', 'entitaPiuVotata'));
    }

    public function showComponent(RequestInput $requestInput, View $view)
    {
        $args = $requestInput->getArguments();
        $nominationService = new NominationService();
        $tipologiaPremioService = new TipologiaPremioService();
        $nominationListTot = $nominationService->getNominationByNomePremioAndAnno($requestInput->getArguments()['idPremio'], $requestInput->getArguments()['edizione']);
        //dd($nominationListTot);
        $nominationList = array();
        if ($args['component'] == 'nomination') {
            foreach ($nominationListTot as $item) {
                if ($item->getStato() == 0) {
                    array_push($nominationList, $item);
                }
            }
            $listaTipologie = $tipologiaPremioService->getAllTipologiaPremio();
            $matriceNomination = array();
            foreach ($listaTipologie as $key => $tipologia) {
                $matriceNomination[$key] = array();
                $matriceNomination[$key][0] = $tipologia->getNome();
                $matriceNomination[$key][1] = array();
                foreach ($nominationList as $nomina) {
                    if ($tipologia->getidTipologiaPremio() == $nomina->getCategoriaPremio()->getTipologia()->getidTipologiaPremio() && $nomina->getStato() == 0) {
                        array_push($matriceNomination[$key][1], $nomina);
                    }
                }
            }
            //dd($matriceNomination[0][1]);
            return $view('front.components_premio.nomination', ["matriceNomination" => $matriceNomination, "nominationList" => $nominationList]);
        } else {
            foreach ($nominationListTot as $item) {
                if ($item->getStato() == 1) {
                    array_push($nominationList, $item);
                }
            }
            return $view('front.components_premio.vinti', ["nominationList" => $nominationList]);
        }
    }
}