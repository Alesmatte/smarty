<?php


namespace App\Http\Controllers\FrontEnd;


use App\Http\Request\StoreImpostazioniAccountUserAdmin;
use App\Http\Request\StoreImpostazioniAccountUserCritica;
use App\Http\Request\StoreImpostazioniAccountUserNormal;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class ImpostazioniAccount
{
    public function show(View $view){
        $userService = new UserService();
        $user = $userService->getUser();
        return $view('front.impostazioni_account', ['titlepage'=>'Impostazioni', 'user'=>$user]);
    }

    public function storeNormal(StoreImpostazioniAccountUserNormal $impostazioniAccountUserNormal){
        if($impostazioniAccountUserNormal->failed()) return redirect('/impostazioni_account');
        $userService = new UserService();
        $userOrigial = $userService->getUser();
        $user = $userService->updateObjUserNormal($impostazioniAccountUserNormal, $userOrigial);
        $result = $userService->updateUser($user);
        if($result['resultUser'] == 1 || $result['resultImg'] == 1 || $userOrigial->getAvatar() != $user->getAvatar()){
            $userService->refreshSessionUser($user);
            $session = app()->resolve(\Boot\Foundation\Http\Session::class);
            $session->flash()->set('updateResult', 'ok');
        }
        return redirect('/impostazioni_account');
    }


    public function storeCritica(StoreImpostazioniAccountUserCritica $impostazioniAccountUserCritica){
        if($impostazioniAccountUserCritica->failed())  return redirect('/impostazioni_account');
        $userService = new UserService();
        $userOrigial = $userService->getUser();
        $user = $userService->updateObjUserCritica($impostazioniAccountUserCritica, $userOrigial);
        $result = $userService->updateUser($user);
        if($result['resultUser'] == 1 || $result['resultImg'] == 1 || $userOrigial->getAvatar() != $user->getAvatar()){
            $userService->refreshSessionUser($user);
            $session = app()->resolve(\Boot\Foundation\Http\Session::class);
            $session->flash()->set('updateResult', 'ok');
        }
        return redirect('/impostazioni_account');
    }

    public function storeAdmin(StoreImpostazioniAccountUserAdmin $impostazioniAccountUserAdmin){
        if($impostazioniAccountUserAdmin->failed()) return redirect('/impostazioni_account');
        $userService = new UserService();
        $userOrigial = $userService->getUser();
        $user = $userService->updateObjUserAdmin($impostazioniAccountUserAdmin, $userOrigial);
        $result = $userService->updateUser($user);
        if($result['resultUser'] == 1 || $result['resultImg'] == 1 || $userOrigial->getAvatar() != $user->getAvatar()){
            $userService->refreshSessionUser($user);
            $session = app()->resolve(\Boot\Foundation\Http\Session::class);
            $session->flash()->set('updateResult', 'ok');
        }
        return redirect('/impostazioni_account');
    }




}