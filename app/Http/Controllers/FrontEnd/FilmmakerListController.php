<?php


namespace App\Http\Controllers\FrontEnd;


use App\Services\FilmMakerService;
use App\Services\NominationService;
use App\Services\RuoloService;
use App\Support\RequestInput;
use App\Support\View;

class FilmmakerListController
{
    public function show(RequestInput $requestInput, View $view){
        $titlepage = 'Lista filmmaker';
        $filmmakerService = new FilmMakerService();
        $parameters = $requestInput->getArguments();
        $tipologia = $parameters['tipologia'];
        if (isset($parameters['offset'])){
            $personaList = $filmmakerService->getFilmmakerByTipologiaFilmmaker($tipologia, 10, (($parameters['offset'])-1) * 10);
            $realPersonaList = array();
            foreach ($personaList as $filmmaker){
                if(!in_array($filmmaker, $realPersonaList)){
                    array_push($realPersonaList, $filmmaker);
                }
            }
            $personaList = $realPersonaList;
            $totFilmmaker = count($personaList);
            $currentPage = $parameters['offset'];
            $numeroPagine = ceil(count($personaList) / 10);
            $personaSplit = array_slice($personaList, (($parameters['offset'])-1) * 10, 10);
            $personaList = $personaSplit;

        } else {
            $personaList = $filmmakerService->getFilmmakerByTipologiaFilmmaker($tipologia, 10, 0);
            $realPersonaList = array();
            foreach ($personaList as $filmmaker){
                if(!in_array($filmmaker, $realPersonaList)){
                    array_push($realPersonaList, $filmmaker);
                }
            }
            $personaList = $realPersonaList;
            $currentPage = 1;
            $totFilmmaker = count($personaList);
            $numeroPagine = ceil(count($personaList) / 10);
            $personaSplit = array_slice($personaList, 0, 10);
            $personaList = $personaSplit;

        }
        $ruoloService = new RuoloService();
        foreach ($personaList as $persona) {
            $ruoli = array();
            $ru = $ruoloService->getRuoliByIdFilmmaker($persona->getIdFilmMaker());
            foreach ($ru as $ruolo){
                if(!in_array($ruolo->getTipologia(), $ruoli)){
                    array_push($ruoli, $ruolo->getTipologia());
                }
            }
            $persona->ruoli = $ruoli;
        }
        /** AGGIUNTA PREMI->QUANTITA' */
        $nominationService = new NominationService();
        foreach ($personaList as $k => $persona) {
            $premioCount = array();
            $premi = array();
            $nominationsPremi = $nominationService->getNominationByIdFilmmaker($persona->getIdFilmMaker(), false);
            foreach ($nominationsPremi as $ke => $nomination) {
                if ($nomination->getStato() == 1 && !in_array($nomination->getCategoriaPremio()->getPremio()->getNome(), $premi)) {
                    array_push($premi, $nomination->getCategoriaPremio()->getPremio()->getNome());
                }
            }
            foreach ($premi as $key => $premio) {
                $cont = 0;
                foreach ($nominationsPremi as $ke => $nomination) {
                    if ($nomination->getStato() == 1 && $nomination->getCategoriaPremio()->getPremio()->getNome() == $premio) {
                        $cont++;
                    }
                }
                array_push($premioCount, $cont);
            }
            $persona->premi = $premi;
            $persona->premioCount = $premioCount;
        }
        /** Titolo */
        switch ($tipologia){
            case '0': {
                $titolo = "Altro";
                break;
            }
            case '1': {
                $titolo = "Attori";
                break;
            }
            case '2': {
                $titolo = "Registi";
                break;
            }
            case '3': {
                $titolo = "Distributori";
                break;
            }
            case '4': {
                $titolo = "Produttori";
                break;
            }
            case '5': {
                $titolo = "Sceneggiatori";
                break;
            }
            case '6': {
                $titolo = "Fotografi";
                break;
            }
            case '7': {
                $titolo = "Montatori";
                break;
            }
            case '8': {
                $titolo = "Compositori";
                break;
            }
            case '9': {
                $titolo = "Scenografi";
                break;
            }
            case '10': {
                $titolo = "Costumisti";
                break;
            }
            case '11': {
                $titolo = "Supervisore SFX";
                break;
            }
            case '12': {
                $titolo = "Art Directors";
                break;
            }
            case '13': {
                $titolo = "Truccatori";
                break;
            }
            default: {
                $titolo = "Filmmakers";
                break;
            }
        }

        $pagine = self::paginazione($currentPage, $numeroPagine);

        return $view('front.filmmaker_list', compact('titlepage', 'totFilmmaker', 'numeroPagine', 'currentPage', 'personaList', 'titolo', 'tipologia', 'pagine'));


    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}