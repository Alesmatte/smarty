<?php


namespace App\Http\Controllers;


use App\Http\Request\StoreUserCritica;
use App\Http\Request\StoreUserNormalRequest;
use App\Services\UserService;
use App\Support\View;

class RegistrationController
{
    public function userNormalShow(View $view){
        return $view('front.registrazione.registrazioneUtenteNormale');
    }

    public function userNormal(StoreUserNormalRequest $input, View $view){
        if ($input->failed())
            return $view('front.registrazione.registrazioneUtenteNormale');
        $userService = new UserService();
        // crea utente
        $user = $userService->createUserNormal($input);
        // salva utente nel db
        $userService->storeNormale($user);
        return redirect('/');
    }

    public function userCriticaShow(View $view){
        return $view('front.registrazione.registrazioneCritico');
    }

    public function userCritica(StoreUserCritica $input, View $view){
        if ($input->failed()) return $view('front.registrazione.registrazioneCritico');
        $userService = new UserService();
        // creo utente
        $user = $userService->createUserCritica($input);
        // salvo nel db l'utente
        $userService->storeCritico($user);
        return redirect('/');
    }
}