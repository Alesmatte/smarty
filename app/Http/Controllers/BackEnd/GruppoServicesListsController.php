<?php


namespace App\Http\Controllers\BackEnd;

use App\Services\GroupsService;
use App\Services\ServicesService;
use App\Support\RequestInput;
use App\Support\View;

class GruppoServicesListsController
{
    public function show(View $view) {

        $groupService = new GroupsService();
        $serviceServices = new ServicesService();
        $groupsList = $groupService->getAllgroups();

        foreach ($groupsList as $key => $group) {
            $groupsList[$key]->setServizi($serviceServices->getServicesByGroup($group));
        }

        $servicesList = $serviceServices->getAllServices();
        $matriceServicesGroups = array();
        foreach ($servicesList as $key => $services) {
            $matriceServicesGroups[$services->getIdServizio()] = $groupService->getGroupsByService($services);
        }

        return $view('back.groups-services', compact('groupsList', 'servicesList', 'matriceServicesGroups'));
    }

    public function matriceGruppiServizi(RequestInput $requestInput) {
        $data = $requestInput->all();
        $groupService = new GroupsService();
        $serviceServices = new ServicesService();
        $groupsList = $groupService->getAllgroups();
        $servicesList = $serviceServices->getAllServices();
        $nuoniGruppiServizi = array();
        foreach ($groupsList as $keyG=>$gruppo) {
            if ($gruppo->getIdGruppo() != 1 && $gruppo->getIdGruppo() != 2 && $gruppo->getIdGruppo() != 3) {
                $serviziDaTenere = array();
                $serviziDaCancellare = array();
                $nuoniGruppiServizi[$keyG] = $gruppo;
                foreach ($servicesList as $servizio) {
                    if (in_array($gruppo->getIdGruppo() . '-' . $servizio->getIdServizio(), $data)) {
                        array_push($serviziDaTenere, $servizio);
                    } else {
                        array_push($serviziDaCancellare, $servizio);
                    }
                }
                $nuoniGruppiServizi[$keyG]->setServizi($serviziDaTenere);
            }
        }
        foreach ($nuoniGruppiServizi as $gruppo) {
            if ($gruppo->getIdGruppo() != 1 && $gruppo->getIdGruppo() != 2 && $gruppo->getIdGruppo() != 3) {
                $groupService->deleteServicesHasGroupsByGruppo($gruppo);
                foreach ($gruppo->getServizi() as $servizio) {
                    $groupService->insertServicesHasGroupsByGruppo($gruppo, $servizio);
                }
            }
        }

        session()->flash()->set(
            'modMatrice', 'Salvato con successo!'
        );
        return redirect('/admin/groups_services');
    }

}