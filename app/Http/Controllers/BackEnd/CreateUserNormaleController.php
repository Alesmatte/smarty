<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreUserNormalRequest;
use App\Model\User;
use App\Services\GroupsService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class CreateUserNormaleController
{
    public function show(View $view) {
        $groupService = new  GroupsService();
        $grouplist = $groupService->getAllgroups();
        $tipologia = 1;
        return $view('back.create-normal-user', compact('grouplist', 'tipologia'));
    }

    public function store(StoreUserNormalRequest $inputUserNormale, RequestInput $requestInput, View $view) {
        if ($inputUserNormale->failed()) return $view('back.create-normal-user');
        $user = new User();
        $userService = new UserService();
        $user->setTipologiaUtenza(0);
        $userService->storeUsers($user, $requestInput, $userService, 'insert', 'back');

        session()->flash()->set(
            'succesMessage',
            $user->getNome() . ' ' . $user->getCognome() . ' creato con successo!'
        );
        return redirect('/admin/user_list');
    }
}