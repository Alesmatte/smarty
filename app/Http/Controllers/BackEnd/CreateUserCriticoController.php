<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreUserCritica;
use App\Model\User;
use App\Services\GroupsService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class CreateUserCriticoController
{
    public function show(View $view) {
        $groupService = new  GroupsService();
        $grouplist = $groupService->getAllgroups();
        $tipologia = 2;
        return $view('back.create-critico-user', compact('grouplist', 'tipologia'));
    }

    public function store(StoreUserCritica $storeUserCritica, RequestInput $requestInput, View $view) {
        if ($storeUserCritica->failed()) return $view('back.create-critico-user');
        $user = new User();
        $userService = new UserService();
        $user->setTipologiaUtenza(1);
        $userService->storeUsers($user, $requestInput, $userService, 'insert', 'back');

        session()->flash()->set(
            'succesMessage',
            $user->getNome() . ' ' . $user->getCognome() . ' Critico creato con successo!'
        );

        return redirect('/admin/user_list');
    }
}