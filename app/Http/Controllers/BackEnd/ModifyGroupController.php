<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreGroup;
use App\Model\Groups;
use App\Services\GroupsService;
use App\Services\ServicesService;
use App\Support\RequestInput;
use App\Support\View;

class ModifyGroupController
{

    public function show(RequestInput $requestInput, View $view) {
        $parameters = $requestInput->getArguments();
        $servicesServices = new ServicesService();
        $groupService = new GroupsService();
        $gruppo = $groupService->getGroupById($parameters['idGruppo']);
        $serviceList = $servicesServices->getAllServices();
        $idServizi = array();
        if (!empty($gruppo->getServizi())) {
            foreach ($gruppo->getServizi() as $servizi) {
                array_push($idServizi, $servizi->getIdServizio());
            }
        }
        return $view('back.modify-groups', compact('gruppo', 'serviceList', 'idServizi'));
    }

    public function store(RequestInput $requestInput, StoreGroup $storeGroup) {
        $parameters = $requestInput->getArguments();
        if ($storeGroup->failed()) return redirect('/admin/modify_group/' . $parameters['idGruppo']);

        $data = $requestInput->all();
        $gruppoService = new GroupsService();
        $servicesServices = new ServicesService();
        $gruppo = new Groups();
        $serviziList = array();
        $gruppo->setIdGruppo($parameters['idGruppo']);

        if ($gruppo->getIdGruppo() == 1 || $gruppo->getIdGruppo() == 2 || $gruppo->getIdGruppo() == 3) {
            session()->flash()->set(
                'errorMessage',
                'Siamo spiacenti, il gruppo ' . $gruppo->getNome() . ' non può essere modificato!'
            );
        } else {
            $gruppo->setNome($data['nome']);
            $gruppo->setDescrizione($data['descrizione']);
            if (!empty($data['service'])) {
                foreach ($data['service'] as $idService) {
                    $servizio =  $servicesServices->getServiceById((int) $idService);
                    array_push($serviziList, $servizio);
                }
            }
            $gruppo->setServizi($serviziList);
            $gruppoService->updateGroup($gruppo);

            session()->flash()->set(
                'succesMessage',
                'Gruppo ' . $gruppo->getNome() . ' modificato con successo!'
            );
        }
        return redirect('/admin/groups_services');
    }



}