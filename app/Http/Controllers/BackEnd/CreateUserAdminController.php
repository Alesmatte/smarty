<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreUserAdmin;
use App\Model\User;
use App\Services\GroupsService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class CreateUserAdminController
{
    public function show(View $view) {
        $groupService = new  GroupsService();
        $grouplist = $groupService->getAllgroups();
        $tipologia = 3;
        return $view('back.create-admin-user', compact('grouplist', 'tipologia'));
    }

    public function store(StoreUserAdmin $storeUserAdmin, RequestInput $requestInput, View $view) {
        if ($storeUserAdmin->failed()) return $view('back.create-admin-user');
        $user = new User();
        $userService = new UserService();
        $user->setTipologiaUtenza(2);
        $user->setStato(1);
        $userService->storeUsers($user, $requestInput, $userService, 'insert');

        session()->flash()->set(
            'succesMessage',
            $user->getNome() . ' ' . $user->getCognome() . ' creato con successo!'
        );
        return redirect('/admin/user_list');
    }
}