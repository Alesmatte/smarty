<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreUserCritica;
use App\Http\Request\StoreUserNormalRequest;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class CreateUserController
{
    private function storeNormale(StoreUserNormalRequest $inputUserNormale, View $view) {
        if ($inputUserNormale->failed()) return $view('back.create-user');
        return $view('back.create-user');
    }

    public function createUser(RequestInput $requestInput)
    {

        $data = $requestInput->all();
        if ($data['tipologia'] == 0) {
            return redirect('/admin/create_normale_user');
        } elseif ($data['tipologia'] == 1) {
            return redirect('/admin/create_critico_user');
        } elseif ($data['tipologia'] == 2) {
            return redirect('/admin/create_admin_user');
        }
        return redirect('/admin/user_list');
    }
}