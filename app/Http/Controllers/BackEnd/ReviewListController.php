<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmService;
use App\Services\RecensioneService;
use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class ReviewListController
{
    public function show(RequestInput $requestInput, View $view) {
        $recensioneService = new RecensioneService();
        $parameters = $requestInput->getArguments();
        $entita = $parameters['entita'];
        $id = $parameters['id'];
        if ($entita == 'film') {
            $filmService = new FilmService();
            $nomeEntita = $filmService->getFilmByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniByIdFilm($id);
            if (isset($parameters['limit']) && isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniByIdFilm($id, $parameters['limit'], (($parameters['offset'])-1) * $parameters['limit']);
                $currentPage = $parameters['offset'];
                $numeroPagine = ceil($totRecensioni / $parameters['limit']);
            } else {
                $recensioniList = $recensioneService->getRecensioniByIdFilm($id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } elseif($entita == 'serietv') {
            $serieTvService = new SerieTvService();
            $nomeEntita = $serieTvService->getSerieTvByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniByIdSerieTv($id);
            if (isset($parameters['limit']) && isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, $parameters['limit'], (($parameters['offset'])-1) * $parameters['limit']);
                $currentPage = $parameters['offset'];
                $numeroPagine = ceil($totRecensioni / $parameters['limit']);
            } else {
                $recensioniList = $recensioneService->getRecensioniByIdSerieTv($id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } else {
            return back();
        }
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('back.review-list', compact('recensioniList', 'entita', 'nomeEntita', 'id',  'numeroPagine', 'currentPage', 'pagine'));
    }

    public function cerca(RequestInput $requestInput, View $view) {
        $recensioneService = new RecensioneService();
        $parameters = $requestInput->getArguments();
        $qstr = $requestInput->getQueryString();
        $entita = $parameters['entita'];
        $cerca = $qstr['ricerca'];
        $id = $parameters['id'];

        if ($entita == 'film') {
            $filmService = new FilmService();
            $nomeEntita = $filmService->getFilmByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniSearchByIdFilm($id, $cerca);
            if (isset($parameters['limit']) && isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniBySearchIdFilm($cerca, $id, $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $numeroPagine = ceil($totRecensioni / $parameters['limit']);
                $currentPage = $parameters['offset'];
            } else {
                $recensioniList = $recensioneService->getRecensioniBySearchIdFilm($cerca, $id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } elseif($entita == 'serietv') {
            $serieTvService = new SerieTvService();
            $nomeEntita = $serieTvService->getSerieTvByIdPerRecensioni($id)->getTitolo();
            $totRecensioni = $recensioneService->countGetRecensioniSearchByIdSerieTv($id, $cerca);
            if (isset($parameters['limit']) && isset($parameters['offset'])) {
                $recensioniList = $recensioneService->getRecensioniBySearchIdSerieTv($cerca, $id, $parameters['limit'], (($parameters['offset']) - 1) * $parameters['limit']);
                $numeroPagine = ceil($totRecensioni / $parameters['limit']);
                $currentPage = $parameters['offset'];
            } else {
                $recensioniList = $recensioneService->getRecensioniBySearchIdSerieTv($cerca, $id, 10, 0);
                $numeroPagine = ceil($totRecensioni / 10);
                $currentPage = 1;
            }
        } else {
            return back();
        }
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('back.review-list', compact('recensioniList', 'entita', 'nomeEntita', 'id',  'numeroPagine', 'currentPage', 'cerca', 'pagine'));
    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}