<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\GroupsService;
use App\Services\UserService;
use App\Support\View;

class UserListController
{
    public function show(View $view) {
        $userService = new UserService();
        $userList = $userService->getAllUser();
        $groupService = new GroupsService();
        $groupListStr = array();
        $groupListOver = array();
            foreach ($userList as $keyUser => $user) {
                $gruppiPerUser = $groupService->getGroupByUser($user);
                if (!empty($gruppiPerUser)) {
                    foreach ($gruppiPerUser as $keyGruppo => $gruppo) {
                        if ($keyGruppo == 0) {
                            $groupListStr[$user->getIdUser()] = $gruppo->getNome();
                        } else {
                            $groupListStr[$user->getIdUser()] = $groupListStr[$user->getIdUser()] . $gruppo->getNome();
                        }
                        if ($keyGruppo + 1 != count($gruppiPerUser)) {
                            $groupListStr[$user->getIdUser()] = $groupListStr[$user->getIdUser()] . ', ';
                        }
                    }
                    if (strlen($groupListStr[$user->getIdUser()]) > 20) {
                        $groupListOver[$user->getIdUser()] = $gruppiPerUser;
                    }
                }
            }
        return $view('back.user-list', compact('userList', 'groupListStr', 'groupListOver'));
    }


}