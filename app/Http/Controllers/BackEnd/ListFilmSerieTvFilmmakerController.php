<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\FilmMakerService;
use App\Services\FilmService;
use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class ListFilmSerieTvFilmmakerController
{

    public function showFilm(RequestInput $requestInput, View $view)
    {
        $entita = 'film';
        $filmService = new FilmService();
        $filmSerietvList = $filmService->getAllFilmParziali();

        return $view('back.film-serietv-list', compact('filmSerietvList', 'entita'));
    }

    public function showSerieTv(RequestInput $requestInput, View $view)
    {
        $entita = 'serietv';
        $serieTvService = new SerieTvService();
        $filmSerietvList = $serieTvService->getAllSerieTvParziali();

        return $view('back.film-serietv-list', compact('filmSerietvList', 'entita'));
    }

    public function showFilmmaker(View $view)
    {
        $filmmakeService = new FilmMakerService();
        $filmmakerList = $filmmakeService->getAllFilmmakerParziali();
        return $view('back.filmmaker-list', compact('filmmakerList'));
    }

}