<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\ModifyUserAdminRequest;
use App\Model\User;
use App\Services\GroupsService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;

class ModifyUserAdminController
{
    public function show(RequestInput $requestInput, View $view) {
        $parameters = $requestInput->getArguments();
        $userService = new UserService();
        $user = $userService->getUserById($parameters['idUser']);
        $groupService = new GroupsService();
        $grouplist = $groupService->getAllgroups();
        $idGruppi = array();
        $userGroups = $groupService->getGroupByUser($user);
        foreach ($userGroups as $gruppo) {
            array_push($idGruppi, $gruppo->getIdGruppo());
        }
        session()->flash()->set('userEmailTemp', $user->getEmail());
        return $view('back.modify-admin-user', compact('user', 'idGruppi', 'grouplist'));
    }

    public function store(ModifyUserAdminRequest $modifyUserAdminRequest, RequestInput $requestInput, View $view) {
        $parameters = $requestInput->getArguments();
        if ($modifyUserAdminRequest->failed()) {

            return redirect('/admin/modify_admin_user/' . $parameters['idUser']);
        }
        $user = new User();
        $userService = new UserService();
        $user->setIdUser($parameters['idUser']);
        $user->setTipologiaUtenza(2);
        $user->setStato(1);
        $userService->storeUsers($user, $requestInput, $userService, 'update');

        session()->flash()->set(
            'succesMessage',
            $user->getNome() . ' ' . $user->getCognome() . ' modificato con successo!'
        );
        return redirect('/admin/user_list');
    }


}