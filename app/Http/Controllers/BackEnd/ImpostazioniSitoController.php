<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreGroup;
use App\Model\Groups;
use App\Model\Immagine;
use App\Services\GroupsService;
use App\Services\ImmagineService;
use App\Services\ServicesService;
use App\Support\RequestInput;
use App\Support\View;

class ImpostazioniSitoController
{

    public function show(View $view) {
        $imgService = new ImmagineService();
        $sliderOriginalList = $imgService->getImgByTipologia(4);
        $sliderOriginalListSerieTv = $imgService->getImgByTipologia(8);
        $sliderOriginalListPremi = $imgService->getImgByTipologia(9);
        if (!empty($sliderOriginalListSerieTv)) {
            foreach ($sliderOriginalListSerieTv as $img) {
                array_push($sliderOriginalList, $img);
            }
        }
        if (!empty($sliderOriginalListPremi)) {
            foreach ($sliderOriginalListPremi as $img) {
                array_push($sliderOriginalList, $img);
            }
        }
        return $view('back.slider-pubblicitari', compact('sliderOriginalList'));
    }

    public function store(RequestInput $requestInput, View $view) {
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $imgService = new ImmagineService();
        $data = $requestInput->all();

        $sliderOriginalList = $imgService->getImgByTipologia(4);

        $array_new_img = array();
        $array_new_img_id = array();
        $array_old_img_id = array();
        if(isset($data['immagini']['Id_immagini'])){
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImg) {
                if ($data['immagini']['scheda'][$kp] == 4) {
                    $slider_film_path = 'lista_film';
                } elseif ($data['immagini']['scheda'][$kp] == 8) {
                    $slider_film_path = 'lista_serietv';
                } else {
                    $slider_film_path = 'premi';
                }
                foreach ($sliderOriginalList as $k => $imgOriginal) {
                    if ($imgOriginal->getIdImmagione() == $idImg) {
                        $img = new Immagine();
                        $uploadedFile=$uploadedFiles['immagini']['file'][$kp];
                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/slider/');
                            deleteFile(public_path($imgOriginal->getDato()));
                            $filename = movefiles("$directory".$slider_film_path, $uploadedFile);
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($uploadedFile->getClientFilename());
                            $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                            $img->setDato("img/slider/".$slider_film_path . DIRECTORY_SEPARATOR . $filename);
                            $img->setTipo($data['immagini']['scheda'][$kp]);
                            // aggiorno l'img

                            $imgService->updateImgSlider($img);

                        } else {
                            // la foto è quella vecchia
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($imgOriginal->getNome());
                            $img->setDato($imgOriginal->getDato());
                            $img->setDescrizione($imgOriginal->getDescrizione());
                            $img->setTipo($imgOriginal->getTipo());

                            //controllo se la descrione o il tipo sono cambiati
                            if($imgOriginal->getDescrizione() != $data['immagini']['descrizione'][$kp] || $imgOriginal->getTipo() != $data['immagini']['scheda'][$kp]) {
                                $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                                $img->setTipo($data['immagini']['scheda'][$kp]);
                                // upload modifica
                                $imgService->updateImgSlider($img);
                            }
                        }
                        array_push($array_new_img, $img);
                    }
                }
            }

            // array_old_id
            foreach ($sliderOriginalList as $k => $immagine) {
                array_push($array_old_img_id, strval($immagine->getIdImmagione()));
            }
            // array_new_id
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImmagine) {
                array_push($array_new_img_id, $idImmagine);
            }

            // array di id da cancellare
            $array_da_cancellare_img = array_diff($array_old_img_id, $array_new_img_id);

            foreach ($array_da_cancellare_img as $k => $idImg){
                foreach($sliderOriginalList as $ki => $immagine){
                    if($immagine->getIdImmagione() == $idImg){
                        // cancella le img
                        deleteFile(public_path($immagine->getDato()));
                        $imgService->deleteImg($immagine);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_img = array_diff($array_new_img_id, $array_old_img_id);

            foreach ($array_da_inserire_img as $kArray => $id) {
                foreach ($data['immagini']['Id_immagini'] as $k => $idImg) {
                    if ($kArray === $k && $id === $idImg) {
                        $uploadedFile = $uploadedFiles['immagini']['file'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/slider/');
                            $filename = movefiles("$directory".$slider_film_path, $uploadedFile);
                            $imgSlider = new Immagine();
                            $imgSlider->setNome($uploadedFile->getClientFilename());
                            $imgSlider->setDescrizione($data['immagini']['descrizione'][$k]);
                            $imgSlider->setDato("img/slider/".$slider_film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgSlider->setTipo($data['immagini']['scheda'][$k]);
                            // salvo le immagini dello slider
                            $imgService->insertImgSlider($imgSlider);
                            array_push($array_new_img, $imgSlider);
                        }
                    }
                }
            }
        } else{
            //elimina tutte le immagini
            foreach ($sliderOriginalList as $k => $immagine){
                deleteFile(public_path($immagine->getDato()));
                $imgService->deleteImg($immagine);
            }
        }
        return redirect('/admin/impostazioni');

    }

}