<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class ModifySerieTvController
{
    public function show(RequestInput $requestInput, View $view)
    {
        $serieTvService = new SerieTvService();
        return $view('back.update-serie-tv',$serieTvService->showModify($requestInput));
    }

    public function store(RequestInput $requestInput, View $view)
    {
        $serieTvService = new SerieTvService();
        if($serieTvService->modifySerietv($requestInput)) return redirect(asset('admin/serietv'));
        else return redirect(asset('admin/modidy_serietv/'.$requestInput->getArguments()['idSerieTv']));
    }

}