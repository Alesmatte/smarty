<?php


namespace App\Http\Controllers\BackEnd;


use App\Http\Request\StoreGroup;
use App\Model\Groups;
use App\Services\GroupsService;
use App\Services\ServicesService;
use App\Support\RequestInput;
use App\Support\View;

class CreateGroupController
{

    public function show(View $view) {
        $servicesServices = new ServicesService();
        $serviceList = $servicesServices->getAllServices();

        return $view('back.create-groups', compact('serviceList'));
    }

    public function store(RequestInput $requestInput, StoreGroup $storeGroup) {
        if ($storeGroup->failed()) return redirect('/admin/create_group');
        $data = $requestInput->all();
        $gruppoService = new GroupsService();
        $servicesServices = new ServicesService();
        $gruppo = new Groups();
        $serviziList = array();
        $gruppo->setNome($data['nome']);
        $gruppo->setDescrizione($data['descrizione']);
        foreach ($data['service'] as $idService) {
            $servizio =  $servicesServices->getServiceById((int) $idService);
            array_push($serviziList, $servizio);
        }
        $gruppo->setServizi($serviziList);
        $gruppoService->insertGroup($gruppo);

        session()->flash()->set(
            'succesMessage',
            'Gruppo ' . $gruppo->getNome() . ' ' . $gruppo->getDescrizione() . ' creato con successo!'
        );
        return redirect('/admin/groups_services');
    }

}