<?php


namespace App\Http\Controllers\BackEnd;


use App\Services\GenereService;
use App\Support\View;

class GenereController
{
    public function showlist(View $view){
        $genereService = new GenereService();
        $listGeneri = $genereService->getAllGenere();
        return $view('back.list-generi', ["listGeneri"=>$listGeneri]);
    }

}