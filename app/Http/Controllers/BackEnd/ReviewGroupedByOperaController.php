<?php


namespace App\Http\Controllers\BackEnd;


use App\Model\Film;
use App\Model\SerieTv;
use App\Services\FilmService;
use App\Services\RecensioneService;
use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class ReviewGroupedByOperaController
{
    public function show(RequestInput $requestInput, View $view) {
        $recensioneService = new RecensioneService();
        $parameters = $requestInput->getArguments();
        $serieTvService = new SerieTvService();
        $filmService = new FilmService();

        $operaList = $filmService->getAllFilmPerRecensioni();
        $serieTvList = $serieTvService->getAllSerieTvPerRecensioni();
        foreach ($serieTvList as $serieTv) {
            array_push($operaList, $serieTv);
        }
        $numeroPagine = ceil(count($operaList) / 10);

        if (isset($parameters['limit']) && isset($parameters['offset'])) {
            $operaList = array_slice($operaList, (($parameters['offset']) - 1) * $parameters['limit'], $parameters['limit']);
            $currentPage = $parameters['offset'];
        } else {
            $operaList = array_slice($operaList, 0,10);
            $currentPage = 1;
        }

        $listaQuantitaRecensioni = array();
        foreach ($operaList as $opera) {
            if ($opera instanceof Film) {
                array_push($listaQuantitaRecensioni, [$recensioneService->countGetRecensioniByIdFilm($opera->getIdFilm()), 'film', $recensioneService->getLastRecensioneByIdFilm($opera->getIdFilm())]);
            } else {
                array_push($listaQuantitaRecensioni, [$recensioneService->countGetRecensioniByIdSerieTv($opera->getIdSerieTv()), 'serietv', $recensioneService->getLastRecensioneByIdSerieTv($opera->getIdSerieTv())]);
            }
        }
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('back.review-grouped-by-opere', compact('operaList', 'listaQuantitaRecensioni', 'numeroPagine', 'currentPage', 'pagine'));
    }

    public function cerca(RequestInput $requestInput, View $view) {
        $recensioneService = new RecensioneService();
        $parameters = $requestInput->getArguments();
        $qstr = $requestInput->getQueryString();
        $serieTvService = new SerieTvService();
        $filmService = new FilmService();
        $cerca = $qstr['ricerca'];

        $operaList = $filmService->getAllFilmPerRecensioniSearch($cerca);
        $serieTvList = $serieTvService->getAllSerieTvPerRecensioniSearch($cerca);
        foreach ($serieTvList as $serieTv) {
            array_push($operaList, $serieTv);
        }
        $numeroPagine = ceil(count($operaList) / 10);

        if (isset($parameters['limit']) && isset($parameters['offset'])) {
            $operaList = array_slice($operaList, (($parameters['offset']) - 1) * $parameters['limit'], $parameters['limit']);
            $currentPage = $parameters['offset'];
        } else {
            $operaList = array_slice($operaList, 0,10);
            $currentPage = 1;
        }

        $listaQuantitaRecensioni = array();
        foreach ($operaList as $opera) {
            if ($opera instanceof Film) {
                array_push($listaQuantitaRecensioni, [$recensioneService->countGetRecensioniByIdFilm($opera->getIdFilm()), 'film', $recensioneService->getLastRecensioneByIdFilm($opera->getIdFilm())]);
            } else {
                array_push($listaQuantitaRecensioni, [$recensioneService->countGetRecensioniByIdSerieTv($opera->getIdSerieTv()), 'serietv', $recensioneService->getLastRecensioneByIdSerieTv($opera->getIdSerieTv())]);
            }
        }
        $pagine = self::paginazione($currentPage, $numeroPagine);
        return $view('back.review-grouped-by-opere', compact('operaList', 'listaQuantitaRecensioni', 'numeroPagine', 'currentPage', 'cerca', 'pagine'));
    }

    public function paginazione($currentPage, $numeroPagine) {
        $pagine = array();
        /** centro */
        if ($currentPage > $numeroPagine || $currentPage <= 0) {
            $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
        } else {
            if ($numeroPagine > 8 && $currentPage > 4 && $currentPage < $numeroPagine - 3) {
                if ($numeroPagine == 9) {
                    for ($i = 1; $i <= $numeroPagine; $i++) {
                        array_push($pagine, $i);
                    }
                } elseif ($currentPage == 5) {
                    $pagine = [1, 2, 3, $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                } elseif($currentPage == $numeroPagine - 4) {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                } else {
                    $pagine = [1, 2, '...', $currentPage - 1, $currentPage , $currentPage +1, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** a sinistra / centro-sinistra */
            } elseif ($numeroPagine > 8 && $currentPage <= 4 && $currentPage < $numeroPagine - 3) {
                /** a sinistra     pagine 3,2,1 */
                if ($currentPage <= 3) {
                    $pagine = [1, 2, 3, 4, '...', $numeroPagine - 1, $numeroPagine];
                }
                /** centro-sinistra     pagina 4 */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, 3, 4, 5, '...', $numeroPagine - 1, $numeroPagine];
                    }
                }
                /** a destra / centro-destra */
            } elseif ($numeroPagine > 8 && $currentPage > 4 && $currentPage >= $numeroPagine - 3) {
                /** a destra */
                if ($currentPage >= $numeroPagine - 2) {
                    $pagine = [1, 2, '...', $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                }
                /** centro-destra     quartultima pagina */
                else {
                    if ($numeroPagine == 9 ) {
                        for ($i = 1; $i <= $numeroPagine; $i++) {
                            array_push($pagine, $i);
                        }
                    } else {
                        $pagine = [1, 2, '...', $numeroPagine - 4, $numeroPagine - 3, $numeroPagine - 2, $numeroPagine - 1, $numeroPagine];
                    }
                }
            } else {
                for ($i = 1; $i <= $numeroPagine; $i++) {
                    array_push($pagine, $i);
                }
            }
        }
        return $pagine;
    }
}