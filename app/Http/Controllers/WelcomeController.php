<?php

namespace App\Http\Controllers;

use App\Model\Film;
use App\Model\Filmmaker;
use App\Model\Immagine;
use App\Model\Nomination;
use App\Model\Stagione;
use App\Model\TipologiaPremio;
use App\Model\User;
use App\Model\Video;
use App\Services\EpisodioService;
use App\Services\FilmMakerService;
use App\Services\FilmService;
use App\Services\ImmagineService;
use App\Services\Implementation\FilmServiceImplementation;
use App\Services\Implementation\RecensioneServiceImplementation;
use App\Services\Implementation\TipologiaPremioServiceImplementation;
use App\Services\NominationService;
use App\Services\PremioService;
use App\Services\RecensioneService;
use App\Services\RuoloService;
use App\Services\SerieTvService;
use App\Services\TipologiaPremioService;
use App\Services\UserService;
use App\Services\VideoService;
use App\Support\View;
use DB;

class WelcomeController
{
    public function index(View $view, DB $db)
    {
        $mario = 1;
        /*metodo 1 per evitare sql injection
        $sql='select * from products where id ='.$mario;
        $st = db::getPdo()->prepare($sql);
        $st->execute();
        $result = $st->fetchAll(\PDO::FETCH_OBJ);
        */

        // metodo 2 per evitare sql injection
//        $g = new Genere();

//    $filmService = new FilmService();
//    $film = new Film();
//    $film->setTitoloOriginale("avatar");
//    $film->setTitolo("giganti");
//    $film->setDescrizione("Bello");
//    $film->setTrama("interessante");
//    $film->setTramaBreve("interessa");
//    $film->setDataSviluppoFilm('2021/02/18');
//    $film->setDataUscita("2021/02/18");
//    $film->setDurata("168");
//    $film->setNazionalita(["IT", "AQ", "BE"]);
//    $film->setAgeRating(13);
//    $film->setInSala(true);
//
//    $film->setDistributori([1, 2]);
//    $film->setGeneri([1, 2]);
//    $film->setNominations([1, 2]);
//    $film->setRuoli([2, 3]);

//        $distrbutoreService = new DistributoreService();
//        $distrbutore = new Distributore();
//        $distrbutore->setNome("01Distribution");
//        $distrbutore->setDescrizione("distributore forte");
//        dd($distrbutoreService->insertDistributore($distrbutore));

//        $genereService = new GenereService();
//        $genere = new Genere();
//        $genere->setNome("fantasy");
//        $genere->setDescrizione('fantastico');

//        $nomination = new Nomination();
//        $nominatioService = new NominationService();
//        $nomination->setDescrizione("vittoria prova");
//        $nomination->setDataPremiazione("2021/02/10");
//        $nomination->setStato(true);
//        $categoriaPremio = new CategoriaPremio();
//        $categoriaPremio->setIdCategoriaPremio(1);
//        $nomination->setCategoriaPremio($categoriaPremio);
//        $idNomination = $nominatioService->insertNomination($nomination);
//        $nominatioService->insertNominationFilmmaker($idNomination, 3);

//        $ruoloService = new RuoloService();
//        $ruolo = new Ruolo();
//        $ruolo->setRuoloInternoOpera('cap');
//        $ruolo->setTipologia(0/1);
//        $filmmaker = new Filmmaker();
//        $filmmaker->setIdFilmMaker(3);
//        $ruolo->setFilmMaker($filmmaker);

//        $filmmaker = new Filmmaker();
//        $filmmakerService = new FilmMakerService();
//        $filmmaker->setNome("franco");
//        $filmmaker->setCognome("Franchi");
//        $filmmaker->setBiografia("Bio di Franco");
//        $filmmaker->setBreveDescrizione("breve di Franco");
//        $filmmaker->setCitazione("Cit Franco");
//        $filmmaker->setTipologiaPrincipale('0/1');
//        $film = new Film();
//        $film->setIdFilm(8);
//        $filmmaker->setFilmSerieTvCitazione($film);
//        dd($filmmakerService->insertFilmmaker($filmmaker));
//        $immagine = new Immagine();
//        $immagineService = new ImmagineService();
//        $immagine->setNome("flash1");
//        $immagine->setDescrizione("des flash1");
//        $immagine->setDato("awddwadwd/dwaawwad/wdadwawda");
//        $immagine->setTipo(5);
////        $immagineService->insertImgDistributore($immagine, 9);
//        dd($immagineService->insertImgEpisodio($immagine, 1));
//        $immagineService->insertImgFilm($immagine, 8);
//        $immagineService->insertImgFilmmaker($immagine, 3);
//        $immagineService->insertImgPremio($immagine, 1);
//        $immagineService->insertImgRuolo($immagine, 5);
//        $immagineService->insertImgSerieTv($immagine, 2);
//        dd($immagineService->insertImgUser($immagine, 1));

//        $nazdaoimpl = new FilmServiceImplementation();
//        dd($nazdaoimpl->getNazionalitasByFilmId(8));

//        $recensioniService = new RecensioneService();
//       dd($recensioniService->countGetRecensioniByIdFilm(9));

//        $imgService = new ImmagineService();
//        $img =new Immagine();
//        $img->setIdImmagione(30);
//        $img->setNome("prova1");
//        $img->setDescrizione("descrizione di prova1");
//        $img->setDato("rfgrgg/rgzdgzrgr1/");
//        $img->setTipo("5");
//        dd($imgService->updateImgUser($img, 1));
//       dd($imgService->getImgByEntitaAndIdEntitaAndTipologia('user', 1, 5));
//        $filmSI = new FilmServiceImplementation();
//        $nazioni = $filmSI->getNazionalitasByFilmId(8);
//        foreach ($nazioni as $codice => $nazione) {
//            dd($codice, $nazione);
//        }

//        $video = new Video();
//        $videoService = new VideoService();
//        $video->setNome("prova");
//        $video->setDescrizione("provaD");
//        $video->setDato("fsefsefsefes/efsefsfesfesesf/esfesfsef.ggg");
//        dd($immagineService->insertImgVideo($immagine, $videoService->insertVideoFilm($video, 2)));

//        $filmService = new FilmService();
//        $result = $filmService->getFilmByAnno("2020");
//        dd($result[0]);
//        $filmmakerService = new FilmMakerService();
//        $nominationService = new NominationService();
//        $nomination = $nominationService->getNominationByIdFilm(9)[1];
//        $tipologiaPremioServiceImpl = new TipologiaPremioServiceImplementation();
//        $premioService = new PremioService();
//        $premioModificato = $premioService->getPremioById(1);
//        $tipologiaPremioModificato = $tipologiaPremioServiceImpl->getTipologiaPremioById(1);
//        $categoriaPremio = $nomination->getCategoriaPremio();
//        $categoriaPremio->setPremio($premioModificato);
//        $categoriaPremio->setTipologia($tipologiaPremioModificato);
//        $nomination->setCategoriaPremio($categoriaPremio);
//        $nomination->setDescrizione("non ha vinto stato 0");
//        $nominationService->updateNomination($nomination);
//        dd($nominationService->getNominationByIdFilm(9)[1]);

//        $filmmakerService = new FilmMakerService();
//        $filmService = new FilmService();
//        $filmmaker = $filmmakerService->getFilmakerById(4);
//        dd($filmmaker);
//        $filmmaker->setNome("Franco");
//        $filmmaker->setBreveDescrizione("breve su Franco");
//        $filmmaker->setFilmSerieTvCitazione($filmService->getFilmById(9));
//        dd($filmmakerService->udpdataFilmmaker($filmmaker));

//        $filmmakerService = new FilmMakerService();
//        dd($filmmakerService->deleteFilmmaker($filmmakerService->getFilmakerById(5)));
//        $ruoloService = new RuoloService();
//        dd($ruoloService->getRuoliByIdFilmmaker(4));
//        $ruolo = $ruoloService->getRuoliById(3);
//        dd($ruolo);
//        $ruoloService->updateRuoloFilmmakerByIdFilmmaker($ruolo->getIdRuolo(), 4);

//        dd($ruoloService->deleteRuoloById($ruolo));
//        dd($ruoloService->getRuoliById());
//        $filmmaker =
//        $filmService = new FilmService();
//        dd($filmService->getFilmById(9));

//        $userService = new UserService();
//        $user = $userService->getUserByEm("mario.rossi@1.it");
//        $user->setCodiceAlbo(25);0
//        dd($userService->updateUser($user));

//        $episodioService = new EpisodioService();
//        dd($episodioService->getEpisodiStagioneByIdStagione(27));
//        $serieTvService = new SerieTvService();
//        dd($serieTvService->getSerieTvById(18));
//        $filmService = new FilmService();
//        dd($filmService->getFilmById(9));
//        $serieTvService = new SerieTvService();
//        $filmmakerService = new FilmMakerService();
//        $filmmaker = new Filmmaker();
//        $filmmaker->setIdFilmMaker(2);
//        dd($filmmakerService->getFilmAndSerieTvByFilmmaker($filmmaker));
//        $recensioniServiceImpl = new RecensioneServiceImplementation();
//        $recensioniServiceImpl->countGetRecensioniByIdFilmDD(9);
//         $f = new Film();
//        dd($f->getIdFilm());
//        $indici = array();
//        for ($i= 0; $i<1000000; $i++) {
//            array_push($indici, $i);
//        }
//        foreach ($indici as $k=>$item) {
//            $indici[$k] = $k+1;
//        }
        $dataOggi = date("Y/m-d");
        $dataListA = explode("-", $dataOggi);
        $dataListB = explode("/", $dataListA[0]);
        dd($dataListA[0] . '/01', $dataListB[0] . ('/' . ($dataListB[1] + 1)) . '/01');
        dd(number_format(0.0, 3, '.', ''));
        $arr = array();
        for ($i = 0; $i < 5; $i++) {
            array_push($arr, [$i, 'mario']);
        }
//        dd(key($arr[0]));
        dd($arr);
        $nomiPremi = ['oscar', 'golden', 'leone'];
        $premi = array();
        for ($i = 0; $i < 5; $i++) {
            $premi[$i] = array();
            foreach ($nomiPremi as $item) {
                $premi[$i][$item] = 5;
            }
        }
        dd($premi);

        return $view('auth.home', compact('groupList'));

    }

    public function show(View $view, $name, $id)
    {
        return $view('user.show', compact('name', 'id'));
    }

    public function home(View $view)
    {
        return $view('skeletons.front.app');
    }

    public function backend(View $view){
        return $view('skeletons.back.app');
    }
}
