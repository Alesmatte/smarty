<?php


namespace App\Http\Controllers\Api;


use App\Http\Request\RecenzioneRequest;
use App\Model\Recensione;
use App\Services\RecensioneService;
use App\Services\UserService;
use App\Support\RequestInput;
use App\Support\View;
use function Composer\Autoload\includeFile;

class RecensioniApiController
{
    public function deleteRecensione(RequestInput $requestInput)
    {
        $data = $requestInput->getArguments();
        $recensioneService = new RecensioneService();
        $recensione = new Recensione();
        $recensione->setIdRecensione(intval($data['idRecensione']));
        if ($recensioneService->deleteRecensione($recensione) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Recensione eliminata con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nella cancellazione della recensione!'
            );
        }
    }

    public function storeRecensioneFilm(RecenzioneRequest $recenzioneInput, RequestInput $requestInput, View $view)
    {
        if ($recenzioneInput->failed()) {
            return $view("components.error", ['error_recensione' => true, 'idOpera' => $requestInput->getArguments()['idFilm']]);
        }
        $recensioneService = new RecensioneService();
        $userService = new UserService();
        if ($recensioneService->getRecensioniByIdUserAndIdFilm($userService->getUser()->getIdUser(), $requestInput->getArguments()['idFilm'])) {
            return $view("components.error", ['error_recensione' => true, 'recensito' => true, 'mexOpera1' => 'questo Film', 'mexOpera2' => 'del Film', 'idOpera' => $requestInput->getArguments()['idFilm']]);
        }
        $data = $recenzioneInput->all();
        $recensione = new Recensione();
        $recensione->setTitolo($data['titolo']);
        $recensione->setVoto($data['valutazione']);
        $recensione->setUtente($userService->getUser());
        $recensione->setContenutoRecensione($data['contenuto']);
        $recensione->setDataRecensione(date(DATE_ISO8601));
        $recensioneService->insertRecensioneFilm($recensione, $requestInput->getArguments()['idFilm']);
        return $view("components.error", ['error_recensione' => false, 'idOpera' => $requestInput->getArguments()['idFilm']]);
    }

    public function storeRecensioneSerieTv(RecenzioneRequest $recenzioneInput, RequestInput $requestInput, View $view)
    {
        if ($recenzioneInput->failed()) {
            return $view("components.error", ['error_recensione' => true, 'idOpera' => $requestInput->getArguments()['idSerieTv']]);
        }
        $recensioneService = new RecensioneService();
        $userService = new UserService();
        if ($recensioneService->getRecensioniByIdUserAndIdSerieTv($userService->getUser()->getIdUser(), $requestInput->getArguments()['idSerieTv'])) {
            return $view("components.error", ['error_recensione' => true, 'recensito' => true, 'mexOpera' => 'questa Serie TV', 'mexOpera2' => 'della Serie TV', 'idOpera' => $requestInput->getArguments()['idSerieTv']]);
        }
        $data = $recenzioneInput->all();
        $recensione = new Recensione();
        $recensione->setTitolo($data['titolo']);
        $recensione->setVoto($data['valutazione']);
        $recensione->setUtente($userService->getUser());
        $recensione->setContenutoRecensione($data['contenuto']);
        $recensione->setDataRecensione(date(DATE_ISO8601));
        $recensioneService->insertRecensioneFilm($recensione, $requestInput->getArguments()['idSerieTv']);
        return $view("components.error", ['error_recensione' => false, 'idOpera' => $requestInput->getArguments()['idSerieTv']]);
    }

}