<?php


namespace App\Http\Controllers\Api;


use App\Model\User;
use App\Services\ImmagineService;
use App\Services\UserService;
use App\Support\RequestInput;
use Slim\Exception\HttpInternalServerErrorException;

class BackManageUserApiController
{

    public function deleteImmage(RequestInput $requestInput) {
        $parameters = $requestInput->getArguments();
        $imgService = new ImmagineService();
        $img = $imgService->getImgByEntitaAndIdEntitaAndTipologia('user', $parameters['idUser'], 0)[0];
        $res = $imgService->deleteImg($img);
        throw_when($res===0, "errore non riesco a cancellare l'immagine",  HttpInternalServerErrorException::class);
        deleteFile(public_path($img->getDato()));
    }

    public function deleteUser(RequestInput $requestInput) {
        $data = $requestInput->getArguments();
        $userService = new UserService();
        $user = new User();
        $user->setIdUser((int) ($data['idUser']));
        $user = $userService->getUserById($user->getIdUser());
        $tipologia = 'Utente Normale';
        if ($user->getTipologiaUtenza() == 1) {
            $tipologia = 'Utente Critico';
        } elseif ($user->getTipologiaUtenza() == 2) {
            $tipologia = 'Utente Admin';
        }

        if ($userService->deleteUser($user) == 1) {
            session()->flash()->set(
                'succesMessage',
                $user->getNome() . ' ' . $user->getCognome() . ' di tipo ' . $tipologia . ' eliminato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nella cancellazione di ' . $user->getNome() . ' ' . $user->getCognome() . ' di tipo ' . $tipologia . ' !'
            );
        }
    }

    public function toggleStateUser(RequestInput $requestInput) {
        $data = $requestInput->getArguments();
        $userService = new UserService();
        $user = new User();
        $user->setIdUser((int) ($data['idUser']));
        $user = $userService->getUserById($user->getIdUser());
        if ($user->getStato() == 1) {
            $user->setStato(0);
        } elseif ($user->getStato() == 0){
            $user->setStato(1);
        }
        $userService->toggleStateUser($user);
    }
}