<?php


namespace App\Http\Controllers\Api;


use App\Services\SerieTvService;
use App\Support\RequestInput;

class SerieTvApiController
{
    public function deleteSerieTv(RequestInput $requestInput)
    {
        $parameters = $requestInput->getArguments();
        $serieTvService = new SerieTvService();
        $serieTv = $serieTvService->getSerieTvById($parameters['idSerieTv']);
        if ($serieTvService->deleteSerieTvStagioneEpisodioByIdSerieTv($serieTv->getIdSerieTv()) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Film ' . $serieTv->getTitolo() . ' eliminato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nell&apos;eliminazione del Film ' . $serieTv->getTitolo() . ' !'
            );
        }
    }

    public function toggleInSala(RequestInput $requestInput)
    {

    }
}