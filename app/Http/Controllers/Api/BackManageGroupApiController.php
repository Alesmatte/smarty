<?php


namespace App\Http\Controllers\Api;


use App\Model\Groups;
use App\Services\GroupsService;
use App\Support\RequestInput;

class BackManageGroupApiController
{


    public function deleteGroup(RequestInput $requestInput) {
        $data = $requestInput->getArguments();
        $groupService = new GroupsService();
        $group = new Groups();
        $group->setIdGruppo((int) ($data['idGroup']));
        $group = $groupService->getGroupById($group->getIdGruppo());
        if ($group->getIdGruppo() == 1 || $group->getIdGruppo() == 2 || $group->getIdGruppo() == 3) {
            session()->flash()->set(
                'errorMessage',
                'Siamo spiacenti, il gruppo ' . $group->getNome() . ' non può essere eliminato!'
            );
        } else {
            if ($groupService->deleteGroup($group) == 1) {
                session()->flash()->set(
                    'succesMessage',
                    'Gruppo ' . $group->getNome() . ' eliminato con successo!'
                );
            } else {
                session()->flash()->set(
                    'errorMessage',
                    'Errore nella cancellazione del gruppo ' . $group->getNome() . ' !'
                );
            }
        }
    }
}