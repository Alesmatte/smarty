<?php


namespace App\Http\Controllers\Api;


use App\Services\FilmMakerService;
use App\Services\FilmService;
use App\Services\SerieTvService;
use App\Support\RequestInput;
use App\Support\View;

class SearchApiController
{
    public function showSuggest(RequestInput $requestInput, View $view)
    {
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $filmmakerService = new FilmMakerService();
        $resultOpera = array();
        $resultFilmList = $filmService->getFilmByTitoloOrTitoloOrigSearch($requestInput->getQueryString()['user_keywords']);
        if (!empty($resultFilmList)) {
            foreach ($resultFilmList as $item) {
                array_push($resultOpera, $item);
            }
        }
        $resultSerieTvList = $serieTvService->getSerieTvByTitoloOrTitoloOrigSearch($requestInput->getQueryString()['user_keywords']);
        if (!empty($resultSerieTvList)) {
            foreach ($resultSerieTvList as $item) {
                array_push($resultOpera, $item);
            }
        }
        $resultFilmmakerList = $filmmakerService->getFilmmakerByNomeOrCognomeSearch($requestInput->getQueryString()['user_keywords']);
        if (!empty($resultFilmmakerList)) {
            foreach ($resultFilmmakerList as $item) {
                array_push($resultOpera, $item);
            }
        }
        return $view('components.search-suggest-list', ["resultOpera" => $resultOpera]);
    }

    public function showRelated(View $view)
    {
        $filmService = new FilmService();
        $serieTvService = new SerieTvService();
        $resultOpera = array();
        $filmPiuVotato = $filmService->getFilmPiuVotato();
        $filmPiuVotato->testoTipo = 'Film più votato fino ad ora';
        if (isset($filmPiuVotato->titolo)) {
            array_push($resultOpera, $filmPiuVotato);
        }
        $filmProssimo = $filmService->getProssimoFilmInUscita();
        $filmProssimo->testoTipo = 'Il prossimo film in uscita';
        if (isset($filmProssimo->titolo)) {
            array_push($resultOpera, $filmProssimo);
        }
        $serieTvPiuVotato = $serieTvService->getSerieTvPiuVotata();
        $serieTvPiuVotato->testoTipo = 'Serie tv più votata fino ad ora';
        if (isset($serieTvPiuVotato->titolo)) {
            array_push($resultOpera, $serieTvPiuVotato);
        }
        $serieTvProssimo = $serieTvService->getProssimaSerieTvInUscita();
        $serieTvProssimo->testoTipo = 'La prossima serie tv in uscita';
        if (isset($serieTvProssimo->titolo)) {
            array_push($resultOpera, $serieTvProssimo);
        }
        return $view('components.search-related-list', ["resultOpera" => $resultOpera]);
    }

}