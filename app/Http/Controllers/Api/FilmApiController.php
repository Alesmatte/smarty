<?php


namespace App\Http\Controllers\Api;


use App\Model\Film;
use App\Services\FilmService;
use App\Support\RequestInput;

class FilmApiController
{

    public function deleteFilm(RequestInput $requestInput)
    {
        $parameters = $requestInput->getArguments();
        $filmService = new FilmService();
        $film = $filmService->getFilmById($parameters['idFilm']);
        if ($filmService->deleteFilm($film) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Film ' . $film->getTitolo() . ' eliminato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nella cancellazione del Film ' . $film->getTitolo() . ' !'
            );
        }
    }

    public function toggleInSala(RequestInput $requestInput)
    {
        $parameters = $requestInput->getArguments();
        $filmService = new FilmService();
        $film = $filmService->getFilmParzialeById($parameters['idFilm']);
        if ($film->getInSala() == 1) {
            $film->setInSala(0);
        } else {
            $film->setInSala(1);
        }
        if ($filmService->updateToggleInSala($film) == 1) {
            session()->flash()->set(
                'succesMessage',
                'Film ' . $film->getTitolo() . ' aggiornato con successo!'
            );
        } else {
            session()->flash()->set(
                'errorMessage',
                'Errore nell\'aggiornamento del Film ' . $film->getTitolo() . '!'
            );
        }
    }
}