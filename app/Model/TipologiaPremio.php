<?php


namespace App\Model;


class TipologiaPremio
{

    private $idTipologiaPremio;
    private $nome;

    /**
     * TipologiaPremio constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getidTipologiaPremio()
    {
        return $this->idTipologiaPremio;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $idTipologiaPremio
     */
    public function setidTipologiaPremio($idTipologiaPremio): void
    {
        $this->idTipologiaPremio = $idTipologiaPremio;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }


}