<?php


namespace App\Model;


class Premio
{
    private $idPremio;
    private $nome;
    private $descrizione;
    private $localita;

    private array $imgPremio;

    /**
     * Premio constructor.
     */
    public function __construct()
    {
        $this->imgPremio = [new Immagine()];
    }

    /**
     * @return mixed
     */
    public function getIdPremio()
    {
        return $this->idPremio;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @return mixed
     */
    public function getLocalita()
    {
        return $this->localita;
    }

    /**
     * @return array
     */
    public function getImgPremio(): array
    {
        return $this->imgPremio;
    }

    /**
     * @param mixed $idPremio
     */
    public function setIdPremio($idPremio): void
    {
        $this->idPremio = $idPremio;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @param mixed $localita
     */
    public function setLocalita($localita): void
    {
        $this->localita = $localita;
    }

    /**
     * @param array $imgPremio
     */
    public function setImgPremio(array $imgPremio): void
    {
        $this->imgPremio = $imgPremio;
    }



}