<?php


namespace App\Model;

use App\Model\Immagine as Img;

class User
{
    private $idUser;
    private $nome;
    private $cognome;
    private $email;
    private $password;
    private $token;
    private $dataNascita;
    private $tipologiaUtenza;
    private $codiceAlbo;
    private $nomeAzienda;
    private $biografia;
    private $stato;

    private Immagine $avatar;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->avatar = new Immagine();
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome): void
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDataNascita()
    {
        return $this->dataNascita;
    }

    /**
     * @param mixed $dataNascita
     */
    public function setDataNascita($dataNascita): void
    {
        $this->dataNascita = $dataNascita;
    }

    /**
     * @return mixed
     */
    public function getTipologiaUtenza()
    {
        return $this->tipologiaUtenza;
    }

    /**
     * @param mixed $tipologiaUtenza
     */
    public function setTipologiaUtenza($tipologiaUtenza): void
    {
        $this->tipologiaUtenza = $tipologiaUtenza;
    }

    /**
     * @return mixed
     */
    public function getCodiceAlbo()
    {
        return $this->codiceAlbo;
    }

    /**
     * @param mixed $codiceAlbo
     */
    public function setCodiceAlbo($codiceAlbo): void
    {
        $this->codiceAlbo = $codiceAlbo;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar(Img $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getBiografia()
    {
        return $this->biografia;
    }

    /**
     * @param mixed $biografia
     */
    public function setBiografia($biografia): void
    {
        $this->biografia = $biografia;
    }

    /**
     * @return mixed
     */
    public function getStato()
    {
        return $this->stato;
    }

    /**
     * @param mixed $stato
     */
    public function setStato($stato): void
    {
        $this->stato = $stato;
    }

    /**
     * @return mixed
     */
    public function getNomeAzienda()
    {
        return $this->nomeAzienda;
    }

    /**
     * @param mixed $nomeAzienda
     */
    public function setNomeAzienda($nomeAzienda): void
    {
        $this->nomeAzienda = $nomeAzienda;
    }



}