<?php


namespace App\Model;

use App\Model\Immagine as Img;

class SerieTv
{
    private $idSerieTv;
    private $tiloloOriginale;
    private $titolo;
    private $descrizione;
    private $trama;
    private $tramaBreve;
    private $dataSviluppoSerieTv;
    private $dataUscita;
    private $nazionalita = array();
    private $ageRating;

    private Immagine $imgCopertina;
    private array $recensioni;
    private array $distributori;
    private array $generi;
    private array $nominations;
    private array $ruoli;
    private array $videos;
    private array $mediaFilmSerieTvs;   // img legate alla Serie TV riguardanti i Filmmaker
    private array $imgs;                // img generali (poster...) che non c'entrano con il Filmmaker
    private array $stagioni;

    /**
     * SerieTv constructor.
     */
    public function __construct()
    {
        $this->imgCopertina = new Img();
        $this->recensioni = [new Recensione()];
        $this->distributori = [new Distributore()];
        $this->generi = [new Genere()];
        $this->nominations = [new Nomination()];
        $this->ruoli = [new Ruolo()];
        $this->videos = [new Video()];
        $this->mediaFilmSerieTvs = [new MediaFilmSerieTv()];
        $this->imgs = [new Immagine()];
        $this->stagioni = [new Stagione()];
    }

    /**
     * @return mixed
     */
    public function getIdSerieTv()
    {
        return $this->idSerieTv;
    }

    /**
     * @param mixed $idSerieTv
     */
    public function setIdSerieTv($idSerieTv): void
    {
        $this->idSerieTv = $idSerieTv;
    }

    /**
     * @return mixed
     */
    public function getTiloloOriginale()
    {
        return $this->tiloloOriginale;
    }

    /**
     * @param mixed $tiloloOriginale
     */
    public function setTiloloOriginale($tiloloOriginale): void
    {
        $this->tiloloOriginale = $tiloloOriginale;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo): void
    {
        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getTrama()
    {
        return $this->trama;
    }

    /**
     * @param mixed $trama
     */
    public function setTrama($trama): void
    {
        $this->trama = $trama;
    }

    /**
     * @return mixed
     */
    public function getTramaBreve()
    {
        return $this->tramaBreve;
    }

    /**
     * @param mixed $tramaBreve
     */
    public function setTramaBreve($tramaBreve): void
    {
        $this->tramaBreve = $tramaBreve;
    }

    /**
     * @return mixed
     */
    public function getDataSviluppoSerieTv()
    {
        return $this->dataSviluppoSerieTv;
    }

    /**
     * @param mixed $dataSviluppoSerieTv
     */
    public function setDataSviluppoSerieTv($dataSviluppoSerieTv): void
    {
        $this->dataSviluppoSerieTv = $dataSviluppoSerieTv;
    }

    /**
     * @return mixed
     */
    public function getDataUscita()
    {
        return $this->dataUscita;
    }

    /**
     * @param mixed $dataUscita
     */
    public function setDataUscita($dataUscita): void
    {
        $this->dataUscita = $dataUscita;
    }

    /**
     * @return array
     */
    public function getNazionalita(): array
    {
        return $this->nazionalita;
    }

    /**
     * @param array $nazionalita
     */
    public function setNazionalita(array $nazionalita): void
    {
        $this->nazionalita = $nazionalita;
    }

    /**
     * @return mixed
     */
    public function getAgeRating()
    {
        return $this->ageRating;
    }

    /**
     * @param mixed $ageRating
     */
    public function setAgeRating($ageRating): void
    {
        $this->ageRating = $ageRating;
    }

    /**
     * @return mixed
     */
    public function getImgCopertina()
    {
        return $this->imgCopertina;
    }

    /**
     * @param mixed $imgCopertina
     */
    public function setImgCopertina(Img $imgCopertina): void
    {
        $this->imgCopertina = $imgCopertina;
    }

    /**
     * @return array
     */
    public function getRecensioni(): array
    {
        return $this->recensioni;
    }

    /**
     * @param array $recensioni
     */
    public function setRecensioni(array $recensioni): void
    {
        $this->recensioni = $recensioni;
    }

    /**
     * @return array
     */
    public function getDistributori(): array
    {
        return $this->distributori;
    }

    /**
     * @param array $distributori
     */
    public function setDistributori(array $distributori): void
    {
        $this->distributori = $distributori;
    }

    /**
     * @return array
     */
    public function getGeneri(): array
    {
        return $this->generi;
    }

    /**
     * @param array $generi
     */
    public function setGeneri(array $generi): void
    {
        $this->generi = $generi;
    }

    /**
     * @return array
     */
    public function getNominations(): array
    {
        return $this->nominations;
    }

    /**
     * @param array $nominations
     */
    public function setNominations(array $nominations): void
    {
        $this->nominations = $nominations;
    }

    /**
     * @return array
     */
    public function getRuoli(): array
    {
        return $this->ruoli;
    }

    /**
     * @param array $ruoli
     */
    public function setRuoli(array $ruoli): void
    {
        $this->ruoli = $ruoli;
    }

    /**
     * @return array
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos(array $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @return array
     */
    public function getMediaFilmSerieTvs(): array
    {
        return $this->mediaFilmSerieTvs;
    }

    /**
     * @param array $mediaFilmSerieTvs
     */
    public function setMediaFilmSerieTvs(array $mediaFilmSerieTvs): void
    {
        $this->mediaFilmSerieTvs = $mediaFilmSerieTvs;
    }

    /**
     * @return array
     */
    public function getImgs(): array
    {
        return $this->imgs;
    }

    /**
     * @param array $imgs
     */
    public function setImgs(array $imgs): void
    {
        $this->imgs = $imgs;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getStagioni()
    {
        return $this->stagioni;
    }

    /**
     * @param mixed $stagioni
     */
    public function setStagioni($stagioni): void
    {
        $this->stagioni = $stagioni;
    }



}