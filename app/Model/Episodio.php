<?php


namespace App\Model;

use App\Model\Immagine as Img;

class Episodio
{
    private $idEpisodio;
    private $titolo;
    private $descrizione;
    private $numero;
    private $durata;
    private $dataInOnda;
    private $numeroStagione;

    private Immagine $img;           //riferimento a classe Immagine

    /**
     * Episodio constructor.
     */
    public function __construct()
    {
        $this->img = new Img();
    }

    /**
     * @return mixed
     */
    public function getIdEpisodio()
    {
        return $this->idEpisodio;
    }

    /**
     * @param mixed $idEpisodio
     */
    public function setIdEpisodio($idEpisodio): void
    {
        $this->idEpisodio = $idEpisodio;
    }

    /**
     * @return mixed
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param mixed $titolo
     */
    public function setTitolo($titolo): void
    {
        $this->titolo = $titolo;
    }

    /**
     * @return mixed
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param mixed $descrizione
     */
    public function setDescrizione($descrizione): void
    {
        $this->descrizione = $descrizione;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getDurata()
    {
        return $this->durata;
    }

    /**
     * @param mixed $durata
     */
    public function setDurata($durata): void
    {
        $this->durata = $durata;
    }

    /**
     * @return mixed
     */
    public function getDataInOnda()
    {
        return $this->dataInOnda;
    }

    /**
     * @param mixed $dataInOnda
     */
    public function setDataInOnda($dataInOnda): void
    {
        $this->dataInOnda = $dataInOnda;
    }

    /**
     * @return mixed
     */
    public function getNumeroStagione()
    {
        return $this->numeroStagione;
    }

    /**
     * @param mixed $numeroStagione
     */
    public function setNumeroStagione($numeroStagione): void
    {
        $this->numeroStagione = $numeroStagione;
    }


    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg(Img $img): void
    {
        $this->img = $img;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "Episodio {" . $this->idEpisodio . ", " . $this->titolo . ", "
            . $this->descrizione . ", " . $this->numero . ", " . $this->durata . ", "
            . $this->dataInOnda . ", " . $this->numeroStagione . ", "
            . $this->img . "} ";
    }


}