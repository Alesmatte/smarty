<?php


namespace App\Model;


class Stagione
{
    private $idStagione;
    private $numero;
    private $inCorso;

    private array $episodi;

    /**
     * Stagione constructor.
     */
    public function __construct()
    {
        $this->episodi = [new Episodio()];
    }

    /**
     * @return mixed
     */
    public function getIdStagione()
    {
        return $this->idStagione;
    }

    /**
     * @param mixed $idStagione
     */
    public function setIdStagione($idStagione): void
    {
        $this->idStagione = $idStagione;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getEpisodi()
    {
        return $this->episodi;
    }

    /**
     * @param mixed $episodi
     * ARRAY DI EPI
     */
    public function setEpisodi(array $episodi): void
    {
        $this->episodi = $episodi;
    }

    /**
     * @return mixed
     */
    public function getInCorso()
    {
        return $this->inCorso;
    }

    /**
     * @param mixed $inCorso
     */
    public function setInCorso($inCorso): void
    {
        $this->inCorso = $inCorso;
    }


}