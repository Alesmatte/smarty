<?php


namespace App\Model;


class Rivista
{
    private $idRivista;
    private $nomeAzienda;

    private array $utenti;

    /**
     * Rivista constructor.
     */
    public function __construct()
    {
        $this->utenti = [new User()];
    }

    /**
     * @return mixed
     */
    public function getIdRivista()
    {
        return $this->idRivista;
    }

    /**
     * @param mixed $idRivista
     */
    public function setIdRivista($idRivista): void
    {
        $this->idRivista = $idRivista;
    }

    /**
     * @return mixed
     */
    public function getNomeAzienda()
    {
        return $this->nomeAzienda;
    }

    /**
     * @param mixed $nomeAzienda
     */
    public function setNomeAzienda($nomeAzienda): void
    {
        $this->nomeAzienda = $nomeAzienda;
    }

    /**
     * @return array
     */
    public function getUtenti(): array
    {
        return $this->utenti;
    }

    /**
     * @param array $utenti
     */
    public function setUtenti(array $utenti): void
    {
        $this->utenti = $utenti;
    }


}