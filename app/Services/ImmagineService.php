<?php


namespace App\Services;


use App\Model\Immagine;
use App\Model\Video;
use App\Services\Implementation\ImmagineServiceImplementation;

class ImmagineService
{

    /**
     * ImmagineService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Immagine $immagine
     * @param $idFilmmaker
     * @return mixed
     */
    public function insertImgFilmmaker(Immagine $immagine, $idFilmmaker) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, $idFilmmaker);
    }

    /**
     * @param Immagine $immagine
     * @param $idFilm
     * @return mixed
     */
    public function insertImgFilm(Immagine $immagine, $idFilm) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, $idFilm);
    }

    /**
     * @param Immagine $immagine
     * @param $idSerieTv
     * @return mixed
     */
    public function insertImgSerieTv(Immagine $immagine, $idSerieTv) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, $idSerieTv);
    }

    /**
     * @param Immagine $immagine
     * @param $idEpisodio
     * @return mixed
     */
    public function insertImgEpisodio(Immagine $immagine, $idEpisodio) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, $idEpisodio);
    }

    /**
     * @param Immagine $immagine
     * @param $idDistributore
     * @return mixed
     */
    public function insertImgDistributore(Immagine $immagine, $idDistributore) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, null, $idDistributore);
    }

    /**
     * @param Immagine $immagine
     * @param $idRuolo
     * @return mixed
     */
    public function insertImgRuolo(Immagine $immagine, $idRuolo) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, null, null, $idRuolo);
    }

    /**
     * @param Immagine $immagine
     * @param $idUser
     * @return mixed
     */
    public function insertImgUser(Immagine $immagine, $idUser) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, null, null, null, $idUser);
    }

    /**
     * @param Immagine $immagine
     * @param $idPremio
     * @return mixed
     */
    public function insertImgPremio(Immagine $immagine, $idPremio) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, null, null, null, null, $idPremio);
    }


    /**
     * @param Immagine $immagine
     * @param $idVideo
     * @return mixed
     */
    public function insertImgVideo(Immagine $immagine, $idVideo) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine, null, null, null, null, null, null, null, null, $idVideo);
    }

    public function insertImgSlider(Immagine $immagine) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->insertImg($immagine);
    }

    /**
     * @param Immagine $immagine
     * @param $idFilmmaker
     * @return mixed
     */
    public function updateImgFilmmaker(Immagine $immagine, $idFilmmaker) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, $idFilmmaker);
    }

    /**
     * @param Immagine $immagine
     * @param $idFilm
     * @return mixed
     */
    public function updateImgFilm(Immagine $immagine, $idFilm) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, $idFilm);
    }

    /**
     * @param Immagine $immagine
     * @param $idSerieTv
     * @return mixed
     */
    public function updateImgSerieTv(Immagine $immagine, $idSerieTv) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, $idSerieTv);
    }

    /**
     * @param Immagine $immagine
     * @param $idEpisodio
     * @return mixed
     */
    public function updateImgEpisodio(Immagine $immagine, $idEpisodio) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, $idEpisodio);
    }

    /**
     * @param Immagine $immagine
     * @param $idDistributore
     * @return mixed
     */
    public function updateImgDistributore(Immagine $immagine, $idDistributore) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, null, $idDistributore);
    }

    /**
     * @param Immagine $immagine
     * @param $idRuolo
     * @return mixed
     */
    public function updateImgRuolo(Immagine $immagine, $idRuolo) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, null, null, $idRuolo);
    }

    /**
     * @param Immagine $immagine
     * @param $idUser
     * @return mixed
     */
    public function updateImgUser(Immagine $immagine, $idUser) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, null, null, null, $idUser);
    }

    /**
     * @param Immagine $immagine
     * @param $idPremio
     * @return mixed
     */
    public function updateImgPremio(Immagine $immagine, $idPremio) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, null, null, null, null, $idPremio);
    }


    /**
     * @param Immagine $immagine
     * @param $idVideo
     * @return mixed
     */
    public function updateImgVideo(Immagine $immagine, $idVideo) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine, null, null, null, null, null, null, null, null, $idVideo);
    }

    public function updateImgSlider(Immagine $immagine) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->updateImg($immagine);
    }

    /**
     * @param string $entita   a scelta tra filmmaker, film, serieTv, episodio, distributore, ruolo, user
     * @param int $idEntita  id dell'entità selezionata
     * @param int $tipologia   tipo di immagine
     *                           0 = avatar
     *                           1 = profilo
     *                           2 = copertina
     *                           3 = cover video
     *                           4 = slider Film
     *                           5 = generica
     *                           6 = poster
     *                           7 = video
     *                           8 = slider SerieTv
     *                           9 = slider Premi
     *
     * @return array   di immagini
     */
    public function getImgByEntitaAndIdEntitaAndTipologia(string $entita, $idEntita, $tipologia): array {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia($entita, $idEntita, $tipologia);
    }

    public function getImgByTipologia(int $tipo): array {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->getImgByTipologia($tipo);
    }

    public function deleteImg(Immagine $immagine) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->deleteImg($immagine);
    }

    public function deleteImgByIdFilm(int $idFilm){
        $imgServiceImpl = new ImmagineServiceImplementation();

        return $imgServiceImpl->deleteImgByIdFilm($idFilm);
    }

    public function deleteImgByIdVideo(Video $video) {
        $imgServiceImpl = new ImmagineServiceImplementation();
        return $imgServiceImpl->deleteImgByIdVideo($video);
    }
}