<?php


namespace App\Services;


use App\Model\Distributore;
use App\Services\Implementation\DistributoreServiceImplementation;

class DistributoreService
{

    /**
     * DistributoreService constructor.
     */
    public function __construct()
    {
    }

    public function insertDistributore(Distributore $distributore) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->insertDistributore($distributore);
    }
    public function insertDistributoreFilm($idDistributore, $idFilm) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->insertDistributoreFilm($idDistributore, $idFilm);
    }

    public function insertDistributoreSerieTv($idDistributore, $idSerieTv) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->insertDistributoreSerieTv($idDistributore, $idSerieTv);
    }

    public function getAllDistrucutore() {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->getAllDistributore();
    }

    public function getDistrucutoreById(int $idDistributore): Distributore
    {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->getDistributoreById($idDistributore);
    }

    public function getDistributoriByIdFilm($idFilm): array {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->getDistributoriByIdFilm($idFilm);
    }

    public function getDistributoriByIdSerieTv($idSerieTv): array {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->getDistributoriByIdSerieTv($idSerieTv);
    }

    public function updateDistributore(Distributore $distributore) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->updateDistributore($distributore);
    }

    public function deleteDistributoreFilm(int $idFilm) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->deleteDistributoreFilm($idFilm);
    }

    public function deleteDistributore(Distributore $distributore) {
        $distributoreServiceImpl = new DistributoreServiceImplementation();
        return $distributoreServiceImpl->deleteDistributore($distributore);
    }
}