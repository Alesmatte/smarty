<?php


namespace App\Services;

use App\Model\Stagione;
use App\Services\Implementation\EpisodioServiceImplementation;
use App\Services\Implementation\StagioneServiceImplementation;


class StagioneService
{
    /**
     * StagioneService constructor.
     */
    public function __construct()
    {
    }
    public function getStagioneById(int $idStagione): Stagione
    {
        $stagioneServiceImp = new StagioneServiceImplementation();
        $stagione = $stagioneServiceImp->getStagioneByID($idStagione);
        $episodioService = new EpisodioService();
        $stagione->setEpisodi($episodioService->getEpisodiStagioneByIdStagione($idStagione));
        return $stagione;
    }

    public function insertStagione(Stagione $stagione, $idSerieTv){
        $stagioneServiceImp = new StagioneServiceImplementation();
        $idStagione = $stagioneServiceImp->insertStagione($stagione, $idSerieTv);
        return $idStagione;
    }

    public function updateStagione(Stagione $stagione, $idSerieTv){
        $stagioneServiceImp = new StagioneServiceImplementation();
        $idStagione = $stagioneServiceImp->updateStagione($stagione, $idSerieTv);
        return $idStagione;
    }

    /** cambia lo stato della stagione da in corso a non in corso */
    public function setStagioneInCorsoById(int $idStagione, $idSerieTv, int $value)
    {
        $stagioneServiceImp = new StagioneServiceImplementation();
        $stagione1 = $stagioneServiceImp->getStagioneByID($idStagione);
        $stagione1->setInCorso($value);
        $risultato = $stagioneServiceImp->updateStagione($stagione1, $idSerieTv);
        return $risultato;
    }

    public function getIdStagioneByIdSerieTvNumStagione(int $numStagione, int $idSerieTv)
    {
        $stagioneServiceImp = new StagioneServiceImplementation();
        $IdStagione = $stagioneServiceImp->getIdStagioneByIdSerieTvNumStagione($numStagione, $idSerieTv);
        return $IdStagione->getIdStagione();
    }

    /*public function deleteStagioneAndEpisodiByIdStagione(int $idStagione): Stagione
    {
        $episodioServiceImp = new EpisodioServiceImplementation();
        $episodioServiceImp->deleteEpisodiByIdStagione($idStagione);
        $stagioneServiceImp = new StagioneServiceImplementation();
        $stagioneServiceImp->deleteStagione($idStagione);
    }*/
}