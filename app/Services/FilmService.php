<?php


namespace App\Services;


use App\Model\Distributore;
use App\Model\Film;
use App\Model\Filmmaker;
use App\Model\Genere;
use App\Model\Immagine;
use App\Model\Nomination;
use App\Model\Premio;
use App\Model\Ruolo;
use App\Model\TipologiaPremio;
use App\Model\Video;
use App\Services\Implementation\FilmServiceImplementation;
use App\Services\Implementation\RecensioneServiceImplementation;
use App\Support\RequestInput;
use Exception;
use Slim\Exception\HttpInternalServerErrorException;

class FilmService
{

    /**
     * FilmService constructor.
     */
    public function __construct()
    {
    }

    private function setFilmComponent(Film $film, FilmServiceImplementation $filmServiceImpl, bool $notRicerca = true): Film
    {

        if ($notRicerca) {
            $film->setNazionalita($filmServiceImpl->getNazionalitasByFilmId($film->getIdFilm()));
        }

        $imgService = new ImmagineService();
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
        if (!empty($result)) {
            $film->setImgCopertina($result[0]);
        }

        $recensioneService = new RecensioneService();
        $film->setRecensioni($recensioneService->getRecensioniByIdFilm($film->getIdFilm()));

        $distributoreService = new DistributoreService();
        $film->setDistributori($distributoreService->getDistributoriByIdFilm($film->getIdFilm()));

        $genereService = new GenereService();
        $film->setGeneri($genereService->getGenereByIdFilm($film->getIdFilm()));;
        $nominationsService = new NominationService();
        $film->setNominations($nominationsService->getNominationByIdFilm($film->getIdFilm()));
        //dd("ho super");
        $ruoloServices = new RuoloService();
        $ruoliList = $ruoloServices->getRuoliByidFilm($film->getIdFilm());

        $film->setRuoli($ruoliList);

        if ($notRicerca) {
            $videoService = new VideoService();
            $film->setVideos($videoList = $videoService->getVideoByIdFilm($film->getIdFilm()));
        }

        if ($notRicerca) {
            foreach ($ruoliList as $ruolo) {
                $film->setMediaFilmSerieTvs($imgService->getImgByEntitaAndIdEntitaAndTipologia('ruolo', $ruolo->getIdRuolo(), 5));
            }
        }

        if ($notRicerca) {
            $imgs_foto = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 5);
            $imgs_poster = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 6);
            $film->setImgs(array_merge($imgs_foto, $imgs_poster));
        }

        return $film;
    }

    public function setFilmComponentParzialiCarosello(Film $film, bool $inCarosello = true): Film
    {
        $imgService = new ImmagineService();
        $idFilm = $film->getIdFilm();
        if (isset($idFilm)) {
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
            if (!empty($result)) {
                $film->setImgCopertina($result[0]);
            }
            if ($inCarosello) {
                $film->mediaVotoRedCarpet = number_format($this->getMediaVotiFilm($film), 2, '.', '');
            }
        }
        return $film;
    }

    public function setComponentFilmParzialiPerLista($film, $filmServiceImpl): Film
    {
        $distributoreService = new DistributoreService();
        $genereService = new GenereService();
        $imgService = new ImmagineService();
        $ruoloServices = new RuoloService();
        $videoService = new VideoService();
        $film->setNazionalita($filmServiceImpl->getNazionalitasByFilmId($film->getIdFilm()));
        $film->setDistributori($distributoreService->getDistributoriByIdFilm($film->getIdFilm()));
        $film->setGeneri($genereService->getGenereByIdFilm($film->getIdFilm()));
        $ruoliList = $ruoloServices->getRuoliByidFilm($film->getIdFilm());
        $film->setRuoli($ruoliList);
        $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
        if (!empty($result)) {
            $film->setImgCopertina($result[0]);
        }
        $film->setVideos($videoList = $videoService->getVideoByIdFilm($film->getIdFilm()));
        $imgs_foto = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 5);
        $imgs_poster = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 6);
        $film->setImgs(array_merge($imgs_foto, $imgs_poster));
        $film->mediaRedCarpet = self::getMediaVotiFilm($film);
        $film->mediaCritica = self::getMediaVotiCritici($film);
        $film->mediaPubblico = self::getMediaVotiUtenti($film);
        $film->maggiorediCinqueVoti = self::maggiorediCinqueVoti($film);

        return $film;
    }

    /**
     * @param int $idFilm
     * @return Film
     */
    public function getFilmById(int $idFilm): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $film = $filmServiceImpl->getFilmById($idFilm);

        return self::setFilmComponent($film, $filmServiceImpl);
    }

    public function getFilmParzialeById(int $idFilm): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getFilmById($idFilm);

    }

    public function getFilmByIdPerRecensioni(int $idFilm): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getFilmById($idFilm);
    }

    public function getFilmByIdRecensioni(int $idRecensione): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return self::setFilmComponentParzialiCarosello($filmServiceImpl->getFilmByIdRecensioni($idRecensione));
    }

    public function getAllFilmPerRecensioni()
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmMezziList = $filmServiceImpl->getAllFilmPerRecensioni();
        $filmList = array();

        foreach ($filmMezziList as $key => $film) {
            $imgService = new ImmagineService();
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
            if (!empty($result)) {
                $film->setImgCopertina($result[0]);
            }
            array_push($filmList, $film);
        }
        return $filmList;
    }

    public function getAllFilmPerRecensioniSearch(string $search)
    {
        $search = '%' . $search . '%';
        $filmServiceImpl = new FilmServiceImplementation();
        $filmMezziList = $filmServiceImpl->getAllFilmPerRecensioniSearch($search);
        $filmList = array();
        if (!empty($filmMezziList)) {
            foreach ($filmMezziList as $key => $film) {
                $imgService = new ImmagineService();
                $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
                if (!empty($result)) {
                    $film->setImgCopertina($result[0]);
                }
                array_push($filmList, $film);
            }
        }
        return $filmList;
    }

    /**
     * @return array
     */
    public function getAllFilm(bool $notRicerca = true): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getAllFilms();
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponent($film, $filmServiceImpl, $notRicerca);
        }
        // TODO da definire cosa torna per la lista film del backend
        return $filmList;
    }

    public function getAllFilmParziali(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getAllFilms();
    }

    public function getAllFilmParzialiPerLista(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmParzialiList = $filmServiceImpl->getAllFilms();
        $filmList = array();
        foreach ($filmParzialiList as $film) {
            array_push($filmList, self::setComponentFilmParzialiPerLista($film, $filmServiceImpl));
        }
        return $filmList;
    }

    public function getAllFilmParzialiPerListaPaginati($limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmParzialiList = $filmServiceImpl->getAllFilmPerListaPaginati($limit, $offset);
        $filmList = array();
        foreach ($filmParzialiList as $film) {
            array_push($filmList, self::setComponentFilmParzialiPerLista($film, $filmServiceImpl));
        }
        return $filmList;
    }

    public function countAllFilmParzialiPerLista()
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countAllFilmParziali();
    }

    /**
     * @return array
     */
    public function getFilmsInSala(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmInSala();
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponentParzialiCarosello($film);
        }
        return $filmList;
    }

    public function getUltimiFilmCarosello(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getUltimiFilmCarosello();
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponentParzialiCarosello($film);
        }
        return $filmList;
    }

    public function getFilmByFilmmakerCarosello(Filmmaker $filmmaker): array
    {
        $filmList = array_slice($this->getFilmByFilmmaker($filmmaker), 0, 16);
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponentParzialiCarosello($film);
        }
        return $filmList;
    }

    public function getFilmInSalaPaginato($limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmListParz = $filmServiceImpl->getFilmInSalaPaginato($limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $key => $film) {
            array_push($filmList, self::setComponentFilmParzialiPerLista($film, $filmServiceImpl));
        }
        return $filmList;
    }

    public function countFilmInSala(): int
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmInSala();
    }

    public function maggiorediCinqueVoti(Film $film): bool
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->maggiorediCinqueVoti($film);
    }

    public function getFilmByAnnoMinoreDi($anno, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnnoMinoreDi($anno . "/1/01");
        foreach ($filmList as $k => $stv) {
            $filmList[$k] = self::setFilmComponent($stv, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getFilmByAnnoMinoreUgualeDi($anno, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnnoMinoreUgualeDi($anno . "/12/31");
        foreach ($filmList as $k => $stv) {
            $filmList[$k] = self::setFilmComponent($stv, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    /** UGUALE A */
    public function getFilmByAnno($anno, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnno($anno . "/1/1", $anno . "/12/31");
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponent($film, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getFilmByAnnoPaginato(int $anno, $limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnnoPaginato($anno . "/1/1", $anno . "/12/31", $limit, $offset);
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setComponentFilmParzialiPerLista($film, $filmServiceImpl);
        }
        return $filmList;
    }

    public function countFilmByAnno(int $anno)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmByAnno($anno . "/1/1", $anno . "/12/31");
    }

    public function getFilmByAnnoMaggioreUgualeDi($anno, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnnoMaggioreUgualeDi($anno . "/1/01");
        foreach ($filmList as $k => $stv) {
            $filmList[$k] = self::setFilmComponent($stv, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getFilmByAnnoMaggioreDi($anno, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByAnnoMaggioreDi($anno . "/12/31");
        foreach ($filmList as $k => $stv) {
            $filmList[$k] = self::setFilmComponent($stv, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    /**
     * @param $codiceNazione
     * @return mixed
     */
    public function getFilmByNazione($codiceNazione)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByNazione($codiceNazione);
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponent($film, $filmServiceImpl);
        }
        return $filmList;
    }

    public function getFilmByNazionePaginato($codiceNazione, $limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmListParz = $filmServiceImpl->getFilmByNazionePaginato($codiceNazione, $limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $key => $film) {
            array_push($filmList, self::setComponentFilmParzialiPerLista($film, $filmServiceImpl));
        }
        return $filmList;
    }

    public function countFilmByNazione($codiceNazione): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmByNazione($codiceNazione);
    }

    public function getFilmByTitoloSearch($titolo): array
    {
        $titolo = '%' . $titolo . '%';
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByTitoloSearch($titolo);
        foreach ($filmList as $key => $film) {
            $filmList[$key] = self::setFilmComponent($film, $filmServiceImpl);
        }
        return $filmList;
    }

    public function getFilmByFilmmaker(Filmmaker $filmmaker)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByFilmmaker($filmmaker);
        foreach ($filmList as $key => $film) {
            $serieTvList[$key] = self::setFilmComponent($film, $filmServiceImpl);
        }
        return $filmList;
    }

    /**
     * @param Film $film
     * @return mixed
     */
    public function insertFilm(Film $film)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $idFilm = $filmServiceImpl->insertFilm($film);

        /* inserisco nella tabella di collegamento tra film e distributore */
        $distributoreService = new DistributoreService();
        foreach ($film->getDistributori() as $distributore) {
            $distributoreService->insertDistributoreFilm($distributore->getIdDistributore(), $idFilm);
        }
        /* inserisco nella tabella di collegamento tra film e NazioneProduzione */
        foreach ($film->getNazionalita() as $codiceNazione) {
            $filmServiceImpl->insertNazioneProduzionesFilms($codiceNazione, $idFilm);
        }
        /* inserisco nella tabella di collegamento tra film e genereService */
        $genereService = new GenereService();
        foreach ($film->getGeneri() as $genere) {
            $genereService->insertGenereFilm($genere->getIdGenere(), $idFilm);
        }
        /* inserisco nella tabella di collegamento tra film e genereService */
        $nominationService = new NominationService;
        foreach ($film->getNominations() as $nomination) {
            $nominationService->insertNominationFilm($nomination->getIdNomination(), $idFilm);
            $nominationService->insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());
        }

        /* inserisco nella tabella di collegamento tra film e genereService */
        $ruoloService = new RuoloService();
        foreach ($film->getRuoli() as $ruolo) {
            $ruoloService->insertRuoloFilm($ruolo->getIdRuolo(), $idFilm);
        }

        return $idFilm;
    }

    public function updateToggleInSala(Film $film)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->updateToggleInSala($film);
    }

    public function updateFilm(Film $film)
    {
        $filmServiceImpl = new FilmServiceImplementation();

        $filmServiceImpl->deleteNazionalitaByIdFilm($film->getIdFilm());
        foreach ($film->getNazionalita() as $codiceNazione) {
            $filmServiceImpl->insertNazioneProduzionesFilms($codiceNazione, $film->getIdFilm());
        }

        $distributoreService = new DistributoreService();
        $distributoreService->deleteDistributoreFilm($film->getIdFilm());
        foreach ($film->getDistributori() as $distributore) {
            $distributoreService->insertDistributoreFilm($distributore->getIdDistributore(), $film->getIdFilm());
        }

        $genereService = new GenereService();
        $genereService->deleteGenereFilmByIdFilm($film->getIdFilm());
        foreach ($film->getGeneri() as $genere) {
            $genereService->insertGenereFilm($genere->getIdGenere(), $film->getIdFilm());
        }

        return $filmServiceImpl->updateFilm($film);
    }

    public function deleteFilm(Film $film)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $distributoreService = new DistributoreService();
        $nominationService = new NominationService();
        $recensioneService = new RecensioneService();
        $immagineService = new ImmagineService();
        $genereService = new GenereService();
        $videoSerrvice = new VideoService();
        $ruoloService = new RuoloService();
        foreach ($film->getRuoli() as $ruolo) {
            $ruoloService->deleteRuoloById($ruolo);
        }
        $distributoreService->deleteDistributoreFilm($film->getIdFilm());
        $filmServiceImpl->deleteNazionalitaByIdFilm($film->getIdFilm());
        $genereService->deleteGenereFilmByIdFilm($film->getIdFilm());
        foreach ($film->getNominations() as $nomination) {
            $nominationService->deleteNominationFilmService($nomination);
        }
        $recensioneService->deleteRecensioneByIdFilm($film->getIdFilm());
        if ($film->getImgCopertina() !== new Immagine()) {
            $result = $immagineService->deleteImg($film->getImgCopertina());
            throw_when($result === 0, "errore non riesco a cancellare l'immagine", HttpInternalServerErrorException::class);
            if ($film->getImgCopertina()->getDato() != null) {
                deleteFile(public_path($film->getImgCopertina()->getDato()));
            }
        }
        if (!empty($film->getImgs())) {
            foreach ($film->getImgs() as $img) {
                if ($img !== new Immagine()) {
                    $result = $immagineService->deleteImg($img);
                    throw_when($result === 0, "errore non riesco a cancellare l'immagine", HttpInternalServerErrorException::class);
                    if ($img->getDato() != null) {
                        deleteFile(public_path($img->getDato()));
                    }
                }
            }
        }

        if (!empty($film->getVideos())) {
            foreach ($film->getVideos() as $video) {
                if ($video !== null) {
                    $immagineVideo = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('video', $video->getIdVideo(), 3);
                    $resultImg = $immagineService->deleteImgByIdVideo($video);
                    throw_when($resultImg === 0, "errore non riesco a cancellare l'immagine del oggetto video", HttpInternalServerErrorException::class);

                    $resultVideo = $videoSerrvice->deleteOnlyVideo($video);

                    if ($result === 0) {
                        // fase di rollback per il video
                        if (!empty($immagineVideo)) {
                            $immagineService->insertImgVideo($immagineVideo[0], $video->getIdVideo());
                            // img è già presente nel filesystem
                        }
                    } else {
                        // procedura di cancellazione foto e video dal filesystem
                        if (!empty($immagineVideo)) {
                            deleteFile(public_path($immagineVideo[0]->getDato()));
                        }
                        deleteFile(public_path($video->getDato()));
                    }
                    throw_when($resultVideo === 0, "errore non riesco a cancellare oggetto video", HttpInternalServerErrorException::class);

                }
            }
        }
        return $filmServiceImpl->deleteFilm($film);
    }

    public function getAllNations(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getAllNazionalitas();
    }

    public function getFilmByTitoloOrTitoloOrig(string $titolo, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByTitoloOrTitoloOrig($titolo);
        foreach ($filmList as $k => $stv) {
            $filmList[$k] = self::setFilmComponent($stv, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    /**
     * @param array $ruoli
     * @return array di film mezzi pieni solo con le nomination
     */
    public function getAllFilmByRuoliAndFilmaker(array $ruoli, Filmmaker $filmmaker): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $ruoloService = new RuoloService();
        $filmMezzoPieno = array();
        foreach ($ruoli as $k => $ruolo) {
            try {
                $filmMezzo = $filmServiceImpl->getFilmByRuolo($ruolo);
                if ($filmMezzo->getIdFilm() != null) {
                    $nominationsService = new NominationService();
                    $filmMezzo->setNominations($nominationsService->getNominationByIdFilm($filmMezzo->getIdFilm()));
                    $filmMezzo->setRuoli($ruoloService->getAllRuoliByIdFilmAndFilmmaker($filmMezzo->getIdFilm(), $filmmaker));
                    array_push($filmMezzoPieno, $filmMezzo);
                }
            } catch (Exception $e) {
            }
        }
        return $filmMezzoPieno;
    }

    public function getMediaVotiUtenti(Film $film)
    {
        $recServiceImpl = new RecensioneServiceImplementation();
        $rec = $recServiceImpl->getVotiByIdFilmUser($film->getIdFilm());
        if (count($rec) == 0) {
            return 0;
        }
        $sommaVoti = 0;
        foreach ($rec as $vot) {
            $sommaVoti = $sommaVoti + $vot->getVoto();
        }
        return $sommaVoti / count($rec);
    }

    public function getMediaVotiCritici(Film $film)
    {
        $recServiceImpl = new RecensioneServiceImplementation();
        $rec = $recServiceImpl->getVotiByIdFilmCritico($film->getIdFilm());
        if (count($rec) == 0) {
            return 0;
        }
        $sommaVoti = 0;
        foreach ($rec as $vot) {
            $sommaVoti = $sommaVoti + $vot->getVoto();
        }
        return $sommaVoti / count($rec);
    }

    public function getMediaVotiFilm(Film $film)
    {
        if (!$this->maggiorediCinqueVoti($film)) {
            return 0;
        }
        if ($this->getMediaVotiUtenti($film) == 0) {
            return $this->getMediaVotiCritici($film);
        }
        if ($this->getMediaVotiCritici($film) == 0) {
            return $this->getMediaVotiUtenti($film);
        }
        if (($this->getMediaVotiUtenti($film) == 0) && ($this->getMediaVotiCritici($film) == 0)) {
            return 0;
        }
        return ($this->getMediaVotiUtenti($film) + $this->getMediaVotiCritici($film)) / 2;
    }

    public function getFilmsPiuVotati(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getAllFilms();
        foreach ($filmList as $k => $fl) {
            $filmList[$k] = self::setFilmComponentParzialiCarosello($fl);
        }
        uasort($filmList, fn($a, $b) => ($a->mediaVotoRedCarpet > $b->mediaVotoRedCarpet) ? -1 : 1);
        return array_slice($filmList, 0, 15);
    }

    public function getSingoloFilmPiuVotato(): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $imgService = new ImmagineService();
        $filmList = $filmServiceImpl->getAllFilms();
        if (empty($filmList)) {
            return new Film();
        }
        $mediaVotoAndIndice = array();
        foreach ($filmList as $key => $film) {
            $mediaVotoAndIndice[$key] = $this->getMediaVotiFilm($film);
        }
        arsort($mediaVotoAndIndice);
        $listFilmPiuVotati = array();
        foreach ($mediaVotoAndIndice as $key => $value) {
            $filmList[$key]->mediaVoto = $value;
            array_push($listFilmPiuVotati, $filmList[$key]);
        }
        if (!empty($listFilmPiuVotati)) {
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('film', $listFilmPiuVotati[0]->getIdFilm(), 2);
            if (!empty($result)) {
                $listFilmPiuVotati[0]->setImgCopertina($result[0]);
            }
        }
        return $listFilmPiuVotati[0];
    }

    public function getFilmByPremio(int $idPremio, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByPremio($idPremio);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setFilmComponent($fi, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function existFilm(int $idNomination): bool{
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->existFilm($idNomination);
    }

    public function getFilmByIdNomination(int $idNomination): Film
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getFilmByIdNomination($idNomination);
    }

    public function getFilmByPremioPaginato(int $idPremio, $limit, $offset)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByPremioPaginato($idPremio, $limit, $offset);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setComponentFilmParzialiPerLista($fi, $filmServiceImpl);
        }
        return $filmList;
    }

    public function countFilmByPremio(int $idPremio): int
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmByPremio($idPremio);
    }

    public function getFilmInUscitaPaginato($limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmInUscitaPaginato($limit, $offset);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setComponentFilmParzialiPerLista($fi, $filmServiceImpl);
        }
        return $filmList;
    }

    public function countFilmInUscita(): int
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmInUscita();
    }

    public function getFilmDelMesePaginato($limit, $offset): array
    {
        $dataOggi = date("Y/m-d");
        $dataListA = explode("-", $dataOggi);
        $dataListB = explode("/", $dataListA[0]);
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmDelMesePaginato($dataListA[0] . '/01', $dataListB[0] . ('/' . ($dataListB[1] + 1)) . '/01', $limit, $offset);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setComponentFilmParzialiPerLista($fi, $filmServiceImpl);
        }
        return $filmList;
    }

    public function countFilmDelMese(): int
    {
        $dataOggi = date("Y/m-d");
        $dataListA = explode("-", $dataOggi);
        $dataListB = explode("/", $dataListA[0]);
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmDelMese($dataListA[0] . '/01', $dataListB[0] . ('/' . ($dataListB[1] + 1)) . '/01');
    }

    public function getFilmByGenere(int $idGenre, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByGenere($idGenre);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setFilmComponent($fi, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getFilmByIdGenerePaginato(int $idGenre, $limit, $offset): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmListParz = $filmServiceImpl->getFilmByIdGenerePaginato($idGenre, $limit, $offset);
        $filmList = array();
        if (empty($filmListParz))
            return [];
        foreach ($filmListParz as $film) {
            array_push($filmList, self::setComponentFilmParzialiPerLista($film, $filmServiceImpl));
        }
        return $filmList;
    }

    public function countFilmByIdGenere(int $idGenre): int
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmByIdGenere($idGenre);
    }

    public function getFilmByDistributore(int $idDistributore, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByDistributore($idDistributore);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setFilmComponent($fi, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getFilmByDistributorePaginato(int $idDistributore, $limit, $offset)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByDistributorePaginato($idDistributore, $limit, $offset);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setComponentFilmParzialiPerLista($fi, $filmServiceImpl);
        }
        return $filmList;
    }

    public function countFilmByDistributore(int $idDistributore)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->countFilmByDistributore($idDistributore);
    }

    public function getFilmByNomeCognomeFilmmaker(string $persona, bool $notRicerca = true)
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getFilmByNomeCognomeFilmmaker($persona);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setFilmComponent($fi, $filmServiceImpl, $notRicerca);
        }
        return $filmList;
    }

    public function getUltimiDieciFilmCreati(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmList = $filmServiceImpl->getUltimiDieciFilmCreati();
        //dd(self::setFilmComponent($filmList[7], $filmServiceImpl));
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setFilmComponent($fi, $filmServiceImpl);
        }
        return $filmList;
    }

    public function getUltimiCinqueFilm(): array
    {
        $filmServiceImpl = new FilmServiceImplementation();
        return $filmServiceImpl->getUltimicinqueFilm();
    }

    public function show()
    {
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremio();
        $distributoreService = new DistributoreService();
        try {
            $distributori = $distributoreService->getAllDistrucutore();
        } catch (\Exception $exception) {
            $distributori = array();
        }
        $genereService = new GenereService();
        try {
            $generi = $genereService->getAllGenere();
        } catch (\Exception $exception) {
            $generi = array();
        }
        return ['distributori' => $distributori, 'generi' => $generi, 'premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi];
    }

    public function storeFilm(RequestInput $requestInput)
    {
        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $categoriaService = new CategoriaPremioService();
        $nominatioService = new NominationService();
        $ruoliService = new RuoloService();
        $imgService = new ImmagineService();
        $videoService = new VideoService();
        $filmObj = new Film();
        $filmObj->setTitoloOriginale($data['titolo_originale_film']);
        $filmObj->setTitolo($data['titolo_film']);
        $filmObj->setDescrizione($data['descrizione_film']);
        $filmObj->setTrama($data['trama']);
        $filmObj->setTramaBreve($data['trama_breve']);
        $filmObj->setDataSviluppoFilm($data['data_sviluppo']);
        $filmObj->setDataUscita($data['data_uscita_italia']);
        $filmObj->setDurata($data['durata_film']);
        $filmObj->setNazionalita($data['nazione']);
        $filmObj->setAgeRating($data['eta_consigliata']);
        $filmObj->setInSala($data['film_in_sala']);

        // salva array di distr
        $array_distributori = array();
        foreach ($data['distributore'] as $k => $distributoreID) {
            $distributoreFake = new Distributore();
            $distributoreFake->setIdDistributore($distributoreID);
            if ($distributoreID != 0) {
                array_push($array_distributori, $distributoreFake);
            }
        }
        $filmObj->setDistributori($array_distributori);


        // salva array di gener
        $array_generi = array();
        foreach ($data['genere'] as $k => $genereID) {
            $genereFake = new Genere();
            $genereFake->setIdGenere($genereID);
            array_push($array_generi, $genereFake);
        }
        $filmObj->setGeneri($array_generi);

        $filmObj->setRecensioni([]);


        // devo creare oggetto nomination e salvare sul db prima del film
        $array_nomination = array();
        if (array_key_exists('premio', $data)) {
            if (array_key_exists('genere', $data['premio'])) {
                foreach ($data['premio']['genere'] as $k => $genereID) {
                    $nomination = new Nomination();
                    $tipologia = new TipologiaPremio();
                    $premio = new Premio();
                    $filmmakerFake = new Filmmaker();
                    $premio->setIdPremio($genereID);
                    $tipologia->setidTipologiaPremio($data['premio']['tipologia'][$k]);
                    $categoria = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($tipologia->getidTipologiaPremio(), $premio->getIdPremio());
                    $nomination->setCategoriaPremio($categoria);
                    $nomination->setStato($data['premio']['stato'][$k]);
                    $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$k]);
                    $nomination->setFilmMaker($filmmakerFake);
                    $nomination->setDescrizione($data['premio']['descrizione'][$k]);
                    $nomination->setDataPremiazione($data['premio']['data_premiazione'][$k]);
                    $res = $nominatioService->insertNomination($nomination);
                    $nomination->setIdNomination($res);
                    array_push($array_nomination, $nomination);
                }
            }
        }
        $filmObj->setNominations($array_nomination);
        // nomination deve avere l'id del filmmaker cosi dopo aver creato la nomination
        //      creare anche nominationFilmmaker, tabella di collegamento, con il metodo
        //      insertNominationFilmmaker() di Nomination service


        // devo creare oggetto Ruolo e salvare sul db prima del film
        $array_ruolo = array();
        foreach ($data['cast']['select_filmmakers'] as $k => $filmmakerID) {
            $ruolo = new Ruolo();
            $filmmakerFake = new Filmmaker();
            $filmmakerFake->setIdFilmMaker($filmmakerID);
            $ruolo->setFilmMaker($filmmakerFake);
            $ruolo->setTipologia($data['cast']['tipologia'][$k]);
            $ruolo->setRuoloInternoOpera($data['cast']['ruolo'][$k]);
            // salva ruolo
            $res = $ruoliService->insertRuolo($ruolo);
            // aggiungi all array id salvato
            $ruolo->setIdRuolo($res);
            array_push($array_ruolo, $ruolo);
        }
        $filmObj->setRuoli($array_ruolo);


        // ######Salvo il film#####
        $res = self::insertFilm($filmObj);
        $filmObj->setIdFilm($res);

        $film_path = bin2hex(random_bytes(8));

        // salvo immagine copertina
        $uploadedFile = $uploadedFiles['immagine_copertina'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/film/');
            $filename = movefiles($directory . $film_path, $uploadedFile);
            $imgCopertina = new Immagine();
            $imgCopertina->setNome($uploadedFile->getClientFilename());
            $imgCopertina->setDescrizione('img copertina');
            $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
            $imgCopertina->setTipo(2);
            // eseguo query di salvataggio img coprtina con id film
            $res = $imgService->insertImgFilm($imgCopertina, $filmObj->getIdFilm());
            $imgCopertina->setIdImmagione($res);
            $filmObj->setImgCopertina($imgCopertina);
        }


        // Salvo i video
        $array_video = array();
        if (array_key_exists('media', $uploadedFiles)) {
            if (array_key_exists('video', $uploadedFiles['media'])) {
                foreach ($uploadedFiles['media']['video'] as $k => $uploadedFile) {
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        if ($uploadedFiles['media']['cover'][$k]->getError() === UPLOAD_ERR_OK) {
                            $directoryVideo = public_path('video/film/');
                            $filenameVideo = movefiles($directoryVideo . $film_path, $uploadedFile);
                            $video = new Video();
                            $video->setNome($uploadedFile->getClientFilename());
                            $video->setDescrizione($data['media']['descrizione'][$k]);
                            $video->setDato("video/film/" . $film_path . DIRECTORY_SEPARATOR . $filenameVideo);
                            // salvo il video con id del film
                            $res = $videoService->insertVideoFilm($video, $filmObj->getIdFilm());
                            $video->setIdVideo($res);

                            $directoryCover = public_path('img/film/');
                            $filename = movefiles($directoryCover . $film_path, $uploadedFiles['media']['cover'][$k]);
                            $imgCopertina = new Immagine();
                            $imgCopertina->setNome($uploadedFiles['media']['cover'][$k]->getClientFilename());
                            $imgCopertina->setDescrizione($data['media']['descrizione'][$k]);
                            $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo(3);

                            // salvo immagine con id video
                            $res = $imgService->insertImgVideo($imgCopertina, $video->getIdVideo());
                            $imgCopertina->setIdImmagione($res);
                            $video->setImmagine($imgCopertina);
                            array_push($array_video, $video);
                        }
                    }
                }
            }
        }
        $filmObj->setVideos($array_video);

        // salvo le immagini del film
        $array_img = array();
        if (array_key_exists('immagini', $uploadedFiles)) {
            if (array_key_exists('file', $uploadedFiles['immagini'])) {
                foreach ($uploadedFiles['immagini']['file'] as $k => $uploadedFile) {
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        $directory = public_path('img/film/');
                        $filename = movefiles($directory . $film_path, $uploadedFile);
                        $imgCopertina = new Immagine();
                        $imgCopertina->setNome($uploadedFile->getClientFilename());
                        $imgCopertina->setDescrizione($data['immagini']['descrizione'][$k]);
                        $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                        $imgCopertina->setTipo($data['immagini']['scheda'][$k]);
                        // salvo le immagini con id del film
                        $imgService->insertImgFilm($imgCopertina, $filmObj->getIdFilm());
                        array_push($array_img, $imgCopertina);
                    }
                }
            }
        }
        $filmObj->setImgs($array_img);
        session()->flash()->set("successCreationFilm", "Il Film: " . $filmObj->getTitolo() . " è stato creato con successo");
        //dd($requestInput->all(), $filmObj,  $requestInput->getRequest()->getUploadedFiles());
        return true;
    }

    public function showModify(RequestInput $requestInput)
    {
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremio();
        $distributoreService = new DistributoreService();
        try {
            $distributori = $distributoreService->getAllDistrucutore();
        } catch (Exception $exception) {
            $distributori = array();
        }
        $genereService = new GenereService();
        try {
            $generi = $genereService->getAllGenere();
        } catch (Exception $exception) {
            $generi = array();
        }
        $ag = $requestInput->getArguments();
        $array_nazioni_unite = self::getAllNations();
        $filmObj = self::getFilmById($ag['idFilm']);
        $qr = $requestInput->getQueryString();
        $showCast = false;
        $showNomination = false;
        foreach ($qr as $qrk => $argument) {
            if ($qrk === 'sc') {
                $showCast = true;
            } else if ($qrk === 'sn') {
                $showNomination = true;
            }
        }
        //dd($filmObj->getNominations(), $filmObj->getNominations()[0]->getCategoriaPremio()->getTipologia()->getidTipologiaPremio(), $listTipologiaPremi);
        return ['distributori' => $distributori, 'generi' => $generi, 'premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi, 'filmObj' => $filmObj, 'array_nazioni' => $array_nazioni_unite, 'showCast' => $showCast, "showNomination" => $showNomination];
    }

    public function modifyFilm(RequestInput $requestInput)
    {
        $ag = $requestInput->getArguments();
        $filmObjOriginal = self::getFilmById($ag['idFilm']);

        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $nominatioService = new NominationService();
        $categoriaService = new CategoriaPremioService();
        $ruoliService = new RuoloService();
        $imgService = new ImmagineService();
        $videoService = new VideoService();

        $filmObjNew = new Film();
        $filmObjNew->setIdFilm($filmObjOriginal->getIdFilm());

        if ($filmObjOriginal->getTitoloOriginale() === $data['titolo_originale_film']) {
            $filmObjNew->setTitoloOriginale($filmObjOriginal->getTitoloOriginale());
        } else {
            $filmObjNew->setTitoloOriginale($data['titolo_originale_film']);
        }

        if ($filmObjOriginal->getTitolo() === $data['titolo_film']) {
            $filmObjNew->setTitolo($filmObjOriginal->getTitolo());
        } else {
            $filmObjNew->setTitolo($data['titolo_film']);
        }

        if ($filmObjOriginal->getDescrizione() === $data['descrizione_film']) {
            $filmObjNew->setDescrizione($filmObjOriginal->getDescrizione());
        } else {
            $filmObjNew->setDescrizione($data['descrizione_film']);
        }

        if ($filmObjOriginal->getTrama() === $data['trama']) {
            $filmObjNew->setTrama($filmObjOriginal->getTrama());
        } else {
            $filmObjNew->setTrama($data['trama']);
        }

        if ($filmObjOriginal->getTramaBreve() === $data['trama_breve']) {
            $filmObjNew->setTramaBreve($filmObjOriginal->getTramaBreve());
        } else {
            $filmObjNew->setTramaBreve($data['trama_breve']);
        }

        if ($filmObjOriginal->getDataSviluppoFilm() === $data['data_sviluppo']) {
            $filmObjNew->setDataSviluppoFilm($filmObjOriginal->getDataSviluppoFilm());
        } else {
            $filmObjNew->setDataSviluppoFilm($data['data_sviluppo']);
        }

        if ($filmObjOriginal->getDataUscita() === $data['data_uscita_italia']) {
            $filmObjNew->setDataUscita($filmObjOriginal->getDataUscita());
        } else {
            $filmObjNew->setDataUscita($data['data_uscita_italia']);
        }

        if ($filmObjOriginal->getDurata() === $data['durata_film']) {
            $filmObjNew->setDurata($filmObjOriginal->getDurata());
        } else {
            $filmObjNew->setDurata($data['durata_film']);
        }

        //prendo la lista delle nazioni perche la cancello e la ricarico
        $filmObjNew->setNazionalita($data['nazione']);


        if ($filmObjOriginal->getAgeRating() === $data['eta_consigliata']) {
            $filmObjNew->setAgeRating($filmObjOriginal->getAgeRating());
        } else {
            $filmObjNew->setAgeRating($data['eta_consigliata']);
        }

        if ($filmObjOriginal->getInSala() === $data['film_in_sala']) {
            $filmObjNew->setInSala($filmObjOriginal->getInSala());
        } else {
            $filmObjNew->setInSala($data['film_in_sala']);
        }

        //prendo l'array dei distributori perche la cacello e la ricarico
        $array_distributori = array();
        foreach ($data['distributore'] as $k => $distributoreID) {
            $distributoreFake = new Distributore();
            $distributoreFake->setIdDistributore($distributoreID);
            array_push($array_distributori, $distributoreFake);
        }
        $filmObjNew->setDistributori($array_distributori);

        //prendo l'array dei generi perche la cacello e la ricarico
        // salva array di gener
        $array_generi = array();
        foreach ($data['genere'] as $k => $genereID) {
            $genereFake = new Genere();
            $genereFake->setIdGenere($genereID);
            array_push($array_generi, $genereFake);
        }
        $filmObjNew->setGeneri($array_generi);

        $filmObjNew->setRecensioni($filmObjOriginal->getRecensioni());

        // Nomination
        $array_operazioni = array();
        $array_new_nomination = array();
        $array_new_nomination_insert = array();
        $array_new_nomination_id = array();
        $array_old_nomination_id = array();
        if (isset($data['premio']['Id_premio'])) {
            foreach ($data['premio']['Id_premio'] as $kp => $idPremio) {
                foreach ($filmObjOriginal->getNominations() as $k => $nominationObjOriginal) {
                    if ($nominationObjOriginal->getIdNomination() == $idPremio) {
                        $nominationObjNew = new Nomination();
                        $categoriaObjOld = $nominationObjOriginal->getCategoriaPremio();
                        $filmmakerFake = new Filmmaker();

                        $nominationObjNew->setIdNomination($nominationObjOriginal->getIdNomination());

                        if ($categoriaObjOld->getPremio()->getIdPremio() != $data['premio']['genere'][$kp]) {
                            if ($categoriaObjOld->getTipologia()->getidTipologiaPremio() !== $data['premio']['tipologia'][$kp]) {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$kp], $data['premio']['genere'][$kp]);
                            } else {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($categoriaObjOld->getTipologia()->getidTipologiaPremio(), $data['premio']['genere'][$kp]);
                            }
                        } else {
                            if ($categoriaObjOld->getTipologia()->getidTipologiaPremio() !== $data['premio']['tipologia'][$kp]) {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$kp], $data['premio']['genere'][$kp]);
                            } else {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($categoriaObjOld->getTipologia()->getidTipologiaPremio(), $data['premio']['genere'][$kp]);
                            }
                        }
                        $nominationObjNew->setCategoriaPremio($categoriaObjnew);

                        if ($nominationObjOriginal->getStato() != $data['premio']['stato'][$kp]) {
                            $nominationObjNew->setStato($data['premio']['stato'][$kp]);
                        } else {
                            $nominationObjNew->setStato($nominationObjOriginal->getStato());
                        }

                        if ($nominationObjOriginal->getFilmMaker()->getIdFilmMaker() != $data['premio']['select_filmmakers'][$kp]) {
                            $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$kp]);
                            $nominationObjNew->setFilmMaker($filmmakerFake);
                        } else {
                            $nominationObjNew->setFilmMaker($nominationObjOriginal->getFilmMaker());
                        }

                        if ($nominationObjOriginal->getDescrizione() != $data['premio']['descrizione'][$kp]) {
                            $nominationObjNew->setDescrizione($data['premio']['descrizione'][$kp]);
                        } else {
                            $nominationObjNew->setDescrizione($nominationObjOriginal->getDescrizione());
                        }

                        if ($nominationObjOriginal->getDataPremiazione() != $data['premio']['data_premiazione'][$kp]) {
                            $nominationObjNew->setDataPremiazione($data['premio']['data_premiazione'][$kp]);
                        } else {
                            $nominationObjNew->setDataPremiazione($nominationObjOriginal->getDataPremiazione());
                        }
                        // update sul db
                        $nominatioService->updateNomination($nominationObjNew, $nominationObjOriginal);
                        array_push($array_operazioni, $nominationObjNew);
                        array_push($array_new_nomination, $nominationObjNew);
                    }
                }
            }

            // array_old_id
            foreach ($filmObjOriginal->getNominations() as $k => $nomination) {
                array_push($array_old_nomination_id, strval($nomination->getIdNomination()));
            }
            // array_new_id
            foreach ($data['premio']['Id_premio'] as $kp => $idPremio) {
                array_push($array_new_nomination_id, $idPremio);
            }

            // array di id da cancellare
            $array_da_cancellare_nomination = array_diff($array_old_nomination_id, $array_new_nomination_id);

            foreach ($array_da_cancellare_nomination as $k => $idNomi) {
                foreach ($filmObjOriginal->getNominations() as $ki => $nomination) {
                    if ($nomination->getIdNomination() == $idNomi) {
                        // cancella le nomination
                        $nominatioService->deleteNominationFilmService($nomination);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_nomination = array_diff($array_new_nomination_id, $array_old_nomination_id);

            // insert new nomination
            foreach ($array_da_inserire_nomination as $kArray => $id) {
                foreach ($data['premio']['Id_premio'] as $k => $idPremio) {
                    if ($kArray === $k && $id === $idPremio) {
                        $nomination = new Nomination();
                        $premio = new Premio();
                        $filmmakerFake = new Filmmaker();
                        $premio->setIdPremio($data['premio']['genere'][$k]);
                        $categoria = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$k], $premio->getIdPremio());
                        $nomination->setCategoriaPremio($categoria);
                        $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$k]);
                        $nomination->setFilmMaker($filmmakerFake);
                        $nomination->setStato($data['premio']['stato'][$k]);
                        $nomination->setDescrizione($data['premio']['descrizione'][$k]);
                        $nomination->setDataPremiazione($data['premio']['data_premiazione'][$k]);
                        // carico sul db nuove nomination
                        $res = $nominatioService->insertNomination($nomination);
                        $nomination->setIdNomination(intval($res));
                        $nominatioService->insertNominationFilm($nomination->getIdNomination(), $filmObjNew->getIdFilm());
                        $nominatioService->insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());

                        array_push($array_new_nomination_insert, $nomination);
                        array_push($array_new_nomination, $nomination);
                    }

                }
            }
        } else {
            // devo eliminare titte le nomiantion
            foreach ($filmObjOriginal->getNominations() as $k => $nominationObjOriginal) {
                $nominatioService->deleteNominationFilmService($nominationObjOriginal);
            }
        }

        $filmObjNew->setNominations($array_new_nomination);


        // Ruoli
        $array_ruolo_new = array();
        $array_ruolo_new_id = array();
        $array_ruolo_new_insert = array();
        $array_ruolo_old_id = array();

        if (isset($data['cast']['id_cast'])) {

            foreach ($data['cast']['id_cast'] as $kp => $idCast) {
                foreach ($filmObjOriginal->getRuoli() as $k => $ruoloObjOriginal) {
                    if ($ruoloObjOriginal->getIdRuolo() == $idCast) {
                        $ruoloObjNew = new Ruolo();
                        $filmmakerFake = new Filmmaker();

                        $ruoloObjNew->setIdRuolo($ruoloObjOriginal->getIdRuolo());

                        if ($ruoloObjOriginal->getFilmMaker()->getIdFilmMaker() != $data['cast']['select_filmmakers'][$kp]) {
                            $filmmakerFake->setIdFilmMaker($data['cast']['select_filmmakers'][$kp]);
                            $ruoloObjNew->setFilmMaker($filmmakerFake);
                        } else {
                            $ruoloObjNew->setFilmMaker($ruoloObjOriginal->getFilmMaker());
                        }

                        if ($ruoloObjOriginal->getTipologia() != $data['cast']['tipologia'][$kp]) {
                            $ruoloObjNew->setTipologia($data['cast']['tipologia'][$kp]);
                        } else {
                            $ruoloObjNew->setTipologia($ruoloObjOriginal->getTipologia());
                        }

                        if ($ruoloObjOriginal->getRuoloInternoOpera() != $data['cast']['ruolo'][$kp]) {
                            $ruoloObjNew->setRuoloInternoOpera($data['cast']['ruolo'][$kp]);
                        } else {
                            $ruoloObjNew->setRuoloInternoOpera($ruoloObjOriginal->getRuoloInternoOpera());
                        }

                        // update
                        $ruoliService->updateRuolo($ruoloObjNew);
                        //update del filmmaker
                        $ruoliService->updateRuoloFilmmakerByIdFilmmaker($ruoloObjNew->getIdRuolo(), $ruoloObjNew->getFilmMaker()->getIdFilmMaker());
                        array_push($array_ruolo_new, $ruoloObjNew);
                    }
                }
            }

            // array_ruolo_old_id
            foreach ($filmObjOriginal->getRuoli() as $k => $ruoli) {
                array_push($array_ruolo_old_id, strval($ruoli->getIdRuolo()));
            }
            // array_ruolo_new_id
            foreach ($data['cast']['id_cast'] as $kp => $idRuoli) {
                array_push($array_ruolo_new_id, $idRuoli);
            }

            // array di id da cancellare
            $array_da_cancellare_ruolo = array_diff($array_ruolo_old_id, $array_ruolo_new_id);

            foreach ($array_da_cancellare_ruolo as $k => $idRuolo) {
                foreach ($filmObjOriginal->getRuoli() as $ki => $ruolo) {
                    if ($ruolo->getIdRuolo() == $idRuolo) {
                        // cancella i ruoli
                        $ruoliService->deleteRuoloById($ruolo);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_ruolo = array_diff($array_ruolo_new_id, $array_ruolo_old_id);

            // insert new ruolo
            foreach ($array_da_inserire_ruolo as $kArray => $id) {
                foreach ($data['cast']['id_cast'] as $k => $idCast) {
                    if ($kArray === $k && $id === $idCast) {
                        $ruolo = new Ruolo();
                        $filmmakerFake = new Filmmaker();
                        $filmmakerFake->setIdFilmMaker($data['cast']['select_filmmakers'][$k]);
                        $ruolo->setTipologia($data['cast']['tipologia'][$k]);
                        $ruolo->setFilmMaker($filmmakerFake);
                        $ruolo->setRuoloInternoOpera($data['cast']['ruolo'][$k]);
                        // salva ruolo
                        $res = $ruoliService->insertRuolo($ruolo);
                        $ruolo->setIdRuolo(intval($res));
                        $ruoliService->insertRuoloFilm($ruolo->getIdRuolo(), $filmObjNew->getIdFilm());

                        array_push($array_ruolo_new_insert, $ruolo);
                        array_push($array_ruolo_new, $ruolo);
                    }
                }
            }

        } else {
            // devo eliminare tutti i ruoli
            foreach ($filmObjOriginal->getRuoli() as $k => $ruoloObjOriginal) {
                $ruoliService->deleteRuoloById($ruoloObjOriginal);
            }
        }

        $filmObjNew->setRuoli($array_ruolo_new);


        #### UPDATE FILM ####
        self::updateFilm($filmObjNew);

        $film_path = explode("/", explode('img/film', $filmObjOriginal->getImgCopertina()->getDato())[1])[1];

        $uploadedFile = $uploadedFiles['immagine_copertina'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/film/');
            deleteFile(public_path($filmObjOriginal->getImgCopertina()->getDato()));
            $filename = movefiles($directory . $film_path, $uploadedFile);
            $img = $filmObjOriginal->getImgCopertina();
            $img->setNome($uploadedFile->getClientFilename());
            $img->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);

            //update img
            $imgService->updateImgFilm($img, $filmObjNew->getIdFilm());
            $filmObjNew->setImgCopertina($img);
        } else {
            $filmObjNew->setImgCopertina($filmObjOriginal->getImgCopertina());
        }

        // aggiorno i video
        $array_new_video = array();
        $array_new_video_id = array();
        $array_old_video_id = array();
        if (isset($data['media']['id_video'])) {
            foreach ($data['media']['id_video'] as $kp => $idVideo) {
                foreach ($filmObjOriginal->getVideos() as $k => $videoOriginal) {
                    if ($videoOriginal->getIdVideo() == $idVideo) {
                        $video = new Video();
                        $uploadedFile = $uploadedFiles['media']['video'][$kp];

                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directoryVideo = public_path('video/film/');
                            deleteFile(public_path($videoOriginal->getDato()));
                            $filenameVideo = movefiles($directoryVideo . $film_path, $uploadedFile);
                            $video->setNome($uploadedFile->getClientFilename());
                            $video->setDescrizione($data['media']['descrizione'][$kp]);
                            $video->setDato("video/film/" . $film_path . DIRECTORY_SEPARATOR . $filenameVideo);
                            // aggiorno il video
                            $videoService->updateVideo($video);
                            $video->setIdVideo($videoOriginal->getIdVideo());

                        } else {
                            // il video è quello vecchio
                            $video->setIdVideo($videoOriginal->getIdVideo());
                            $video->setNome($videoOriginal->getNome());
                            $video->setImmagine($videoOriginal->getImmagine());
                            $video->setDescrizione($videoOriginal->getDescrizione());
                            $video->setDato($videoOriginal->getDato());

                            // controllo se devo caricare una descrizione modificata
                            if ($video->getDescrizione() != $data['media']['descrizione'][$kp]) {
                                $video->setDescrizione($data['media']['descrizione'][$kp]);

                                //upload modifica video
                                $videoService->updateVideo($video);
                            }
                        }

                        $imgCopertina = new Immagine();
                        // controllo se ho caricato una copertina nuova per il video
                        if ($uploadedFiles['media']['cover'][$kp]->getError() === UPLOAD_ERR_OK) {
                            $directoryCover = public_path('img/film/');
                            deleteFile(public_path($videoOriginal->getImmagine()->getDato()));
                            $filename = movefiles($directoryCover . $film_path, $uploadedFiles['media']['cover'][$kp]);
                            $imgCopertina->setNome($uploadedFiles['media']['cover'][$kp]->getClientFilename());
                            $imgCopertina->setDescrizione($data['media']['descrizione'][$kp]);
                            $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo(3);
                            $imgCopertina->setIdImmagione($videoOriginal->getImmagine()->getIdImmagione());

                            // update img di copertina
                            $imgService->updateImgVideo($imgCopertina, $video->getIdVideo());
                            $video->setImmagine($imgCopertina);

                        } else {
                            // il video ha la copertina vecchia
                            $video->setImmagine($videoOriginal->getImmagine());

                            // controllo se devo caricare una descrizione modificata
                            if ($video->getImmagine()->getDescrizione() != $data['media']['descrizione'][$kp]) {
                                $video->getImmagine()->setDescrizione($data['media']['descrizione'][$kp]);

                                //upload modifica img del video
                                $imgService->updateImgVideo($video->getImmagine(), $video->getIdVideo());
                                $videoService->updateVideo($video);
                            }
                        }
                        array_push($array_new_video, $video);
                    }
                }
            }

            // array_old_id
            foreach ($filmObjOriginal->getVideos() as $k => $video) {
                array_push($array_old_video_id, strval($video->getIdVideo()));
            }
            // array_new_id
            foreach ($data['media']['id_video'] as $kp => $idVideo) {
                array_push($array_new_video_id, $idVideo);
            }

            // array di id da cancellare
            $array_da_cancellare_video = array_diff($array_old_video_id, $array_new_video_id);

            foreach ($array_da_cancellare_video as $k => $idVideo) {
                foreach ($filmObjOriginal->getVideos() as $kv => $video) {
                    if ($video->getIdVideo() == $idVideo) {
                        // cancella il video
                        deleteFile(public_path($video->getDato()));
                        deleteFile(public_path($video->getImmagine()->getDato()));
                        $videoService->deleteVideo($video);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_video = array_diff($array_new_video_id, $array_old_video_id);

            // insert nuovo video
            foreach ($array_da_inserire_video as $kArray => $id) {
                foreach ($data['media']['id_video'] as $k => $idVideo) {
                    if ($kArray === $k && $id === $idVideo) {
                        $uploadedFile = $uploadedFiles['media']['video'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            if ($uploadedFiles['media']['cover'][$k]->getError() === UPLOAD_ERR_OK) {
                                $directoryVideo = public_path('video/film/');
                                $filenameVideo = movefiles($directoryVideo . $film_path, $uploadedFile);
                                $video = new Video();
                                $video->setNome($uploadedFile->getClientFilename());
                                $video->setDescrizione($data['media']['descrizione'][$k]);
                                $video->setDato("video/film/" . $film_path . DIRECTORY_SEPARATOR . $filenameVideo);
                                // salvo il video con id del film
                                $res = $videoService->insertVideoFilm($video, $filmObjOriginal->getIdFilm());
                                $video->setIdVideo($res);

                                $directoryCover = public_path('img/film/');
                                $filename = movefiles($directoryCover . $film_path, $uploadedFiles['media']['cover'][$k]);
                                $imgCopertina = new Immagine();
                                $imgCopertina->setNome($uploadedFiles['media']['cover'][$k]->getClientFilename());
                                $imgCopertina->setDescrizione($data['media']['descrizione'][$k]);
                                $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                                $imgCopertina->setTipo(3);

                                // salvo immagine con id video
                                $res = $imgService->insertImgVideo($imgCopertina, $video->getIdVideo());
                                $imgCopertina->setIdImmagione($res);
                                $video->setImmagine($imgCopertina);
                                array_push($array_new_video, $video);
                            }
                        }
                    }
                }
            }
        } else {
            // non ci sono video quindi li cancello tutti
            foreach ($filmObjOriginal->getVideos() as $k => $video) {
                // cancella il video
                deleteFile(public_path($video->getDato()));
                deleteFile(public_path($video->getImmagine()->getDato()));
                $videoService->deleteVideo($video);
            }
        }
        $filmObjNew->setVideos($array_new_video);


        // immaggini
        $array_new_img = array();
        $array_new_img_id = array();
        $array_old_img_id = array();
        $array_da_cancellare_img = array();
        $array_da_inserire_img = array();
        if (isset($data['immagini']['Id_immagini'])) {
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImg) {
                foreach ($filmObjOriginal->getImgs() as $k => $imgOriginal) {
                    if ($imgOriginal->getIdImmagione() == $idImg) {
                        $img = new Immagine();
                        $uploadedFile = $uploadedFiles['immagini']['file'][$kp];
                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/film/');
                            deleteFile(public_path($imgOriginal->getDato()));
                            $filename = movefiles("$directory" . $film_path, $uploadedFile);
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($uploadedFile->getClientFilename());
                            $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                            $img->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $img->setTipo($data['immagini']['scheda'][$kp]);
                            // aggiorno l'img

                            $imgService->updateImgFilm($img, $filmObjNew->getIdFilm());

                        } else {
                            // la foto è quella vecchia
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($imgOriginal->getNome());
                            $img->setDato($imgOriginal->getDato());
                            $img->setDescrizione($imgOriginal->getDescrizione());
                            $img->setTipo($imgOriginal->getTipo());

                            //controllo se la descrione o il tipo sono cambiati
                            if ($imgOriginal->getDescrizione() != $data['immagini']['descrizione'][$kp] || $imgOriginal->getTipo() != $data['immagini']['scheda'][$kp]) {
                                $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                                $img->setTipo($data['immagini']['scheda'][$kp]);
                                // upload modifica
                                $imgService->updateImgFilm($img, $filmObjNew->getIdFilm());
                            }
                        }
                        array_push($array_new_img, $img);
                    }
                }
            }

            // array_old_id
            foreach ($filmObjOriginal->getImgs() as $k => $immagine) {
                array_push($array_old_img_id, strval($immagine->getIdImmagione()));
            }
            // array_new_id
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImmagine) {
                array_push($array_new_img_id, $idImmagine);
            }

            // array di id da cancellare
            $array_da_cancellare_img = array_diff($array_old_img_id, $array_new_img_id);

            foreach ($array_da_cancellare_img as $k => $idImg) {
                foreach ($filmObjOriginal->getImgs() as $ki => $immagine) {
                    if ($immagine->getIdImmagione() == $idImg) {
                        // cancella le img
                        deleteFile(public_path($immagine->getDato()));
                        $imgService->deleteImg($immagine);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_img = array_diff($array_new_img_id, $array_old_img_id);

            foreach ($array_da_inserire_img as $kArray => $id) {
                foreach ($data['immagini']['Id_immagini'] as $k => $idImg) {
                    if ($kArray === $k && $id === $idImg) {
                        $uploadedFile = $uploadedFiles['immagini']['file'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/film/');
                            $filename = movefiles("$directory" . $film_path, $uploadedFile);
                            $imgCopertina = new Immagine();
                            $imgCopertina->setNome($uploadedFile->getClientFilename());
                            $imgCopertina->setDescrizione($data['immagini']['descrizione'][$k]);
                            $imgCopertina->setDato("img/film/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo($data['immagini']['scheda'][$k]);
                            // salvo le immagini con id del film
                            $imgService->insertImgFilm($imgCopertina, $filmObjNew->getIdFilm());
                            array_push($array_new_img, $imgCopertina);
                        }
                    }
                }
            }
        } else {
            //elimina tutte le immagini
            foreach ($filmObjOriginal->getImgs() as $k => $immagine) {
                deleteFile(public_path($immagine->getDato()));
                $imgService->deleteImg($immagine);
            }
        }
        $filmObjNew->setImgs($array_new_img);

        //dd($filmObjOriginal->getNominations(), $filmObjNew->getNominations(), $requestInput->all(), $requestInput->getRequest()->getUploadedFiles(),$array_new_img_id, $array_da_cancellare_img, $array_da_inserire_img, $array_new_img);
        session()->flash()->set("successModifyFilm", "Il Film: " . $filmObjNew->getTitolo() . " è stato modificato con successo");
        return true;
    }

    public function countFilm()
    {
        $filmServiceImplementation = new FilmServiceImplementation();
        return $filmServiceImplementation->countFilm();
    }

    public function getFilmByTitoloOrTitoloOrigSearch(string $search): array
    {
        $filmServiceImplementation = new FilmServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();

        $resultFilm = $filmServiceImplementation->getFilmByTitoloOrTitoloOrigSearch($search);
        if (!empty($resultFilm)) {
            $objList = array();
            foreach ($resultFilm as $key => $film) {
                $obj = new \stdClass();
                $obj->titolo = $film->getTitolo();
                $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
                if (!empty($imgCopertina)) {
                    $obj->datoImg = asset($imgCopertina[0]->getDato());
                } else {
                    $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
                }
                $obj->annoUscita = substr($film->getDataUscita(), 0, 4);
                $obj->tipo = 'FILM';
                $nomeeCognomeRegistiList = array();
                foreach ($filmmakerService->getFilmmakerByFilmAndTipologiaRuolo($film, 2) as $regista) {
                    array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
                }
                $obj->registi = $nomeeCognomeRegistiList;
                $obj->link = asset('film/' . $film->getIdFilm());
                array_push($objList, $obj);
            }
            return $objList;
        }
        return [];
    }

    public function getFilmPiuVotato()
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();
        $filmList = $filmServiceImpl->getAllFilms();
        $mediaVotoAndIndice = array();
        foreach ($filmList as $key => $film) {
            $mediaVotoAndIndice[$key] = $this->getMediaVotiFilm($film);
        }
        arsort($mediaVotoAndIndice);
        $listFilmPiuVotati = array();
        foreach ($mediaVotoAndIndice as $key => $value) {
            $filmList[$key]->mediaVoto = $value;
            array_push($listFilmPiuVotati, $filmList[$key]);
        }
        $obj = new \stdClass();
        if (!empty($listFilmPiuVotati)) {
            $obj->titolo = $listFilmPiuVotati[0]->getTitolo();
            $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('film', $listFilmPiuVotati[0]->getIdFilm(), 2);
            if (!empty($imgCopertina)) {
                $obj->datoImg = asset($imgCopertina[0]->getDato());
            } else {
                $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
            }
            $obj->annoUscita = substr($listFilmPiuVotati[0]->getDataUscita(), 0, 4);
            $obj->tipo = 'FILM';
            $nomeeCognomeRegistiList = array();
            foreach ($filmmakerService->getFilmmakerByFilmAndTipologiaRuolo($listFilmPiuVotati[0], 2) as $regista) {
                array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
            }
            $obj->registi = $nomeeCognomeRegistiList;
            $obj->link = asset('film/' . $listFilmPiuVotati[0]->getIdFilm());
        }
        return $obj;
    }

    public function getProssimoFilmInUscita()
    {
        $filmServiceImpl = new FilmServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();
        $film = $filmServiceImpl->getProssimoFilmInUscita();
        $obj = new \stdClass();

        if ($film->getIdFilm() !== null) {
            $obj->titolo = $film->getTitolo();
            $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('film', $film->getIdFilm(), 2);
            if (!empty($imgCopertina)) {
                $obj->datoImg = asset($imgCopertina[0]->getDato());
            } else {
                $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
            }
            $obj->annoUscita = substr($film->getDataUscita(), 0, 4);
            $obj->tipo = 'FILM';
            $nomeeCognomeRegistiList = array();
            foreach ($filmmakerService->getFilmmakerByFilmAndTipologiaRuolo($film, 2) as $regista) {
                array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
            }
            $obj->registi = $nomeeCognomeRegistiList;
            $obj->link = asset('film/' . $film->getIdFilm());
        }
        return $obj;
    }
}
