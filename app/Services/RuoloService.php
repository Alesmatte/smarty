<?php


namespace App\Services;


use App\Model\Filmmaker;
use App\Model\Ruolo;
use App\Services\Implementation\RuoloServiceImplementation;

class RuoloService
{

    /**
     * RuoloService constructor.
     */
    public function __construct()
    {
    }

    public function getRuoliById(int $idRuolo): Ruolo {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        $ruolo = $ruoloServiceImpl->getRuoloById($idRuolo);
        $filmmakerService = new FilmMakerService();
        $ruolo->setFilmMaker($filmmakerService->getFilmakerById($ruolo->getFilmMaker()->getIdFilmMaker()));
        return $ruolo;
    }

    /**
     * @param Ruolo $ruolo
     * @return mixed
     */
    public function insertRuolo(Ruolo $ruolo) {
        $ruoloServiceImpl = new RuoloServiceImplementation();

        $idRuoloInserito = $ruoloServiceImpl->insertRuolo($ruolo);

        $ruoloServiceImpl->insertRuoloFilmmaker($idRuoloInserito, $ruolo->getFilmMaker()->getIdFilmMaker());

        return $idRuoloInserito;
    }

    /**
     * @param int $idRuolo
     * @param int $idFilm
     * @return mixed
     */
    public function insertRuoloFilm(int $idRuolo, int $idFilm) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->insertRuoloFilm($idRuolo, $idFilm);
    }

    /**
     * @param int $idRuolo
     * @param int $idSerieTv
     * @return mixed
     */
    public function insertRuoloSerieTv(int $idRuolo, int $idSerieTv) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->insertRuoloSerieTv($idRuolo, $idSerieTv);
    }

    public function getRuoliByidFilm($idFilm): array {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        $ruoloList = $ruoloServiceImpl->getRuoliByidFilm($idFilm);
        foreach ($ruoloList as $key=>$ruolo) {
            $filmmakerService = new FilmMakerService();
            $ruoloList[$key]->setFilmMaker($filmmakerService->getFilmakerById($ruolo->getFilmMaker()->getIdFilmMaker()));
        }
        return $ruoloList;
    }

    public function updateRuolo(Ruolo $ruolo) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->updateRuolo($ruolo);
    }

    public function deleteRuoloSerietvById(Ruolo $ruolo) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        $r1=$ruoloServiceImpl->deleteRuoloSerieTvByIdRuolo($ruolo->getIdRuolo());
        $r2=$ruoloServiceImpl->deleteRuoloFilmmakerByIdRuolo($ruolo->getIdRuolo());
        $r3=$ruoloServiceImpl->deleteRuoloById($ruolo);
        return [$r1, $r2, $r3];
    }

    public function deleteRuoloById(Ruolo $ruolo) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        $ruoloServiceImpl->deleteRuoloFilmByIdRuolo($ruolo->getIdRuolo());
        $ruoloServiceImpl->deleteRuoloFilmmakerByIdRuolo($ruolo->getIdRuolo());
        return $ruoloServiceImpl->deleteRuoloById($ruolo);
    }

    public function deleteRuoloByIdFilmmaker(int $idFilmmaker) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        foreach (self::getRuoliByIdFilmmaker($idFilmmaker) as $ruolo) {
            $ruoloServiceImpl->deleteRuoloFilmByIdRuolo($ruolo->getIdRuolo());
            $ruoloServiceImpl->deleteRuoloFilmmakerByIdRuolo($ruolo->getIdRuolo());
            $ruoloServiceImpl->deleteRuoloById($ruolo);
        }
    }

    public function getRuoliByIdFilmmaker(int $idFilmmaker): array {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->getRuoliByIdFilmmaker($idFilmmaker);
    }

    public function updateRuoloFilmmakerByIdFilmmaker(int $idRuolo, int $idFilmmaker) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->updateRuoloFilmmakerByIdFilmmaker($idRuolo, $idFilmmaker);
    }

    public function deleteRuoloFilmByIdFilm(int $idFilm) {
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->deleteRuoloFilmByIdFilm($idFilm);
    }

    public function getAllRuoliByIdFilmAndFilmmaker(int $idFilm, Filmmaker $filmmaker){
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->getRuoloByidFilmAndFilmmaker($idFilm, $filmmaker);
    }

    public function getAllRuoliByIdSerieTvAndFilmmaker(int $idSerieTv, Filmmaker $filmmaker){
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->getRuoloByidSerieTvAndFilmmaker($idSerieTv, $filmmaker);
    }

    public function getTipologiaRuolo(){
        $ruoloServiceImpl = new RuoloServiceImplementation();
        return $ruoloServiceImpl->getTipologiaRuolo();
    }

}