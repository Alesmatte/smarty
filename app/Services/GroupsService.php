<?php


namespace App\Services;


use App\Model\Groups;
use App\Model\Services;
use App\Model\User;
use App\Services\Implementation\GroupServiceImplementation;
use App\Services\Implementation\ServicesServiceImplementation;

class GroupsService
{

    /**
     * GroupsService constructor.
     */
    public function __construct()
    {

    }

    private function ifServizioInServizi(Services $service, array $serviziList): bool
    {
        if (!empty($serviziList)) {
            foreach ($serviziList as $serv) {
                if ($service->getIdServizio() == $serv->getIdServizio()) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public function getAllGroupAndServiceAndUserByGroupName(string $groupName): Groups {
        $groupServiceImplement = new GroupServiceImplementation();
        $serviceServiceImplement = new ServicesServiceImplementation();
        $group = $groupServiceImplement->getGroupByName($groupName);
        $serviceList = $serviceServiceImplement->getServicesByGroup($group);
        $group->setServizi($serviceList);
        return $group;
    }

    public function insertGroup(Groups $groups) {
        $groupServiceImple = new GroupServiceImplementation();
        $resInsertGruppo =  $groupServiceImple->insertGroup($groups);
        $groups->setIdGruppo($resInsertGruppo);
        if (!empty($groups->getServizi())) {
            foreach ($groups->getServizi() as $servizio) {
                self::insertServicesHasGroupsByGruppo($groups, $servizio);
            }
        }
        return $resInsertGruppo;
    }

    public function updateGroup(Groups $groups) {
        $groupServiceImple = new GroupServiceImplementation();
        $serviceService = new ServicesService();
        $oldServiceGruppo = $serviceService->getServicesByGroup($groups);

        foreach ($oldServiceGruppo as $oldService) {
            if (!self::ifServizioInServizi($oldService, $groups->getServizi())) {
                self::deleteServicesHasGroupsByGruppoAndService($groups, $oldService);
            }
        }

        foreach ($groups->getServizi() as $newService) {
            if (!self::ifServizioInServizi($newService, $oldServiceGruppo)) {
                self::insertServicesHasGroupsByGruppo($groups, $newService);
            }
        }

        return $groupServiceImple->updateGroup($groups);
    }

    public function deleteGroup(Groups $groups) {
        $groupServiceImple = new GroupServiceImplementation();
        $groupServiceImple->deleteServicesHasGroupsByGruppo($groups);
        return $groupServiceImple->deleteGroup($groups);
    }

    public function getAllgroups(): array {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->getAllgroups();
    }

    public function getGroupById(int $idGruppo): Groups {
        $groupServiceImple = new GroupServiceImplementation();
        $serviceService = new ServicesService();
        $gruppo = $groupServiceImple->getGroupById($idGruppo);
        $gruppo->setServizi($serviceService->getServicesByGroup($gruppo));
        return $gruppo;
    }

    public function getGroupsByService(Services $services): array {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->getGroupsByService($services);
    }

    public function getGroupByUser(User $user): array {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->getGroupByUser($user);
    }

    public function deleteServicesHasGroupsByGruppo(Groups $groups) {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->deleteServicesHasGroupsByGruppo($groups);
    }

    public function deleteServicesHasGroupsByGruppoAndService(Groups $groups, Services $services) {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->deleteServicesHasGroupsByGruppoAndService($groups, $services);
    }

    public function insertServicesHasGroupsByGruppo(Groups $groups, Services $services) {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->insertServicesHasGroups($groups, $services);
    }

    public function deleteUserHasGroupByUser(User $user) {
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->deleteUserHasGroupByUser($user);
    }

    public function insertUserHasGroup(User $user, Groups $groups){
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->insertUserHasGroup($user, $groups);
    }

    public function deleteUserHasGroupByUserAndGroup(User $user, Groups $groups){
        $groupServiceImple = new GroupServiceImplementation();
        return $groupServiceImple->deleteUserHasGroupByUserAndGroup($user, $groups);
    }

}