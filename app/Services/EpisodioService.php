<?php


namespace App\Services;

use App\Model\Episodio;
use App\Services\Implementation\EpisodioServiceImplementation;
use App\Services\Implementation\ImmagineServiceImplementation;
use App\Services\Implementation\StagioneServiceImplementation;

class EpisodioService
{
    /**
     * EpisodioService constructor.
     */
    public function __construct()
    {
    }
    public function getEpisodioById(int $idEpisodio): Episodio
    {
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $episodio = $episodioServiceImpl->getEpisodioByID($idEpisodio);
        $imgServiceImpl = new ImmagineServiceImplementation();
        $result = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('episodio', $idEpisodio, 5);
        if (count($result)>0){
            $episodio->setImg($result[0]);
        }
        return $episodio;
    }

    /*public function getEpisodiImmgineStagioneByIdStagione($idStagione){
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $episodiList = $episodioServiceImpl->getEpisodiByIdStagione($idStagione);
        $i=0;
        foreach ($episodiList as $episodi){
            $episodiList[$i] = $this->getEpisodioById($episodi->getIdEpisodio());
            $i++;
        }
        return $episodiList;
    }*/

    public function getEpisodiStagioneByIdStagione(int $idStagione){
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $episodioList = $episodioServiceImpl->getEpisodiByIdStagione($idStagione);
        $imgServiceImpl = new ImmagineServiceImplementation();
        foreach ($episodioList as $key=>$episodio) {
            $result = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('episodio', $episodio->getIdEpisodio(), 5);
            if (count($result)>0) {
                $episodioList[$key]->setImg($result[0]);
            }
        }
        return $episodioList;
    }

    public function deleteEpisodioImmagineByIdEpi(int $idEpisodio){
        $imgServiceImpl = new ImmagineServiceImplementation();
        $imgServiceImpl->deleteImgByIdEpisodio($idEpisodio);
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $result = $episodioServiceImpl->deleteEpisodio($idEpisodio);
        return $result;
    }

    public function deleteEpisodiImmaginiStagioneByIdStagione(int $idStagione){
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $result = $stagioneServiceImpl->deleteStagione($idStagione);
        return $result;
    }

    public function insertEpisodio(Episodio $episodio, $idStagione){
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $idEpisodio = $episodioServiceImpl->insertEpisodio($episodio, $idStagione);
        return $idEpisodio;
    }

    public function updateEpisodio(Episodio $episodio, $idStagione){
        $episodioServiceImpl = new EpisodioServiceImplementation();
        $idEpisodio = $episodioServiceImpl->updateEpisodio($episodio, $idStagione);
        return $idEpisodio;
    }
}