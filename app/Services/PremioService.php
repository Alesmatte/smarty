<?php


namespace App\Services;


use App\Model\Premio;
use App\Services\Implementation\PremioServiceImplementation;

class PremioService
{

    /**
     * PremioService constructor.
     */
    public function __construct()
    {
    }

    public function getAllPremio(): array {
        $premioServiceImpl = new PremioServiceImplementation();
        return $premioServiceImpl->getAllPremios();
    }

    public function getPremioById($idPremio): Premio {
        $premioServiceImpl = new PremioServiceImplementation();
        return $premioServiceImpl->getPremioById($idPremio);
    }
}