<?php


namespace App\Services;

use App\Http\Request\StoreImpostazioniAccountUserAdmin;
use App\Http\Request\StoreImpostazioniAccountUserCritica;
use App\Http\Request\StoreImpostazioniAccountUserNormal;
use App\Http\Request\StoreUserCritica;
use App\Http\Request\StoreUserNormalRequest;
use App\Model\Groups;
use App\Model\Immagine;
use App\Model\User;
use App\Services\Implementation\UserServiceImplementation;
use App\Support\RequestInput;
use DB;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpForbiddenException;

class UserService
{
    /**
     * UserService constructor.
     */
    public function __construct()
    {
    }

    public function validLogin($email, $password)
    {
        $userServiceImpl = new UserServiceImplementation();
        $imgService = new ImmagineService();
        $img = new Immagine();
        $user = $userServiceImpl->getUserByEmail($email);
        $array_user = ['id' => $user->getIdUser(), 'nome' => $user->getNome(), 'cognome' => $user->getCognome(), 'email' => $user->getEmail(), 'password' => $user->getPassword(), 'date_login' => date("Y-m-d H:i:s")];
        $array_img = $imgService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
        if (count($array_img) > 0) {
            $img = $array_img[0];
            $array_user['img'] = $img->getDato();
        } else {
            $array_user['img'] = null;
        }
        $user->setAvatar($img);
        if ($user->getPassword() !== $password) {
            return false;
        }

        session('user', $array_user);
        return true;
    }

    public function printcos()
    {
        dd($_SESSION['user']);
    }

    public function logout()
    {
        $session = app()->resolve(\Boot\Foundation\Http\Session::class);
        $session->remove('user');
        return true;
    }

    public function getUser()
    {
        $userSession = session('user');
        $img = new Immagine();
        if (isset($userSession)) {
            if (isset($userSession['id']) || isset($userSession['email']) || isset($userSession['password'])) {
                $userServiceImpl = new UserServiceImplementation();
                $imgService = new ImmagineService();
                $user = $userServiceImpl->getUserByIdByEmailByPassword($userSession['id'], $userSession['email'], $userSession['password']);
                $array_img = $imgService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
                if (count($array_img) > 0) {
                    $img = $array_img[0];
                }
                $user->setAvatar($img);
                return $user;
            }
        } else throw_when(true, "Permesso Negato, non sei logato", HttpForbiddenException::class);
    }

    public function user()
    {
        $userSession = session('user');
        if (isset($userSession)) {
            if (!isset($userSession['id']) || !isset($userSession['email']) || !isset($userSession['password'])) {
                return false;
            } else {
                $userServiceImpl = new UserServiceImplementation();
                $user = $userServiceImpl->getUserByIdByEmailByPassword($userSession['id'], $userSession['email'], $userSession['password']);
                return true;
            }
        } else return false;

    }

    public function check(): bool
    {
        return (bool)self::user();
    }

    public function guest(): bool
    {
        return (bool)!self::check();
    }

    public function storeCritico(User $user): int
    {
        $userServiceImpl = new UserServiceImplementation();
        $last_id = $userServiceImpl->insertUserCritico($user);
        $user->setIdUser($last_id);
        $userServiceImpl->insertUserGroupsTwo($user);
        return $last_id;
    }

    public function storeNormale(User $user): int
    {
        $userServiceImpl = new UserServiceImplementation();
        $last_id = $userServiceImpl->insertUserNormale($user);
        $user->setIdUser($last_id);
        $userServiceImpl->insertUserGroupsOne($user);
        return $last_id;
    }

    public function storeAdmin(User $user): int
    {
        $userServiceImpl = new UserServiceImplementation();
        $last_id = $userServiceImpl->insertUserAdmin($user);
        $user->setIdUser($last_id);
        $userServiceImpl->insertUserGroupsThree($user);
        return $last_id;
    }

    public function updateUser(User $user)
    {
        $userServiceImpl = new UserServiceImplementation();
        $resultUser = $userServiceImpl->UpdateUser($user);
        $imgService = new ImmagineService();
        $resultImg = $imgService->updateImgUser($user->getAvatar(), $user->getIdUser());
        return ['resultUser' => $resultUser, 'resultImg' => $resultImg];
    }

    public function refreshSessionUser(User $user)
    {
        $session = app()->resolve(\Boot\Foundation\Http\Session::class);
        $session->remove('user');
        $array_user = ['id' => $user->getIdUser(), 'nome' => $user->getNome(), 'cognome' => $user->getCognome(), 'email' => $user->getEmail(), 'password' => $user->getPassword(), 'date_login' => date("Y-m-d H:i:s")];
        if ($user->getAvatar() != new Immagine()) {
            $array_user['img'] = $user->getAvatar()->getDato();
        } else {
            $array_user['img'] = null;
        }
        session('user', $array_user);
    }

    /**
     * @param User $user
     * @param RequestInput $requestInput
     * @param UserService $userService
     * @param string $tipoDirichiesta insert o update
     * @param string $origin front o back
     */
    public function storeUsers(User $user, RequestInput $requestInput, UserService $userService, string $tipoDirichiesta = 'insert', $origin = 'front')
    {
        $imgService = new ImmagineService();
        $groupService = new GroupsService();
        $data = $requestInput->all();
        if ($origin == 'back') {
            $user->setStato($data['stato']);
        }
        if ($user->getTipologiaUtenza() === 0) {
            $user->setNome($data['nome']);
            $user->setCognome($data['cognome']);
            $user->setDataNascita($data['birthday']);
            $user->setEmail($data['email']);
            $user->setBiografia($data['biografia']);
        } else if ($user->getTipologiaUtenza() === 1) {
            $user->setNome($data['nome']);
            $user->setCognome($data['cognome']);
            $user->setCodiceAlbo($data['codice_identificativo_albo']);
            $user->setNomeAzienda($data['azienda']);
            $user->setBiografia($data['biografia']);
            $user->setEmail($data['email']);
            $user->setDataNascita($data['birthday']);
        } else if ($user->getTipologiaUtenza() === 2) {
            $user->setNome($data['nome']);
            $user->setCognome($data['cognome']);
            $user->setEmail($data['email']);
            $user->setDataNascita($data['birthday']);
        }
        if ($data['account_password'] !== '') {
            $user->setPassword(sha1($data['account_password']));
        } else {
            $user->setPassword($userService->getUserById($user->getIdUser())->getPassword());
        }
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();

        if (isset($uploadedFiles['avatar_file'])) {
            $uploadedFile = $uploadedFiles['avatar_file'];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $directory = public_path('img/user/avatar');
                if ($tipoDirichiesta === 'update') {
                    if ($user->getAvatar()->getDato() != null) {
                        deleteFile(public_path($user->getAvatar()->getDato()));
                    }
                }
                $filename = movefiles($directory, $uploadedFile);
                $imgAvatar = new Immagine();
                $imgAvatar->setNome($uploadedFile->getClientFilename());
                $imgAvatar->setDato('img/user/avatar' . DIRECTORY_SEPARATOR . $filename);
                $imgAvatar->setTipo(0);
                $imgAvatar->setDescrizione('img di profilo');
                if ($tipoDirichiesta === 'update') {
                    if ($user->getAvatar() == new Immagine()) {
                        // non aveva la foto prima
                        $user->setAvatar($imgAvatar);
                        $last_id = $imgService->insertImgUser($imgAvatar, $user->getIdUser());
                        $imgAvatar->setIdImmagione($last_id);
                        $userService->updateUser($user);
                    } else {
                        // aveva la foto ma la cambia
                        $user->setAvatar($imgAvatar);
                        $userService->updateUser($user);
                    }
                } else {
                    $user->setAvatar($imgAvatar);
                    switch (intval($user->getTipologiaUtenza())) {
                        case 0:
                        {
                            $idUserRes = $userService->storeNormale($user);
                            $user->setIdUser($idUserRes);
                            $imgService->insertImgUser($user->getAvatar(), $idUserRes);
                            break;
                        }
                        case 1:
                        {
                            $idUserRes = $userService->storeCritico($user);
                            $user->setIdUser($idUserRes);
                            $imgService->insertImgUser($user->getAvatar(), $idUserRes);
                            break;
                        }
                        case 2:
                        {
                            $idUserRes = $userService->storeAdmin($user);
                            $user->setIdUser($idUserRes);
                            $imgService->insertImgUser($user->getAvatar(), $idUserRes);
                            break;
                        }
                    }
                }
            } else {
                if ($tipoDirichiesta === 'update') {
                    // non ha inviato la foto
                    $userService->updateUser($user);
                } else {
                    switch (intval($user->getTipologiaUtenza())) {
                        case 0:
                        {
                            $idUserRes = $userService->storeNormale($user);
                            $user->setIdUser($idUserRes);
                            break;
                        }
                        case 1:
                        {
                            $idUserRes = $userService->storeCritico($user);
                            $user->setIdUser($idUserRes);
                            break;
                        }
                        case 2:
                        {
                            $idUserRes = $userService->storeAdmin($user);
                            $user->setIdUser($idUserRes);
                            break;
                        }
                    }
                }
            }
        } else {
            if ($tipoDirichiesta === 'update') {
                // non ha inviato la foto
                $userService->updateUser($user);
            } else {
                switch (intval($user->getTipologiaUtenza())) {
                    case 0:
                    {
                        $idUserRes = $userService->storeNormale($user);
                        $user->setIdUser($idUserRes);
                        break;
                    }
                    case 1:
                    {
                        $idUserRes = $userService->storeCritico($user);
                        $user->setIdUser($idUserRes);
                        break;
                    }
                    case 2:
                    {
                        $idUserRes = $userService->storeAdmin($user);
                        $user->setIdUser($idUserRes);
                        break;
                    }
                }
            }
        }
        $groupService->deleteUserHasGroupByUser($user);
        if (isset($data['group']) && !empty($data['group'])) {
            foreach ($data['group'] as $idGroup) {
                $gruppo = new Groups();
                $gruppo->setIdGruppo($idGroup);
                $groupService->insertUserHasGroup($user, $gruppo);
            }
        } else {
            $gruppo = new Groups();
            switch (intval($user->getTipologiaUtenza())) {
                case 0:
                {
                    $gruppo->setIdGruppo(1);
                    break;
                }
                case 1:
                {
                    $gruppo->setIdGruppo(2);
                    break;
                }
                case 2:
                {
                    $gruppo->setIdGruppo(3);
                    break;
                }
            }
            $groupService->insertUserHasGroup($user, $gruppo);
        }
    }

    public function getAllUser()
    {
        $userServiceImpl = new UserServiceImplementation();
        return $userServiceImpl->getAllUser();
    }

    public function getUserById(int $idUser)
    {
        $userServiceImpl = new UserServiceImplementation();
        $immagineService = new ImmagineService();
        $user = $userServiceImpl->getUserById($idUser);
        $immagineResult = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('user', $idUser, 0);
        if (!empty($immagineResult)) {
            $user->setAvatar($immagineResult[0]);
        }

        return $user;
    }

    public function toggleStateUser(User $user)
    {
        $userServiceImpl = new UserServiceImplementation();
        return $userServiceImpl->toggleStateUser($user);
    }

    public function deleteUser(User $user)
    {
        $userServiceImpl = new UserServiceImplementation();
        $immagineService = new ImmagineService();
        $groupService = new GroupsService();
        $imgAvatar = $user->getAvatar()->getIdImmagione();
        if (isset($imgAvatar)) {
            deleteFile(public_path($user->getAvatar()->getDato()));
        }
        $immagineService->deleteImg($user->getAvatar());
        $groupService->deleteUserHasGroupByUser($user);
        return $userServiceImpl->deleteUser($user);
    }

    public function createUserNormal(StoreUserNormalRequest $inputStoreUserNormal): User{
        $input = $inputStoreUserNormal->all();
        $user = new User();
        $user->setNome($input['nome']);
        $user->setCognome($input['cognome']);
        $user->setDataNascita($input['birthday']);
        $user->setEmail($input['email']);
        $user->setPassword($input['account_password']);
        $user->setTipologiaUtenza(0);
        $user->setStato(1);
        return $user;
    }

    public function createUserCritica(StoreUserCritica $inputStoreUserCritica): User{
        $input = $inputStoreUserCritica->all();
        $user = new User();
        $user->setNome($input['nome']);
        $user->setCognome($input['cognome']);
        $user->setDataNascita($input['birthday']);
        $user->setCodiceAlbo($input['codice_identificativo_albo']);
        $user->setNomeAzienda($input['azienda']);
        $user->setEmail($input['email']);
        $user->setPassword($input['account_password']);
        $user->setTipologiaUtenza(1);
        $user->setStato(1);
        return $user;
    }

    public function updateObjUserNormal(StoreImpostazioniAccountUserNormal $inputStoreUserNormal, User $userOriginal): User{
        $input = $inputStoreUserNormal->all();
        $user = new User();
        $user->setIdUser($userOriginal->getIdUser());
        $user->setNome($input['nome']);
        $user->setCognome($input['cognome']);
        $user->setDataNascita($input['birthday']);
        $user->setEmail($input['email']);
        if(isset($input['account_password'])){
            $user->setPassword($input['account_password']);
        } else {
            $user->setPassword($userOriginal->getPassword());
        }
        if(isset($input['biografia'])){
            $user->setBiografia($input['biografia']);
        }
        $user->setTipologiaUtenza(0);
        $user->setStato(1);

        $uploadedFiles = $inputStoreUserNormal->getRequest()->getUploadedFiles();
        if (isset($uploadedFiles['avatar_file'])) {
            $uploadedFile = $uploadedFiles['avatar_file'];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $directory = public_path('img/user/avatar');
                if ($userOriginal->getAvatar()->getDato() !== null){
                    deleteFile(public_path($userOriginal->getAvatar()->getDato()));
                }
                $filename = movefiles($directory, $uploadedFile);
                $imgAvatar = new Immagine();
                $imgService = new ImmagineService();
                $imgAvatar->setNome($uploadedFile->getClientFilename());
                $imgAvatar->setDato('img/user/avatar' . DIRECTORY_SEPARATOR . $filename);
                $imgAvatar->setTipo(0);
                $imgAvatar->setDescrizione('img di profilo');
                if ($userOriginal->getAvatar()->getDato() !== null){
                   $res = $imgService->insertImgUser($imgAvatar, $user->getIdUser());
                   $imgAvatar->setIdImmagione(intval($res));
                } else {
                    $imgAvatar->setIdImmagione($userOriginal->getAvatar()->getIdImmagione());
                    $imgService->updateImgUser($imgAvatar, $user->getIdUser());
                }
                $user->setAvatar($imgAvatar);
            } else {
                $user->setAvatar($userOriginal->getAvatar());
            }
        } else{
            $user->setAvatar($userOriginal->getAvatar());
        }

        return $user;
    }

    public function updateObjUserCritica(StoreImpostazioniAccountUserCritica $inputStoreUserCritica, User $userOriginal): User{
        $input = $inputStoreUserCritica->all();
        $user = new User();
        $user->setIdUser($userOriginal->getIdUser());
        $user->setNome($input['nome']);
        $user->setCognome($input['cognome']);
        $user->setDataNascita($input['birthday']);
        $user->setCodiceAlbo($input['codice_identificativo_albo']);
        $user->setNomeAzienda($input['azienda']);
        $user->setEmail($input['email']);
        if(isset($input['account_password'])){
            $user->setPassword($input['account_password']);
        } else {
            $user->setPassword($userOriginal->getPassword());
        }
        if(isset($input['biografia'])){
            $user->setPassword($input['biografia']);
        }
        $user->setTipologiaUtenza(1);
        $user->setStato(1);

        $uploadedFiles = $inputStoreUserCritica->getRequest()->getUploadedFiles();
        if (isset($uploadedFiles['avatar_file'])) {
            $uploadedFile = $uploadedFiles['avatar_file'];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $directory = public_path('img/user/avatar');
                if ($userOriginal->getAvatar()->getDato() !== null){
                    deleteFile(public_path($userOriginal->getAvatar()->getDato()));
                }
                $filename = movefiles($directory, $uploadedFile);
                $imgAvatar = new Immagine();
                $imgService = new ImmagineService();
                $imgAvatar->setNome($uploadedFile->getClientFilename());
                $imgAvatar->setDato('img/user/avatar' . DIRECTORY_SEPARATOR . $filename);
                $imgAvatar->setTipo(0);
                $imgAvatar->setDescrizione('img di profilo');
                if ($userOriginal->getAvatar()->getDato() === null){
                    $res = $imgService->insertImgUser($imgAvatar, $user->getIdUser());
                    $imgAvatar->setIdImmagione(intval($res));
                } else {
                    $imgAvatar->setIdImmagione($userOriginal->getAvatar()->getIdImmagione());
                }
                $user->setAvatar($imgAvatar);
            } else {
                $user->setAvatar($userOriginal->getAvatar());
            }
        } else{
            $user->setAvatar($userOriginal->getAvatar());
        }

        return $user;
    }

    public function updateObjUserAdmin(StoreImpostazioniAccountUserAdmin $inputStoreUserAdmin, User $userOriginal): User{
        $input = $inputStoreUserAdmin->all();
        $user = new User();
        $user->setIdUser($userOriginal->getIdUser());
        $user->setNome($input['nome']);
        $user->setCognome($input['cognome']);
        $user->setDataNascita($input['birthday']);
        $user->setEmail($input['email']);
        if(isset($input['account_password'])){
            $user->setPassword($input['account_password']);
        } else {
            $user->setPassword($userOriginal->getPassword());
        }
        $user->setTipologiaUtenza(2);
        $user->setStato(1);

        $uploadedFiles = $inputStoreUserAdmin->getRequest()->getUploadedFiles();
        if (isset($uploadedFiles['avatar_file'])) {
            $uploadedFile = $uploadedFiles['avatar_file'];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $directory = public_path('img/user/avatar');
                if ($userOriginal->getAvatar()->getDato() !== null){
                    deleteFile(public_path($userOriginal->getAvatar()->getDato()));
                }
                $filename = movefiles($directory, $uploadedFile);
                $imgAvatar = new Immagine();
                $imgService = new ImmagineService();
                $imgAvatar->setNome($uploadedFile->getClientFilename());
                $imgAvatar->setDato('img/user/avatar' . DIRECTORY_SEPARATOR . $filename);
                $imgAvatar->setTipo(0);
                $imgAvatar->setDescrizione('img di profilo');
                if ($userOriginal->getAvatar()->getDato() !== null){
                    $res = $imgService->insertImgUser($imgAvatar, $user->getIdUser());
                    $imgAvatar->setIdImmagione(intval($res));
                } else {
                    $imgAvatar->setIdImmagione($userOriginal->getAvatar()->getIdImmagione());
                }
                $user->setAvatar($imgAvatar);
            } else {
                $user->setAvatar($userOriginal->getAvatar());
            }
        } else{
            $user->setAvatar($userOriginal->getAvatar());
        }

        return $user;
    }

    public function deleteImgUser(){
        $user = self::getUser();
        if($user->getAvatar()->getDato() != null){
            deleteFile(public_path($user->getAvatar()->getDato()));
            $imgService = new ImmagineService();
            return $imgService->deleteImg($user->getAvatar());
        }
        return 0;
    }

    public function setUltimiDieciUtenti(User $user): User{
        $imgService = new ImmagineService();
        $immagineResult = $imgService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
        if (!empty($immagineResult)) {
            $user->setAvatar($immagineResult[0]);
        }
        $user->setPassword('');
        $user->setEmail('');
        return $user;
    }

    public function getUltimiDieciUtentiNormaliRegistrati():array{
        $userServiceImplementation = new UserServiceImplementation();
        $array_user = array();
        foreach ($userServiceImplementation->getUltimiDieciUtentiNormaliRegistrati() as $user){
            array_push($array_user, self::setUltimiDieciUtenti($user));
        }
        return $array_user;
    }

    public function getUltimiDieciUtentiCriticiRegistrati():array{
        $userServiceImplementation = new UserServiceImplementation();
        $array_user = array();
        foreach ($userServiceImplementation->getUltimiDieciUtentiCriticiRegistrati() as $user){
            array_push($array_user, self::setUltimiDieciUtenti($user));
        }
        return $array_user;
    }

    public function getUltimiDieciUtentiAdminRegistrati():array{
        $userServiceImplementation = new UserServiceImplementation();
        $array_user = array();
        foreach ($userServiceImplementation->getUltimiDieciUtentiAdminRegistrati() as $user){
            array_push($array_user, self::setUltimiDieciUtenti($user));
        }
        return $array_user;
    }

    public function getUltimiTrentaUtentiRegistrati():array{
        $userServiceImplementation = new UserServiceImplementation();
        $array_user = array();
        foreach ($userServiceImplementation->getUltimiTrentaUtentiRegistrati() as $user){
            array_push($array_user, self::setUltimiDieciUtenti($user));
        }
        return $array_user;
    }

    public function countUser(){
        $userServiceImplementation = new UserServiceImplementation();
        return $userServiceImplementation->countUser();
    }

}
