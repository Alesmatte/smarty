<?php


namespace App\Services;


use App\Model\CategoriaPremio;
use App\Services\Implementation\CategoriaPremioServiceImplementation;

class CategoriaPremioService
{
    /* rappresenta la tabella intermedia Premio_TipologiaPremio */

    /**
     * CategoriaPremioService constructor.
     */
    public function __construct()
    {
    }
    /* NON DOVREBBE SERVIRE */
    public function insertCategoriaPremio(CategoriaPremio $categoriaPremio) {
        $categoriaPremioServiceImpl = new CategoriaPremioServiceImplementation();
        return $categoriaPremioServiceImpl->insertCategoriaPremio($categoriaPremio);
    }
    /* NON DOVREBBE SERVIRE */
    public function updateCategoriaPremio(CategoriaPremio $categoriaPremio) {
        $categoriaPremioServiceImpl = new CategoriaPremioServiceImplementation();
        return $categoriaPremioServiceImpl->updateCategoriaPremio($categoriaPremio);
    }
    /* NON DOVREBBE SERVIRE */
    public function deleteCategoriaPremio(CategoriaPremio $categoriaPremio) {
        $categoriaPremioServiceImpl = new CategoriaPremioServiceImplementation();
        return $categoriaPremioServiceImpl->deleteCategoriaPremio($categoriaPremio);
    }

    public function getCategoriaPremioByIdTipologiaPremioAndByIdPremio($idTipologiaPremio, $idPremio): CategoriaPremio {
        $categoriaPremioServiceImpl = new CategoriaPremioServiceImplementation();
        return $categoriaPremioServiceImpl->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($idTipologiaPremio, $idPremio);
    }
}