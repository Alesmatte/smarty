<?php


namespace App\Services;


use App\Services\Implementation\TipologiaPremioServiceImplementation;

class TipologiaPremioService
{

    /**
     * TipologiaPremioService constructor.
     */
    public function __construct()
    {
    }

    public function getAllTipologiaPremio(): array {
        $premioServiceImpl = new TipologiaPremioServiceImplementation();
        return $premioServiceImpl->getAllTipologiaPremio();
    }

    public function getAllTipologiaPremioForFilmmakers(): array{
        $listTipologiaPremi = self::getAllTipologiaPremio();
        $listTipologiaPremiVeri = array();
        foreach ($listTipologiaPremi as $premioTipologia){
            if ($premioTipologia->getNome() != 'Miglior Serie TV' && $premioTipologia->getNome() != 'Miglior Film'){
                array_push($listTipologiaPremiVeri, $premioTipologia);
            }
        }
        return $listTipologiaPremiVeri;
    }

}