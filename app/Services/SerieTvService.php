<?php


namespace App\Services;

use App\Model\Distributore;
use App\Model\Episodio;
use App\Model\Filmmaker;
use App\Model\Genere;
use App\Model\Immagine;
use App\Model\Nomination;
use App\Model\Premio;
use App\Model\Ruolo;
use App\Model\SerieTv;
use App\Model\Stagione;
use App\Model\TipologiaPremio;
use App\Model\Video;
use App\Services\Implementation\DistributoreServiceImplementation;
use App\Services\Implementation\FilmMakerServiceImplementation;
use App\Services\Implementation\GenereServiceImplementation;
use App\Services\Implementation\ImmagineServiceImplementation;
use App\Services\Implementation\NominationServiceImplementation;
use App\Services\Implementation\RecensioneServiceImplementation;
use App\Services\Implementation\RuoloServiceImplementation;
use App\Services\Implementation\SerieTvServiceImplementation;
use App\Services\Implementation\StagioneServiceImplementation;
use App\Services\Implementation\VideoServiceImplementation;
use App\Support\RequestInput;
use Exception;


class SerieTvService
{
    /**
     * SerieTvService constructor.
     */
    public function __construct()
    {
    }

    private function setSerieTvComponent(SerieTv $serieTv, SerieTvServiceImplementation $serieTVServiceImpl, bool $notRicerca = true): SerieTv
    {
        $imgServiceImpl = new ImmagineServiceImplementation();
        $recServiceImpl = new RecensioneServiceImplementation();
        $distrServiceImpl = new DistributoreServiceImplementation();
        $generiServiceImpl = new GenereServiceImplementation();
        $nomServiceImpl = new NominationServiceImplementation();
        $videoServiceImpl = new VideoServiceImplementation();

//        $serieTv = $serieTVServiceImpl->getSerieTvById($serieTv->getIdSerieTv());
        if ($notRicerca) {
            $serieTv->setNazionalita($serieTVServiceImpl->getNazionalitasByIdSerieTv($serieTv->getIdSerieTv()));
        }
        $resultimg = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 2);
        if(count($resultimg)>0){
            $serieTv->setImgCopertina($resultimg[0]);
        }
        if ($notRicerca) {
            $serieTv->setImgs(array_merge($imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 5), $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 6)));
        }

        $serieTv->setRecensioni($recServiceImpl->getRecensioniByIdSerieTv($serieTv->getIdSerieTv()));

        $serieTv->setDistributori($distrServiceImpl->getDistributoriByIdSerieTv($serieTv->getIdSerieTv()));
        $serieTv->setGeneri($generiServiceImpl->getGenereByIdSerieTv($serieTv->getIdSerieTv()));

        if ($notRicerca) {
            $videoList = $videoServiceImpl->getVideoByIdSerieTv($serieTv->getIdSerieTv());
            $serieTv->setVideos($videoList);
        }

        if ($notRicerca) {
            /** blocco stagioni-episodi */
            $stagioneServiceImpl = new StagioneServiceImplementation();
            $stagioneList = $stagioneServiceImpl->getStagionebyIdSerieTv($serieTv->getIdSerieTv());
            foreach ($stagioneList as $key => $stagione) {
                $episodioService = new EpisodioService();
                $stagioneList[$key]->setEpisodi($episodioService->getEpisodiStagioneByIdStagione($stagione->getIdStagione()));
            }
            $serieTv->setStagioni($stagioneList);
        }

        /**  blocco nomination-premi */
        $nominationList = $nomServiceImpl->getNominationByIdSerieTv($serieTv->getIdSerieTv());
        $serieTv->setNominations($nominationList);
        /**  blocco ruoli */
        $ruoloServicesImpl = new RuoloServiceImplementation();
        $ruoliList = $ruoloServicesImpl->getRuoliByidSerieTv($serieTv->getIdSerieTv());
        foreach ($ruoliList as $key => $ruolo) {
            $filmmakerService = new FilmMakerService();
            $ruoliList[$key]->setFilmMaker($filmmakerService->getFilmakerById($ruolo->getFilmMaker()->getIdFilmMaker()));
        }
        $serieTv->setRuoli($ruoliList);
        if ($notRicerca) {
            /** immagini delle persone in quel ruolo */
            foreach ($ruoliList as $ruolo) {
                $serieTv->setMediaFilmSerieTvs($imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('ruolo', $ruolo->getIdRuolo(), 5));
            }
        }
        return $serieTv;
    }

    public function setSerieTvComponentParzialeCarosello(SerieTv $serieTv, bool $inCarosello = true): SerieTv
    {
        $imgService = new ImmagineService();
        $idSerieTv = $serieTv->getIdSerieTv();
        if (isset($idSerieTv)) {
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 2);
            if (!empty($result)) {
                $serieTv->setImgCopertina($result[0]);
            }
            if ($inCarosello) {
                $serieTv->mediaVotoRedCarpet = number_format($this->getMediaVotiSerieTv($serieTv), 2, '.', '');
            }
        }
        return $serieTv;
    }

    private function setSerieTvComponentParzialePerLista(SerieTv $serieTv, SerieTvServiceImplementation $serieTVServiceImpl): SerieTv
    {
        $distrServiceImpl = new DistributoreServiceImplementation();
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $mmagineServiceImpl = new ImmagineServiceImplementation();
        $generiServiceImpl = new GenereServiceImplementation();
        $ruoloServicesImpl = new RuoloServiceImplementation();
        $videoServiceImpl = new VideoServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $episodioService = new EpisodioService();

        $serieTv->setNazionalita($serieTVServiceImpl->getNazionalitasByIdSerieTv($serieTv->getIdSerieTv()));
        $serieTv->setImgCopertina($mmagineServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 2)[0]);
        $serieTv->setDistributori($distrServiceImpl->getDistributoriByIdSerieTv($serieTv->getIdSerieTv()));
        $serieTv->setGeneri($generiServiceImpl->getGenereByIdSerieTv($serieTv->getIdSerieTv()));
        $serieTv->setVideos($videoServiceImpl->getVideoByIdSerieTv($serieTv->getIdSerieTv()));

        $stagioneList = $stagioneServiceImpl->getStagionebyIdSerieTv($serieTv->getIdSerieTv());
        foreach ($stagioneList as $key => $stagione) {
            $stagioneList[$key]->setEpisodi($episodioService->getEpisodiStagioneByIdStagione($stagione->getIdStagione()));
        }
        $serieTv->setStagioni($stagioneList);

        $ruoliList = $ruoloServicesImpl->getRuoliByidSerieTv($serieTv->getIdSerieTv());
        foreach ($ruoliList as $key => $ruolo) {
            $ruoliList[$key]->setFilmMaker($filmmakerService->getFilmakerById($ruolo->getFilmMaker()->getIdFilmMaker()));
        }
        $serieTv->setRuoli($ruoliList);

        $serieTv->mediaRedCarpet = self::getMediaVotiSerieTv($serieTv);
        $serieTv->mediaCritica = self::mediaVotiCritici($serieTv);
        $serieTv->mediaPubblico = self::mediaVotiUtenti($serieTv);
        $serieTv->maggiorediCinqueVoti = self::maggiorediCinqueVoti($serieTv);

        return $serieTv;
    }

    /**
     * @param int $idSerieTv
     * @return SerieTv
     */
    public function getSerieTvById(int $idSerieTv): SerieTv
    {
        $serieTVServiceImpl = new SerieTvServiceImplementation();
        $serieTv = $serieTVServiceImpl->getSerieTvById($idSerieTv);
        return self::setSerieTvComponent($serieTv, $serieTVServiceImpl);
    }

    public function getAllSerieTv(bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getAllSerieTv();
        foreach ($serieTvList as $k => $stvl) {
            $serieTvList[$k] = self::setSerieTvComponent($stvl, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getAllSerieTvParziali()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getAllSerieTv();
        foreach ($serieTvList as $k => $stvl) {
            $stvl->numeroStagioni = $serieTvServiceImpl->getCountStagioniBySerieTv($stvl);
        }
        return $serieTvList;
    }

    public function getSerieTvParzialeById(int $id)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvById($id);
        foreach ($serieTvList as $k => $stvl) {
            $stvl->numeroStagioni = $serieTvServiceImpl->getCountStagioniBySerieTv($stvl);
        }
        return $serieTvList;
    }

    public function getSerieTvByIdRecensioni(int $idRecensione): SerieTv
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return self::setSerieTvComponentParzialeCarosello($serieTvServiceImpl->getSerieTvByIdRecensioni($idRecensione));
    }

    /** Restituisce solo TUTTI gli elementi nella tabella SerieTv, L'IMG DI COPERTINA e le RECENSIONI per prendere i voti
     * @param int $idSerieTv
     * @param int $limit
     * @param int $offset
     * @return SerieTv
     */
    public function getSerieTvImgRecensByIdSerieTv(int $idSerieTv, $limit = PHP_INT_MAX, $offset = 0): SerieTv
    {
        $serieTVServiceImpl = new SerieTvServiceImplementation();
        $imgServiceImpl = new ImmagineServiceImplementation();
        $recServiceImpl = new RecensioneServiceImplementation();
        $serieTv = $serieTVServiceImpl->getSerieTvById($idSerieTv);
        $serieTv->setImgCopertina($imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTv->getIdSerieTv(), 2)[0]);
        $serieTv->setRecensioni($recServiceImpl->getRecensioniByIdSerieTv($serieTv->getIdSerieTv(), $limit, $offset));
        return $serieTv;
    }

    public function getAllSerieTvPerRecensioni()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvMezzeList = $serieTvServiceImpl->getAllSerieTvPerRecensioni();
        $serieTvList = array();
        foreach ($serieTvMezzeList as $k => $stvl) {
            $imgService = new ImmagineService();
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $stvl->getIdSerieTv(), 2);
            if (!empty($result)) {
                $stvl->setImgCopertina($result[0]);
            }
            array_push($serieTvList, $stvl);
        }
        return $serieTvList;
    }

    public function getAllSerieTvPerRecensioniSearch(string $search)
    {
        $search = '%' . $search . '%';
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvMezzeList = $serieTvServiceImpl->getAllSerieTvPerRecensioniSearch($search);
        $serieTvList = array();
        if (!empty($serieTvMezzeList)) {
            foreach ($serieTvMezzeList as $k => $stvl) {
                $imgService = new ImmagineService();
                $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $stvl->getIdSerieTv(), 2);
                if (!empty($result)) {
                    $stvl->setImgCopertina($result[0]);
                }
                array_push($serieTvList, $stvl);
            }
        }
        return $serieTvList;
    }

    public function deleteSerieTvStagioneEpisodioByIdSerieTv(int $idSerieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $imgServiceImpl = new ImmagineServiceImplementation();
        $recServiceImpl = new RecensioneServiceImplementation();
        $distrServiceImpl = new DistributoreServiceImplementation();
        $generiServiceImpl = new GenereServiceImplementation();
        $videoServiceImpl = new VideoServiceImplementation();
        $nomServiceImpl = new NominationServiceImplementation();
        $ruoloServicesImpl = new RuoloServiceImplementation();
        $filmmakerservice = new FilmMakerServiceImplementation();
        $stagioneList = $stagioneServiceImpl->getStagionebyIdSerieTv($idSerieTv);
        foreach ($stagioneList as $stagioni) {
            $episodioService = new EpisodioService();
            $listEpisodi = $episodioService->getEpisodiStagioneByIdStagione($stagioni->getIdStagione());
            foreach ($listEpisodi as $episodio) {
                if ($episodio->getImg() !== new Immagine()) {
                    if ($episodio->getImg()->getDato() != null) {
                        deleteFile(public_path($episodio->getImg()->getDato()));
                    }
                }
                $episodioService->deleteEpisodioImmagineByIdEpi($episodio->getIdEpisodio());
            }
            $episodioService->deleteEpisodiImmaginiStagioneByIdStagione($stagioni->getIdStagione());
        }
        $serieTvServiceImpl->deleteNazioneProduzioneSerieTvByIdSerieTv($idSerieTv);

        // cancello imgs
        $listImgsCopertina = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $idSerieTv, 2);
        $listImgsPoster = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $idSerieTv, 6);
        $listImgsGeneric = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $idSerieTv, 5);
        foreach ($listImgsCopertina as $img) {
            if ($img !== new Immagine()) {
                if ($img->getDato() != null) {
                    deleteFile(public_path($img->getDato()));
                }
            }
        }
        foreach ($listImgsPoster as $img) {
            if ($img !== new Immagine()) {
                if ($img->getDato() != null) {
                    deleteFile(public_path($img->getDato()));
                }
            }
        }
        foreach ($listImgsGeneric as $img) {
            if ($img !== new Immagine()) {
                if ($img->getDato() != null) {
                    deleteFile(public_path($img->getDato()));
                }
            }
        }
        $imgServiceImpl->deleteImgByIDSerieTv($idSerieTv);

        $recServiceImpl->deleteRecensioneByIdSerieTv($idSerieTv);
        $distrServiceImpl->deleteDistributoreSerieTvByIdSerieTv($idSerieTv);
        $generiServiceImpl->deleteGenereSerieTvByIdSerieTv($idSerieTv);

        //cancello video
        $videoList = $videoServiceImpl->getVideoByIdSerieTv($idSerieTv);
        if(count($videoList)>0){
            foreach ($videoList as $video) {
                if ($video !== null) {
                    $immagineVideo = $imgServiceImpl->getImgByEntitaAndIdEntitaAndTipologia('video', $video->getIdVideo(), 3);
                    if (!empty($immagineVideo)) {
                        deleteFile(public_path($immagineVideo[0]->getDato()));
                    }
                    deleteFile(public_path($video->getDato()));
                    $imgServiceImpl->deleteImgByIdVideo($video);
                }
            }
            $videoServiceImpl->deleteVideoByIdSerieTv($idSerieTv);
        }

        $nominList = $nomServiceImpl->getNominationByIdSerieTv($idSerieTv);
        $nomServiceImpl->deleteNominationSerieTvByIdSerieTv($idSerieTv);
        foreach ($nominList as $nominats) {
            $filmmakerservice->deleteFilmMakerNominationByFilmmaker($nominats->getFilmMaker());
            $nomServiceImpl->deleteNomination($nominats);
        }
        $ruoloList = $ruoloServicesImpl->getRuoloSerieTvRuoloByidSerieTv($idSerieTv);
        foreach ($ruoloList as $ruoli) {
            $imgServiceImpl->deleteImgByIdRuolo($ruoli);
        }
        $ruoloServicesImpl->deleteRuoloSerieTvByIdSerieTv($idSerieTv);
        return $serieTvServiceImpl->deleteSerieTv($idSerieTv);

    }

    public function insertSerieTV(SerieTv $serieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $idSerieTv = $serieTvServiceImpl->insertSerieTv($serieTv);

        /* inserisco nella tabella di collegamento tra SerieTv e distributore */
        $distributoreService = new DistributoreService();
        foreach ($serieTv->getDistributori() as $distributore) {
            $distributoreService->insertDistributoreSerieTv($distributore->getIdDistributore(), $idSerieTv);
        }
        /* inserisco nella tabella di collegamento tra SerieTv e NazioneProduzione */
        foreach ($serieTv->getNazionalita() as $codiceNazione) {
            $serieTvServiceImpl->insertNazioneProduzionesSerieTvs($codiceNazione, $idSerieTv);
        }
        /* inserisco nella tabella di collegamento tra SerieTv e GenereService */
        $genereService = new GenereService();
        foreach ($serieTv->getGeneri() as $genere) {
            $genereService->insertGenereSerieTv($genere->getIdGenere(), $idSerieTv);
        }
        /* inserisco nella tabella di collegamento tra SerieTv e NominationService */
        $nominationService = new NominationService;
        foreach ($serieTv->getNominations() as $nomination) {
            $nominationService->insertNominationSerieTv($nomination->getIdNomination(), $idSerieTv);
            $nominationService->insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());
        }
        /* inserisco nella tabella di collegamento tra SerieTv e RuoloService */
        $ruoloService = new RuoloService();
        foreach ($serieTv->getRuoli() as $ruolo) {
            $ruoloService->insertRuoloSerieTv($ruolo->getIdRuolo(), $idSerieTv);
        }
        return $idSerieTv;
    }

    public function updateSerieTv(SerieTv $serieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $distrServiceImpl = new DistributoreServiceImplementation();
        $generiServiceImpl = new GenereServiceImplementation();
        $nomServiceImpl = new NominationServiceImplementation();
        $ruoloServicesImpl = new RuoloServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $serieTvServiceImpl->deleteNazioneProduzioneSerieTvByIdSerieTv($serieTv->getIdSerieTv());
        foreach ($serieTv->getNazionalita() as $codiceNazione) {
            $serieTvServiceImpl->insertNazioneProduzionesSerieTvs($codiceNazione, $serieTv->getIdSerieTv());
        }
        $distrServiceImpl->deleteDistributoreSerieTvByIdSerieTv($serieTv->getIdSerieTv());
        foreach ($serieTv->getDistributori() as $distrbutori) {
            $distrServiceImpl->insertDistributoreSerieTv($distrbutori->getIdDistributore(), $serieTv->getIdSerieTv());
        }
        $generiServiceImpl->deleteGenereSerieTvByIdSerieTv($serieTv->getIdSerieTv());
        foreach ($serieTv->getGeneri() as $generi) {
            $generiServiceImpl->insertGenereSerieTv($generi->getIdGenere(), $serieTv->getIdSerieTv());
        }
        $nomServiceImpl->deleteNominationSerieTvByIdSerieTv($serieTv->getIdSerieTv());
        foreach ($serieTv->getNominations() as $nominations) {
            $nomServiceImpl->insertNominationSerieTv($nominations->getIdNomination(), $serieTv->getIdSerieTv());
            $filmmakerService->deleteFilmMakerNominationByIdFilmmakerAndIdNomination($nominations->getFilmMaker()->getIdFilmMaker(), $nominations->getIdNomination());
            $nomServiceImpl->insertNominationFilmmaker($nominations->getIdNomination(), $nominations->getFilmMaker()->getIdFilmMaker());
        }
        $ruoloServicesImpl->deleteRuoloSerieTvByIdSerieTv($serieTv->getIdSerieTv());
        foreach ($serieTv->getRuoli() as $ruoli) {
            $ruoloServicesImpl->insertRuoloSerieTv($ruoli->getIdRuolo(), $serieTv->getIdSerieTv());
        }
        return $serieTvServiceImpl->updateSerieTv($serieTv);
    }

    public function getSerieTvInCorso()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $serieTvList = array_slice($serieTvServiceImpl->getSerieTvInCorso(), 0, 16);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponentParzialeCarosello($stv);
            $stagione = array();
            array_push($stagione, $stagioneServiceImpl->getStagioneByIncorsoIdSerieTv($stv->getIdSerieTv()));
            $serieTvList[$k]->setStagioni($stagione);
        }
        return $serieTvList;
    }

    public function getUltimeSerieTvCarosello()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getUltimeSerieTvCarosello();
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponentParzialeCarosello($stv);
            $stagione = array();
            array_push($stagione, $stagioneServiceImpl->getStagioneByIncorsoIdSerieTv($stv->getIdSerieTv()));
            $serieTvList[$k]->setStagioni($stagione);
        }
        return $serieTvList;
    }

    public function getSerieTvByFilmmakerCarosello(Filmmaker $filmmaker)
    {
        $stagioneServiceImpl = new StagioneServiceImplementation();
        $serieTvList = array_slice($this->getSerieTvByFilmmaker($filmmaker), 0, 16);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponentParzialeCarosello($stv);
            $stagione = array();
            array_push($stagione, $stagioneServiceImpl->getStagioneByIncorsoIdSerieTv($stv->getIdSerieTv()));
            $serieTvList[$k]->setStagioni($stagione);
        }
        return $serieTvList;
    }

    public function getSerieTvByTitoloSearch($titolo): array
    {
        $titolo = '%' . $titolo . '%';
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByTitoloSearch($titolo);

        foreach ($serieTvList as $key => $serieTv) {
            $serieTvList[$key] = self::setSerieTvComponent($serieTv, $serieTvServiceImpl);
        }
        return $serieTvList;
    }

    public function getSerieTvByFilmmaker(Filmmaker $filmmaker)
    {
        $serieTVServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTVServiceImpl->getSerieTvByFilmmaker($filmmaker);
        foreach ($serieTvList as $key => $serieTv) {
            $serieTvList[$key] = self::setSerieTvComponent($serieTv, $serieTVServiceImpl);
        }
        return $serieTvList;
    }

    public function mediaVotiUtenti(SerieTv $serieTv)
    {
        $recServiceImpl = new RecensioneServiceImplementation();
        $rec = $recServiceImpl->getVotiByIdSerieTvUser($serieTv->getIdSerieTv());
        if (count($rec) == 0) {
            return 0;
        }
        $sommaVoti = 0;
        foreach ($rec as $vot) {
            $sommaVoti = $sommaVoti + $vot->getVoto();
        }
        return $sommaVoti / count($rec);
    }

    public function mediaVotiCritici(SerieTv $serieTv)
    {
        $recServiceImpl = new RecensioneServiceImplementation();
        $rec = $recServiceImpl->getVotiByIdSerieTvCritico($serieTv->getIdSerieTv());
        if (count($rec) == 0) {
            return 0;
        }
        $sommaVoti = 0;
        foreach ($rec as $vot) {
            $sommaVoti = $sommaVoti + $vot->getVoto();
        }
        return $sommaVoti / count($rec);
    }

    public function getMediaVotiSerieTv(SerieTv $serieTv)
    {
        if (!$this->maggiorediCinqueVoti($serieTv)) {
            return 0;
        }
        if ($this->mediaVotiUtenti($serieTv) == 0) {
            return $this->mediaVotiCritici($serieTv);
        }
        if ($this->mediaVotiCritici($serieTv) == 0) {
            return $this->mediaVotiUtenti($serieTv);
        }
        if (($this->mediaVotiUtenti($serieTv) == 0) && ($this->mediaVotiCritici($serieTv) == 0)) {
            return 0;
        }
        return ($this->mediaVotiUtenti($serieTv) + $this->mediaVotiCritici($serieTv)) / 2;
    }

    public function getSerieTvPiuVotate(): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getAllSerieTv();
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponentParzialeCarosello($stv);
        }
        uasort($serieTvList, fn($a, $b) => ($a->mediaVotoRedCarpet > $b->mediaVotoRedCarpet) ? -1 : 1);
        return array_slice($serieTvList, 0, 15);
    }

    public function getSingolaSerieTvPiuVotata(): SerieTv
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $imgService = new ImmagineService();
        $serieTvList = $serieTvServiceImpl->getAllSerieTv();
        if (empty($serieTvList)) {
            return new SerieTv();
        }
        $mediaVotoAndIndice = array();
        foreach ($serieTvList as $key => $serieTv) {
            $mediaVotoAndIndice[$key] = $this->getMediaVotiSerieTv($serieTv);
        }
        arsort($mediaVotoAndIndice);
        $serieTvPiuVotateList = array();
        foreach ($mediaVotoAndIndice as $key => $value) {
            $serieTvList[$key]->mediaVoto = $value;
            array_push($serieTvPiuVotateList, $serieTvList[$key]);
        }
        if (!empty($serieTvPiuVotateList)) {
            $result = $imgService->getImgByEntitaAndIdEntitaAndTipologia('serieTv', $serieTvPiuVotateList[0]->getIdSerieTv(), 2);
            if (!empty($result)) {
                $serieTvPiuVotateList[0]->setImgCopertina($result[0]);
            }
        }
        return $serieTvPiuVotateList[0];
    }

    public function getSerieTvByAnnoMinoreDi($anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnnoMinoreDi($anno . "/1/01");
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByAnnoMinoreUgualeDi($anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnnoMinoreUgualeDi($anno . "/12/31");
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    /** UGUALE A */
    public function getSerieTvByAnno($anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnno($anno . "/1/01", $anno . "/12/31");
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByAnnoMaggioreUgualeDi($anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnnoMaggioreUgualeDi($anno . "/1/01");
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByAnnoMaggioreDi($anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnnoMaggioreDi($anno . "/12/31");
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByNazionalita(string $codNaz)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByNazionalita($codNaz);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl);
        }
        return $serieTvList;
    }

    public function getAllgetAllNazionalitas()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $nazList = $serieTvServiceImpl->getAllNazionalitas();
        return $nazList;
    }

    /** SERVE PER LA RICERCA */
    public function getSerieTvByTitoloOrTitoloOrig(string $titolo, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByTitoloOrTitoloOrig($titolo);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByTitolo(string $titolo, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByTitolo($titolo);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByTitoloOriginale(string $titoloOrig, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByTitoloOriginale($titoloOrig);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByGenere(int $idGenere, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByGenere($idGenere);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByPremio(int $idPremio, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByPremio($idPremio);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByAnnoVittoria(string $anno, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByAnnoVittoria($anno . "/1/01", $anno . "/12/31", 1);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getSerieTvByDistributore(int $idDistributore, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvDistributore($idDistributore);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getAllSerieTvByRuoli(array $ruoli, Filmmaker $filmmaker): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $nominationsService = new NominationService();
        $ruoloService = new RuoloService();
        $serieTvmezzoPieno = array();
        foreach ($ruoli as $k => $ruolo) {
            try {
                $serietvMezzo = $serieTvServiceImpl->getSerieTvByRuolo($ruolo);
                $serietvMezzo->setNominations($nominationsService->getNominationByIdSerieTv($serietvMezzo->getIdSerieTv()));
                $serietvMezzo->setRuoli($ruoloService->getAllRuoliByIdSerieTvAndFilmmaker($serietvMezzo->getIdSerieTv(), $filmmaker));
                array_push($serieTvmezzoPieno, $serietvMezzo);
            } catch (Exception $e) {
            }
        }
        return $serieTvmezzoPieno;
    }

    public function getSerieTvByIdPerRecensioni(int $idSerieTv): SerieTv
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getSerieTvById($idSerieTv);
    }

    public function getSerieTvByNomeCognomeFilmmaker(string $persona, bool $notRicerca = true)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvByNomeCognomeFilmmaker($persona);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl, $notRicerca);
        }
        return $serieTvList;
    }

    public function getCountStagioniBySerieTv(SerieTv $serieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getCountStagioniBySerieTv($serieTv);
    }

    public function getCountEpisodiBySerieTv(SerieTv $serieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getCountEpisodiBySerieTv($serieTv);
    }

    public function getDurataEpisodioBySerieTv(SerieTv $serieTv)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getDurataEpisodiBySerieTv($serieTv);
    }

    public function getUltimeDieciSerieTvCreate(): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getUltimeDieciSerieTvCreate();
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponent($stv, $serieTvServiceImpl);
        }
        return $serieTvList;
    }

    public function getUltimecinqueSerieTv(): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getUltimecinqueSerieTv();
    }

    public function show()
    {
        $filmService = new FilmService();
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremio();
        $distributoreService = new DistributoreService();
        try {
            $distributori = $distributoreService->getAllDistrucutore();
        } catch (\Exception $exception) {
            $distributori = array();
        }
        $genereService = new GenereService();
        try {
            $generi = $genereService->getAllGenere();
        } catch (\Exception $exception) {
            $generi = array();
        }
        $array_nazioni = $filmService->getAllNations();
        return ['array_nazioni' => $array_nazioni, 'distributori' => $distributori, 'generi' => $generi, 'premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi];
    }

    public function store(RequestInput $requestInput)
    {
        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $categoriaService = new CategoriaPremioService();
        $nominatioService = new NominationService();
        $ruoliService = new RuoloService();
        $stagioneService = new StagioneService();
        $episodioService = new EpisodioService();
        $imgService = new ImmagineService();
        $videoService = new VideoService();

        $serieTvObj = new SerieTv();
        $serieTvObj->setTiloloOriginale($data['titolo_originale_serie_tv']);
        $serieTvObj->setTitolo($data['titolo_serie_tv']);
        $serieTvObj->setDescrizione($data['descrizione_serie_Tv']);
        $serieTvObj->setTrama($data['trama']);
        $serieTvObj->setTramaBreve($data['trama_breve']);
        $serieTvObj->setDataUscita($data['data_uscita_italia']);
        $serieTvObj->setDataSviluppoSerieTv($data['data_sviluppo']);
        $serieTvObj->setNazionalita($data['nazione']);
        $serieTvObj->setAgeRating($data['eta_consigliata']);

        // array di distributori
        $array_distributori = array();
        foreach ($data['distributore'] as $k => $distributoreID) {
            $distributoreFake = new Distributore();
            $distributoreFake->setIdDistributore($distributoreID);
            if ($distributoreID != 0) {
                array_push($array_distributori, $distributoreFake);
            }
        }
        $serieTvObj->setDistributori($array_distributori);


        // array di generi
        $array_generi = array();
        foreach ($data['genere'] as $k => $genereID) {
            $genereFake = new Genere();
            $genereFake->setIdGenere($genereID);
            array_push($array_generi, $genereFake);
        }
        $serieTvObj->setGeneri($array_generi);

        $serieTvObj->setRecensioni([]);

        // devo creare oggetto nomination e salvare sul db prima del film
        $array_nomination = array();
        if (array_key_exists('premio', $data)) {
            if (array_key_exists('genere', $data['premio'])) {
                foreach ($data['premio']['genere'] as $k => $genereID) {
                    $nomination = new Nomination();
                    $tipologia = new TipologiaPremio();
                    $premio = new Premio();
                    $filmmakerFake = new Filmmaker();
                    $premio->setIdPremio($genereID);
                    $tipologia->setidTipologiaPremio($data['premio']['tipologia'][$k]);
                    $categoria = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($tipologia->getidTipologiaPremio(), $premio->getIdPremio());
                    $nomination->setCategoriaPremio($categoria);
                    $nomination->setStato($data['premio']['stato'][$k]);
                    $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$k]);
                    $nomination->setFilmMaker($filmmakerFake);
                    $nomination->setDescrizione($data['premio']['descrizione'][$k]);
                    $nomination->setDataPremiazione($data['premio']['data_premiazione'][$k]);

                    // salva sul db
                    $res = $nominatioService->insertNomination($nomination);
                    //$res = 0;
                    $nomination->setIdNomination($res);

                    array_push($array_nomination, $nomination);
                }
            }
        }
        $serieTvObj->setNominations($array_nomination);


        // devo creare oggetto Ruolo e salvare sul db prima del film
        $array_ruolo = array();
        foreach ($data['cast']['select_filmmakers'] as $k => $filmmakerID) {
            $ruolo = new Ruolo();
            $filmmakerFake = new Filmmaker();
            $filmmakerFake->setIdFilmMaker($filmmakerID);
            $ruolo->setFilmMaker($filmmakerFake);
            $ruolo->setTipologia($data['cast']['tipologia'][$k]);
            $ruolo->setRuoloInternoOpera($data['cast']['ruolo'][$k]);
            // salva ruolo
            $res = $ruoliService->insertRuolo($ruolo);
            // aggiungi all array id salvato
            //$res = 0;
            $ruolo->setIdRuolo($res);
            array_push($array_ruolo, $ruolo);
        }
        $serieTvObj->setRuoli($array_ruolo);

        // Salva serie TV
        #### Salvattaggio serie tv ###

        $res = self::insertSerieTV($serieTvObj);
        //$res = 1;
        $serieTvObj->setIdSerieTv($res);


        $serieTv_path = bin2hex(random_bytes(8));


        //genero le stagioni
        $array_staggioniObj = array();
        $array_staggioni_nomi = array_unique($data['stagioni_episodi']['numero_stagione']);
        foreach ($array_staggioni_nomi as $k => $nomeStagione) {
            $stagioneObj = new Stagione();
            $stagioneObj->setNumero($nomeStagione);
            $stagioneObj->setInCorso($data['serie_tv_in_corso']);
            // salva sul db la stagione
            $res = $stagioneService->insertStagione($stagioneObj, $serieTvObj->getIdSerieTv());
            //$res = 0;
            $stagioneObj->setIdStagione($res);
            array_push($array_staggioniObj, $stagioneObj);
        }

        //salvo gli episodi
        foreach ($array_staggioniObj as $ks => $stagione) {
            $array_episodiObj = array();
            $numero_episodio = 1;
            foreach ($data['stagioni_episodi']['numero_stagione'] as $k => $nomeStagione) {
                if ($stagione->getNumero() == $nomeStagione) {
                    $episodio = new Episodio();
                    $episodio->setNumero($numero_episodio);
                    $numero_episodio++;
                    $episodio->setTitolo($data['stagioni_episodi']['titolo'][$k]);
                    $episodio->setDescrizione($data['stagioni_episodi']['descrizione'][$k]);
                    $episodio->setDurata($data['stagioni_episodi']['dutata_puntata'][$k]);
                    $episodio->setDataInOnda(date('Y-m-d H:i:s'));
                    $episodio->setNumeroStagione($nomeStagione);

                    //salvo episodio
                    $res = $episodioService->insertEpisodio($episodio, $stagione->getIdStagione());
                    //$res = 0;
                    $episodio->setIdEpisodio($res);


                    // salvo un eventuale img

                    $uploadedFile = $uploadedFiles['stagioni_episodi']['copertina'][$k];
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        $directory = public_path('img/serietv/');
                        $filename = movefiles($directory . $serieTv_path, $uploadedFile);
                        //$filename = "copertina episodio";
                        $imgCopertina = new Immagine();
                        $imgCopertina->setNome($uploadedFile->getClientFilename());
                        $imgCopertina->setDescrizione($data['stagioni_episodi']['descrizione'][$k]);
                        $imgCopertina->setDato("img/serietv/" . $serieTv_path . DIRECTORY_SEPARATOR . $filename);
                        $imgCopertina->setTipo(5);

                        // salvo l'immagine
                        $res = $imgService->insertImgEpisodio($imgCopertina, $episodio->getIdEpisodio());
                        //$res = 1;
                        $imgCopertina->setIdImmagione($res);
                        $episodio->setImg($imgCopertina);
                    }

                    array_push($array_episodiObj, $episodio);

                }
            }
            $stagione->setEpisodi($array_episodiObj);
        }

        $serieTvObj->setStagioni($array_staggioniObj);

        // salvo immagine copertina
        $uploadedFile = $uploadedFiles['immagine_copertina'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/serietv/');
            $filename = movefiles($directory . $serieTv_path, $uploadedFile);
            //$filename = "copertina serie";
            $imgCopertina = new Immagine();
            $imgCopertina->setNome($uploadedFile->getClientFilename());
            $imgCopertina->setDescrizione('img copertina');
            $imgCopertina->setDato("img/serietv/" . $serieTv_path . DIRECTORY_SEPARATOR . $filename);
            $imgCopertina->setTipo(2);
            // eseguo query di salvataggio img coprtina con id film
            $res = $imgService->insertImgSerieTv($imgCopertina, $serieTvObj->getIdSerieTv());
            //$res = 0;
            $imgCopertina->setIdImmagione($res);
            $serieTvObj->setImgCopertina($imgCopertina);
        }


        // Salvo i video
        $array_video = array();
        if (array_key_exists('media', $uploadedFiles)) {
            if (array_key_exists('video', $uploadedFiles['media'])) {
                foreach ($uploadedFiles['media']['video'] as $k => $uploadedFile) {
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        if ($uploadedFiles['media']['cover'][$k]->getError() === UPLOAD_ERR_OK) {
                            $directoryVideo = public_path('video/serietv/');
                            $filenameVideo = movefiles($directoryVideo . $serieTv_path, $uploadedFile);
                            //$filenameVideo = "nome video";
                            $video = new Video();
                            $video->setNome($uploadedFile->getClientFilename());
                            $video->setDescrizione($data['media']['descrizione'][$k]);
                            $video->setDato("video/serietv/" . $serieTv_path . DIRECTORY_SEPARATOR . $filenameVideo);
                            // salvo il video con id del film
                            $res = $videoService->insertVideoSerieTv($video, $serieTvObj->getIdSerieTv());
                            //$res = 0;
                            $video->setIdVideo($res);

                            $directoryCover = public_path('img/serietv/');
                            $filename = movefiles($directoryCover . $serieTv_path, $uploadedFiles['media']['cover'][$k]);
                            //$filename = "coperina video";
                            $imgCopertina = new Immagine();
                            $imgCopertina->setNome($uploadedFiles['media']['cover'][$k]->getClientFilename());
                            $imgCopertina->setDescrizione($data['media']['descrizione'][$k]);
                            $imgCopertina->setDato("img/serietv/" . $serieTv_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo(3);

                            // salvo immagine con id video
                            $res = $imgService->insertImgVideo($imgCopertina, $video->getIdVideo());
                            //$res = 0;
                            $imgCopertina->setIdImmagione($res);
                            $video->setImmagine($imgCopertina);
                            array_push($array_video, $video);
                        }
                    }
                }
            }
        }
        $serieTvObj->setVideos($array_video);


        // salvo le immagini del film
        $array_img = array();
        if (array_key_exists('immagini', $uploadedFiles)) {
            if (array_key_exists('file', $uploadedFiles['immagini'])) {
                foreach ($uploadedFiles['immagini']['file'] as $k => $uploadedFile) {
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        $directory = public_path('img/serietv/');
                        $filename = movefiles($directory . $serieTv_path, $uploadedFile);
                        //$filename = "img file";
                        $imgCopertina = new Immagine();
                        $imgCopertina->setNome($uploadedFile->getClientFilename());
                        $imgCopertina->setDescrizione($data['immagini']['descrizione'][$k]);
                        $imgCopertina->setDato("img/serietv/" . $serieTv_path . DIRECTORY_SEPARATOR . $filename);
                        $imgCopertina->setTipo($data['immagini']['scheda'][$k]);
                        // salvo le immagini con id del film
                        $imgService->insertImgSerieTv($imgCopertina, $serieTvObj->getIdSerieTv());
                        array_push($array_img, $imgCopertina);
                    }
                }
            }
        }
        $serieTvObj->setImgs($array_img);
        //dd("so arrivato");
        //dd($serieTvObj);

        //dd($requestInput->all(), $serieTvObj, $requestInput->getRequest()->getUploadedFiles());
        session()->flash()->set("successCreationSerieTv", "La SerieTv: " . $serieTvObj->getTitolo() . " è stata creata con successo");
        return true;
    }

    public function showModify(RequestInput $requestInput)
    {
        $premioService = new PremioService();
        $tipologiaPremioService = new TipologiaPremioService();
        $listPremi = $premioService->getAllPremio();
        $listTipologiaPremi = $tipologiaPremioService->getAllTipologiaPremio();
        $distributoreService = new DistributoreService();
        try {
            $distributori = $distributoreService->getAllDistrucutore();
        } catch (Exception $exception) {
            $distributori = array();
        }
        $genereService = new GenereService();
        try {
            $generi = $genereService->getAllGenere();
        } catch (Exception $exception) {
            $generi = array();
        }
        $filmService = new FilmService();
        $ag = $requestInput->getArguments();
        $array_nazioni = $filmService->getAllNations();
        $serieTvObj = self::getSerieTvById($ag['idSerieTv']);
        $qr = $requestInput->getQueryString();
        $showCast = false;
        $showNomination = false;
        foreach ($qr as $qrk => $argument) {
            if ($qrk === 'sc') {
                $showCast = true;
            } else if ($qrk === 'sn') {
                $showNomination = true;
            }
        }
        //dd($serieTvObj->getStagioni(),$serieTvObj->getStagioni()[0]->getEpisodi(), $serieTvObj->getStagioni()[0]->getEpisodi()[0], $serieTvObj->getStagioni()[0]->getEpisodi()[1]);
        return ['array_nazioni' => $array_nazioni, 'distributori' => $distributori, 'generi' => $generi, 'premi' => $listPremi, 'tipologiaPremi' => $listTipologiaPremi, 'serieTvObj' => $serieTvObj, 'showCast' => $showCast, "showNomination" => $showNomination];
    }

    public function modifySerietv(RequestInput $requestInput)
    {
        $ag = $requestInput->getArguments();
        $serieTvObjOriginal = self::getSerieTvById($ag['idSerieTv']);

        $data = $requestInput->all();
        $uploadedFiles = $requestInput->getRequest()->getUploadedFiles();
        $nominatioService = new NominationService();
        $categoriaService = new CategoriaPremioService();
        $ruoliService = new RuoloService();
        $imgService = new ImmagineService();
        $videoService = new VideoService();
        $stagioneService = new StagioneService();
        $episodioService = new EpisodioService();

        $serieTvObjNew = new SerieTv();
        $serieTvObjNew->setIdSerieTv($serieTvObjOriginal->getIdSerieTv());



        if ($serieTvObjOriginal->getTiloloOriginale() === $data['titolo_originale_serie_tv']) {
            $serieTvObjNew->setTiloloOriginale($serieTvObjOriginal->getTiloloOriginale());
        } else {
            $serieTvObjNew->setTiloloOriginale($data['titolo_originale_serie_tv']);
        }

        if($serieTvObjOriginal->getStagioni()[0]->getInCorso() != $data['serie_tv_in_corso']){
            $listStagioni = $serieTvObjOriginal->getStagioni();
            foreach ($listStagioni as $stagione){
                $stagione->setInCorso($data['serie_tv_in_corso']);
                $stagioneService->updateStagione($stagione, $serieTvObjOriginal->getIdSerieTv());
            }
        }
        //dd($data, $serieTvObjOriginal);
        if ($serieTvObjOriginal->getTitolo() === $data['titolo_serie_tv']) {
            $serieTvObjNew->setTitolo($serieTvObjOriginal->getTitolo());
        } else {
            $serieTvObjNew->setTitolo($data['titolo_serie_tv']);
        }

        if ($serieTvObjOriginal->getDescrizione() === $data['descrizione_serie_Tv']) {
            $serieTvObjNew->setDescrizione($serieTvObjOriginal->getDescrizione());
        } else {
            $serieTvObjNew->setDescrizione($data['descrizione_serie_Tv']);
        }

        if ($serieTvObjOriginal->getTrama() === $data['trama']) {
            $serieTvObjNew->setTrama($serieTvObjOriginal->getTrama());
        } else {
            $serieTvObjNew->setTrama($data['trama']);
        }

        if ($serieTvObjOriginal->getTramaBreve() === $data['trama_breve']) {
            $serieTvObjNew->setTramaBreve($serieTvObjOriginal->getTramaBreve());
        } else {
            $serieTvObjNew->setTramaBreve($data['trama_breve']);
        }

        if ($serieTvObjOriginal->getDataSviluppoSerieTv() === $data['data_sviluppo']) {
            $serieTvObjNew->setDataSviluppoSerieTv($serieTvObjOriginal->getDataSviluppoSerieTv());
        } else {
            $serieTvObjNew->setDataSviluppoSerieTv($data['data_sviluppo']);
        }

        if ($serieTvObjOriginal->getDataUscita() === $data['data_uscita_italia']) {
            $serieTvObjNew->setDataUscita($serieTvObjOriginal->getDataUscita());
        } else {
            $serieTvObjNew->setDataUscita($data['data_uscita_italia']);
        }

        if ($serieTvObjOriginal->getAgeRating() === $data['eta_consigliata']) {
            $serieTvObjNew->setAgeRating($serieTvObjOriginal->getAgeRating());
        } else {
            $serieTvObjNew->setAgeRating($data['eta_consigliata']);
        }

        //prendo la lista delle nazioni perche la cancello e la ricarico
        $serieTvObjNew->setNazionalita($data['nazione']);

        //prendo l'array dei distributori perche la cacello e la ricarico
        $array_distributori = array();
        foreach ($data['distributore'] as $k => $distributoreID) {
            $distributoreFake = new Distributore();
            $distributoreFake->setIdDistributore($distributoreID);
            array_push($array_distributori, $distributoreFake);
        }
        $serieTvObjNew->setDistributori($array_distributori);

        //prendo l'array dei generi perche la cacello e la ricarico
        // salva array di gener
        $array_generi = array();
        foreach ($data['genere'] as $k => $genereID) {
            $genereFake = new Genere();
            $genereFake->setIdGenere($genereID);
            array_push($array_generi, $genereFake);
        }
        $serieTvObjNew->setGeneri($array_generi);

        $serieTvObjNew->setRecensioni($serieTvObjOriginal->getRecensioni());


        // Nomination
        $array_new_nomination = array();
        $array_da_cancellare_nomination = array();
        $array_update = array();
        $array_delete = array();
        $array_new_nomination_insert = array();
        $array_new_nomination_id = array();
        $array_old_nomination_id = array();
        if (isset($data['premio']['Id_premio'])) {
            foreach ($data['premio']['Id_premio'] as $kp => $idPremio) {
                foreach ($serieTvObjOriginal->getNominations() as $k => $nominationObjOriginal) {
                    if ($nominationObjOriginal->getIdNomination() == $idPremio) {
                        $nominationObjNew = new Nomination();
                        $categoriaObjOld = $nominationObjOriginal->getCategoriaPremio();
                        $filmmakerFake = new Filmmaker();

                        $nominationObjNew->setIdNomination($nominationObjOriginal->getIdNomination());

                        if ($categoriaObjOld->getPremio()->getIdPremio() != $data['premio']['genere'][$kp]) {
                            if ($categoriaObjOld->getTipologia()->getidTipologiaPremio() !== $data['premio']['tipologia'][$kp]) {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$kp], $data['premio']['genere'][$kp]);
                            } else {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($categoriaObjOld->getTipologia()->getidTipologiaPremio(), $data['premio']['genere'][$kp]);
                            }
                        } else {
                            if ($categoriaObjOld->getTipologia()->getidTipologiaPremio() !== $data['premio']['tipologia'][$kp]) {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$kp], $data['premio']['genere'][$kp]);
                            } else {
                                $categoriaObjnew = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($categoriaObjOld->getTipologia()->getidTipologiaPremio(), $data['premio']['genere'][$kp]);
                            }
                        }
                        $nominationObjNew->setCategoriaPremio($categoriaObjnew);

                        if ($nominationObjOriginal->getStato() != $data['premio']['stato'][$kp]) {
                            $nominationObjNew->setStato($data['premio']['stato'][$kp]);
                        } else {
                            $nominationObjNew->setStato($nominationObjOriginal->getStato());
                        }

                        if ($nominationObjOriginal->getFilmMaker()->getIdFilmMaker() != $data['premio']['select_filmmakers'][$kp]) {
                            $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$kp]);
                            $nominationObjNew->setFilmMaker($filmmakerFake);
                        } else {
                            $nominationObjNew->setFilmMaker($nominationObjOriginal->getFilmMaker());
                        }

                        if ($nominationObjOriginal->getDescrizione() != $data['premio']['descrizione'][$kp]) {
                            $nominationObjNew->setDescrizione($data['premio']['descrizione'][$kp]);
                        } else {
                            $nominationObjNew->setDescrizione($nominationObjOriginal->getDescrizione());
                        }

                        if ($nominationObjOriginal->getDataPremiazione() != $data['premio']['data_premiazione'][$kp]) {
                            $nominationObjNew->setDataPremiazione($data['premio']['data_premiazione'][$kp]);
                        } else {
                            $nominationObjNew->setDataPremiazione($nominationObjOriginal->getDataPremiazione());
                        }
                        // update sul db
                        $nominatioService->updateNomination($nominationObjNew, $nominationObjOriginal);
                        array_push($array_new_nomination, $nominationObjNew);
                        array_push($array_update, $nominationObjNew);
                    }
                }
            }

            // array_old_id
            foreach ($serieTvObjOriginal->getNominations() as $k => $nomination) {
                array_push($array_old_nomination_id, strval($nomination->getIdNomination()));
            }
            // array_new_id
            foreach ($data['premio']['Id_premio'] as $kp => $idPremio) {
                array_push($array_new_nomination_id, $idPremio);
            }

            // array di id da cancellare
            $array_da_cancellare_nomination = array_diff($array_old_nomination_id, $array_new_nomination_id);

            foreach ($array_da_cancellare_nomination as $k => $idNomi) {
                foreach ($serieTvObjOriginal->getNominations() as $ki => $nomination) {
                    if ($nomination->getIdNomination() == $idNomi) {
                        // cancella le nomination
                        $nominatioService->deleteNominationSerieTvService($nomination);
                        array_push($array_delete, $nomination);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_nomination = array_diff($array_new_nomination_id, $array_old_nomination_id);

            // insert new nomination
            foreach ($array_da_inserire_nomination as $kArray => $id) {
                foreach ($data['premio']['Id_premio'] as $k => $idPremio) {
                    if ($kArray === $k && $id === $idPremio) {
                        $nomination = new Nomination();
                        $premio = new Premio();
                        $filmmakerFake = new Filmmaker();
                        $premio->setIdPremio($data['premio']['genere'][$k]);
                        $categoria = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($data['premio']['tipologia'][$k], $premio->getIdPremio());
                        $nomination->setCategoriaPremio($categoria);
                        $filmmakerFake->setIdFilmMaker($data['premio']['select_filmmakers'][$k]);
                        $nomination->setStato($data['premio']['stato'][$k]);
                        $nomination->setDescrizione($data['premio']['descrizione'][$k]);
                        $nomination->setFilmMaker($filmmakerFake);
                        $nomination->setDataPremiazione($data['premio']['data_premiazione'][$k]);
                        // carico sul db nuove nomination
                        $res = $nominatioService->insertNomination($nomination);
                        $nomination->setIdNomination(intval($res));
                        $nominatioService->insertNominationSerieTv($nomination->getIdNomination(), $serieTvObjNew->getIdSerieTv());
                        $nominatioService->insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());
                        array_push($array_new_nomination_insert, $nomination);
                        array_push($array_new_nomination, $nomination);
                    }

                }
            }
        } else {
            // devo eliminare titte le nomiantion
            foreach ($serieTvObjOriginal->getNominations() as $k => $nominationObjOriginal) {
                array_push($array_delete, $nominationObjOriginal);
                $nominatioService->deleteNominationSerieTvService($nominationObjOriginal);
            }
        }


        $serieTvObjNew->setNominations($array_new_nomination);

        // Ruoli
        $array_operazioni = array();
        $array_ruolo_new = array();
        $array_ruolo_new_id = array();
        $array_ruolo_new_insert = array();
        $array_ruolo_old_id = array();
        if (isset($data['cast']['id_cast'])) {

            foreach ($data['cast']['id_cast'] as $kp => $idCast) {
                foreach ($serieTvObjOriginal->getRuoli() as $k => $ruoloObjOriginal) {
                    if ($ruoloObjOriginal->getIdRuolo() == $idCast) {
                        $ruoloObjNew = new Ruolo();
                        $filmmakerFake = new Filmmaker();

                        $ruoloObjNew->setIdRuolo($ruoloObjOriginal->getIdRuolo());

                        if ($ruoloObjOriginal->getFilmMaker()->getIdFilmMaker() != $data['cast']['select_filmmakers'][$kp]) {
                            $filmmakerFake->setIdFilmMaker($data['cast']['select_filmmakers'][$kp]);
                            $ruoloObjNew->setFilmMaker($filmmakerFake);
                        } else {
                            $ruoloObjNew->setFilmMaker($ruoloObjOriginal->getFilmMaker());
                        }

                        if ($ruoloObjOriginal->getTipologia() != $data['cast']['tipologia'][$kp]) {
                            $ruoloObjNew->setTipologia($data['cast']['tipologia'][$kp]);
                        } else {
                            $ruoloObjNew->setTipologia($ruoloObjOriginal->getTipologia());
                        }

                        if ($ruoloObjOriginal->getRuoloInternoOpera() != $data['cast']['ruolo'][$kp]) {
                            $ruoloObjNew->setRuoloInternoOpera($data['cast']['ruolo'][$kp]);
                        } else {
                            $ruoloObjNew->setRuoloInternoOpera($ruoloObjOriginal->getRuoloInternoOpera());
                        }

                        // update
                        $ruoliService->updateRuolo($ruoloObjNew);
                        //update del filmmaker
                        $ruoliService->updateRuoloFilmmakerByIdFilmmaker($ruoloObjNew->getIdRuolo(), $ruoloObjNew->getFilmMaker()->getIdFilmMaker());

                        array_push($array_ruolo_new, $ruoloObjNew);
                    }
                }
            }

            // array_ruolo_old_id
            foreach ($serieTvObjOriginal->getRuoli() as $k => $ruoli) {
                array_push($array_ruolo_old_id, strval($ruoli->getIdRuolo()));
            }
            // array_ruolo_new_id
            foreach ($data['cast']['id_cast'] as $kp => $idRuoli) {
                array_push($array_ruolo_new_id, $idRuoli);
            }

            // array di id da cancellare
            $array_da_cancellare_ruolo = array_diff($array_ruolo_old_id, $array_ruolo_new_id);

            foreach ($array_da_cancellare_ruolo as $k => $idRuolo) {
                foreach ($serieTvObjOriginal->getRuoli() as $ki => $ruolo) {
                    if ($ruolo->getIdRuolo() == $idRuolo) {
                        // cancella i ruoli
                        $ruoliService->deleteRuoloSerietvById($ruolo);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_ruolo = array_diff($array_ruolo_new_id, $array_ruolo_old_id);

            // insert new ruolo
            foreach ($array_da_inserire_ruolo as $kArray => $id) {
                foreach ($data['cast']['id_cast'] as $k => $idCast) {
                    if ($kArray === $k && $id === $idCast) {
                        $ruolo = new Ruolo();
                        $filmmakerFake = new Filmmaker();
                        $filmmakerFake->setIdFilmMaker($data['cast']['select_filmmakers'][$k]);
                        $ruolo->setTipologia($data['cast']['tipologia'][$k]);
                        $ruolo->setFilmMaker($filmmakerFake);
                        $ruolo->setRuoloInternoOpera($data['cast']['ruolo'][$k]);
                        // salva ruolo
                        $res = $ruoliService->insertRuolo($ruolo);
                        $ruolo->setIdRuolo(intval($res));
                        $ruoliService->insertRuoloSerieTv($ruolo->getIdRuolo(), $serieTvObjNew->getIdSerieTv());
                        array_push($array_ruolo_new_insert, $ruolo);
                        array_push($array_ruolo_new, $ruolo);
                    }
                }
            }

        } else {
            // devo eliminare tutti i ruoli
            foreach ($serieTvObjNew->getRuoli() as $k => $ruoloObjOriginal) {
                $ruoliService->deleteRuoloSerietvById($ruoloObjOriginal);
            }
        }
        //dd($requestInput->all(), '$array_ruolo_new', $array_ruolo_new, '$array_ruolo_new_id', $array_ruolo_new_id, '$array_ruolo_new_insert', $array_ruolo_new_insert, '$array_ruolo_old_id', $array_ruolo_old_id);

        $serieTvObjNew->setRuoli($array_ruolo_new);

        #### UPDATE FILM ####
        self::updateSerieTv($serieTvObjNew);

        $film_path = explode("/", explode('img/serietv', $serieTvObjOriginal->getImgCopertina()->getDato())[1])[1];

        // Stagioni
        $array_stagione_new = array();
        $array_stagione_new_id = array();
        $array_stagione_new_insert = array();
        $array_stagione_old_id = array();
        $array_stagione_delete = array();

        if (isset($data['stagioni_episodi']['id_stagione'])) {
            $array_stagione_new_id = array_unique($data['stagioni_episodi']['id_stagione']);

            foreach ($array_stagione_new_id as $ks => $idStagione) {
                foreach ($serieTvObjOriginal->getStagioni() as $k => $stagione) {
                    if ($stagione->getIdStagione() == $idStagione) {
                        $stagioneNew = new Stagione();
                        $stagioneNew->setIdStagione($stagione->getIdStagione());
                        $stagioneNew->setEpisodi($stagione->getEpisodi());
                        $stagioneNew->setNumero($stagione->getNumero());
                        $stagioneNew->setInCorso($data['serie_tv_in_corso']);

                        //update stagione
                        $stagioneService->updateStagione($stagioneNew, $serieTvObjNew->getIdSerieTv());
                        array_push($array_stagione_new, $stagioneNew);
                    }
                }
            }

            // $array_stagione_old_id
            foreach ($serieTvObjOriginal->getStagioni() as $ks => $stagione) {
                array_push($array_stagione_old_id, $stagione->getIdStagione());
            }
            // array di id da inserire
            $array_stagione_new_insert = array_diff($array_stagione_new_id, $array_stagione_old_id);
            $array_appoggio = array();
            $array_appoggio2 = array();
            for ($i = 0; count($data['stagioni_episodi']['id_stagione']) > $i; $i++) {
                if ($data['stagioni_episodi']['id_stagione'][$i] === '') {
                    array_push($array_appoggio, $data['stagioni_episodi']['numero_stagione'][$i]);
                } else {
                    array_push($array_appoggio2, $data['stagioni_episodi']['numero_stagione'][$i]);
                }
            }
            $array_staggioni_nomi = array_unique($array_appoggio);
            $array_staggioni_nomi2 = array_unique($array_appoggio2);
            $array_staggioni_nomi_insert = array_diff($array_staggioni_nomi, $array_staggioni_nomi2);

            foreach ($array_staggioni_nomi_insert as $k => $nomeStagione) {
                $stagioneObj = new Stagione();
                $stagioneObj->setNumero($nomeStagione);
                $stagioneObj->setInCorso($data['serie_tv_in_corso']);
                // salva sul db la stagione
                $res = $stagioneService->insertStagione($stagioneObj, $serieTvObjNew->getIdSerieTv());
                $stagioneObj->setIdStagione($res);
                array_push($array_stagione_new, $stagioneObj);
            }

            // array di id da cancellare
            $array_stagione_delete = array_diff($array_stagione_old_id, $array_stagione_new_id);

            foreach ($array_stagione_delete as $k => $idStagione) {
                // delete stagione ma prima devo eliminare gli episodi collegati alla stagione
                $array_episodi_collegati_alla_stagione = array();
                foreach ($serieTvObjOriginal->getStagioni() as $ks => $stagione) {
                    if ($stagione->getIdStagione() == $idStagione) array_push($array_episodi_collegati_alla_stagione, $stagione->getEpisodi());
                }
                // delete degli episodi
                foreach ($array_episodi_collegati_alla_stagione as $item) {
                    foreach ($item as $episodio) {
                        // delete episodio ricordarsi di cancellare le img dallo storage
                        deleteFile(public_path($episodio->getImg()->getDato()));
                        $episodioService->deleteEpisodioImmagineByIdEpi($episodio->getIdEpisodio());
                    }
                }
                // delete della stagione
                $episodioService->deleteEpisodiImmaginiStagioneByIdStagione($idStagione);
            }
            $serieTvObjNew->setStagioni($array_stagione_new);
        } else {
            // devo eliminare tutte le stagioni
            foreach ($serieTvObjOriginal->getStagioni() as $k => $stagione) {
                // delete stagione ma prima devo eliminare tutti gli episodi
                $array_episodi_collegati_alla_stagione = array();
                foreach ($serieTvObjOriginal->getStagioni() as $ks => $stagiones) {
                    if ($stagione->getIdStagione() == $stagiones->getIdStagione()) array_push($array_episodi_collegati_alla_stagione, $stagione->getEpisodi());
                }
                // delete degli episodi
                foreach ($array_episodi_collegati_alla_stagione as $item) {
                    foreach ($item as $episodio) {
                        // delete episodio ricordarsi di cancellare le img dallo storage
                        deleteFile(public_path($episodio->getImg()->getDato()));
                        $episodioService->deleteEpisodioImmagineByIdEpi($episodio->getIdEpisodio());
                    }
                }
                // delete della stagione
                $episodioService->deleteEpisodiImmaginiStagioneByIdStagione($stagione->getIdStagione());
            }
            $serieTvObjNew->setStagioni([]);
        }


        // EPISODI
        // ricordarsi che già ho effettuato le cancellazioni sopra nella sezione Stagioni
        $array_episodi_new = array();
        $array_episodi_new_id = array();
        $array_episodi_new_insert = array();
        $array_stagione_delete = array();
        $array_episodi_old_id = array();

        if (isset($data['stagioni_episodi']['id_episodio'])) {
            foreach ($data['stagioni_episodi']['id_episodio'] as $ke => $idEpisodio) {
                // $array_episodi_new_id
                array_push($array_episodi_new_id, $idEpisodio);
                foreach ($serieTvObjOriginal->getStagioni() as $ks => $stagione) {
                    foreach ($stagione->getEpisodi() as $k => $episodio) {

                        if ($episodio->getIdEpisodio() == $idEpisodio) {
                            $episodioNew = new Episodio();
                            $episodioNew->setIdEpisodio($episodio->getIdEpisodio());
                            $episodioNew->setNumero($episodio->getNumero());

                            if ($episodio->getTitolo() != $data['stagioni_episodi']['titolo'][$ke]) {
                                $episodioNew->setTitolo($data['stagioni_episodi']['titolo'][$ke]);
                            } else {
                                $episodioNew->setTitolo($episodio->getTitolo());
                            }

                            if ($episodio->getDescrizione() != $data['stagioni_episodi']['descrizione'][$ke]) {
                                $episodioNew->setDescrizione($data['stagioni_episodi']['descrizione'][$ke]);
                            } else {
                                $episodioNew->setDescrizione($episodio->getDescrizione());
                            }

                            if ($episodio->getDurata() != $data['stagioni_episodi']['dutata_puntata'][$ke]) {
                                $episodioNew->setDurata($data['stagioni_episodi']['dutata_puntata'][$ke]);
                            } else {
                                $episodioNew->setDurata($episodio->getDurata());
                            }

                            $episodioNew->setDataInOnda($episodio->getDataInOnda());

                            // controllo se devo cambiare l'immagine

                            $uploadedFile = $uploadedFiles['stagioni_episodi']['copertina'][$ke];
                            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                                $directory = public_path('img/serietv/');
                                deleteFile(public_path($episodio->getImg()->getDato()));
                                $filename = movefiles($directory . $film_path, $uploadedFile);
                                $imgCopertina = new Immagine();
                                $imgCopertina->setIdImmagione($episodio->getImg()->getIdImmagione());
                                $imgCopertina->setNome($uploadedFile->getClientFilename());
                                $imgCopertina->setDescrizione($data['stagioni_episodi']['descrizione'][$ke]);
                                $imgCopertina->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                                $imgCopertina->setTipo(5);

                                // salvo l'immagine
                                $imgService->updateImgEpisodio($imgCopertina, $episodio->getIdEpisodio());

                                $episodio->setImg($imgCopertina);
                            } else {
                                // non ha caricato la foto
                                $episodioNew->setImg($episodio->getImg());
                            }

                            // controllo se ha cambiato la stagione
                            if ($episodio->getNumeroStagione() != $data['stagioni_episodi']['numero_stagione'][$ke]) {
                                $episodioNew->setNumeroStagione($data['stagioni_episodi']['numero_stagione'][$ke]);
                                // eliminazione episodio dalla stagione
                                $episodioService->deleteEpisodioImmagineByIdEpi($episodioNew->getIdEpisodio());

                                // inserimento episodio nella stagione giusta
                                $res = $episodioService->insertEpisodio($episodioNew, $stagione->getIdStagione());
                                $episodioNew->setIdEpisodio($res);
                            } else {
                                $episodioNew->setNumeroStagione($episodio->getNumeroStagione());
                                $episodioService->updateEpisodio($episodioNew, $stagione->getIdStagione());
                            }

                            array_push($array_episodi_new, $episodioNew);

                        }
                    }
                }
            }

            // array_episodi_old_id
            foreach ($serieTvObjOriginal->getStagioni() as $ks => $stagione) {
                foreach ($stagione->getEpisodi() as $ke => $episodio) {
                    array_push($array_episodi_old_id, strval($episodio->getIdEpisodio()));
                }
            }

            // array di id da inserire
            $array_episodi_new_insert = array_diff($array_episodi_new_id, $array_episodi_old_id);

            // salvo i nuovi episodi
            foreach ($serieTvObjNew->getStagioni() as $ks => $stagione) {
                foreach ($data['stagioni_episodi']['id_episodio'] as $k => $id_episodio) {
                    if ($stagione->getNumero() == $data['stagioni_episodi']['numero_stagione'][$k]) {
                        if ($id_episodio == '') {
                            $episodioNew = new Episodio();
                            $episodioNew->setNumero(random_int(1, 200));
                            $episodioNew->setTitolo($data['stagioni_episodi']['titolo'][$k]);
                            $episodioNew->setDescrizione($data['stagioni_episodi']['descrizione'][$k]);
                            $episodioNew->setDurata($data['stagioni_episodi']['dutata_puntata'][$k]);
                            $episodioNew->setDataInOnda(date('Y-m-d H:i:s'));
                            $episodioNew->setNumeroStagione($data['stagioni_episodi']['numero_stagione'][$k]);

                            //salvo episodio
                            $res = $episodioService->insertEpisodio($episodioNew, $stagione->getIdStagione());

                            $episodioNew->setIdEpisodio($res);
                            // salvo un eventuale img

                            $uploadedFile = $uploadedFiles['stagioni_episodi']['copertina'][$k];
                            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                                $directory = public_path('img/serietv/');
                                $filename = movefiles($directory . $film_path, $uploadedFile);
                                $imgCopertina = new Immagine();
                                $imgCopertina->setNome($uploadedFile->getClientFilename());
                                $imgCopertina->setDescrizione($data['stagioni_episodi']['descrizione'][$k]);
                                $imgCopertina->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                                $imgCopertina->setTipo(5);

                                // salvo l'immagine
                                $res = $imgService->insertImgEpisodio($imgCopertina, $episodioNew->getIdEpisodio());
                                $imgCopertina->setIdImmagione($res);
                                $episodioNew->setImg($imgCopertina);
                            }
                            array_push($array_episodi_new, $episodioNew);
                        }
                    }
                }
            }

            // array di id da cancellare
            $array_stagione_delete = array_diff($array_episodi_old_id, $array_episodi_new_id);
            foreach ($array_stagione_delete as $kar => $idEpisodioDel) {
                foreach ($serieTvObjNew->getStagioni() as $ks => $stagione) {
                    foreach ($stagione->getEpisodi() as $ke => $episodio) {
                        if ($idEpisodioDel == $episodio->getIdEpisodio()) {
                            // delete episodio ricordarsi di cancellare le img dallo storage
                            deleteFile(public_path($episodio->getImg()->getDato()));
                            $episodioService->deleteEpisodioImmagineByIdEpi($episodio->getIdEpisodio());
                        }
                    }
                }
            }

            // modifico i numero episodio
            $serieDimo = self::getSerieTvById($serieTvObjNew->getIdSerieTv());
            foreach ($serieDimo->getStagioni() as $ks => $stagione) {
                $array_episodi_new = array();
                $numero_episodio = 1;
                foreach ($stagione->getEpisodi() as $ke => $episodio) {
                    $episodio->setNumero($numero_episodio);
                    $numero_episodio++;
                    // aggiono epi
                    $episodioService->updateEpisodio($episodio, $stagione->getIdStagione());
                    array_push($array_episodi_new, $episodio);
                }
                $stagione->setEpisodi($array_episodi_new);
            }
        }


        // MEDIA

        $uploadedFile = $uploadedFiles['immagine_copertina'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $directory = public_path('img/serietv/');
            deleteFile(public_path($serieTvObjOriginal->getImgCopertina()->getDato()));
            $filename = movefiles($directory . $film_path, $uploadedFile);
            $img = $serieTvObjOriginal->getImgCopertina();
            $img->setNome($uploadedFile->getClientFilename());
            $img->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);

            //update img
            $imgService->updateImgSerieTv($img, $serieTvObjNew->getIdSerieTv());
            $serieTvObjNew->setImgCopertina($img);
        } else {
            $serieTvObjNew->setImgCopertina($serieTvObjOriginal->getImgCopertina());
        }

        // aggiorno i video
        $array_new_video = array();
        $array_new_video_id = array();
        $array_old_video_id = array();
        if (isset($data['media']['id_video'])) {
            foreach ($data['media']['id_video'] as $kp => $idVideo) {
                foreach ($serieTvObjOriginal->getVideos() as $k => $videoOriginal) {
                    if ($videoOriginal->getIdVideo() == $idVideo) {
                        $video = new Video();
                        $uploadedFile = $uploadedFiles['media']['video'][$kp];

                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directoryVideo = public_path('video/serietv/');
                            deleteFile(public_path($videoOriginal->getDato()));
                            $filenameVideo = movefiles($directoryVideo . $film_path, $uploadedFile);
                            $video->setNome($uploadedFile->getClientFilename());
                            $video->setDescrizione($data['media']['descrizione'][$kp]);
                            $video->setDato("video/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filenameVideo);
                            // aggiorno il video
                            $video->setIdVideo($videoOriginal->getIdVideo());
                            $videoService->updateVideo($video);
                        } else {
                            // il video è quello vecchio
                            $video->setIdVideo($videoOriginal->getIdVideo());
                            $video->setNome($videoOriginal->getNome());
                            $video->setImmagine($videoOriginal->getImmagine());
                            $video->setDescrizione($videoOriginal->getDescrizione());
                            $video->setDato($videoOriginal->getDato());

                            // controllo se devo caricare una descrizione modificata
                            if ($video->getDescrizione() != $data['media']['descrizione'][$kp]) {
                                $video->setDescrizione($data['media']['descrizione'][$kp]);

                                //upload modifica video
                                $videoService->updateVideo($video);
                            }
                        }

                        $imgCopertina = new Immagine();
                        // controllo se ho caricato una copertina nuova per il video
                        if ($uploadedFiles['media']['cover'][$kp]->getError() === UPLOAD_ERR_OK) {
                            $directoryCover = public_path('img/serietv/');
                            deleteFile(public_path($videoOriginal->getImmagine()->getDato()));
                            $filename = movefiles($directoryCover . $film_path, $uploadedFiles['media']['cover'][$kp]);
                            $imgCopertina->setNome($uploadedFiles['media']['cover'][$kp]->getClientFilename());
                            $imgCopertina->setDescrizione($data['media']['descrizione'][$kp]);
                            $imgCopertina->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo(3);
                            $imgCopertina->setIdImmagione($videoOriginal->getImmagine()->getIdImmagione());

                            // update img di copertina
                            $imgService->updateImgVideo($imgCopertina, $video->getIdVideo());
                            $video->setImmagine($imgCopertina);

                        } else {
                            // il video ha la copertina vecchia
                            $video->setImmagine($videoOriginal->getImmagine());

                            // controllo se devo caricare una descrizione modificata
                            if ($video->getImmagine()->getDescrizione() != $data['media']['descrizione'][$kp]) {
                                $video->getImmagine()->setDescrizione($data['media']['descrizione'][$kp]);

                                //upload modifica img del video
                                $imgService->updateImgVideo($video->getImmagine(), $video->getIdVideo());
                                $videoService->updateVideo($video);
                            }
                        }
                        array_push($array_new_video, $video);
                    }
                }
            }

            // array_old_id
            foreach ($serieTvObjOriginal->getVideos() as $k => $video) {
                array_push($array_old_video_id, strval($video->getIdVideo()));
            }
            // array_new_id
            foreach ($data['media']['id_video'] as $kp => $idVideo) {
                array_push($array_new_video_id, $idVideo);
            }

            // array di id da cancellare
            $array_da_cancellare_video = array_diff($array_old_video_id, $array_new_video_id);

            foreach ($array_da_cancellare_video as $k => $idVideo) {
                foreach ($serieTvObjOriginal->getVideos() as $kv => $video) {
                    if ($video->getIdVideo() == $idVideo) {
                        // cancella il video
                        deleteFile(public_path($video->getDato()));
                        deleteFile(public_path($video->getImmagine()->getDato()));
                        $videoService->deleteVideo($video);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_video = array_diff($array_new_video_id, $array_old_video_id);

            // insert nuovo video
            foreach ($array_da_inserire_video as $kArray => $id) {
                foreach ($data['media']['id_video'] as $k => $idVideo) {
                    if ($kArray === $k && $id === $idVideo) {
                        $uploadedFile = $uploadedFiles['media']['video'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            if ($uploadedFiles['media']['cover'][$k]->getError() === UPLOAD_ERR_OK) {
                                $directoryVideo = public_path('video/serietv/');
                                $filenameVideo = movefiles($directoryVideo . $film_path, $uploadedFile);
                                $video = new Video();
                                $video->setNome($uploadedFile->getClientFilename());
                                $video->setDescrizione($data['media']['descrizione'][$k]);
                                $video->setDato("video/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filenameVideo);
                                // salvo il video con id del film
                                $res = $videoService->insertVideoSerieTv($video, $serieTvObjOriginal->getIdSerieTv());
                                $video->setIdVideo($res);

                                $directoryCover = public_path('img/serietv/');
                                $filename = movefiles($directoryCover . $film_path, $uploadedFiles['media']['cover'][$k]);
                                $imgCopertina = new Immagine();
                                $imgCopertina->setNome($uploadedFiles['media']['cover'][$k]->getClientFilename());
                                $imgCopertina->setDescrizione($data['media']['descrizione'][$k]);
                                $imgCopertina->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                                $imgCopertina->setTipo(3);

                                // salvo immagine con id video
                                $res = $imgService->insertImgVideo($imgCopertina, $video->getIdVideo());
                                $imgCopertina->setIdImmagione($res);
                                $video->setImmagine($imgCopertina);
                                array_push($array_new_video, $video);
                            }
                        }
                    }
                }
            }
        } else {
            // non ci sono video quindi li cancello tutti
            foreach ($serieTvObjOriginal->getVideos() as $k => $video) {
                // cancella il video
                deleteFile(public_path($video->getDato()));
                deleteFile(public_path($video->getImmagine()->getDato()));
                $videoService->deleteVideo($video);
            }
        }
        $serieTvObjNew->setVideos($array_new_video);


        // immaggini
        $array_new_img = array();
        $array_new_img_id = array();
        $array_old_img_id = array();
        $array_da_cancellare_img = array();
        $array_da_inserire_img = array();
        if (isset($data['immagini']['Id_immagini'])) {
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImg) {
                foreach ($serieTvObjOriginal->getImgs() as $k => $imgOriginal) {
                    if ($imgOriginal->getIdImmagione() == $idImg) {
                        $img = new Immagine();
                        $uploadedFile = $uploadedFiles['immagini']['file'][$kp];
                        // controllo se ho caricato un file video
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/serietv/');
                            deleteFile(public_path($imgOriginal->getDato()));
                            $filename = movefiles("$directory" . $film_path, $uploadedFile);
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($uploadedFile->getClientFilename());
                            $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                            $img->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $img->setTipo($data['immagini']['scheda'][$kp]);

                            // aggiorno l'img
                            $imgService->updateImgSerieTv($img, $serieTvObjNew->getIdSerieTv());

                        } else {
                            // la foto è quella vecchia
                            $img->setIdImmagione($imgOriginal->getIdImmagione());
                            $img->setNome($imgOriginal->getNome());
                            $img->setDato($imgOriginal->getDato());
                            $img->setDescrizione($imgOriginal->getDescrizione());
                            $img->setTipo($imgOriginal->getTipo());

                            //controllo se la descrione o il tipo sono cambiati
                            if ($imgOriginal->getDescrizione() != $data['immagini']['descrizione'][$kp] || $imgOriginal->getTipo() != $data['immagini']['scheda'][$kp]) {
                                $img->setDescrizione($data['immagini']['descrizione'][$kp]);
                                $img->setTipo($data['immagini']['scheda'][$kp]);

                                // upload modifica
                                $imgService->updateImgSerieTv($img, $serieTvObjNew->getIdSerieTv());
                            }
                        }
                        array_push($array_new_img, $img);
                    }
                }
            }

            // array_old_id
            foreach ($serieTvObjOriginal->getImgs() as $k => $immagine) {
                array_push($array_old_img_id, strval($immagine->getIdImmagione()));
            }
            // array_new_id
            foreach ($data['immagini']['Id_immagini'] as $kp => $idImmagine) {
                array_push($array_new_img_id, $idImmagine);
            }

            // array di id da cancellare
            $array_da_cancellare_img = array_diff($array_old_img_id, $array_new_img_id);

            foreach ($array_da_cancellare_img as $k => $idImg) {
                foreach ($serieTvObjOriginal->getImgs() as $ki => $immagine) {
                    if ($immagine->getIdImmagione() == $idImg) {
                        // cancella le img
                        deleteFile(public_path($immagine->getDato()));
                        $imgService->deleteImg($immagine);
                    }
                }
            }

            // array di id da inserire
            $array_da_inserire_img = array_diff($array_new_img_id, $array_old_img_id);

            foreach ($array_da_inserire_img as $kArray => $id) {
                foreach ($data['immagini']['Id_immagini'] as $k => $idImg) {
                    if ($kArray === $k && $id === $idImg) {
                        $uploadedFile = $uploadedFiles['immagini']['file'][$k];
                        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                            $directory = public_path('img/serietv/');
                            $filename = movefiles("$directory" . $film_path, $uploadedFile);
                            $imgCopertina = new Immagine();
                            $imgCopertina->setNome($uploadedFile->getClientFilename());
                            $imgCopertina->setDescrizione($data['immagini']['descrizione'][$k]);
                            $imgCopertina->setDato("img/serietv/" . $film_path . DIRECTORY_SEPARATOR . $filename);
                            $imgCopertina->setTipo($data['immagini']['scheda'][$k]);
                            // salvo le immagini con id del film
                            $imgService->insertImgSerieTv($imgCopertina, $serieTvObjNew->getIdSerieTv());
                            array_push($array_new_img, $imgCopertina);
                        }
                    }
                }
            }
        } else {
            //elimina tutte le immagini
            foreach ($serieTvObjOriginal->getImgs() as $k => $immagine) {
                deleteFile(public_path($immagine->getDato()));
                $imgService->deleteImg($immagine);
            }
        }
        $serieTvObjNew->setImgs($array_new_img);

        //dd($requestInput->all(), '$array_da_cancellare_img', $array_da_cancellare_img, $requestInput->getRequest()->getUploadedFiles(), $serieTvObjNew);
        session()->flash()->set("successModifySerieTv", "La SerieTv: " . $serieTvObjNew->getTitolo() . " è stata modificata con successo");
        return true;
    }

    public function getAllSerieTvParzialiPerListaPaginati($limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvParzialiList = $serieTvServiceImpl->getAllSerieTvPaginati($limit, $offset);
        $serieTvList = array();
        foreach ($serieTvParzialiList as $serieTv) {
            array_push($serieTvList, self::setSerieTvComponentParzialePerLista($serieTv, $serieTvServiceImpl));
        }
        return $serieTvList;
    }

    public function countAllSerieTvParziali()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countAllSerieTvParzialiPerLista();
    }

    public function getSerieTvInCorsoPerListaPaginato($limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmListParz = $serieTvServiceImpl->getSerieTvInCorsoPaginato($limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $key => $film) {
            array_push($filmList, self::setSerieTvComponentParzialePerLista($film, $serieTvServiceImpl));
        }
        return $filmList;
    }

    public function countSerieTvInCorso(): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvInCorso();
    }

    public function getSerieTvByAnnoPerListaPaginato(int $anno, $limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmListParz = $serieTvServiceImpl->getSerieTvByAnnoPaginato($anno . "/1/1", $anno . "/12/31", $limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $key => $film) {
            array_push($filmList, self::setSerieTvComponentParzialePerLista($film, $serieTvServiceImpl));
        }
        return $filmList;
    }

    public function countSerieTvByAnno(int $anno)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvByAnno($anno . "/1/1", $anno . "/12/31");
    }

    public function getSerieTvByNazionePerListaPaginato($codiceNazione, $limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmListParz = $serieTvServiceImpl->getSerieTvByNazionePaginato($codiceNazione, $limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $key => $film) {
            array_push($filmList, self::setSerieTvComponentParzialePerLista($film, $serieTvServiceImpl));
        }
        return $filmList;
    }

    public function countSerieTvByNazione($codiceNazione): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvByNazione($codiceNazione);
    }

    public function getSerieTvByPremioPerListaPaginato(int $idPremio, $limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmListParz = $serieTvServiceImpl->getSerieTvByPremioPaginato($idPremio, $limit, $offset);
        $filmList = array();
        foreach ($filmListParz as $k => $fi) {
            array_push($filmList, self::setSerieTvComponentParzialePerLista($fi, $serieTvServiceImpl));
        }
        return $filmList;
    }

    public function countSerieTvByPremio(int $idPremio)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvByPremio($idPremio);
    }

    public function getSerieTvInUscitaPaginato($limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $serieTvList = $serieTvServiceImpl->getSerieTvInUscitaPaginato($limit, $offset);
        foreach ($serieTvList as $k => $stv) {
            $serieTvList[$k] = self::setSerieTvComponentParzialePerLista($stv, $serieTvServiceImpl);
        }
        return $serieTvList;
    }

    public function countSerieTvInUscita(): int
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvInUscita();
    }

    /*
        public function getSerieTvDelMesePaginato($limit, $offset): array
        {
            $dataOggi = date("Y/m-d");
            $dataListA = explode("-", $dataOggi);
            $dataListB = explode("/", $dataListA[0]);
            $serieTvServiceImpl = new SerieTvServiceImplementation();
            $serieTvList = $serieTvServiceImpl->getSerieTvDelMesePaginato($dataListA[0] . '/01', $dataListB[0] . ('/' . ($dataListB[1] + 1)) . '/01', $limit, $offset);
            foreach ($serieTvList as $k => $stv) {
                $serieTvList[$k] = self::setSerieTvComponentParzialePerLista($stv, $serieTvServiceImpl);
            }
            return $serieTvList;
        }

        public function countSerieTvDelMese(): int
        {
            $dataOggi = date("Y/m-d");
            $dataListA = explode("-", $dataOggi);
            $dataListB = explode("/", $dataListA[0]);
            $serieTvServiceImpl = new SerieTvServiceImplementation($dataListA[0] . '/01', $dataListB[0] . ('/' . ($dataListB[1] + 1)) . '/01');
            return $serieTvServiceImpl->countSerieTvDelMese();
        }
    */
    public function getSerieTvByIdGenerePerListaPaginato(int $idGenre, $limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmListParz = $serieTvServiceImpl->getSerieTvByIdGenerePaginato($idGenre, $limit, $offset);
        $filmList = array();
        if (empty($filmListParz))
            return [];
        foreach ($filmListParz as $film) {
            array_push($filmList, self::setSerieTvComponentParzialePerLista($film, $serieTvServiceImpl));
        }
        return $filmList;
    }

    public function countSerieTvByIdGenere(int $idGenre)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvByIdGenere($idGenre);
    }

    public function getSerieTvByDistributorePerListaPaginato(int $idDistributore, $limit, $offset): array
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmList = $serieTvServiceImpl->getSerieTvByDistributorePaginato($idDistributore, $limit, $offset);
        foreach ($filmList as $k => $fi) {
            $filmList[$k] = self::setSerieTvComponentParzialePerLista($fi, $serieTvServiceImpl);
        }
        return $filmList;
    }

    public function countSerieTvByDistributore(int $idDistributore)
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTvByDistributore($idDistributore);
    }

    public function maggiorediCinqueVoti(SerieTv $serieTv): bool
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->maggiorediCinqueVoti($serieTv);
    }

    public function getSerieTvByIdNomination(int $idNomination): SerieTv
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->getSerieTvByIdNomination($idNomination);
    }

    public function countSerieTv()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        return $serieTvServiceImpl->countSerieTv();
    }

    public function getSerieTvByTitoloOrTitoloOrigSearch(string $search): array
    {
        $serieTvServiceImplementation = new SerieTvServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();

        $resultSerieTv = $serieTvServiceImplementation->getSerieTvByTitoloOrTitoloOrigSearch($search);
        if (!empty($resultSerieTv)) {
            $objList = array();
            foreach ($resultSerieTv as $key => $serieTv) {
                $obj = new \stdClass();
                $obj->titolo = $serieTv->getTitolo();
                $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('serietv', $serieTv->getIdSerieTv(), 2);
                if (!empty($imgCopertina)) {
                    $obj->datoImg = asset($imgCopertina[0]->getDato());
                } else {
                    $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
                }
                $obj->annoUscita = substr($serieTv->getDataUscita(), 0, 4);
                $obj->tipo = 'SERIE TV';
                $nomeeCognomeRegistiList = array();
                foreach ($filmmakerService->getFilmmakerBySerieTvAndTipologiaRuolo($serieTv, 2) as $regista) {
                    array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
                }
                $obj->registi = $nomeeCognomeRegistiList;
                $obj->link = asset('serietv/' . $serieTv->getIdSerieTv());
                array_push($objList, $obj);
            }
            return $objList;
        }
        return [];
    }

    public function getSerieTvPiuVotata()
    {
        $serieTvServiceImpl = new SerieTvServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();
        $serieTvList = $serieTvServiceImpl->getAllSerieTv();
        $mediaVotoAndIndice = array();
        foreach ($serieTvList as $key => $serieTv) {
            $mediaVotoAndIndice[$key] = $this->getMediaVotiSerieTv($serieTv);
        }
        arsort($mediaVotoAndIndice);
        $serieTvPiuVotateList = array();
        foreach ($mediaVotoAndIndice as $key => $value) {
            $serieTvList[$key]->mediaVoto = $value;
            array_push($serieTvPiuVotateList, $serieTvList[$key]);
        }
        $obj = new \stdClass();
        if (!empty($serieTvPiuVotateList)) {
            $obj->titolo = $serieTvPiuVotateList[0]->getTitolo();
            $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('film', $serieTvPiuVotateList[0]->getIdSerieTv(), 2);
            if (!empty($imgCopertina)) {
                $obj->datoImg = asset($imgCopertina[0]->getDato());
            } else {
                $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
            }
            $obj->annoUscita = substr($serieTvPiuVotateList[0]->getDataUscita(), 0, 4);
            $obj->tipo = 'SERIE TV';
            $nomeeCognomeRegistiList = array();
            foreach ($filmmakerService->getFilmmakerBySerieTvAndTipologiaRuolo($serieTvPiuVotateList[0], 2) as $regista) {
                array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
            }
            $obj->registi = $nomeeCognomeRegistiList;
            $obj->link = asset('serietv/' . $serieTvPiuVotateList[0]->getIdSerieTv());
        }
        return $obj;
    }

    public function getProssimaSerieTvInUscita()
    {
        $serieTvServiceImplementation = new SerieTvServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $immagineService = new ImmagineService();
        $serieTv = $serieTvServiceImplementation->getProssimoFilmInUscita();
        $obj = new \stdClass();
        if ($serieTv->getIdSerieTv() !== null) {
            $obj->titolo = $serieTv->getTitolo();
            $imgCopertina = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('serietv', $serieTv->getIdSerieTv(), 2);
            if (!empty($imgCopertina)) {
                $obj->datoImg = asset($imgCopertina[0]->getDato());
            } else {
                $obj->datoImg = asset('img/filmmakers/avatar/star_big.jpg');
            }
            $obj->annoUscita = substr($serieTv->getDataUscita(), 0, 4);
            $obj->tipo = 'SERIE TV';
            $nomeeCognomeRegistiList = array();
            foreach ($filmmakerService->getFilmmakerBySerieTvAndTipologiaRuolo($serieTv, 2) as $regista) {
                array_push($nomeeCognomeRegistiList, $regista->getNome() . ' ' . $regista->getCognome());
            }
            $obj->registi = $nomeeCognomeRegistiList;
            $obj->link = asset('serietv/' . $serieTv->getIdSerieTv());
        }
        return $obj;
    }
}