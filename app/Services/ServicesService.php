<?php


namespace App\Services;


use App\Model\Groups;
use App\Model\Services;
use App\Model\User;
use App\Services\Implementation\ServicesServiceImplementation;

class ServicesService
{

    /**
     * ServicesService constructor.
     */
    public function __construct()
    {
    }

    public function getService(string $name): Services{
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->getServiceByName($name);
    }

    public function getServiceById(int $idService): Services{
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->getServiceById($idService);
    }

    public function getAllServices(){
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->getAllServices();
    }

    public function updateService(Services $services) {
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->updateService($services);
    }

    public function getServiceByUser(User $user): array {
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->getServiceByUser($user);
    }
    public function ifPermissByNomeServizioAndUser(string $nomeServizio, User $user): bool {
        $listaServizi = self::getServiceByUser($user);
        $result = false;
        foreach ($listaServizi as $servizio) {
            if ($servizio->getNome() == $nomeServizio) {
                $result = true;
                break;
            }
        }
        if (!$result) {
            session()->flash()->set('errorPermission', 'Non hai il permesso di eseguire questa operazione!');
        }

        return $result;
    }

    public function ifCanByNomeServizioAndUser(string $nomeServizio, User $user): bool {
        $listaServizi = self::getServiceByUser($user);
        $result = false;
        foreach ($listaServizi as $servizio) {
            if ($servizio->getNome() == $nomeServizio) {
                $result = true;
                break;
            }
        }
        return $result;
    }


    public function getServicesByGroup(Groups $group): array {
        $serviceServiceImplement = new ServicesServiceImplementation();
        return $serviceServiceImplement->getServicesByGroup($group);
    }
}