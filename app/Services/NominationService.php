<?php


namespace App\Services;


use App\Model\Film;
use App\Model\Filmmaker;
use App\Model\Nomination;
use App\Model\SerieTv;
use App\Services\Implementation\NominationServiceImplementation;

class NominationService
{

    /**
     * NominationService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Nomination $nomination
     * @return mixed
     */
    public function insertNomination(Nomination $nomination) {
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->insertNomination($nomination);
    }

    /**
     * @param  $idNomination
     * @param  $idFilm
     * @return mixed
     */
    public function insertNominationFilm( $idNomination, $idFilm) {
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->insertNominationFilm($idNomination, $idFilm);
    }

    /**
     * @param  $idNomination
     * @param  $idSerieTv
     * @return mixed
     */
    public function insertNominationSerieTv($idNomination, $idSerieTv) {
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->insertNominationSerieTv($idNomination, $idSerieTv);
    }

    /**
     * @param  $idNomination
     * @param  $idFilmmaker
     * @return mixed
     */
    public function insertNominationFilmmaker($idNomination, $idFilmmaker) {
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->insertNominationFilmmaker($idNomination, $idFilmmaker);
    }

    public function getNominationByIdFilm($idFilm): array {
        $nominationServiceImpl = new NominationServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $nominationList = $nominationServiceImpl->getNominationByIdFilm($idFilm);
        //dd($nominationList[3]);
        foreach ($nominationList as $key=>$nomination) {
            $nominationList[$key]->setFilmMaker($filmmakerService->getFilmMakerByIdNomination($nomination->getIdNomination()));
        }
        return $nominationList;
    }

    public function getNominationByIdSerieTv($idSerieTv): array {
        $nominationServiceImpl = new NominationServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $nominationList = $nominationServiceImpl->getNominationByIdSerieTv($idSerieTv);
        foreach ($nominationList as $key=>$nomination) {
            $nominationList[$key]->setFilmMaker($filmmakerService->getFilmMakerByIdNomination($nomination->getIdNomination()));
        }
        return $nominationList;
    }

    /**
     * Metodo da usare per avere il filmmaker con le sue nomination
     *
     * @param $idFilmmaker
     * @param bool $notRicerca
     * @return array struttura oggetto ritornato: lista di Nomination del filmmaker con all'interno di ogniuna l'oggetto Filmmaker
     */
    public function getNominationByIdFilmmaker($idFilmmaker, bool $notRicerca = true ): array {
        $nominationServiceImpl = new NominationServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $nominationList = $nominationServiceImpl->getNominationByIdFilmmaker($idFilmmaker);
        if($notRicerca){
            foreach ($nominationList as $key=>$nomination) {
                $nominationList[$key]->setFilmMaker($filmmakerService->getFilmMakerByIdNomination($nomination->getIdNomination()));
            }
        }
        return $nominationList;
    }

    public function updateNomination(Nomination $nomination, Nomination $nominationOriginal) {
        $nominationServiceImpl = new NominationServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $res = $filmmakerService->deleteFilmMakerNominationByIdFilmmakerAndIdNomination($nominationOriginal->getFilmMaker()->getIdFilmMaker(), $nomination->getIdNomination());
        $res2 = self::insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());
        return $nominationServiceImpl->updateNomination($nomination, $res, $res2, $nomination->getFilmMaker()->getIdFilmMaker(), $nomination->getIdNomination());
    }

    /*public function updateNominationS(Nomination $nomination) {
        $nominationServiceImpl = new NominationServiceImplementation();
        $categoriaService = new CategoriaPremioService();
        $filmmakerService = new FilmMakerService();
        $categoriaPremio = $categoriaService->getCategoriaPremioByIdTipologiaPremioAndByIdPremio($nomination->getCategoriaPremio()->getTipologia()->getidTipologiaPremio(), $nomination->getCategoriaPremio()->getPremio()->getIdPremio());
        $nomination->setCategoriaPremio($categoriaPremio);
        $filmmakerService->deleteFilmMakerNominationByIdFilmmakerAndIdNomination($nomination->getFilmMaker()->getIdFilmMaker(), $nomination->getIdNomination());
        self::insertNominationFilmmaker($nomination->getIdNomination(), $nomination->getFilmMaker()->getIdFilmMaker());
        return $nominationServiceImpl->updateNomination($nomination);
    }*/

    public function deleteNomination(Nomination $nomination) {
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->deleteNomination($nomination);
    }

    public function deleteNominationFilm(Nomination $nomination){
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->deleteNominationFilmByidNomination($nomination->getIdNomination());
    }


    public function deleteNominationSerieTv(Nomination $nomination){
        $nominationServiceImpl = new NominationServiceImplementation();
        return $nominationServiceImpl->deleteNominationSerieTvByNomination($nomination->getIdNomination());
    }

    public function deleteNominationFilmmaker(Nomination $nomination){
        $filmmakerService = new FilmMakerService();
        return $filmmakerService->deleteFilmMakerNominationByIdFilmmakerAndIdNomination($nomination->getFilmMaker()->getIdFilmMaker(),$nomination->getIdNomination());
    }

    public function deleteNominationFilmService(Nomination $nomination) {
        self::deleteNominationFilm($nomination);
        self::deleteNominationFilmmaker($nomination);
        self::deleteNomination($nomination);
    }

    public function deleteNominationSerieTvService(Nomination $nomination) {
        self::deleteNominationSerieTv($nomination);
        self::deleteNominationFilmmaker($nomination);
        self::deleteNomination($nomination);
    }

    public function getAllNominationFormListFilmByFilmmaker(array $listFilm, Filmmaker $filmmaker){
        $listNomination = array();
        foreach ($listFilm as $kf => $film){
            foreach ($film->getNominations() as $kn => $nomination){
                if($nomination->getFilmMaker()->getIdFilmMaker() === $filmmaker->getIdFilmMaker()){
                    array_push($listNomination, $nomination);
                }
            }
        }
        return $listNomination;
    }

    public function getNominationByNomePremioAndAnno(int $idPremio, string $anno): array {
        $nominationServiceImpl = new NominationServiceImplementation();
        $filmmakerService = new FilmMakerService();
        $serieTvService = new SerieTvService();
        $filmService = new FilmService();
        $nominationList = $nominationServiceImpl->getNominationByNomePremioAndAnno($idPremio, $anno);
        foreach ($nominationList as $key=>$nomination) {
            $filmmaker = $filmmakerService->getFilmMakerByIdNominationParziale($nomination->getIdNomination());
            if ($filmmaker !== new Filmmaker()) {
                $nomination->setFilmMaker($filmmaker);
            }
            $film = $filmService->getFilmByIdNomination($nomination->getIdNomination());
            if ($film !== new Film()) {
                $nomination->nomeOpera = $film->getTitolo();
                $nomination->tipoOpera = 'film';
                $nomination->idOpera = $film->getIdFilm();
            } else {
                $serieTv = $serieTvService->getserieTvByIdNomination($nomination->getIdNomination());
                if ($serieTv !== new SerieTv()) {
                    $nomination->nomeOpera = $serieTv->getTitolo();
                    $nomination->tipoOpera = 'serietv';
                    $nomination->idOpera = $serieTv->getIdSerieTv();
                }
            }

        }
        return $nominationList;
    }
}