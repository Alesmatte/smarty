<?php


namespace App\Services\Implementation;

use DB;
use App\Model\Stagione;
use Slim\Exception\HttpBadRequestException;

class StagioneServiceImplementation
{
    /**
     * StagioneServiceImplementation constructor.
     */

    public function __construct()
    {
    }
    private function setStagioneObject(Stagione $stagione, $result): Stagione
    {
        $stagione->setIdStagione($result->idStagione);
        $stagione->setNumero($result->numero);
        $stagione->setInCorso($result->inCorso);
        return $stagione;
    }

    private function setStagioneObjectList($stagioneList, $result) {
        foreach ($result as $obj) {
            $stagione = new Stagione();
            $stagione = $this->setStagioneObject($stagione, $obj);
            array_push($stagioneList, $stagione);
        }
        return $stagioneList;
    }

    public function getStagioneByID(int $idStagione): Stagione {
        $stagione = new Stagione();
        $result = DB::select("SELECT * FROM stagiones where idStagione = ?", [$idStagione]);
        throw_when(empty($result), "risultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setStagioneObject($stagione, $result[0]);
    }

    public function getAllStagione(){
        $result = DB::select("SELECT * FROM stagiones");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setStagioneObjectList([], $result);
    }

    public function insertStagione(Stagione $stagione, int $idSerieTv) {
        $result = DB::insert('INSERT INTO stagiones ( numero, inCorso, serieTv) VALUE (?, ?, ?)',
            [
                $stagione->getNumero(),
                $stagione->getInCorso(),
                $idSerieTv
            ]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateStagione(Stagione $stagione, int $idSerieTv) {
        $result = DB::update('UPDATE stagiones SET numero=?, inCorso=?, serieTv=? WHERE idStagione = ?',
            [
                $stagione->getNumero(),
                $stagione->getInCorso(),
                $idSerieTv,
                $stagione->getIdStagione()
            ]);
        return $result;
    }

    public function deleteStagione($idStagione) {
        $result = DB::delete('DELETE FROM stagiones WHERE idStagione=?', [$idStagione]);
        return $result;
    }

    public function getStagioneByIdSerieTv($idSerieTv){
        $result = DB::select('SELECT * FROM stagiones WHERE serieTv=?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setStagioneObjectList([], $result);
    }

    private function setIdStagione(Stagione $stagione, $result) {
        $stagione->setIdStagione($result->idStagione);
        return $stagione;
    }

    public function getIdStagioneByIdSerieTvNumStagione(int $numStagione, int $idSerieTv)
    {
        $stagione = new Stagione();
        $result = DB::select('SELECT idStagione FROM stagiones WHERE numero=? AND serieTv=?', [$numStagione, $idSerieTv]);
        throw_when(empty($result), "risultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setIdStagione($stagione, $result[0]);
    }

    public function getStagioneByIncorsoIdSerieTv(int $idSerieTv){
        $stagione = new Stagione();
        $result = DB::select("SELECT * FROM stagiones where inCorso=1 AND serieTv = ? ORDER BY numero DESC LIMIT 1", [$idSerieTv]);
        throw_when(empty($result), "risultato query vuoto", HttpBadRequestException::class);
        return self::setStagioneObject($stagione, $result[0]);
    }

}