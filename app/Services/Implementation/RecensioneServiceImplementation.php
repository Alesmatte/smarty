<?php


namespace App\Services\Implementation;

use App\Model\Film;
use App\Services\FilmService;
use App\Services\SerieTvService;
use DB;
use App\Model\Recensione;
use App\Model\User;
use Slim\Exception\HttpBadRequestException;

class RecensioneServiceImplementation
{

    /**
     * RecensioneServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setRecensioneObject(Recensione $recensione, $result): Recensione
    {
        $recensione->setIdRecensione($result->idRecensione);
        $recensione->setTitolo($result->titolo);
        $recensione->setContenutoRecensione($result->contenutoRecensione);
        $recensione->setVoto($result->voto);
        $recensione->setDataRecensione($result->createDate);

        if (isset($result->idUser)) {
            $user = new User();
            $user->setIdUser($result->idUser);
            $user->setNome($result->nome);
            $user->setCognome($result->cognome);
            $user->setEmail($result->email);
            $user->setPassword($result->password);
            $user->setToken($result->token);
            $user->setDataNascita($result->dataNascita);
            $user->setTipologiaUtenza($result->tipologiaUtenza);
            if (isset($result->codiceAlbo)) {
                $user->setCodiceAlbo($result->codiceAlbo);
            }
            if (isset($result->nomeAzienda)) {
                $user->setNomeAzienda($result->nomeAzienda);
            }
            $user->setBiografia($result->biografia);
            $user->setStato($result->stato);
            $recensione->setUtente($user);
        }

        return $recensione;
    }

    private function setRecensioneObjectList($recensioneList, $result): array
    {
        foreach ($result as $obj) {
            $recensione = new Recensione();
            $recensione = $this->setRecensioneObject($recensione, $obj);
            array_push($recensioneList, $recensione);
        }
        return $recensioneList;
    }

    public function getRecensioneById(int $idRecensione): Recensione
    {
        $recensione = new Recensione();
        $result = DB::select("SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and r.idRecensione = ?", [$idRecensione]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result) > 1, "errore valori multipli");
        return self::setRecensioneObject($recensione, $result[0]);
    }

    public function getAllRecensioni(): array
    {
        $result = DB::select("SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser");
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniByIdUser(int $idUser): array
    {
        $result = DB::select('SELECT 
                                    idRecensione, titolo, contenutoRecensione, voto, createDate, updateDate, serieTv, user, film
                              FROM recensiones  where user = ? ORDER BY createDate', [$idUser]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniPersonalByUserPaginato(int $idUser, $limit, $offset): array
    {
        $result = DB::select('SELECT 
                                    idRecensione, titolo, contenutoRecensione, voto, createDate, updateDate, serieTv, user, film
                              FROM recensiones  where user = ? ORDER BY createDate LIMIT ? OFFSET ?', [$idUser, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function countRecensioniPersonalByUser(int $idUser): int
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT * FROM recensiones  where user = ?) as r', [$idUser]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function getRecensioniByIdFilm(int $idFilm, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and r.film = ? ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idFilm, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniByIdFilmNormale(int $idFilm, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and u.tipologiaUtenza = 0 and r.film = ? ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idFilm, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniByIdFilmCritico(int $idFilm, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and u.tipologiaUtenza = 1 and r.film = ? ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idFilm, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getLastRecensioneByIdFilm(int $idFilm): Recensione
    {
        $recensione = new Recensione();
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and r.film = ? ORDER BY r.idRecensione DESC LIMIT 1', [$idFilm]);
        if (empty($result)) {
            return $recensione;
        }
        return self::setRecensioneObject($recensione, $result[0]);
    }

    public function getLastRecensioni($limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato FROM recensiones r, users u WHERE r.user = u.idUser ORDER BY r.createDate DESC LIMIT ? OFFSET ?', [$limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getLastRecensioniBySearchPaginato(string $search, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT * FROM recensiones WHERE UPPER(recensiones.titolo) LIKE UPPER(?) ORDER BY createDate DESC LIMIT ? OFFSET ?', [$search, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function countLastRecensioniBySearch(string $search): int
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT * FROM recensiones WHERE UPPER(recensiones.titolo) LIKE UPPER(?)) as r', [$search]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function countGetRecensioniByIdFilm(int $idFilm)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT * FROM recensiones  where film = ?) as r', [$idFilm]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function countGetRecensioniPrTipoUtenteByIdFilm(int $idFilm, int $tipoUtente)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT r.* FROM recensiones r, users u WHERE r.user = u.idUser AND r.film = ? AND u.tipologiaUtenza = ?) as r', [$idFilm, $tipoUtente]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function getRecensioniByIdSerieTv(int $idSerieTv, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and r.serieTv = ? ORDER BY idRecensione LIMIT ? OFFSET ?', [$idSerieTv, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniByIdSerieTvNormale(int $idSerieTv, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and u.tipologiaUtenza = 0 and r.serieTv = ? ORDER BY idRecensione LIMIT ? OFFSET ?', [$idSerieTv, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniByIdSerieTvCritico(int $idSerieTv, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and u.tipologiaUtenza = 1 and r.serieTv = ? ORDER BY idRecensione LIMIT ? OFFSET ?', [$idSerieTv, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getLastRecensioneByIdSerieTv(int $idSerieTv): Recensione
    {
        $recensione = new Recensione();
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film,
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato
                              FROM recensiones r, users u where r.user = u.idUser and r.serieTv = ? ORDER BY idRecensione DESC LIMIT 1', [$idSerieTv]);
        return self::setRecensioneObject($recensione, $result[0]);
    }

    public function countGetRecensioniByIdSerieTv(int $idSerieTv)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT * FROM recensiones  where serieTv = ?) as r', [$idSerieTv]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function countGetRecensioniPerTipoUserByIdSerieTv(int $idSerieTv, int $tipoUser)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT r.* FROM recensiones r, users u WHERE r.user = u.idUser AND serieTv = ? AND u.tipologiaUtenza = ?) as r', [$idSerieTv, $tipoUser]);

        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function getRecensioniBySearchIdFilm(string $search, int $idFilm, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film, 
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato 
                                FROM 
                                     recensiones r, users u where r.user = u.idUser and r.film = ? and (UPPER(r.titolo) LIKE UPPER(?) ||  UPPER(u.nome) LIKE UPPER(?) || UPPER(u.cognome) LIKE UPPER(?)) ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idFilm, $search, $search, $search, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getRecensioniPersonalSearchByUser(string $search, int $idUser, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film
                                FROM 
                                     recensiones r where r.user = ? and UPPER(r.titolo) LIKE UPPER(?) ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idUser, $search, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function countRecensioniPersonalSearchByUser(int $idUser, string $search): int
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT 
                                    r.idRecensione
                                FROM 
                                     recensiones r where r.user = ? and UPPER(r.titolo) LIKE UPPER(?) ORDER BY r.idRecensione) as m', [$idUser, $search, $search]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function countGetRecensioniSearchByIdFilm(int $idFilm, string $search)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT  
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film, 
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato 
                                 FROM recensiones r, users u where film = ? and (r.titolo LIKE ? || u.nome LIKE ? || u.cognome LIKE ?) GROUP BY r.idRecensione) as m', [$idFilm, $search, $search, $search]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function getRecensioniBySearchIdSerieTv(string $search, int $idSerieTv, $limit, $offset): array
    {
        $result = DB::select('SELECT 
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film, 
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato 
                                FROM 
                                     recensiones r, users u where r.user = u.idUser and r.serieTv = ? and (r.titolo LIKE ? || u.nome LIKE ? || u.cognome LIKE ?) ORDER BY r.idRecensione LIMIT ? OFFSET ?', [$idSerieTv, $search, $search, $search, $limit, $offset]);
        return self::setRecensioneObjectList([], $result);
    }

    public function countGetRecensioniSearchByIdSerieTv(int $idSerieTv, string $search)
    {
        $result = DB::select('SELECT COUNT(idRecensione) as contatore FROM (SELECT  
                                    r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user, film, 
                                    u.idUser, u.nome, u.cognome, u.email, u.password, u.token, u.dataNascita, u.tipologiaUtenza, u.codiceAlbo, u.nomeAzienda, u.biografia, u.stato 
                                 FROM recensiones r, users u where serieTv = ? and (r.titolo LIKE ? || u.nome LIKE ? || u.cognome LIKE ?) GROUP BY r.idRecensione) as m', [$idSerieTv, $search, $search, $search]);
        if (empty($result)) return 0;
        return $result[0]->contatore;
    }

    public function getRecensioniByIdUserAndIdFilm(int $idUser, int $idFilm): bool
    {
        $result = DB::select('SELECT r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, user, film FROM recensiones r where r.user = ? and r.film = ? ORDER BY r.idRecensione', [$idUser, $idFilm]);
        if (!empty($result)) {
            return true;
        }
        return false;
    }

    public function getRecensioniByIdUserAndIdSerieTv(int $idUser, int $idSerieTv): bool
    {
        $result = DB::select('SELECT r.idRecensione, titolo, contenutoRecensione, voto, r.createDate, r.updateDate, serieTv, user FROM recensiones r where r.user = ? and r.serieTv = ? ORDER BY idRecensione', [$idUser, $idSerieTv]);
        if (!empty($result)) {
            return true;
        }
        return false;
    }

    public function insertRecensione(Recensione $recensione, $idSerieTv = null, $idFilm = null)
    {
        $result = DB::insert('INSERT INTO recensiones (titolo, contenutoRecensione, voto, serieTv, user, film) VALUE (?, ?, ?, ?, ?, ?)',
            [$recensione->getTitolo(), $recensione->getContenutoRecensione(), $recensione->getVoto(), $idSerieTv, $recensione->getUtente()->getIdUser(), $idFilm]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateRecensione(Recensione $recensione)
    {
        return DB::update('UPDATE recensiones SET titolo = ?, contenutoRecensione = ?, voto = ?, user = ? WHERE idRecensione = ?',
            [$recensione->getTitolo(), $recensione->getContenutoRecensione(), $recensione->getVoto(), $recensione->getUtente()->getIdUser(), $recensione->getIdRecensione()]);
    }

    public function deleteRecensione(Recensione $recensione)
    {
        return DB::delete('DELETE FROM recensiones WHERE idRecensione = ?', [$recensione->getIdRecensione()]);
    }

    public function deleteRecensioneByIdFilm(int $idFilm)
    {
        return DB::delete('DELETE FROM recensiones WHERE film= ?', [$idFilm]);
    }

    public function deleteRecensioneByIdSerieTv(int $idSerieTv)
    {
        $result = DB::delete('DELETE FROM recensiones WHERE  serieTv= ?', [$idSerieTv]);
        return $result;
    }

    public function getVotiByIdSerieTvUser(int $idSerieTv)
    {
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza=0 AND r.serieTv = ?', [$idSerieTv]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getVotiByIdSerieTvCritico(int $idSerieTv)
    {
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza=1 AND r.serieTv = ?', [$idSerieTv]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getVotiByIdFilmUser(int $idFilm)
    {
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza=0 AND r.film = ?', [$idFilm]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getVotiByIdFilmCritico(int $idFilm)
    {
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza=1 AND r.film = ?', [$idFilm]);
        return self::setRecensioneObjectList([], $result);
    }

    public function getUltimiTrentaRecensioniRegistrate(): array{
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser ORDER BY r.createDate DESC LIMIT 30');
        return self::setRecensioneObjectList([], $result);
    }

    public function getUltimiQuindiciRecensioniUtenteNormaleRegistrate(): array{
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza = 0 ORDER BY r.createDate DESC LIMIT 15');
        return self::setRecensioneObjectList([], $result);
    }

    public function getUltimiQuindiciRecensioniUtenteCriticoRegistrate(): array{
        $result = DB::select('SELECT * FROM recensiones r, users u WHERE r.user = u.idUser AND u.tipologiaUtenza = 1 ORDER BY r.createDate DESC LIMIT 15');
        return self::setRecensioneObjectList([], $result);
    }

    public function getFilmORSerietvFromRecensione(Recensione $recensione){
        $result = DB::select('SELECT r.film, r.serieTv FROM recensiones r WHERE idRecensione = ?', [$recensione->getIdRecensione()]);
        if(!empty($result[0]->film)){
        if ($result[0]->film != null){
            $filmService = new FilmService();
            return $filmService->getFilmParzialeById($result[0]->film);
        }
        }
        if(!empty($result[0]->serieTv)){
            if ($result[0]->serieTv != null){
                $serieTvService = new SerieTvService();
                return $serieTvService->getSerieTvParzialeById($result[0]->serieTv);
            }
        }
        return null;
    }

    public function countRecensioni(): int
    {
        $result = DB::select("SELECT COUNT(idRecensione) as numerorec FROM recensiones ");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerorec;
    }

}