<?php


namespace App\Services\Implementation;


use App\Model\Groups;
use App\Model\Services;
use App\Model\User;
use DB;
use Slim\Exception\HttpBadRequestException;

class ServicesServiceImplementation
{

    /**
     * ServicesServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setServiceObject(Services $services, $result): Services{
        $services->setIdServizio($result->idServizio);
        $services->setNome($result->nome);
        $services->setDescrizione($result->descrizione);
        return $services;
    }

    private function setServiceObjectList($serviceList, $result) {
        foreach ($result as $obj) {
            $service = new Services();
            $service = $this->setServiceObject($service, $obj);
            array_push($serviceList, $service);
        }
        return $serviceList;
    }

    public function getServiceByName(string $nome): Services{
        $service = new Services();
        $result = DB::select('SELECT * FROM services WHERE nome = ?', [$nome]);
        throw_when(empty($result), "e vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "so troppi");
        return self::setServiceObject($service, $result[0]);
    }

    public function getServiceByUser(User $user): array {
        $result = DB::select('SELECT s.idServizio, s.nome, s.descrizione FROM services s, 
                                  services_has_groups shg, 
                                  `groups`g, 
                                  users_has_groups uhg, 
                                  users u 
                              WHERE 
                                    s.idServizio = shg.servizio and 
                                    shg.gruppo = g.idGruppo and 
                                    g.idGruppo = uhg.gruppo and
                                    uhg.user = ? GROUP BY s.idServizio', [$user->getIdUser()]);
        return self::setServiceObjectList([], $result);
    }

    public function getServiceById(int $idService): Services {
        $service = new Services();
        $result = DB::select('SELECT * FROM services WHERE idServizio = ?', [$idService]);
        return self::setServiceObject($service, $result[0]);
    }

    public function getAllServices(){
        $result = DB::select('select * from services');
        return self::setServiceObjectList([], $result);
    }

    public function getServicesByGroup(Groups $group){
        $result = DB::select('SELECT s.idServizio, s.nome, s.descrizione FROM services s, services_has_groups sg WHERE s.idServizio = sg.servizio and sg.gruppo = ?', [$group->getIdGruppo()]);
        return self::setServiceObjectList([], $result);
    }

    public function updateService(Services $services){
        return DB::update('UPDATE `services` SET nome = ?, descrizione = ? WHERE idServizio = ?', [$services->getNome(), $services->getDescrizione(), $services->getIdServizio()]);
    }


}
