<?php

namespace App\Services\Implementation;

use DB;
use App\Model\Episodio;
use Slim\Exception\HttpBadRequestException;

class EpisodioServiceImplementation{
    /**
     * EpisodioServiceImplementation constructor.
     */

    public function __construct()
    {
    }

    private function setEpisodioObject(Episodio $episodio, $result): Episodio{
        $episodio->setIdEpisodio($result -> idEpisodio);
        $episodio->setTitolo($result->titolo);
        $episodio->setDescrizione($result->descrizione);
        $episodio->setNumero($result->numero);
        $episodio->setDurata($result->durata);
        $episodio->setDataInOnda($result->dataInOnda);
        $episodio->setNumeroStagione($result->numeroStagione);
        return $episodio;
    }

    private function setEpisodioObjectList($episodioList, $result) {
        foreach ($result as $obj) {
            $episodio = new Episodio();
            $episodio = $this->setEpisodioObject($episodio, $obj);
            array_push($episodioList, $episodio);
        }
        return $episodioList;
    }

    public function getEpisodioByID(int $idEpisodio): Episodio {
        $episodio = new Episodio();
        $result = DB::select("SELECT * FROM episodios where idEpisodio = ?", [$idEpisodio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setEpisodioObject($episodio, $result[0]);
    }

    public function getAllEpisodio(){
        $result = DB::select("SELECT * FROM episodios");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setEpisodioObjectList([], $result);
    }

    public function getEpisodiByIdStagione($idStagione){
        $result = DB::select("SELECT * FROM episodios WHERE stagione=?", [$idStagione]);
        throw_when(empty($result), "risultato query episodio vuoto", HttpBadRequestException::class);
        return self::setEpisodioObjectList([], $result);
    }

    public function insertEpisodio(Episodio $episodio, int $idStagione) {
        $result = DB::insert('INSERT INTO episodios (titolo, descrizione, numero, durata, dataInOnda, numeroStagione, stagione) VALUE (?, ?, ?, ?, ?, ?, ?)',
            [
                $episodio->getTitolo(),
                $episodio->getDescrizione(),
                $episodio->getNumero(),
                $episodio->getDurata(),
                $episodio->getDataInOnda(),
                $episodio->getNumeroStagione(),
                $idStagione
            ]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateEpisodio(Episodio $episodio, int $idStagione) {
        $result = DB::update('UPDATE episodios SET titolo=?, descrizione=?, numero=?, durata=?, dataInOnda=?, numeroStagione=?, stagione=? WHERE idEpisodio = ?',
            [
                $episodio->getTitolo(),
                $episodio->getDescrizione(),
                $episodio->getNumero(),
                $episodio->getDurata(),
                $episodio->getDataInOnda(),
                $episodio->getNumeroStagione(),
                $idStagione,
                $episodio->getIdEpisodio()
            ]);
        return $result;
    }

    public function deleteEpisodio(int $idEpisodio) {
        $result = DB::delete('DELETE FROM episodios WHERE idEpisodio=?', [$idEpisodio]);
        return $result;
    }

    public function deleteEpisodiByIdStagione($idStagione){
        $result = DB::delete('DELETE FROM episodios WHERE stagione=?', [$idStagione]);
        return $result;
    }
}
