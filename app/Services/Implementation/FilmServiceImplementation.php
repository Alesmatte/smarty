<?php


namespace App\Services\Implementation;


use App\Model\Film;
use App\Model\Filmmaker;
use App\Model\Ruolo;
use DB;
use Slim\Exception\HttpBadRequestException;
use function DI\string;

class FilmServiceImplementation
{

    /**
     * FilmServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setNazionalitaObject($result)
    {
        $nazionalita = array();
        $nazionalita[$result->codice] = $result->nazione;
        return $nazionalita;
        //return $result->codice;
    }

    private function setNazionalitaObjectList($result): array
    {
        $nazionalitaList = array();
        foreach ($result as $obj) {
            $nazionalitaList[$obj->codice] = $obj->nazione;
//            $nazionalita = $this->setNazionalitaObject($obj);
//            array_push($nazionalitaList, $nazionalita);
        }
        return $nazionalitaList;
    }

    private function setFilmObject(Film $film, $result): Film
    {
        $film->setIdFilm($result->idFilm);
        $film->setTitoloOriginale($result->titoloOriginale);
        $film->setTitolo($result->titolo);
        $film->setDescrizione($result->descrizione);
        $film->setTrama($result->trama);
        $film->setTramaBreve($result->tramaBreve);
        $film->setDataSviluppoFilm($result->dataSviluppoFilm);
        $film->setDataUscita($result->dataUscita);
        $film->setDurata($result->durata);
        $film->setAgeRating($result->ageRating);
        $film->setInSala($result->inSala);
        return $film;
    }

    private function setFilmObjectList($filmList, $result)
    {
        foreach ($result as $obj) {
            $film = new Film();
            $film = $this->setFilmObject($film, $obj);
            array_push($filmList, $film);
        }
        return $filmList;
    }

    public function getAllNazionalitas(): array
    {
        $result = DB::select('SELECT * FROM nazioneProduziones');
        return self::setNazionalitaObjectList($result);
    }

    public function getNazionalitasByFilmId(int $idFilm): array
    {
        $result = DB::select('SELECT codice, nazione FROM nazioneProduziones n, nazioneProduziones_films nf WHERE n.codice = nf.nazioneProduzione and nf.film = ?', [$idFilm]);
        return self::setNazionalitaObjectList($result);
    }

    public function getFilmById(int $idFilm): Film
    {
        $film = new Film();
        $result = DB::select("SELECT * FROM films where idFilm = ?", [$idFilm]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result) > 1, "errore valori multipli");
        return self::setFilmObject($film, $result[0]);
    }

    public function getFilmByIdRecensioni(int $idRecensione)
    {
        $film = new Film();
        $result = DB::select("SELECT f.* FROM films f, recensiones r WHERE r.film = f.idFilm AND r.idRecensione = ? GROUP BY f.idFilm", [$idRecensione]);
        if (empty($result)) return $film;
        return self::setFilmObject($film, $result[0]);
    }

    public function getAllFilms(): array
    {
        $result = DB::select("SELECT * FROM films ORDER BY dataUscita");
        return self::setFilmObjectList([], $result);
    }

    public function getAllFilmPerRecensioni()
    {
        $result = DB::select("SELECT f.* FROM films f, recensiones r WHERE r.film = f.idFilm GROUP BY f.idFilm");
        return self::setFilmObjectList([], $result);
    }

    public function getAllFilmPerRecensioniSearch(string $search)
    {
        $result = DB::select("SELECT f.* FROM films f, recensiones r WHERE r.film = f.idFilm AND UPPER(f.titolo) LIKE UPPER(?) GROUP BY f.idFilm", [$search]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByAnnoMinoreDi($anno)
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita < ?  ORDER BY dataUscita DESC", [(string)$anno]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByAnnoMinoreUgualeDi($anno)
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita <= ?  ORDER BY dataUscita DESC", [(string)$anno]);
        return self::setFilmObjectList([], $result);
    }

    /** UGUALE A */
    public function getFilmByAnno($annoInit, $annoFin): array
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita >= ? and dataUscita <= ? ORDER BY dataUscita DESC", [(string)$annoInit, (string)$annoFin]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByAnnoMaggioreUgualeDi($anno)
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita >= ?  ORDER BY dataUscita DESC", [(string)$anno]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByAnnoMaggioreDi($anno)
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita > ?  ORDER BY dataUscita DESC", [(string)$anno]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByNazione($codiceNazione): array
    {
        $result = DB::select("SELECT * FROM films f, nazioneProduziones_films npf WHERE f.idFilm = npf.film and npf.nazioneProduzione = ? ORDER BY dataUscita DESC", [$codiceNazione]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmInSala()
    {
        $result = DB::select("SELECT * FROM films WHERE inSala = true ORDER BY dataUscita DESC");
        return self::setFilmObjectList([], $result);
    }

    public function getUltimiFilmCarosello()
    {
        $result = DB::select("SELECT * FROM films ORDER BY dataUscita DESC LIMIT 20");
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByTitoloSearch($titolo): array
    {
        $result = DB::select("SELECT * FROM films WHERE titolo LIKE ? GROUP BY idFilm ORDER BY dataUscita DESC", [$titolo]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByFilmmaker(Filmmaker $filmmaker): array
    {
        //$result = DB::select("SELECT f.idFilm, f.titoloOriginale, f.titolo, f.descrizione, f.trama, f.tramaBreve, f.dataSviluppoFilm, f.dataUscita, f.durata, f.ageRating, f.inSala FROM films f, ruolos_films rf, ruolos r, ruolos_filmmakers rfm WHERE f.idFilm = rf.film AND rf.ruolo = r.idRuolo AND r.idRuolo = rfm.ruolo AND rfm.filmmaker = ? GROUP BY f.idFilm DESC", [$filmmaker->getIdFilmMaker()]);
        $result = DB::select("SELECT DISTINCT f.* FROM films f, ruolos_films rf, ruolos r, ruolos_filmmakers rfm WHERE f.idFilm = rf.film AND rf.ruolo = r.idRuolo AND r.idRuolo = rfm.ruolo AND rfm.filmmaker = ? ORDER BY f.idFilm DESC", [$filmmaker->getIdFilmMaker()]);
        if(empty($result)){
            return [];
        }
        return self::setFilmObjectList([], $result);
    }

    public function insertFilm(Film $film)
    {
        $result = DB::insert('INSERT INTO films (titoloOriginale, titolo, descrizione, trama, tramaBreve, dataSviluppoFilm, dataUscita, durata, ageRating, inSala) 
                            VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$film->getTitoloOriginale(), $film->getTitolo(), $film->getDescrizione(), $film->getTrama(), $film->getTramaBreve(), $film->getDataSviluppoFilm(), $film->getDataUscita(), $film->getDurata(), $film->getAgeRating(), $film->getInSala()]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertNazioneProduzionesFilms($codiceNazioneProduzione, $idFilm)
    {
        $result = DB::insert('INSERT INTO nazioneProduziones_films (nazioneProduzione, film) VALUE (?, ?)', [$codiceNazioneProduzione, $idFilm]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateToggleInSala(Film $film) {
        return DB::update('UPDATE films SET inSala = ? WHERE idFilm = ?', [$film->getInSala(), $film->getIdFilm()]);
    }

    public function updateFilm(Film $film)
    {
        return DB::update('UPDATE films SET titoloOriginale = ?, titolo = ?, trama = ?, tramaBreve = ?, dataSviluppoFilm = ?, dataUscita = ?, durata = ?, ageRating = ?, inSala = ? WHERE idFilm = ?',
            [$film->getTitoloOriginale(), $film->getTitolo(), $film->getTrama(), $film->getTramaBreve(), $film->getDataSviluppoFilm(), $film->getDataUscita(), $film->getDurata(), $film->getAgeRating(), $film->getInSala(), $film->getIdFilm()]);

    }

    public function deleteNazionalitaByIdFilm(int $idFilm)
    {
        return DB::delete('DELETE FROM nazioneProduziones_films WHERE film = ?', [$idFilm]);
    }

    public function deleteFilm(Film $film) {
        return DB::delete('DELETE FROM films WHERE idFilm = ?', [$film->getIdFilm()]);
    }

    public function getFilmByTitoloOrTitoloOrig($titolo)
    {
        $titoloList = explode(" ", $titolo);
        $titoloList2 = array();
        foreach ($titoloList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($titoloList2, "%" . $cerca . "%");
            }
        }
        foreach ($titoloList2 as $k => $tl) {
            $resultList[$k] = DB::select("SELECT * FROM films WHERE UPPER(titoloOriginale) LIKE UPPER(?) OR UPPER(titolo) LIKE UPPER(?) GROUP BY idFilm ORDER BY dataUscita DESC", [$tl, $tl]);
        }
        $resultMonoList = array();
        $idList = array();
        foreach ($resultList as $k => $results) {
            foreach ($results as $kr => $result) {
                if (count($idList) === 0) {
                    array_push($idList, $result->idFilm);
                    array_push($resultMonoList, $result);
                } else {
                    $trovato = false;
                    foreach ($idList as $key => $value) {
                        if ($value === $result->idFilm) {
                            $trovato = true;
                        }
                    }
                    if (!$trovato) {
                        array_push($idList, $result->idFilm);
                        array_push($resultMonoList, $result);
                    }
                }
            }
        }
        throw_when(empty($resultList), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setFilmObjectList([], $resultMonoList);
    }

    public function getFilmByTitoloOrTitoloOrigSearch(string $search): array
    {
        $sql_statico0 = "SELECT ";
        $sql_statico1 = "f.* FROM films f WHERE (";
        $sql_statico2 = ") GROUP BY f.idFilm, f.dataUscita ORDER BY f.dataUscita";
        $sql_dinamico = "";
        $searchList = explode(" ", $search);
        $searchList2 = array();
        foreach ($searchList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($searchList2, "%" . $cerca . "%");
            }
        }
        foreach ($searchList2 as $key => $search) {
            if ($key === count($searchList2) - 1) {
                $sql_dinamico = $sql_dinamico . " UPPER(f.titolo) LIKE UPPER('" . $search . "')";
            } else {
                $sql_dinamico = $sql_dinamico . " UPPER(f.titolo) LIKE UPPER('" . $search . "') OR";
            }
        }
        $sql = $sql_statico0 . $sql_statico1 . $sql_dinamico . $sql_statico2;
        $result = DB::select($sql);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByRuolo(Ruolo $ruolo)
    {
        $film = new Film();
        $result = DB::select("SELECT f.idFilm as idFilm, f.titoloOriginale, f.titolo, f.descrizione, f.trama, f.tramaBreve, f.dataSviluppoFilm, f.dataUscita, f.durata, f.ageRating, f.inSala FROM ruolos_films as rf, films as f WHERE rf.film = f.idFilm AND rf.ruolo = ? GROUP BY f.idFilm", [$ruolo->getIdRuolo()]);
        if (empty($result)) {
            return new Film();
        } else {
            return self::setFilmObject($film, $result[0]);
        }
    }

    public function getFilmByPremio(int $idPremio){
        $result = DB::select("SELECT * FROM films f, nominations_films nf, nominations n, premio_tipologiaPremios ptp WHERE f.idFilm = nf.film AND nf.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio=? GROUP BY f.idFilm, f.dataUscita ORDER BY f.dataUscita DESC", [$idPremio]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByIdNomination(int $idNomination): Film
    {
        $film = new Film();
        $result = DB::select("SELECT f.* FROM films f, nominations_films nf WHERE f.idFilm = nf.film AND nf.nomination = ?", [$idNomination]);
        if (empty($result)) {
            return $film;
        }
        return self::setFilmObject($film, $result[0]);
    }

    public function getFilmByGenere(int $idGenere){
        $result = DB::select("SELECT * FROM films f, generes_films gf WHERE f.idFilm = gf.film AND gf.genere=? GROUP BY f.idFilm, f.dataUscita ORDER BY f.dataUscita DESC", [$idGenere]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByDistributore(int $idDistributore){
        $result = DB::select("SELECT * FROM films f, distributores_films df WHERE f.idFilm = df.film AND df.distributore=? GROUP BY f.idFilm, f.dataUscita ORDER BY f.dataUscita DESC",[$idDistributore]);
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByNomeCognomeFilmmaker(string $persona){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, $cerca);
            }
        }
        $sql_statico1 = "SELECT f.* FROM ruolos_films rf, ruolos_filmmakers rfm, filmmakers fm, films f WHERE rf.ruolo = rfm.ruolo AND rfm.filmmaker = fm.idFilmMaker AND rf.film = f.idFilm AND (";
        $sql_statico2 = ") GROUP BY f.idFilm, f.dataUscita ORDER BY f.dataUscita";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql);
        return self::setFilmObjectList([], $result);
    }

    public function getAllFilmPerListaPaginati($limit, $offset): array
    {
        $result = DB::select("SELECT * FROM films ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countAllFilmParziali(): int
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films")[0]->numerofilm;
        if (empty($result)) return 0;
        return $result;
    }

    public function getUltimiDieciFilmCreati(): array
    {
        $result = DB::select("SELECT * FROM films ORDER BY createDate DESC LIMIT 10");
        return self::setFilmObjectList([], $result);
    }

    public function getUltimicinqueFilm(): array
    {
        $result = DB::select("SELECT * FROM films ORDER BY dataUscita DESC LIMIT 5");
        return self::setFilmObjectList([], $result);
    }

    public function getFilmByDistributorePaginato(int $idDistributore, $limit, $offset){
        $result = DB::select("SELECT DISTINCT f.* FROM films f, distributores_films df WHERE f.idFilm = df.film AND df.distributore=? ORDER BY f.dataUscita DESC LIMIT ? OFFSET ?",[$idDistributore, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmByDistributore(int $idDistributore): int
    {
        $result =  DB::select("SELECT COUNT(idFilm) as numerofilm FROM films f, distributores_films df WHERE f.idFilm = df.film AND df.distributore=? GROUP BY f.idFilm",[$idDistributore]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmByIdGenerePaginato(int $idGenere, $limit, $offset){
        $result = DB::select("SELECT DISTINCT f.* FROM films f, generes_films gf WHERE f.idFilm = gf.film AND gf.genere = ? ORDER BY f.dataUscita DESC LIMIT ? OFFSET ?", [$idGenere, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmByIdGenere(int $idGenere): int
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films f, generes_films gf WHERE f.idFilm = gf.film AND gf.genere = ? GROUP BY f.idFilm", [$idGenere]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmByPremioPaginato(int $idPremio, $limit, $offset){
        $result = DB::select("SELECT DISTINCT f.* FROM films f, nominations_films nf, nominations n, premio_tipologiaPremios ptp WHERE f.idFilm = nf.film AND nf.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio=? ORDER BY f.dataUscita DESC LIMIT ? OFFSET ?", [$idPremio, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmByPremio(int $idPremio){
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films f, nominations_films nf, nominations n, premio_tipologiaPremios ptp WHERE f.idFilm = nf.film AND nf.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio=? GROUP BY f.idFilm", [$idPremio]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmByAnnoPaginato($annoInit, $annoFin, $limit, $offset): array
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita >= ? and dataUscita <= ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [(string)$annoInit, (string)$annoFin, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmByAnno($annoInit, $annoFin)
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films WHERE dataUscita >= ? and dataUscita <= ?", [(string)$annoInit, (string)$annoFin]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmByNazionePaginato($codiceNazione, $limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT f.* FROM films f, nazioneProduziones_films npf WHERE f.idFilm = npf.film and npf.nazioneProduzione = ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$codiceNazione, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmByNazione($codiceNazione)
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films f, nazioneProduziones_films npf WHERE f.idFilm = npf.film and npf.nazioneProduzione = ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$codiceNazione]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmInSalaPaginato($limit, $offset): array
    {
        $result = DB::select("SELECT * FROM films WHERE inSala = true ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmInSala()
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films WHERE inSala = true");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function maggiorediCinqueVoti(Film $film): bool
    {
        $result = DB::select("SELECT COUNT(idRecensione) as numerorecensioni FROM recensiones WHERE film = ?", [$film->getIdFilm()]);
        if (!empty($result)) {
            if ($result[0]->numerorecensioni < 5) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public function countFilm(){
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films ");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmInUscitaPaginato($limit, $offset): array {
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT * FROM films WHERE dataUscita > ? ORDER BY dataUscita LIMIT ? OFFSET ?", [(string)$dataOggi, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmInUscita(){
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films WHERE dataUscita > ?", [(string)$dataOggi]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getFilmDelMesePaginato($annoInit, $annoFin, $limit, $offset): array
    {
        $result = DB::select("SELECT * FROM films WHERE dataUscita >= ? and dataUscita < ? ORDER BY dataUscita LIMIT ? OFFSET ?", [(string)$annoInit, (string)$annoFin, $limit, $offset]);
        return self::setFilmObjectList([], $result);
    }

    public function countFilmDelMese($dataInit, $dataFin): int
    {
        $result = DB::select("SELECT COUNT(idFilm) as numerofilm FROM films WHERE dataUscita >= ? and dataUscita < ?", [(string)$dataInit, (string)$dataFin]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilm;
    }

    public function getProssimoFilmInUscita(): Film
    {
        $film = new Film();
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT * FROM films WHERE dataUscita > ? ORDER BY dataUscita LIMIT 1", [(string)$dataOggi]);
        if (empty($result)) return $film;
        return self::setFilmObject($film, $result[0]);
    }

    public function existFilm(int $idNomination): bool{
        $res = DB::select("SELECT * FROM films f, nominations_films nf WHERE f.idFilm = nf.film and nf.nomination = ?", [$idNomination]);
        return !empty($res);
    }

}