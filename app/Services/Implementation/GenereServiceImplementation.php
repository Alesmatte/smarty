<?php


namespace App\Services\Implementation;


use App\Model\Genere;
use DB;
use Slim\Exception\HttpBadRequestException;

class GenereServiceImplementation
{

    /**
     * GenereServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setGenereObject(Genere $genere, $result): Genere {
        $genere->setIdGenere($result->idGenere);
        $genere->setNome($result->nome);
        $genere->setDescrizione($result->descrizione);
        return $genere;
    }

    private function setGenereObjectList($genereList, $result) {
        foreach ($result as $obj) {
            $genere = new Genere();
            $genere = $this->setGenereObject($genere, $obj);
            array_push($genereList, $genere);
        }
        return $genereList;
    }

    public function getGenereById(int $idGenere): Genere {
        $genere = new Genere();
        $result = DB::select("SELECT * FROM generes where idGenere = ?", [$idGenere]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setGenereObject($genere, $result[0]);
    }

    public function getAllGenere() {
        $result = DB::select("SELECT * FROM generes");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setGenereObjectList([], $result);
    }

    public function getAllGenereFilm() {
        $result = DB::select("SELECT * FROM generes g, generes_films gf WHERE g.idGenere = gf.genere GROUP BY g.idGenere");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setGenereObjectList([], $result);
    }

    public function getAllGenereSerieTv() {
        $result = DB::select("SELECT * FROM generes g, generes_films gf WHERE g.idGenere = gf.genere GROUP BY g.idGenere");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setGenereObjectList([], $result);
    }

    public function getGenereByIdFilm(int $idFilm): array {
        $result = DB::select('SELECT * FROM generes g, generes_films gf WHERE g.idGenere = gf.genere and gf.film = ?', [$idFilm]);
        return self::setGenereObjectList([], $result);
    }

    public function getGenereByIdSerieTv(int $idSerieTv): array {
        $result = DB::select('SELECT * FROM generes g, generes_serieTvs gs WHERE g.idGenere = gs.genere and gs.serieTv = ?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setGenereObjectList([], $result);
    }

    public function insertGenere(Genere $genere) {
        $result = DB::insert('INSERT INTO generes (nome, descrizione) VALUE (?, ?)',
            [$genere->getNome(), $genere->getDescrizione()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertGenereFilm($idGenere, $idFilm) {
        $result = DB::insert('INSERT INTO generes_films (genere, film) VALUE (?, ?)',
            [$idGenere, $idFilm]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertGenereSerieTv($idGenere, $idSerieTv) {
        $result = DB::insert('INSERT INTO generes_serieTvs (genere, serieTv) VALUE (?, ?)',
            [$idGenere, $idSerieTv]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateGenere(Genere $genere) {
        $result = DB::update('UPDATE generes SET nome = ?, descrizione = ? WHERE idGenere = ?',
            [$genere->getNome(), $genere->getDescrizione(), $genere->getIdGenere()]);
        return $result;
    }

    public function deleteGenere(Genere $genere) {
        $result = DB::delete('DELETE FROM generes WHERE idGenere = ?', [$genere->getIdGenere()]);
        return $result;
    }

    public function deleteGenereFilmByIdFilm(int $idFilm) {
        return DB::delete('DELETE FROM generes_films WHERE film = ?', [$idFilm]);
    }

    public function deleteGenereSerieTvByIdSerieTv(int $idSerieTv){
        $result = DB::delete('DELETE FROM generes_serieTvs WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }

    public function getIDGenereNomeGenere() {
        $result = DB::select("SELECT idGenere, nome, descrizione FROM generes");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setGenereObjectList([], $result);
    }

}