<?php


namespace App\Services\Implementation;


use App\Model\Premio;
use DB;
use Slim\Exception\HttpBadRequestException;

class PremioServiceImplementation
{

    /**
     * PremioServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setPremioObject(Premio $premio, $result): Premio {
        $premio->setIdPremio($result->idPremio);
        $premio->setNome($result->nome);
        $premio->setDescrizione($result->descrizione);
        $premio->setLocalita($result->localita);
        return $premio;
    }

    private function setPremioObjectList($premioList, $result) {
        foreach ($result as $obj) {
            $premio = new Premio();
            $premio = $this->setPremioObject($premio, $obj);
            array_push($premioList, $premio);
        }
        return $premioList;
    }

    public function getPremioById(int $idPremio): Premio {
        $categoriaPremio = new Premio();
        $result = DB::select("SELECT * FROM premios where idPremio = ?", [$idPremio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setPremioObject($categoriaPremio, $result[0]);
    }

    public function getAllPremios(): array {
        $result = DB::select("SELECT * FROM premios");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setPremioObjectList([], $result);
    }

    /* NON DOVREBBE SERVIRE  */
    public function insertPremioCategoriaPremio($idPremio, $idTipologiaPremio) {
        $result = DB::insert('INSERT INTO premio_tipologiaPremios (premio, tipologiaPremio) VALUE (?, ?)',
            [$idPremio, $idTipologiaPremio]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    /* NON DOVREBBE SERVIRE  */
    public function updatePremio(Premio $premio) {
        $result = DB::update('UPDATE premios SET nome = ?, descrizione = ?, localita = ? WHERE idPremio = ?',
            [$premio->getNome(), $premio->getDescrizione()->getIdPremio(), $premio->getLocalita()]);
        throw_when(!($result), "errore nella modifica",  HttpBadRequestException::class);
    }

    /* NON DOVREBBE SERVIRE  */
    public function deletePremio(Premio $premio) {
        $result = DB::delete('DELETE FROM premios WHERE idPremio = ?', [$premio->getImgPremio()]);
        throw_when(!($result), "errore nella cancellazione",  HttpBadRequestException::class);
    }
}