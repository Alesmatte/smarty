<?php


namespace App\Services\Implementation;


use App\Model\CategoriaPremio;
use App\Model\Filmmaker;
use App\Model\Nomination;
use App\Model\Premio;
use App\Model\TipologiaPremio;
use App\Services\ImmagineService;
use DB;
use Slim\Exception\HttpBadRequestException;

class NominationServiceImplementation
{

    /**
     * NominationServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setNominationObject(Nomination $nomination, $result): Nomination
    {
        $nomination->setIdNomination($result->idNomination);
        $nomination->setDescrizione($result->descrizioneNomination);
        $nomination->setDataPremiazione($result->dataPremiazione);
        $nomination->setStato($result->stato);
        if (isset($result->idPremio_tipologiaPremio)) {
            $categoriaPremio = new CategoriaPremio();
            $categoriaPremio->setIdCategoriaPremio($result->premio_tipologiaPremio);
            $nomination->setCategoriaPremio($categoriaPremio);

            $premio = new Premio();
            $premio->setIdPremio($result->premio);
            $premio->setNome($result->nomePremio);
            $premio->setDescrizione($result->descrizionePremio);
            $premio->setLocalita($result->localita);
            $categoriaPremio->setPremio($premio);

            $tipologiaPremio = new TipologiaPremio();
            $tipologiaPremio->setidTipologiaPremio($result->tipologiaPremio);
            $tipologiaPremio->setNome($result->nomeTipologiaPremio);
            $categoriaPremio->setTipologia($tipologiaPremio);
        }
        return $nomination;
    }

    private function setNominationObjFromSerietv($result): Nomination
    {
        $nomination = new Nomination();
        $nomination->setIdNomination($result->idNomination);
        $nomination->setDescrizione($result->nomination_serietv_descrizione);
        $nomination->setDataPremiazione($result->dataPremiazione);
        $nomination->setStato($result->stato);
        $filmmaker = new Filmmaker();
        $filmmaker->setIdFilmMaker($result->idFilmMaker);
        $filmmaker->setNome($result->nome);
        $filmmaker->setCognome($result->cognome);
        $filmmaker->setBiografia($result->biografia);
        $filmmaker->setBreveDescrizione($result->breveDescrizione);
        $filmmaker->setCitazione($result->citazione);
        $filmmaker->setTipologiaPrincipale(decodifica_array_filmmakers_to_string($result->tipologiaPrincipale));
        $imgService = new ImmagineService();
        $filmmaker->setImgAvatar($imgService->getImgByEntitaAndIdEntitaAndTipologia('filmmaker', $filmmaker->getIdFilmMaker(), 0)[0]);
        $nomination->setFilmMaker($filmmaker);
        if (isset($result->idPremio_tipologiaPremio)) {
            $categoriaPremio = new CategoriaPremio();
            $categoriaPremio->setIdCategoriaPremio($result->idPremio_tipologiaPremio);
            $nomination->setCategoriaPremio($categoriaPremio);

            $premio = new Premio();
            $premio->setIdPremio($result->idPremio);
            $premio->setNome($result->premio_nome);
            $premio->setDescrizione($result->descrizione);
            $premio->setLocalita($result->localita);
            $categoriaPremio->setPremio($premio);

            $tipologiaPremio = new TipologiaPremio();
            $tipologiaPremio->setidTipologiaPremio($result->idTipologiaPremio);
            $tipologiaPremio->setNome($result->tipologia_premio_nome);
            $categoriaPremio->setTipologia($tipologiaPremio);
        }
        return $nomination;
    }

    private function setNominationObjectList($nominationList, $result)
    {
        foreach ($result as $obj) {
            $nomination = new Nomination();
            $nomination = $this->setNominationObject($nomination, $obj);
            array_push($nominationList, $nomination);
        }
        return $nominationList;
    }

    private function setNominationSerieTvObjectList($result)
    {
        $idNominationList = array();
        foreach ($result as $obj) {
            $nomination = new Nomination();
            $nomination = $this->setNominationObjFromSerietv($obj);
            array_push($idNominationList, $nomination);
        }
        return $idNominationList;
    }

    /**
     * @param int $idNomination
     * @return Nomination
     */
    public function getNominationById(int $idNomination): Nomination
    {
        $nomination = new Nomination();
        $result = DB::select("SELECT
                            n.idNomination, 
                            n.descrizione AS descrizioneNomination, 
                            n.dataPremiazione, 
                            n.stato, 
                            n.premio_tipologiaPremio, 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio, 
                            p.descrizione AS descrizionePremio, 
                            p.localita, 
                            tp.idTipologiaPremio, 
                            tp.nome AS nomeTipologiaPremio 
                            FROM 
                                 nominations n, 
                                 premio_tipologiaPremios ptp, 
                                 premios p, 
                                 tipologiaPremios tp 
                            WHERE 
                                  n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio and 
                                  ptp.premio = p.idPremio and 
                                  ptp.tipologiaPremio = tp.idTipologiaPremio and 
                                  n.idNomination = ?", [$idNomination]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result) > 1, "errore valori multipli");
        return self::setNominationObject($nomination, $result[0]);
    }

    /**
     * @param int $idFilm
     * @return array
     */
    public function getNominationByIdFilm(int $idFilm): array
    {
        $result = DB::select('SELECT
                            n.idNomination, 
                            n.descrizione AS descrizioneNomination, 
                            n.dataPremiazione, 
                            n.stato, 
                            n.premio_tipologiaPremio, 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio, 
                            p.descrizione AS descrizionePremio, 
                            p.localita, 
                            tp.idTipologiaPremio, 
                            tp.nome AS nomeTipologiaPremio 
                            FROM 
                                 nominations n, 
                                 premio_tipologiaPremios ptp, 
                                 premios p, 
                                 tipologiaPremios tp, 
                                 nominations_films nf
                            WHERE 
                                  n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio and 
                                  ptp.premio = p.idPremio and 
                                  ptp.tipologiaPremio = tp.idTipologiaPremio and 
                                  nf.nomination = n.idNomination and
                                  nf.film = ?', [$idFilm]);
        return self::setNominationObjectList([], $result);
    }

    /**
     * @param int $idFilmmaker
     * @return array
     */
    public function getNominationByIdFilmmaker(int $idFilmmaker): array
    {
        $result = DB::select('SELECT
                            n.idNomination, 
                            n.descrizione AS descrizioneNomination, 
                            n.dataPremiazione, 
                            n.stato, 
                            n.premio_tipologiaPremio, 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio, 
                            p.descrizione AS descrizionePremio, 
                            p.localita, 
                            tp.idTipologiaPremio, 
                            tp.nome AS nomeTipologiaPremio 
                            FROM 
                                 nominations n, 
                                 premio_tipologiaPremios ptp, 
                                 premios p, 
                                 tipologiaPremios tp, 
                                 nominations_filmmakers nf
                            WHERE 
                                  n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio and 
                                  ptp.premio = p.idPremio and 
                                  ptp.tipologiaPremio = tp.idTipologiaPremio and 
                                  nf.nomination = n.idNomination and
                                  nf.fimmaker = ?', [$idFilmmaker]);
        return self::setNominationObjectList([], $result);
    }

    /**
     * @param int $idSerieTv
     * @return array
     */
    public function getNominationByIdSerieTv(int $idSerieTv): array
    {
        $result = DB::select('SELECT ns.idNomSerieTv,
       ns.idNomSerieTv,      
       ns.nomination as nomination_serietv_idNomination,
       ns.serieTv,
       n.idNomination,
       n.descrizione as nomination_serietv_descrizione,
       n.dataPremiazione,
       n.stato,
       premio_tipologiaPremio,
       nfil.idNomFilmmakers,
       nfil.nomination,
       fimmaker,
       fil.idFilmMaker,
       fil.nome,
       fil.cognome,
       fil.biografia,
       fil.breveDescrizione,
       fil.citazione,
       fil.tipologiaPrincipale,
       film,
       ptP.idPremio_tipologiaPremio,
       ptP.premio,
       tipologiaPremio,
       pr.idPremio,
       pr.nome as premio_nome,
       pr.descrizione,
       pr.localita,
       tP.idTipologiaPremio,
       tP.nome as tipologia_premio_nome FROM 
              nominations_serieTvs ns,
              nominations n,
              nominations_filmmakers nfil,
              filmmakers fil,
              premio_tipologiaPremios ptP,
              premios pr,
              tipologiaPremios tP WHERE
                                        ns.serieTv = ?
                                    and n.idNomination = ns.nomination
                                    and nfil.nomination = n.idNomination
                                    and fil.idFilmMaker= nfil.fimmaker
                                    and ptP.idPremio_tipologiaPremio = n.premio_tipologiaPremio
                                    and pr.idPremio = ptP.premio 
                                    and tP.idTipologiaPremio = ptP.tipologiaPremio;', [$idSerieTv]);
        return self::setNominationSerieTvObjectList($result);

    }

    /**
     * @return array
     */
    public function getAllNominations(): array
    {
        $result = DB::select("SELECT 
                            n.idNomination, 
                            n.descrizione AS descrizioneNomination, 
                            n.dataPremiazione, 
                            n.stato, 
                            n.premio_tipologiaPremio, 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio, 
                            p.descrizione AS descrizionePremio, 
                            p.localita, 
                            tp.idTipologiaPremio, 
                            tp.nome AS nomeTipologiaPremio 
                            FROM 
                                 nominations n, 
                                 premio_tipologiaPremios ptp, 
                                 premios p, 
                                 tipologiaPremios tp
                            WHERE 
                                  n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio and 
                                  ptp.premio = p.idPremio and 
                                  ptp.tipologiaPremio = tp.idTipologiaPremio");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setNominationObjectList([], $result);
    }

    /**
     * @param Nomination $nomination
     * @return mixed
     */
    public function insertNomination(Nomination $nomination)
    {
        $result = DB::insert('INSERT INTO nominations (descrizione, dataPremiazione, stato, premio_tipologiaPremio) VALUE (?, ?, ?, ?)',
            [$nomination->getDescrizione(), $nomination->getDataPremiazione(), $nomination->getStato(), $nomination->getCategoriaPremio()->getIdCategoriaPremio()]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    /**
     * @param $idNomination
     * @param $idFilm
     * @return mixed
     */
    public function insertNominationFilm($idNomination, $idFilm)
    {
        $result = DB::insert('INSERT INTO nominations_films (nomination, film) VALUE (?, ?)',
            [$idNomination, $idFilm]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    /**
     * @param $idNomination
     * @param $idSerieTv
     * @return mixed
     */
    public function insertNominationSerieTv($idNomination, $idSerieTv)
    {
        $result = DB::insert('INSERT INTO nominations_serieTvs (nomination, serieTv) VALUE (?, ?)',
            [$idNomination, $idSerieTv]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    /**
     * @param $idNomination
     * @param $idFilmmaker
     * @return mixed
     */
    public function insertNominationFilmmaker($idNomination, $idFilmmaker)
    {
        $result = DB::insert('INSERT INTO nominations_filmmakers (nomination, fimmaker) VALUE (?, ?)',
            [$idNomination, $idFilmmaker]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    /**
     * @param Nomination $nomination
     */
    public function updateNomination(Nomination $nomination, int $r1, int $r2, int $r4, int $r5)
    {
        $r3 = DB::update('UPDATE nominations SET descrizione = ?, dataPremiazione = ?, stato = ?, premio_tipologiaPremio = ? WHERE idNomination = ?',
            [$nomination->getDescrizione(), $nomination->getDataPremiazione(), $nomination->getStato(), $nomination->getCategoriaPremio()->getIdCategoriaPremio(), $nomination->getIdNomination()]);
        return [$r1, $r2, $r3, $r4, $r5];
    }

    /**
     * @param Nomination $nomination
     */
    public function deleteNomination(Nomination $nomination)
    {
        return DB::delete('DELETE FROM nominations WHERE idNomination = ?', [$nomination->getIdNomination()]);
    }

    public function deleteNominationSerieTvByIdSerieTv(int $idSerieTv)
    {
        $result = DB::delete('DELETE FROM nominations_serieTvs WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }

    public function deleteNominationFilmByidFilm(int $idFilm){
        $result =  DB::delete('DELETE FROM nominations_films  WHERE film = ?', [$idFilm]);
        return $result;
    }

    public function deleteNominationFilmByidNomination(int $idNomination){
        $result =  DB::delete('DELETE FROM nominations_films  WHERE nomination = ?', [$idNomination]);
        return $result;
    }

    public function deleteNominationSerieTvByNomination(int $idNomination){
        $result =  DB::delete('DELETE FROM nominations_serieTvs  WHERE nomination = ?', [$idNomination]);
        return $result;
    }

    public function getNominationSerieTvNominationByIdSerieTv(int $idSerieTv): array
    {
        $result = DB::select('SELECT * FROM nominations_serieTvs  WHERE serieTv = ?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setNominationSerieTvObjectList($result);
    }

    public function getNominationByNomePremioAndAnno(int $idPremio, string $anno): array
    {
        $result = DB::select('SELECT
                                    n.idNomination,
                                    n.descrizione AS descrizioneNomination,
                                    n.dataPremiazione,
                                    n.stato,
                                    n.premio_tipologiaPremio,
                                    ptp.idPremio_tipologiaPremio,
                                    ptp.premio,
                                    ptp.tipologiaPremio,
                                    p.idPremio,
                                    p.nome AS nomePremio,
                                    p.descrizione AS descrizionePremio,
                                    p.localita,
                                    tp.idTipologiaPremio,
                                    tp.nome AS nomeTipologiaPremio
                                FROM
                                    nominations n,
                                    premios p,
                                    premio_tipologiaPremios ptp,
                                    tipologiaPremios tp
                                WHERE
                                    p.idPremio=ptp.premio and
                                    ptp.tipologiaPremio = tp.idTipologiaPremio and
                                    ptp.idPremio_tipologiaPremio = n.premio_tipologiaPremio and
                                      p.idPremio =? and
                                      n.dataPremiazione >= ? and
                                      n.dataPremiazione <= ?',
                                [$idPremio, $anno . '/01/01', $anno . '/12/31']);
        return self::setNominationObjectList([], $result);
    }
}