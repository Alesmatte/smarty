<?php


namespace App\Services\Implementation;


use App\Model\Immagine;
use App\Model\Video;
use DB;
use Slim\Exception\HttpBadRequestException;

class ImmagineServiceImplementation
{

    /**
     * ImmagineServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setImgObject(Immagine $immagine, $result): Immagine {
        $immagine->setIdImmagione($result->idImmagine);
        $immagine->setNome($result->nome);
        $immagine->setDescrizione($result->descrizione);
        $immagine->setDato($result->dato);
        $immagine->setTipo($result->tipo);
        return $immagine;
    }

    private function setImgObjectList($immagineList, $result) {
        foreach ($result as $obj) {
            $immagine = new Immagine();
            $immagine = $this->setImgObject($immagine, $obj);
            array_push($immagineList, $immagine);
        }
        return $immagineList;
    }

    public function getImgById(int $idImmagine): Immagine {
        $immagine = new Immagine();
        $result = DB::select("SELECT * FROM immagines where idImmagine = ?", [$idImmagine]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setImgObject($immagine, $result[0]);
    }

    /**
     * @param string $entita   a scelta tra filmmaker, film, serieTv, episodio, distributore, ruolo, user
     * @param int $idEntita  id dell'entità selezionata
     * @param int $tipologia   tipo di immagine
     *                           0 = avatar
     *                           1 = profilo
     *                           2 = copertina
     *                           3 = cover video
     *                           4 = slider
     *                           5 = generica
     *
     * @return array   di immagini
     */
    public function getImgByEntitaAndIdEntitaAndTipologia(string $entita, int $idEntita, int $tipologia): array {
        $sql = 'SELECT * FROM immagines where ' . $entita;
        $result = DB::select($sql . ' = ? and tipo = ?', [$idEntita, $tipologia]);
        if (empty($result)) {
            return array();
        }
        return self::setImgObjectList([], $result);
    }

    public function getAllImg(): array {
        $result = DB::select("SELECT * FROM immagines");
        return self::setImgObjectList([], $result);
    }

    public function getImgByTipologia(int $tipo): array {
        $result = DB::select("SELECT * FROM immagines WHERE tipo = ?", [$tipo]);
        return self::setImgObjectList([], $result);
    }

    public function insertImg(Immagine $immagine, $filmmaker = null, $film = null, $serieTv = null, $episodio = null, $distributore = null, $ruolo = null, $user = null, $premio = null, $video = null) {
        $result = DB::insert('INSERT INTO immagines (nome, descrizione, dato, tipo, filmmaker, film, serieTv, episodio, distributore, ruolo, user, premio, video) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                        [$immagine->getNome(), $immagine->getDescrizione(), $immagine->getDato(), $immagine->getTipo(), $filmmaker, $film, $serieTv, $episodio, $distributore, $ruolo, $user, $premio, $video]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateImg(Immagine $immagine, $filmmaker = null, $film = null, $serieTv = null, $episodio = null, $distributore = null, $ruolo = null, $user = null, $premio = null, $video = null) {

        return DB::update('UPDATE immagines SET nome = ?, descrizione = ?, dato = ?, tipo = ?, filmmaker = ?, film = ?, serieTv = ?, episodio = ?, distributore = ?, ruolo = ?, user = ?, premio = ?, video = ?  WHERE idImmagine = ?',
            [$immagine->getNome(), $immagine->getDescrizione(), $immagine->getDato(), $immagine->getTipo(), $filmmaker, $film, $serieTv, $episodio, $distributore, $ruolo, $user, $premio, $video , $immagine->getIdImmagione()]);
    }

    public function deleteImg(Immagine $immagine) {
        $result = DB::delete('DELETE FROM immagines WHERE idImmagine = ?', [$immagine->getIdImmagione()]);
        return $result;
    }

    public function deleteImgByIdEpisodio(int $idEpisodio){
        $result = DB::delete('DELETE FROM immagines WHERE episodio=?', [$idEpisodio]);
        return $result;
    }

    public function deleteImgByIdFilm(int $idFilm){
        return DB::delete('DELETE FROM immagines WHERE film=?', [$idFilm]);
    }

    public function deleteImgByIDSerieTv(int $idSerieTv){
        $result = DB::delete('DELETE FROM immagines WHERE serieTv=?', [$idSerieTv]);
        return $result;
    }

    public function deleteImgByIdRuolo(int $idRuolo){
        $result = DB::delete('DELETE FROM immagines WHERE ruolo =?', [$idRuolo]);
        return $result;
    }

    public function deleteImgByIdVideo(Video $video) {
        return DB::delete('DELETE FROM immagines WHERE video = ?', [$video->getIdVideo()]);
    }
}