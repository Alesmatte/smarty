<?php


namespace App\Services\Implementation;


use App\Model\Groups;
use App\Model\Services;
use App\Model\User;
use DB;
use Slim\Exception\HttpBadRequestException;

class GroupServiceImplementation
{

    /**
     * GroupServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setGroupObject(Groups $groups, $result): Groups{
        $groups->setIdGruppo($result->idGruppo);
        $groups->setNome($result->nome);
        $groups->setDescrizione($result->descrizione);
        return $groups;
    }

    private function setGroupObjectList($groupList, $result) {
        foreach ($result as $obj) {
            $group = new Groups();
            $group = $this->setGroupObject($group, $obj);
            array_push($groupList, $group);
        }
        return $groupList;
    }

    public function getGroupByUser(User $user): array {
        $result = DB::select('SELECT * FROM `groups` g, users_has_groups uhs WHERE g.idGruppo = uhs.gruppo and uhs.user = ?', [$user->getIdUser()]);
        return self::setGroupObjectList([], $result);
    }

    public function getGroupByName(string $nome): Groups{
        $group = new Groups();
        $result = DB::select('SELECT * FROM `groups` g where g.nome = ?', [$nome]);
        throw_when(empty($result), "e vuoto",  HttpBadRequestException::class);
        throw_when(count($result)>1, "so troppi");
        return self::setGroupObject($group, $result[0]);
    }

    public function getGroupById(int $idGruppo): Groups{
        $group = new Groups();
        $result = DB::select('SELECT * FROM `groups`  where idGruppo = ?', [$idGruppo]);
        throw_when(empty($result), "e vuoto",  HttpBadRequestException::class);
        throw_when(count($result)>1, "so troppi");
        return self::setGroupObject($group, $result[0]);
    }

    public function getAllgroups(){
        $result = DB::select('SELECT * FROM `groups`');
        return self::setGroupObjectList([], $result);
    }

    public function getGroupsByService(Services $service): array {
        $result = DB::select('SELECT * FROM `groups` g, services_has_groups shg WHERE g.idGruppo = shg.gruppo and shg.servizio = ?', [$service->getIdServizio()]);
        return self::setGroupObjectList([], $result);
    }

    public function insertGroup(Groups $group){
        $result = DB::insert('INSERT INTO `groups` (nome, descrizione) VALUE (?, ?)', [$group->getNome(), $group->getDescrizione()]);
        throw_when(empty($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertServicesHasGroups(Groups $group, Services $services){
        $result = DB::insert('INSERT INTO `services_has_groups` (gruppo, servizio) VALUE (?, ?)', [$group->getIdGruppo(), $services->getIdServizio()]);
        throw_when(empty($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateGroup(Groups $group){
        return DB::update('UPDATE `groups` set nome = ?, descrizione = ? WHERE idGruppo = ?', [$group->getNome(), $group->getDescrizione(), $group->getIdGruppo()]);
    }

    public function updateServicesHasGroups(Groups $group, Services $services){
        return DB::update('UPDATE `services_has_groups` set gruppo = ?, servizio = ? WHERE gruppo = ?', [$group->getIdGruppo(), $services->getIdServizio(), $group->getIdGruppo()]);
    }

    public function deleteGroup(Groups $group) {
        return DB::delete('DELETE FROM `groups` WHERE idGruppo = ?', [$group->getIdGruppo()]);
    }

    public function deleteServicesHasGroupsByGruppo(Groups $group) {
        return DB::delete('DELETE FROM `services_has_groups` WHERE gruppo = ?', [$group->getIdGruppo()]);
    }

    public function deleteServicesHasGroupsByGruppoAndService(Groups $group, Services $services) {
        return DB::delete('DELETE FROM `services_has_groups` WHERE gruppo = ? and servizio = ?', [$group->getIdGruppo(), $services->getIdServizio()]);
    }

    public function deleteUserHasGroupByUser(User $user) {
        return DB::delete('DELETE FROM `users_has_groups` WHERE user = ?', [$user->getIdUser()]);
    }

    public function insertUserHasGroup(User $user, Groups $group){
        $result = DB::insert('INSERT INTO users_has_groups (user, gruppo) VALUE (?, ?)', [$user->getIdUser(), $group->getIdGruppo()]);
        throw_when(empty($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function deleteUserHasGroupByUserAndGroup(User $user, Groups $group) {
        return DB::delete('DELETE FROM `users_has_groups` WHERE user = ? AND gruppo = ?', [$user->getIdUser(), $group->getIdGruppo()]);
    }
}

