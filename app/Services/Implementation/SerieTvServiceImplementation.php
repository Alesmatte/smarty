<?php


namespace App\Services\Implementation;

use App\Model\Filmmaker;
use App\Model\Ruolo;
use DB;
use App\Model\SerieTv;
use Slim\Exception\HttpBadRequestException;
use function Symfony\Component\String\s;

class SerieTvServiceImplementation
{
    /**
     * EpisodioServiceImplementation constructor.
     */

    public function __construct()
    {
    }

    private function setNazionalitaObject($result)
    {
        return $result->nazione;
    }

    private function setNazionalitaObjectList($result): array
    {
        $nazionalitaList = [];
        foreach ($result as $obj) {
            $nazionalita = $this->setNazionalitaObject($obj);
            array_push($nazionalitaList, $nazionalita);
        }
        return $nazionalitaList;
    }

    private function setSerieTvObject(SerieTv $serieTv, $result): SerieTv
    {
        $serieTv->setIdSerieTv($result->idSerieTv);
        $serieTv->setTiloloOriginale($result->titoloOriginale);
        $serieTv->setTitolo($result->titolo);
        $serieTv->setDescrizione($result->descrizione);
        $serieTv->setTrama($result->trama);
        $serieTv->setTramaBreve($result->tramaBreve);
        $serieTv->setDataSviluppoSerieTv($result->dataSviluppoSerieTv);
        $serieTv->setDataUscita($result->dataUscita);
        $serieTv->setAgeRating($result->ageRating);
        return $serieTv;
    }

    private function setSerieTvObjectList($serieTvList, $result)
    {
        foreach ($result as $obj) {
            $serieTv = new SerieTv();
            $serieTv = $this->setSerieTvObject($serieTv, $obj);
            array_push($serieTvList, $serieTv);
        }
        return $serieTvList;
    }

    public function getAllNazionalitas(): array
    {
        $result = DB::select('SELECT * FROM distributores_serieTvs');
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setNazionalitaObjectList($result);
    }

    public function getNazionalitasByIdSerieTv(int $idSerieTv): array
    {
        $result = DB::select('SELECT * FROM nazioneProduziones n, nazioneProduziones_serieTvs ns WHERE n.codice = ns.nazioneProduzione and serieTv=?', [$idSerieTv]);
        return self::setNazionalitaObjectList($result);
    }

    public function getSerieTvById(int $idSerieTv): SerieTv
    {
        $serieTv = new SerieTv();
        $result = DB::select("SELECT * FROM serieTvs where idSerieTv = ?", [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result) > 1, "errore valori multipli");
        return self::setSerieTvObject($serieTv, $result[0]);
    }

    public function getAllSerieTv()
    {
        $result = DB::select("SELECT * FROM serieTvs ORDER BY dataUscita");
        return self::setSerieTvObjectList([], $result);
    }

    public function getAllSerieTvPerRecensioni()
    {
        $result = DB::select("SELECT s.* FROM serieTvs s, recensiones r WHERE r.serieTv = s.idSerieTv GROUP BY s.idSerieTv");
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByIdRecensioni(int $idRecensione): SerieTv
    {
        $serieTv = new SerieTv();
        $result = DB::select("SELECT s.* FROM serieTvs s, recensiones r WHERE r.serieTv = s.idSerieTv AND r.idRecensione = ? GROUP BY s.idSerieTv", [$idRecensione]);
        if (empty($result)) return $serieTv;
        return self::setSerieTvObject($serieTv, $result[0]);
    }

    public function getAllSerieTvPerRecensioniSearch(string $search)
    {
        $result = DB::select("SELECT s.* FROM serieTvs s, recensiones r WHERE r.serieTv = s.idSerieTv AND UPPER(s.titolo) LIKE UPPER(?) GROUP BY s.idSerieTv", [$search]);
        return self::setSerieTvObjectList([], $result);
    }

    public function insertSerieTv(SerieTv $serieTv)
    {
        $result = DB::insert('INSERT INTO serieTvs (titoloOriginale, titolo, descrizione, trama, tramaBreve, dataSviluppoSerieTv, dataUscita, ageRating) 
                            VALUE (?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $serieTv->getTiloloOriginale(),
                $serieTv->getTitolo(),
                $serieTv->getDescrizione(),
                $serieTv->getTrama(),
                $serieTv->getTramaBreve(),
                $serieTv->getDataSviluppoSerieTv(),
                $serieTv->getDataUscita(),
                $serieTv->getAgeRating()
            ]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateSerieTv(SerieTv $serieTv)
    {
        $result = DB::update('UPDATE serieTvs SET titoloOriginale = ?, titolo = ?, descrizione=?, trama = ?, tramaBreve = ?, dataSviluppoSerieTv = ?, dataUscita = ?, ageRating = ? WHERE idSerieTv = ?',
            [
                $serieTv->getTiloloOriginale(),
                $serieTv->getTitolo(),
                $serieTv->getDescrizione(),
                $serieTv->getTrama(),
                $serieTv->getTramaBreve(),
                $serieTv->getDataSviluppoSerieTv(),
                $serieTv->getDataUscita(),
                $serieTv->getAgeRating(),
                $serieTv->getIdSerieTv()
            ]);
        return $result;
    }

    public function deleteSerieTv(int $idSerieTv)
    {
        $result = DB::delete('DELETE FROM serieTvs WHERE idSerieTv = ?', [$idSerieTv]);
        return $result;
    }

    public function deleteNazioneProduzioneSerieTvByIdSerieTv(int $idSerieTv)
    {
        $result = DB::delete('DELETE FROM nazioneProduziones_serieTvs WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }

    public function insertNazioneProduzionesSerieTvs($codiceNazioneProduzione, $idSerieTv)
    {
        $result = DB::insert('INSERT INTO nazioneProduziones_serieTvs (nazioneProduzione, serieTv) VALUE (?, ?)', [$codiceNazioneProduzione, $idSerieTv]);
        throw_when(!($result), "errore inserimento", HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function getSerieTvInCorso()
    {
        $result = DB::select("SELECT * FROM serieTvs stv, stagiones s WHERE stv.idSerieTv = s.serieTv AND s.inCorso=1 GROUP BY stv.idSerieTv, stv.dataUscita ORDER BY stv.dataUscita DESC");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByAnnoMinoreDi($anno)
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita < ?  ORDER BY dataUscita DESC", [(string)$anno]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByAnnoMinoreUgualeDi($anno)
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita <= ?  ORDER BY dataUscita DESC", [(string)$anno]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    /** UGUALE A */
    public function getSerieTvByAnno($annoInit, $annoFin): array
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita >= ? and dataUscita <= ? ORDER BY dataUscita DESC", [(string)$annoInit, (string)$annoFin]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByAnnoMaggioreUgualeDi($anno)
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita >= ?  ORDER BY dataUscita DESC", [(string)$anno]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByAnnoMaggioreDi($anno)
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita > ?  ORDER BY dataUscita DESC", [(string)$anno]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByNazionalita(string $codNaz)
    {
        $result = DB::select("SELECT * FROM serieTvs st, nazioneProduziones_serieTvs npst WHERE st.idSerieTv = npst.serieTv AND npst.nazioneProduzione=? ORDER BY st.dataUscita DESC", [$codNaz]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByFilmmaker(Filmmaker $filmmaker): array
    {
        //$result = DB::select("SELECT * FROM serieTvs s, ruolos_serieTvs rs, ruolos r, ruolos_filmmakers rf WHERE s.idSerieTv = rs.serieTv AND rs.ruolo = r.idRuolo AND r.idRuolo = rf.ruolo AND rf.filmmaker = ? GROUP BY s.idSerieTv DESC", [$filmmaker->getIdFilmMaker()]);
        $result = DB::select("SELECT DISTINCT s.* FROM serieTvs s, ruolos_serieTvs rs, ruolos r, ruolos_filmmakers rf WHERE s.idSerieTv = rs.serieTv AND rs.ruolo = r.idRuolo AND r.idRuolo = rf.ruolo AND rf.filmmaker = ?  ORDER BY s.idSerieTv DESC", [$filmmaker->getIdFilmMaker()]);
        if(empty($result)){
            return [];
        }
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByTitoloOrTitoloOrig(string $titolo)
    {
        $titoloList = explode(" ", $titolo);
        $titoloList2 = array();
        foreach ($titoloList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($titoloList2, "%" . $cerca . "%");
            }
        }
        foreach ($titoloList2 as $k => $tl) {
            $resultList[$k] = DB::select("SELECT * FROM serieTvs WHERE UPPER(titoloOriginale) LIKE UPPER(?) OR UPPER(titolo) LIKE UPPER(?) GROUP BY idSerieTv ORDER BY dataUscita DESC", [$tl, $tl]);
        }
        $resultMonoList = array();
        $idList = array();
        foreach ($resultList as $k => $results) {
            foreach ($results as $kr => $result) {
                if (count($idList) === 0) {
                    array_push($idList, $result->idSerieTv);
                    array_push($resultMonoList, $result);
                } else {
                    $trovato = false;
                    foreach ($idList as $key => $value) {
                        if ($value === $result->idSerieTv) {
                            $trovato = true;
                        }
                    }
                    if (!$trovato) {
                        array_push($idList, $result->idSerieTv);
                        array_push($resultMonoList, $result);
                    }
                }
            }
        }
        throw_when(empty($resultList), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $resultMonoList);
    }

    public function getSerieTvByTitolo(string $titolo)
    {
        $titoloList = explode(" ", $titolo);
        $titoloList2 = array();
        foreach ($titoloList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($titoloList2, "%" . $cerca . "%");
            }
        }
        foreach ($titoloList2 as $k => $tl) {
            $resultList[$k] = DB::select("SELECT * FROM serieTvs WHERE UPPER(titolo) LIKE UPPER(?) GROUP BY idSerieTv ORDER BY dataUscita DESC", [$tl]);
        }
        $resultMonoList = array();
        $idList = array();
        foreach ($resultList as $k => $results) {
            foreach ($results as $kr => $result) {
                if (count($idList) === 0) {
                    array_push($idList, $result->idSerieTv);
                    array_push($resultMonoList, $result);
                } else {
                    $trovato = false;
                    foreach ($idList as $key => $value) {
                        if ($value === $result->idSerieTv) {
                            $trovato = true;
                        }
                    }
                    if (!$trovato) {
                        array_push($idList, $result->idSerieTv);
                        array_push($resultMonoList, $result);
                    }
                }
            }
        }
        throw_when(empty($resultList), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $resultMonoList);
    }

    public function getSerieTvByTitoloSearch($titolo): array
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE titolo LIKE ? GROUP BY idSerieTv ORDER BY dataUscita DESC", [$titolo]);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByTitoloOriginale(string $titoloOrig)
    {
        $titoloList = explode(" ", $titoloOrig);
        $titoloList2 = array();
        foreach ($titoloList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($titoloList2, "%" . $cerca . "%");
            }
        }
        foreach ($titoloList2 as $k => $tl) {
            $resultList[$k] = DB::select("SELECT * FROM serieTvs WHERE UPPER(titoloOriginale) LIKE UPPER(?) GROUP BY idSerieTv ORDER BY dataUscita DESC", [$tl]);
        }
        $resultMonoList = array();
        $idList = array();
        foreach ($resultList as $k => $results) {
            foreach ($results as $kr => $result) {
                if (count($idList) === 0) {
                    array_push($idList, $result->idSerieTv);
                    array_push($resultMonoList, $result);
                } else {
                    $trovato = false;
                    foreach ($idList as $key => $value) {
                        if ($value === $result->idSerieTv) {
                            $trovato = true;
                        }
                    }
                    if (!$trovato) {
                        array_push($idList, $result->idSerieTv);
                        array_push($resultMonoList, $result);
                    }
                }
            }
        }
        throw_when(empty($resultList), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $resultMonoList);
    }

    public function getSerieTvByGenere(int $idGenere)
    {
        $result = DB::select("SELECT * FROM serieTvs st, generes_serieTvs gst WHERE st.idSerieTv = gst.serieTv AND gst.genere=? GROUP BY st.idSerieTv, st.dataUscita ORDER BY st.dataUscita DESC", [$idGenere]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByPremio(int $idPremio)
    {
        $result = DB::select("SELECT * FROM serieTvs st, nominations_serieTvs nst, nominations n, premio_tipologiaPremios ptp WHERE st.idSerieTv = nst.serieTv AND nst.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio=? GROUP BY st.idSerieTv, st.dataUscita ORDER BY st.dataUscita DESC", [$idPremio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByAnnoVittoria($annoInit, $annoFin, int $vi)
    {
        $result = DB::select("SELECT * FROM serieTvs st, nominations_serieTvs nst, nominations n WHERE st.idSerieTv = nst.serieTv AND nst.nomination = n.idNomination AND n.dataPremiazione >= ? AND n.dataPremiazione <= ? AND n.stato = ? GROUP BY st.idSerieTv, st.dataUscita ORDER BY st.dataUscita", [$annoInit, $annoFin, $vi]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvDistributore(int $idDistributore)
    {
        $result = DB::select("SELECT * FROM serieTvs st, distributores_serieTvs dst WHERE st.idSerieTv = dst.serieTv AND dst.distributore = ? GROUP BY st.idSerieTv, st.dataUscita ORDER BY st.dataUscita", [$idDistributore]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObjectList([], $result);
    }

    public function getSerieTvByRuolo(Ruolo $ruolo)
    {
        $result = DB::select("SELECT idSerieTv, titoloOriginale, titolo, descrizione, trama, tramaBreve, dataSviluppoSerieTv, dataUscita, ageRating, rs.idRuoloSerieTv FROM ruolos_serieTvs as rs , serieTvs as s WHERE rs.serieTv = s.idSerieTv and rs.ruolo = ? GROUP BY s.idSerieTv", [$ruolo->getIdRuolo()]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setSerieTvObject(new SerieTv(), $result[0]);
    }

    public function getSerieTvByNomeCognomeFilmmaker(string $persona)
    {
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, $cerca);
            }
        }
        $sql_statico1 = "SELECT stv.* FROM ruolos_serieTvs rstv, ruolos_filmmakers rfm, filmmakers fm, serieTvs stv WHERE rstv.ruolo = rfm.ruolo AND rfm.filmmaker = fm.idFilmMaker AND rstv.serieTv = stv.idSerieTv AND (";
        $sql_statico2 = ") GROUP BY stv.idSerieTv, stv.dataUscita ORDER BY stv.dataUscita";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql);
        return self::setSerieTvObjectList([], $result);
    }

    public function getCountStagioniBySerieTv(SerieTv $serieTv){
        return DB::select("SELECT COUNT(idStagione) AS 'numeroStagioni' FROM stagiones WHERE serieTv=?", [$serieTv->getIdSerieTv()])[0]->numeroStagioni;
    }

    public function getCountEpisodiBySerieTv(SerieTv $serieTv){
        return DB::select("SELECT COUNT(ep.idEpisodio) AS 'numeroEpisodi' FROM stagiones st, episodios ep WHERE st.idStagione=ep.stagione AND st.serieTv=?", [$serieTv->getIdSerieTv()])[0]->numeroEpisodi;
    }

    public function getDurataEpisodiBySerieTv(SerieTv $serieTv){
        return DB::select("SELECT ep.durata AS 'durata' FROM stagiones st, episodios ep WHERE st.idStagione=ep.stagione AND st.serieTv=? LIMIT 1", [$serieTv->getIdSerieTv()])[0]->durata;
    }

    public function getUltimeSerieTvCarosello(): array{
        $result = DB::select("SELECT * FROM serieTvs ORDER BY createDate DESC LIMIT 16");
        return self::setSerieTvObjectList([], $result);
    }

    public function getUltimeDieciSerieTvCreate(): array
    {
        $result = DB::select("SELECT * FROM serieTvs ORDER BY createDate DESC LIMIT 10");
        return self::setSerieTvObjectList([], $result);
    }

    public function getUltimecinqueSerieTv(): array
    {
        $result = DB::select("SELECT * FROM serieTvs ORDER BY dataUscita DESC LIMIT 5");
        return self::setSerieTvObjectList([], $result);
    }

    public function getAllSerieTvPaginati($limit, $offset): array
    {
        $result = DB::select("SELECT * FROM serieTvs ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countAllSerieTvParzialiPerLista()
    {
        $result = DB::select("SELECT COUNT(idSerieTv) as numeroserieTvs FROM serieTvs");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvInCorsoPaginato($limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT stv.* FROM serieTvs stv, stagiones s WHERE s.serieTv = stv.idSerieTv and s.inCorso = true ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvInCorso()
    {
        $result = DB::select("SELECT COUNT(stv.idSerieTv) as numeroserieTvs FROM serieTvs stv, stagiones s WHERE s.serieTv = stv.idSerieTv and s.inCorso = true GROUP BY stv.idSerieTv");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvByAnnoPaginato($annoInit, $annoFin, $limit, $offset): array
    {
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita >= ? and dataUscita <= ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [(string)$annoInit, (string)$annoFin, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvByAnno($annoInit, $annoFin)
    {
        $result = DB::select("SELECT COUNT(idSerieTv) as numeroserieTvs FROM serieTvs WHERE dataUscita >= ? and dataUscita <= ? GROUP BY idSerieTv", [(string)$annoInit, (string)$annoFin]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvByNazionePaginato($codiceNazione, $limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT stv.* FROM serieTvs stv, nazioneProduziones_serieTvs npstv WHERE stv.idSerieTv = npstv.serieTv AND npstv.nazioneProduzione = ? ORDER BY stv.dataUscita DESC LIMIT ? OFFSET ?", [$codiceNazione, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvByNazione($codiceNazione)
    {
        $result = DB::select("SELECT COUNT(DISTINCT stv.idSerieTv) as numeroserieTvs FROM serieTvs stv, nazioneProduziones_serieTvs npstv WHERE stv.idSerieTv = npstv.serieTv and npstv.nazioneProduzione = ? GROUP BY stv.idSerieTv", [$codiceNazione]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvByPremioPaginato($idPremio, $limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT stv.*  FROM serieTvs stv, nominations_serieTvs ns, nominations n, premio_tipologiaPremios ptp WHERE stv.idSerieTv = ns.serieTv AND ns.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio = ? ORDER BY stv.dataUscita DESC LIMIT ? OFFSET ?", [$idPremio, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvByPremio($idPremio)
    {
        $result =  DB::select("SELECT COUNT(stv.idSerieTv) as numeroserieTvs FROM serieTvs stv, nominations_serieTvs ns, nominations n, premio_tipologiaPremios ptp WHERE stv.idSerieTv = ns.serieTv AND ns.nomination = n.idNomination AND n.premio_tipologiaPremio = ptp.idPremio_tipologiaPremio AND ptp.premio = ? GROUP BY stv.idSerieTv", [$idPremio]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvByIdGenerePaginato($idGenere, $limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT stv.* FROM serieTvs stv, generes_serieTvs gstv WHERE stv.idSerieTv = gstv.serieTv AND gstv.genere = ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$idGenere, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvByIdGenere($idGenere)
    {
        $result = DB::select("SELECT COUNT(idSerieTv) as numeroserieTvs FROM serieTvs stv, generes_serieTvs gstv WHERE stv.idSerieTv = gstv.serieTv AND gstv.genere = ? GROUP BY stv.idSerieTv", [$idGenere]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function getSerieTvByDistributorePaginato($idDistributore, $limit, $offset): array
    {
        $result = DB::select("SELECT DISTINCT stv.* FROM serieTvs stv, distributores_serieTvs dstv WHERE stv.idSerieTv = dstv.serieTv AND dstv.distributore = ? ORDER BY dataUscita DESC LIMIT ? OFFSET ?", [$idDistributore, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvByDistributore($idDistributore): int
    {
        $result = DB::select("SELECT COUNT(stv.idSerieTv) as numeroserieTvs FROM serieTvs stv, distributores_serieTvs dstv WHERE stv.idSerieTv = dstv.serieTv AND dstv.distributore = ? GROUP BY stv.idSerieTv", [$idDistributore]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroserieTvs;
    }

    public function maggiorediCinqueVoti(SerieTv $serieTv): bool
    {
        $result = DB::select("SELECT COUNT(idRecensione) as numerorecensioni FROM recensiones WHERE serieTv = ?", [$serieTv->getIdSerieTv()]);
        if (!empty($result)) {
            if ($result[0]->numerorecensioni < 5) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public function getSerieTvByIdNomination(int $idNomination): SerieTv
    {
        $serieTv = new SerieTv();
        $result = DB::select("SELECT * FROM serieTvs stv, nominations_serieTvs nstv WHERE stv.idSerieTv = nstv.serieTv AND nstv.nomination = ?", [$idNomination]);
        if (empty($result)) {
            return $serieTv;
        }
        return self::setSerieTvObject($serieTv, $result);
    }

    public function countSerieTv(){
        $result = DB::select("SELECT COUNT(idSerieTv) as numeroSerieTv FROM serieTvs ");
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroSerieTv;
    }

    public function getSerieTvInUscitaPaginato($limit, $offset):array{
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita > ? ORDER BY dataUscita LIMIT ? OFFSET ?", [(string)$dataOggi, $limit, $offset]);
        return self::setSerieTvObjectList([], $result);
    }

    public function countSerieTvInUscita(){
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT COUNT(idSerieTv) as numeroSerieTv FROM serieTvs WHERE dataUscita > ?", [(string)$dataOggi]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numeroSerieTv;
    }

    public function getProssimoFilmInUscita(): SerieTv
    {
        $serieTv = new SerieTv();
        $dataOggi = date("Y/m/d");
        $result = DB::select("SELECT * FROM serieTvs WHERE dataUscita > ? ORDER BY dataUscita LIMIT 1", [(string)$dataOggi]);
        if (empty($result)) return $serieTv;
        return self::setSerieTvObject($serieTv, $result);
    }

    public function getSerieTvByTitoloOrTitoloOrigSearch(string $search): array
    {
        $sql_statico0 = "SELECT ";
        $sql_statico1 = "s.* FROM serieTvs s WHERE (";
        $sql_statico2 = ") GROUP BY s.idSerieTv, s.dataUscita ORDER BY s.dataUscita";
        $sql_dinamico = "";
        $searchList = explode(" ", $search);
        $searchList2 = array();
        foreach ($searchList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($searchList2, "%" . $cerca . "%");
            }
        }
        foreach ($searchList2 as $key => $search) {
            if ($key === count($searchList2) - 1) {
                $sql_dinamico = $sql_dinamico . " UPPER(s.titolo) LIKE UPPER('" . $search . "')";
            } else {
                $sql_dinamico = $sql_dinamico . " UPPER(s.titolo) LIKE UPPER('" . $search . "') OR";
            }
        }
        $sql = $sql_statico0 . $sql_statico1 . $sql_dinamico . $sql_statico2;
        $result = DB::select($sql);
        return self::setSerieTvObjectList([], $result);
    }

}