<?php


namespace App\Services\Implementation;


use App\Model\Distributore;
use DB;
use Slim\Exception\HttpBadRequestException;

class DistributoreServiceImplementation
{

    /**
     * DistributoreServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setDistributoreObject(Distributore $distributore, $result): Distributore {

        $distributore->setIdDistributore($result->idDistributore);
        $distributore->setNome($result->nome);
        $distributore->setDescrizione($result->descrizione);
        return $distributore;
    }

    private function setDistributoreObjectList($distributoreList, $result) {
        foreach ($result as $obj) {
            $distributore = new Distributore();
            $distributore = $this->setDistributoreObject($distributore, $obj);
            array_push($distributoreList, $distributore);
        }
        return $distributoreList;
    }

    public function getDistributoreById(int $idDistributore): Distributore {
        $distributore = new Distributore();
        $result = DB::select("SELECT * FROM distributores WHERE idDistributore = ?", [$idDistributore]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setDistributoreObject($distributore, $result[0]);
    }

    public function getAllDistributore() {
        $result = DB::select("SELECT * FROM distributores");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setDistributoreObjectList([], $result);
    }

    public function getDistributoriByIdFilm(int $idFilm): array {
        $result = DB::select('SELECT * FROM distributores d, distributores_films df WHERE d.idDistributore = df.distributore and df.film = ?', [$idFilm]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setDistributoreObjectList([], $result);
    }

    public function getDistributoriByIdSerieTv(int $idSerieTv): array {
        $result = DB::select('SELECT * FROM distributores d, distributores_serieTvs ds WHERE d.idDistributore = ds.distributore and ds.serieTv = ?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setDistributoreObjectList([], $result);
    }

    public function insertDistributore(Distributore $distributore) {
        $result = DB::insert('INSERT INTO distributores (nome, descrizione) VALUE (?, ?)',
            [$distributore->getNome(), $distributore->getDescrizione()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertDistributoreFilm($idDistributore, $idFilm) {
        $result = DB::insert('INSERT INTO distributores_films (distributore, film) VALUE (?, ?)',
            [$idDistributore, $idFilm]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertDistributoreSerieTv($idDistributore, $idSerieTv) {
        $result = DB::insert('INSERT INTO distributores_serieTvs (distributore, serieTv) VALUE (?, ?)',
            [$idDistributore, $idSerieTv]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateDistributore(Distributore $distributore) {
        $result = DB::update('UPDATE distributores SET nome = ?, descrizione = ? WHERE idDistributore = ?',
            [$distributore->getNome(), $distributore->getDescrizione(), $distributore->getIdDistributore()]);
        return $result;
    }

    public function deleteDistributoreFilm(int $idFilm) {
        return DB::delete('DELETE FROM distributores_films WHERE film = ?', [$idFilm]);
    }

    public function deleteDistributore(Distributore $distributore) {
        $result = DB::delete('DELETE FROM distributores WHERE idDistributore = ?', [$distributore->getIdDistributore()]);
        return $result;
    }

    public function deleteDistributoreSerieTvByIdSerieTv(int $idSerieTv){
        $result = DB::delete('DELETE FROM distributores_serieTvs WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }
}