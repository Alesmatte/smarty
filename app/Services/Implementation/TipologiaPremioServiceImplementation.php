<?php


namespace App\Services\Implementation;


use App\Model\TipologiaPremio;
use Slim\Exception\HttpBadRequestException;
use DB;

class TipologiaPremioServiceImplementation
{

    /**
     * TipologiaPremioServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setTipologiaPremioObject(TipologiaPremio $tipologiaPremio, $result): TipologiaPremio {
        $tipologiaPremio->setidTipologiaPremio($result->idTipologiaPremio);
        $tipologiaPremio->setNome($result->nome);
        return $tipologiaPremio;
    }

    private function setTipologiaPremioObjectList($tipologiaPremioList, $result) {
        foreach ($result as $obj) {
            $tipologiaPremio = new TipologiaPremio();
            $tipologiaPremio = $this->setTipologiaPremioObject($tipologiaPremio, $obj);
            array_push($tipologiaPremioList, $tipologiaPremio);
        }
        return $tipologiaPremioList;
    }

    public function getTipologiaPremioById($idTipologiaPremio): TipologiaPremio {
        $tipologiaPremio = new TipologiaPremio();
        $result = DB::select("SELECT * FROM tipologiaPremios WHERE idTipologiaPremio = ?", [$idTipologiaPremio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setTipologiaPremioObject($tipologiaPremio, $result[0]);
    }

    public function getAllTipologiaPremio() {
        $result = DB::select("SELECT * FROM tipologiaPremios");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setTipologiaPremioObjectList([], $result);
    }


    /* NON DOVREBBE SERVIRE  */
    public function insertTipologiaPremio(TipologiaPremio $tipologiaPremio) {
        $result = DB::insert('INSERT INTO tipologiaPremios (nome) VALUE (?)',
            [$tipologiaPremio->getNome()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

}