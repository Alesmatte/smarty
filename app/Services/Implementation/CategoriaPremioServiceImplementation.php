<?php


namespace App\Services\Implementation;


use App\Model\CategoriaPremio;
use App\Model\Premio;
use App\Model\TipologiaPremio;
use DB;
use Slim\Exception\HttpBadRequestException;

class CategoriaPremioServiceImplementation
{
    /* tabella intermedia Premio_TipologiaPremio */

    /**
     * CategoriaPremioServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setCategoriaPremioObject(CategoriaPremio $categoriaPremio, $result): CategoriaPremio {
        $categoriaPremio->setIdCategoriaPremio($result->idPremio_tipologiaPremio);

        $premio = new Premio();
        $premio->setIdPremio($result->premio);
        $premio->setNome($result->nomePremio);
        $premio->setDescrizione($result->descrizione);
        $premio->setLocalita($result->localita);
        $categoriaPremio->setPremio($premio);

        $tipologiaPremio = new TipologiaPremio();
        $tipologiaPremio->setidTipologiaPremio($result->tipologiaPremio);
        $tipologiaPremio->setNome($result->nomeTipologiaPremio);
        $categoriaPremio->setTipologia($tipologiaPremio);

        return $categoriaPremio;
    }

    private function setCategoriaPremioObjectList($categoriaPremioList, $result) {
        foreach ($result as $obj) {
            $categoriaPremio = new CategoriaPremio();
            $categoriaPremio = $this->setCategoriaPremioObject($categoriaPremio, $obj);
            array_push($categoriaPremioList, $categoriaPremio);
        }
        return $categoriaPremioList;
    }

    public function getCategoriaPremioById(int $idCategoriaPremio): CategoriaPremio {
        $categoriaPremio = new CategoriaPremio();
        $result = DB::select("SELECT 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio,
                            p.descrizione, 
                            p.localita, 
                            tp.idTipologiaPremio,
                            tp.nome AS nomeTipologiaPremio
                            FROM premio_tipologiaPremios ptp, premios p, tipologiaPremios tp
                            WHERE ptp.premio = p.idPremio and ptp.tipologiaPremio = tp.idTipologiaPremio and ptp.idPremio_tipologiaPremio = ?", [$idCategoriaPremio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setCategoriaPremioObject($categoriaPremio, $result[0]);
    }

    public function getCategoriaPremioByIdTipologiaPremioAndByIdPremio($idTipologiaPremio, $idPremio): CategoriaPremio {
        $categoriaPremio = new CategoriaPremio();
        $result = DB::select("SELECT 
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio,
                            p.descrizione, 
                            p.localita, 
                            tp.idTipologiaPremio,
                            tp.nome AS nomeTipologiaPremio
                            FROM premio_tipologiaPremios ptp, premios p, tipologiaPremios tp
                            WHERE ptp.premio = p.idPremio and ptp.tipologiaPremio = tp.idTipologiaPremio and ptp.tipologiaPremio = ? and ptp.premio = ?", [$idTipologiaPremio, $idPremio]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setCategoriaPremioObject($categoriaPremio, $result[0]);
    }

    public function getAllCategoriaPremio(): array {
        $result = DB::select("SELECT  
                            ptp.idPremio_tipologiaPremio, 
                            ptp.premio, 
                            ptp.tipologiaPremio, 
                            p.idPremio, 
                            p.nome AS nomePremio,
                            p.descrizione, 
                            p.localita, 
                            tp.idTipologiaPremio,
                            tp.nome AS nomeTipologiaPremio
                            FROM premio_tipologiaPremios ptp, premios p, tipologiaPremios tp");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setCategoriaPremioObjectList([], $result);
    }

    public function insertCategoriaPremio(CategoriaPremio $categoriaPremio) {
        $result = DB::insert('INSERT INTO premio_tipologiaPremios (premio, tipologiaPremio) VALUE (?, ?)',
            [$categoriaPremio->getPremio(), $categoriaPremio->getTipologia()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateCategoriaPremio(CategoriaPremio $categoriaPremio) {
        return DB::update('UPDATE premio_tipologiaPremios SET premio = ?, tipologiaPremio = ? WHERE idPremio_tipologiaPremio = ?',
            [$categoriaPremio->getPremio(), $categoriaPremio->getTipologia(), $categoriaPremio->getIdCategoriaPremio()]);
    }

    public function deleteCategoriaPremio(CategoriaPremio $categoriaPremio) {
        return DB::delete('DELETE FROM premio_tipologiaPremios WHERE idPremio_tipologiaPremio', [$categoriaPremio->getIdCategoriaPremio()]);
    }
}