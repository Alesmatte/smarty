<?php


namespace App\Services\Implementation;

use App\Model\Film;
use App\Model\SerieTv;
use DB;
use App\Model\Filmmaker;
use Slim\Exception\HttpBadRequestException;

class FilmMakerServiceImplementation
{

    /**
     * FilmMakerServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setFilmMakerObject(Filmmaker $filmmaker, $result): Filmmaker {
        $filmmaker->setIdFilmMaker($result->idFilmMaker);
        $filmmaker->setNome($result->nome);
        $filmmaker->setCognome($result->cognome);
        $filmmaker->setBiografia($result->biografia);
        $filmmaker->setBreveDescrizione($result->breveDescrizione);
        $filmmaker->setCitazione($result->citazione);
        $filmmaker->setTipologiaPrincipale($result->tipologiaPrincipale);
        if (isset($result->serieTv)) {
            $serieTv = new SerieTv();
            $serieTv->setIdSerieTv($result->serieTv);
            $filmmaker->setFilmSerieTvCitazione($serieTv);
        } else if (isset($result->film)){
            $film = new Film();
            $film->setIdFilm($result->film);
            $filmmaker->setFilmSerieTvCitazione($film);
        }
        return $filmmaker;
    }

    private function setFilmMakerObjectList($filmMakerList, $result) {
        foreach ($result as $obj) {
            $filmMaker = new Filmmaker();
            $filmMaker = $this->setFilmMakerObject($filmMaker, $obj);
            array_push($filmMakerList, $filmMaker);
        }
        return $filmMakerList;
    }

    public function searchFilmMakerByNameAndSurname(string $queryString){
        $queryString = "%".$queryString."%";
        $filmMaker = new Filmmaker();
        $result = DB::select("SELECT * FROM filmmakers where nome like ? || cognome like ?", [$queryString, $queryString]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmMakerById(int $idFilmMaker): Filmmaker {
        $filmMaker = new Filmmaker();
        $result = DB::select("SELECT * FROM filmmakers where idFilmMaker = ?", [$idFilmMaker]);
        throw_when(empty($result), "rusultato query per il filmmaker vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setFilmMakerObject($filmMaker, $result[0]);
    }

    public function getFilmMakerByIdNomination(int $idNomination): Filmmaker {
        $filmMaker = new Filmmaker();
        $result = DB::select("SELECT * FROM filmmakers f, nominations_filmmakers nf where f.idFilmMaker = nf.fimmaker and nf.nomination = ?", [$idNomination]);
        if (empty($result)) {
            return $filmMaker;
        }
        return self::setFilmMakerObject($filmMaker, $result[0]);
    }

    public function getAllFilmMaker() {
        $result = DB::select("SELECT * FROM filmmakers");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setFilmMakerObjectList([], $result);
    }

    public function insertFilmMaker(Filmmaker $filmmaker) {
        if($filmmaker->getFilmSerieTvCitazione() !== null){
            if ($filmmaker->getFilmSerieTvCitazione() instanceof Film) {
                $result = DB::insert('INSERT INTO filmmakers (nome, cognome, biografia, breveDescrizione, citazione, tipologiaPrincipale, film) 
            VALUE (?, ?, ?, ?, ?, ?, ?)',
                    [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale(), $filmmaker->getFilmSerieTvCitazione()->getIdFilm()]);
            } else {
                $result = DB::insert('INSERT INTO filmmakers (nome, cognome, biografia, breveDescrizione, citazione, tipologiaPrincipale, serieTv) 
            VALUE (?, ?, ?, ?, ?, ?, ?)',
                    [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale(), $filmmaker->getFilmSerieTvCitazione()->getIdSerieTv()]);
            }
        } else {
            $result = DB::insert('INSERT INTO filmmakers (nome, cognome, biografia, breveDescrizione, citazione, tipologiaPrincipale) 
            VALUE (?, ?, ?, ?, ?, ?)',
                [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale()]);

        }
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateFilmMaker(Filmmaker $filmmaker) {
        if($filmmaker->getFilmSerieTvCitazione() !== null){
            if ($filmmaker->getFilmSerieTvCitazione() instanceof Film) {
                $result = DB::update('UPDATE filmmakers SET nome = ?, cognome = ?, biografia = ?, breveDescrizione = ?, citazione = ?, tipologiaPrincipale = ?, film = ? WHERE idFilmMaker = ?',
                    [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale(), $filmmaker->getFilmSerieTvCitazione()->getIdFilm(), $filmmaker->getIdFilmMaker()]);
            } else {
                $result = DB::update('UPDATE filmmakers SET nome = ?, cognome = ?, biografia = ?, breveDescrizione = ?, citazione = ?, tipologiaPrincipale = ?, serieTv = ? WHERE idFilmMaker = ?',
                    [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale(), $filmmaker->getFilmSerieTvCitazione()->getIdSerieTv(), $filmmaker->getIdFilmMaker()]);
            }
        } else {
            $result = DB::update('UPDATE filmmakers SET nome = ?, cognome = ?, biografia = ?, breveDescrizione = ?, citazione = ?, tipologiaPrincipale = ?, film = null, serieTv = null WHERE idFilmMaker = ?',
                [$filmmaker->getNome(), $filmmaker->getCognome(), $filmmaker->getBiografia(), $filmmaker->getBreveDescrizione(), $filmmaker->getCitazione(), $filmmaker->getTipologiaPrincipale(), $filmmaker->getIdFilmMaker()]);

        }

        return $result;
    }

    public function deleteFilmMaker(Filmmaker $filmMaker) {
        return DB::delete('DELETE FROM filmmakers WHERE idFilmMaker = ?', [$filmMaker->getIdFilmMaker()]);
    }

    public function deleteFilmMakerNominationByIdFilmmakerAndIdNomination($idFilmmaker, $idNomination) {
        return DB::delete('DELETE FROM nominations_filmmakers WHERE fimmaker = ? and nomination = ?', [$idFilmmaker, $idNomination]);
    }

    public function deleteFilmMakerNominationByFilmmaker(Filmmaker $filmmaker) {
        return DB::delete('DELETE FROM nominations_filmmakers WHERE fimmaker = ?', [$filmmaker->getIdFilmMaker()]);
    }

    public function getFilmmakerFilmByNomeCognome(string $persona){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, "%" . $cerca . "%");
            }
        }
        $sql_statico1 = "SELECT fm.* FROM ruolos_films rf, ruolos_filmmakers rfm, filmmakers fm, films f WHERE rf.ruolo = rfm.ruolo AND rfm.filmmaker = fm.idFilmMaker AND rf.film = f.idFilm AND (";
        $sql_statico2 = ") GROUP BY fm.idFilmMaker, fm.nome ORDER BY fm.nome";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('".$pl."') OR UPPER(fm.nome) LIKE UPPER('".$pl."')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('".$pl."') OR UPPER(fm.nome) LIKE UPPER('".$pl."') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerFilmByNomeCognomeRuolo(string $persona, int $ruolo){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, "%" . $cerca . "%");
            }
        }
        $sql_statico1 = "SELECT fm.* FROM filmmakers fm, films f, ruolos_films rf, ruolos r, ruolos_filmmakers rfm WHERE f.idFilm = rf.film AND  rf.ruolo = r.idRuolo AND r.idRuolo = rfm.ruolo AND fm.idFilmMaker = rfm.filmmaker AND r.tipologia = ? AND (";
        $sql_statico2 = ") GROUP BY fm.idFilmMaker, fm.nome ORDER BY fm.nome";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql,[$ruolo]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerSerieTvByNomeCognome(string $persona){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, "%" . $cerca . "%");
            }
        }
        $sql_statico1 = "SELECT fm.* FROM ruolos_serieTvs rstv, ruolos_filmmakers rfm, filmmakers fm, serieTvs stv WHERE rstv.ruolo = rfm.ruolo AND rfm.filmmaker = fm.idFilmMaker AND rstv.serieTv = stv.idSerieTv AND (";
        $sql_statico2 = ") GROUP BY fm.idFilmMaker, fm.nome ORDER BY fm.nome";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerSerieTvByNomeCognomeRuolo(string $persona, int $ruolo){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, "%" . $cerca . "%");
            }
        }
        $sql_statico1 = "SELECT fm.* FROM filmmakers fm, serieTvs stv, ruolos_serieTvs rstv, ruolos r, ruolos_filmmakers rfm WHERE stv.idSerieTv = rstv.serieTv AND  rstv.ruolo = r.idRuolo AND r.idRuolo = rfm.ruolo AND fm.idFilmMaker = rfm.filmmaker AND r.tipologia = ? AND (";
        $sql_statico2 = ") GROUP BY fm.idFilmMaker, fm.nome ORDER BY fm.nome";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('%".$pl."%') OR UPPER(fm.nome) LIKE UPPER('%".$pl."%') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql,[$ruolo]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerByNomeOrCognome(string $persona){
        $personaList = explode(" ", $persona);
        $personaList2 = array();
        foreach ($personaList as $key => $cerca) {
            if ($cerca !== '') {
                array_push($personaList2, "%" . $cerca . "%");
            }
        }
        $sql_statico1 = "SELECT fm.* FROM filmmakers fm WHERE ";
        $sql_statico2 = " GROUP BY fm.idFilmMaker, fm.nome ORDER BY fm.nome";
        $sql_dinamico = "";
        foreach ($personaList2 as $ke=>$pl){
            if($ke === count($personaList2)-1){
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('".$pl."') OR UPPER(fm.nome) LIKE UPPER('".$pl."')";
            } else {
                $sql_dinamico = $sql_dinamico. " UPPER(fm.cognome) LIKE UPPER('".$pl."') OR UPPER(fm.nome) LIKE UPPER('".$pl."') OR";
            }
        }
        $sql = $sql_statico1.$sql_dinamico.$sql_statico2;
        $result = DB::select($sql);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerByFilmAndTipologiaRuolo(Film $film, int $ruolo): array {
        $result = DB::select("SELECT * FROM filmmakers fm, ruolos_filmmakers rfm, ruolos r, ruolos_films rf WHERE fm.idFilmMaker = rfm.filmmaker AND rfm.ruolo = r.idRuolo AND r.idRuolo = rf.ruolo AND rf.film = ? AND r.tipologia = ?", [$film->getIdFilm(), $ruolo]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerBySerieTvAndTipologiaRuolo(SerieTv $serieTv, int $ruolo): array {
        $result = DB::select("SELECT * FROM filmmakers fm, ruolos_filmmakers rfm, ruolos r, ruolos_serieTvs rs WHERE fm.idFilmMaker = rfm.filmmaker AND rfm.ruolo = r.idRuolo AND r.idRuolo = rs.ruolo AND rs.serieTv = ? AND r.tipologia = ?", [$serieTv->getIdSerieTv(), $ruolo]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerByTipologiaPrincipale(int $tp, $limit, $offset):array {
        $result = DB::select("SELECT * FROM filmmakers WHERE tipologiaPrincipale = ? ORDER BY nome LIMIT ? OFFSET ?", [$tp, $limit, $offset]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function getFilmmakerByTipologiaFilmmaker(int $tp, $limit, $offset):array {
        $result = DB::select("SELECT * FROM filmmakers fm, ruolos_filmmakers rfm, ruolos r WHERE fm.idFilmMaker = rfm.filmmaker AND rfm.ruolo = r.idRuolo AND r.tipologia = ?", [$tp]);
        return self::setFilmMakerObjectList([], $result);
    }

    public function countFilmmakerByTipologiaPrincipale(int $tp){
        $result = DB::select("SELECT DISTINCT COUNT(idFilmMaker) as numerofilmmaker FROM filmmakers fm, ruolos_filmmakers rfm, ruolos r WHERE fm.idFilmMaker = rfm.filmmaker AND rfm.ruolo = r.idRuolo AND r.tipologia = ?", [$tp]);
        if (empty($result)) {
            return 0;
        }
        return $result[0]->numerofilmmaker;
    }
}