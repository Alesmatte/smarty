<?php


namespace App\Services\Implementation;

use App\Model\Filmmaker;
use DB;
use App\Model\Ruolo;
use Slim\Exception\HttpBadRequestException;

class RuoloServiceImplementation
{

    /**
     * RuoloServiceImplementation constructor.
     */
    public function __construct()
    {
    }


    private function setRuoloObject(Ruolo $ruolo, $result): Ruolo {

        $ruolo->setIdRuolo($result->idRuolo);
        $ruolo->setRuoloInternoOpera($result->ruoloInternoOpera);
        $ruolo->setTipologia($result->tipologia);

        if (isset($result->filmmaker)) {
            $filmmaker = new Filmmaker();
            $filmmaker->setIdFilmMaker($result->filmmaker);
            $ruolo->setFilmMaker($filmmaker);
        }

        return $ruolo;
    }

    private function setRuoloObjectList($ruoloList, $result) {
        foreach ($result as $obj) {
            $ruolo = new Ruolo();
            $ruolo = $this->setRuoloObject($ruolo, $obj);
            array_push($ruoloList, $ruolo);
        }
        return $ruoloList;
    }

    private function setTipologiaRuoloObjectList($ruoloList, $result) {
        foreach ($result as $obj) {
            $ruolo = new Ruolo();
            $ruolo = $this->setTipologiaRuoloObject($ruolo, $obj);
            array_push($ruoloList, $ruolo);
        }
        return $ruoloList;
    }

    private function setTipologiaRuoloObject(Ruolo $ruolo, $result): Ruolo {
        $ruolo->setTipologia($result->tipologia);
        return $ruolo;
    }

    public function getRuoloById(int $idRuolo): Ruolo {
        $ruolo = new Ruolo();
        $result = DB::select("SELECT * FROM ruolos r, ruolos_filmmakers rf WHERE r.idRuolo = rf.ruolo and  idRuolo = ?", [$idRuolo]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setRuoloObject($ruolo, $result[0]);
    }

    public function getAllRuolo(): array {
        $result = DB::select("SELECT * FROM ruolos r, ruolos_filmmakers rf WHERE r.idRuolo = rf.ruolo");
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloObjectList([], $result);
    }

    public function getRuoliByidFilm(int $idFilm): array {
        $result = DB::select('SELECT * FROM ruolos r, ruolos_films rfi, ruolos_filmmakers rf WHERE r.idRuolo = rfi.ruolo and r.idRuolo = rf.ruolo and rfi.film = ? GROUP BY r.idRuolo', [$idFilm]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloObjectList([], $result);
    }

    public function getRuoliByidSerieTv(int $idSerieTv): array {
        $result = DB::select('SELECT * FROM ruolos r, ruolos_serieTvs rs, ruolos_filmmakers rf WHERE r.idRuolo = rs.ruolo and r.idRuolo = rf.ruolo and rs.serieTv = ? GROUP BY r.idRuolo', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloObjectList([], $result);
    }

    public function getRuoliByIdFilmmaker(int $idFilmmaker): array {
        $result = DB::select('SELECT * FROM ruolos r, ruolos_filmmakers rfi, ruolos_filmmakers rf WHERE r.idRuolo = rfi.ruolo and r.idRuolo = rf.ruolo and rfi.filmmaker = ? GROUP BY r.idRuolo', [$idFilmmaker]);
        return self::setRuoloObjectList([], $result);
    }

    public function insertRuolo(Ruolo $ruolo) {
        $result = DB::insert('INSERT INTO ruolos (ruoloInternoOpera, tipologia) VALUE (?, ?)',
            [$ruolo->getRuoloInternoOpera(), $ruolo->getTipologia()]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertRuoloFilm(int $idRuolo, int $idFilm) {
        $result = DB::insert('INSERT INTO ruolos_films (ruolo, film) VALUE (?, ?)',
            [$idRuolo, $idFilm]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertRuoloSerieTv(int $idRuolo, int $idSerieTv) {
        $result = DB::insert('INSERT INTO ruolos_serieTvs (ruolo, serieTv) VALUE (?, ?)',
            [$idRuolo, $idSerieTv]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function insertRuoloFilmmaker(int $idRuolo, int $idFilmmaker) {
        $result = DB::insert('INSERT INTO ruolos_filmmakers (ruolo, filmmaker) VALUE (?, ?)',
            [$idRuolo, $idFilmmaker]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateRuolo(Ruolo $ruolo) {
        return DB::update('UPDATE ruolos SET ruoloInternoOpera = ?, tipologia = ? WHERE idRuolo = ?',
            [$ruolo->getRuoloInternoOpera(), $ruolo->getTipologia(), $ruolo->getIdRuolo()]);
    }

    public function updateRuoloFilmmakerByIdFilmmaker(int $idRuolo, int $idFilmmaker) {
        return DB::update('UPDATE ruolos_filmmakers SET ruolo = ?, filmmaker = ? WHERE ruolo = ?',
            [$idRuolo, $idFilmmaker, $idRuolo]);
    }

    public function deleteRuoloById(Ruolo $ruolo) {
        return DB::delete('DELETE FROM ruolos WHERE idRuolo = ?', [$ruolo->getIdRuolo()]);
    }

    public function deleteRuoloFilmByIdFilm(int $idFilm) {
        return DB::delete('DELETE FROM ruolos_films WHERE film = ?', [$idFilm]);
    }

    public function deleteRuoloFilmByIdRuolo(int $idRuolo) {
        return DB::delete('DELETE FROM ruolos_films WHERE ruolo = ?', [$idRuolo]);
    }

    public function deleteRuoloSerieTvByIdRuolo(int $idRuolo) {
        return DB::delete('DELETE FROM ruolos_serieTvs WHERE ruolo = ?', [$idRuolo]);
    }

    public function deleteRuoloFilmmakerByIdRuolo(int $idRuolo) {
        return DB::delete('DELETE FROM ruolos_filmmakers WHERE ruolo = ?', [$idRuolo]);
    }

    public function deleteRuoloSerieTvByIdSerieTv(int $idSerieTv){
        $result = DB::delete('DELETE FROM ruolos_serieTvs WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }

    private function setRuoloSerieTvObjectList($result): array {
        $idRuoloList = array();
        foreach ($result as $obj) {
            array_push($idRuoloList, $obj->ruolo);
        }
        return $idRuoloList;
    }

    public function getRuoloSerieTvRuoloByidSerieTv(int $idSerieTv) {
        $result = DB::select('SELECT * FROM ruolos_serieTvs  WHERE serieTv = ?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloSerieTvObjectList($result);
    }
    public function getRuoloByidFilmAndFilmmaker(int $idFilm, Filmmaker $filmmaker) {
        $result = DB::select('SELECT r.idRuolo, r.ruoloInternoOpera, r.tipologia, rfil.filmmaker FROM ruolos_films as rf, ruolos as r, ruolos_filmmakers as rfil WHERE rf.ruolo = r.idRuolo AND rfil.ruolo = r.idRuolo AND rf.film=? AND rfil.filmmaker=?', [$idFilm, $filmmaker->getIdFilmMaker()]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloObjectList([], $result);
    }

    public function getRuoloByidSerieTvAndFilmmaker(int $idSerieTv, Filmmaker $filmmaker) {
        $result = DB::select('SELECT r.idRuolo, r.ruoloInternoOpera, r.tipologia, rfil.filmmaker FROM ruolos_serieTvs as rs, ruolos as r, ruolos_filmmakers as rfil WHERE rs.ruolo = r.idRuolo AND rfil.ruolo = r.idRuolo AND rs.serieTv=? AND rfil.filmmaker=?', [$idSerieTv, $filmmaker->getIdFilmMaker()]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setRuoloObjectList([], $result);
    }

    public function getTipologiaRuolo(){
        $result = DB::select('SELECT tipologia FROM ruolos GROUP BY tipologia ORDER BY tipologia');
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setTipologiaRuoloObjectList([], $result);
    }
}