<?php


namespace App\Services\Implementation;

use App\Model\Immagine;
use App\Model\Video;
use DB;
use Slim\Exception\HttpBadRequestException;

class VideoServiceImplementation
{

    /**
     * VideoServiceImplementation constructor.
     */
    public function __construct()
    {
    }

    private function setVideoObject(Video $video, $result): Video {
        $video->setIdVideo($result->idVideo);
        $video->setNome($result->nomeVideo);
        $video->setDescrizione($result->descrizioneVideo);
        $video->setDato($result->datoVideo);

        $immagine = new Immagine();
        $immagine->setIdImmagione($result->idImmagine);
        $immagine->setNome($result->nomeImg);
        $immagine->setDescrizione($result->descrizioneImg);
        $immagine->setDato($result->datoImg);
        $immagine->setTipo($result->tipo);
        $video->setImmagine($immagine);
        return $video;
    }

    private function setVideoObjectList($videoList, $result) {
        foreach ($result as $obj) {
            $video = new Video();
            $video = $this->setVideoObject($video, $obj);
            array_push($videoList, $video);
        }
        return $videoList;
    }

    public function getVideoById(int $idVideo): Video {
        $video = new Video();
        $result = DB::select('SELECT
                                    v.idVideo, 
                                    v.nome AS nomeVideo, 
                                    v.descrizione AS descrizioneVideo, 
                                    v.dato AS datoVideo, 
                                    v.serieTv, 
                                    v.film,
                                    i.idImmagine, 
                                    i.nome AS nomeImg, 
                                    i.descrizione AS descrizioneImg, 
                                    i.dato AS datoImg,  
                                    i.tipo, 
                                    i.video
                                    FROM videos v, immagines i WHERE v.idVideo = i.video AND idVideo = ?', [$idVideo ]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        throw_when(count($result)>1, "errore valori multipli");
        return self::setVideoObject($video, $result[0]);
    }

    public function getAllVideo(): array {
        $result = DB::select('SELECT
                                    v.idVideo, 
                                    v.nome AS nomeVideo, 
                                    v.descrizione AS descrizioneVideo, 
                                    v.dato AS datoVideo, 
                                    v.serieTv, 
                                    v.film,
                                    i.idImmagine, 
                                    i.nome AS nomeImg, 
                                    i.descrizione AS descrizioneImg, 
                                    i.dato AS datoImg,  
                                    i.tipo, 
                                    i.video
                                    FROM videos v, immagines i WHERE v.idVideo = i.video');
        if (empty($result)) {
            return [];
        }
        return self::setVideoObjectList([], $result);
    }

    public function getVideoByIdFilm(int $idFilm): array {
        $result = DB::select('SELECT
                                    v.idVideo, 
                                    v.nome AS nomeVideo, 
                                    v.descrizione AS descrizioneVideo, 
                                    v.dato AS datoVideo, 
                                    v.serieTv, 
                                    v.film,
                                    i.idImmagine, 
                                    i.nome AS nomeImg, 
                                    i.descrizione AS descrizioneImg, 
                                    i.dato AS datoImg,  
                                    i.tipo, 
                                    i.video
                                    FROM videos v, immagines i WHERE v.idVideo = i.video and v.film = ?', [$idFilm]);
        if (empty($result)) {
            return [];
        }
        return self::setVideoObjectList([], $result);
    }

    public function getVideoByIdSerieTv(int $idSerieTv): array {
        $result = DB::select('SELECT
                                    v.idVideo, 
                                    v.nome AS nomeVideo, 
                                    v.descrizione AS descrizioneVideo, 
                                    v.dato AS datoVideo, 
                                    v.serieTv, 
                                    v.film,
                                    i.idImmagine, 
                                    i.nome AS nomeImg, 
                                    i.descrizione AS descrizioneImg, 
                                    i.dato AS datoImg,  
                                    i.tipo, 
                                    i.video
                                    FROM videos v, immagines i WHERE v.idVideo = i.video and v.serieTv = ?', [$idSerieTv]);
        if (empty($result)) {
            return [];
        }
        return self::setVideoObjectList([], $result);
    }

    public function insertVideo(Video $video, $idSerieTv = null, $idFilm = null) {
        $result = DB::insert('INSERT INTO videos (nome, descrizione, dato, serieTv, film) VALUE (?, ?, ?, ?, ?)',
            [$video->getNome(), $video->getDescrizione(), $video->getDato(), $idSerieTv, $idFilm]);
        throw_when(!($result), "errore inserimento",  HttpBadRequestException::class);
        return DB::connection()->getPdo()->lastInsertId();
    }

    public function updateVideo(Video $video) {
        return DB::update('UPDATE videos SET nome = ?, descrizione = ?, dato = ? WHERE idVideo = ?',
            [$video->getNome(), $video->getDescrizione(), $video->getDato(), $video->getIdVideo()]);
    }

    public function deleteVideo(Video $video) {
        return DB::delete('DELETE FROM videos WHERE idVideo = ?', [$video->getIdVideo()]);
    }

    public function deleteVideoByIdSerieTv(int $idSerieTv){
        $result = DB::delete('DELETE FROM videos WHERE serieTv = ?', [$idSerieTv]);
        return $result;
    }

    public function getIdVideoByIdSerieTv(int $idSerieTv){
        $result = DB::select('SELECT * FROM videos WHERE serieTv = ?', [$idSerieTv]);
        throw_when(empty($result), "rusultato query vuoto", HttpBadRequestException::class);
        return self::setVideoObjectList([], $result);
    }



}