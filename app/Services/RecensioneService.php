<?php


namespace App\Services;


use App\Model\Recensione;
use App\Model\User;
use App\Services\Implementation\ImmagineServiceImplementation;
use App\Services\Implementation\RecensioneServiceImplementation;

class RecensioneService
{

    /**
     * RecensioneService constructor.
     */
    public function __construct()
    {
    }

    public function getRecensioniPersonalByUserFrom(User $user): array
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getRecensioniByIdUser($user->getIdUser());
    }

    public function getRecensioniPersonalByUserPaginato(User $user, $limit = PHP_INT_MAX, $offset = 0): array
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getRecensioniPersonalByUserPaginato($user->getIdUser(), $limit, $offset);
    }

    public function countRecensioniPersonalByUser(User $user): int
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countRecensioniPersonalByUser($user->getIdUser());
    }

    public function getRecensioniPersonalSearchByUserPaginato(string $search, User $user, $limit, $offset): array
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $immagineService = new ImmagineService();
        $recensioneList = $recensioneServiceImpl->getRecensioniPersonalSearchByUser($search, $user->getIdUser(), $limit, $offset);
        if (!empty($recensioneList)) {
            foreach ($recensioneList as $key => $recensione) {
                $recensione->setUtente($user);
                $recensioneList[$key] = $recensione;
            }
        }
        return $recensioneList;
    }

    public function countRecensioniPersonalSearchByUser(User $user, string $search): int
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countRecensioniPersonalSearchByUser($user->getIdUser(), $search);
    }

    public function getRecensioniByIdFilm(int $idaFilm, $limit = PHP_INT_MAX, $offset = 0, int $tipo = 2): array
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $immagineService = new ImmagineService();
        if ($tipo == 0) {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdFilmNormale($idaFilm, $limit, $offset);
        } elseif ($tipo == 1) {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdFilmCritico($idaFilm, $limit, $offset);
        } else {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdFilm($idaFilm, $limit, $offset);
        }
        if (!empty($recensioneList)) {
            foreach ($recensioneList as $key => $recensione) {
                $user = $recensione->getUtente();
                $result = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
                if (!empty($result)) {
                    $user->setAvatar($result[0]);
                }
                $recensione->setUtente($user);
                $recensioneList[$key] = $recensione;
            }
        }
        return $recensioneList;
    }

    public function getLastRecensioneByIdFilm(int $idaFilm): Recensione
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getLastRecensioneByIdFilm($idaFilm);
    }

    public function getLastRecensioniPaginato($limit, $offset): array
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $imgServiceImp = new ImmagineServiceImplementation();
        $listrec= $recensioneServiceImpl->getLastRecensioni($limit, $offset);
        foreach ($listrec as $rec){
            $user = $rec->getUtente();
            $imgs = $imgServiceImp->getImgByEntitaAndIdEntitaAndTipologia("user",$user->getIdUser(),0);
            if(!empty($imgs)){
                $user->setAvatar($imgs[0]);
            }
            $rec->setUtente($user);
        }
        return $listrec;
    }

    public function getLastRecensioniBySearchPaginato($cerca, $limit, $offset): array
    {
        $cerca = "%" . $cerca . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getLastRecensioniBySearchPaginato($cerca, $limit, $offset);
    }

    public function countLastRecensioniBySearch($cerca): int
    {
        $cerca = "%" . $cerca . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countLastRecensioniBySearch($cerca);
    }

    public function countGetRecensioniByIdFilm(int $idaFilm)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniByIdFilm($idaFilm);
    }

    public function countGetRecensioniPrTipoUtenteByIdFilm(int $idaFilm, int $tipoUtente)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniPrTipoUtenteByIdFilm($idaFilm, $tipoUtente);
    }



    public function getRecensioniByIdSerieTv(int $idSerieTv, $limit = PHP_INT_MAX, $offset = 0, int $tipo = 2): array
    {

        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $immagineService = new ImmagineService();
        if ($tipo == 0) {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdSerieTvNormale($idSerieTv, $limit, $offset);
        } elseif ($tipo == 1) {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdSerieTvCritico($idSerieTv, $limit, $offset);
        } else {
            $recensioneList = $recensioneServiceImpl->getRecensioniByIdSerieTv($idSerieTv, $limit, $offset);
        }
        if (!empty($recensioneList)) {
            foreach ($recensioneList as $key => $recensione) {
                $user = $recensione->getUtente();
                $result = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
                if (!empty($result)) {
                    $user->setAvatar($result[0]);
                }
                $recensione->setUtente($user);
                $recensioneList[$key] = $recensione;
            }
        }
        return $recensioneList;
    }

    public function getLastRecensioneByIdSerieTv(int $idSerieTv): Recensione
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getLastRecensioneByIdSerieTv($idSerieTv);
    }

    public function countGetRecensioniByIdSerieTv(int $idSerieTv)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniByIdSerieTv($idSerieTv);
    }

    public function countGetRecensioniPerTipoUserByIdSerieTv(int $idSerieTv, int $tipoUser)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniPerTipoUserByIdSerieTv($idSerieTv, $tipoUser);
    }


    public function getRecensioniBySearchIdFilm(string $search, int $idaFilm, $limit, $offset): array
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $immagineService = new ImmagineService();
        $recensioneList = $recensioneServiceImpl->getRecensioniBySearchIdFilm($search, $idaFilm, $limit, $offset);
        if (!empty($recensioneList)) {
            foreach ($recensioneList as $key => $recensione) {
                $user = $recensione->getUtente();
                $result = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
                if (!empty($result)) {
                    $user->setAvatar($result[0]);
                }
                $recensione->setUtente($user);
                $recensioneList[$key] = $recensione;
            }
        }
        return $recensioneList;
    }

    public function countGetRecensioniSearchByIdFilm(int $idaFilm, string $search)
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniSearchByIdFilm($idaFilm, $search);
    }

    public function getRecensioniBySearchIdSerieTv(string $search, int $idSerieTv, $limit, $offset): array
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $immagineService = new ImmagineService();
        $recensioneList = $recensioneServiceImpl->getRecensioniBySearchIdSerieTv($search, $idSerieTv, $limit, $offset);
        if (!empty($recensioneList)) {
            foreach ($recensioneList as $key => $recensione) {
                $user = $recensione->getUtente();
                $result = $immagineService->getImgByEntitaAndIdEntitaAndTipologia('user', $user->getIdUser(), 0);
                if (!empty($result)) {
                    $user->setAvatar($result[0]);
                }
                $recensione->setUtente($user);
                $recensioneList[$key] = $recensione;
            }
        }
        return $recensioneList;
    }

    public function countGetRecensioniSearchByIdSerieTv(int $idSerieTv, string $search)
    {
        $search = "%" . $search . "%";
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countGetRecensioniSearchByIdSerieTv($idSerieTv, $search);
    }

    public function getRecensioniByIdUserAndIdFilm(int $idUser, int $idFilm): bool
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getRecensioniByIdUserAndIdFilm($idUser, $idFilm);
    }

    public function getRecensioniByIdUserAndIdSerieTv(int $idUser, int $idSerieTv): bool
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getRecensioniByIdUserAndIdSerieTv($idUser, $idSerieTv);
    }

    public function insertRecensioneFilm(Recensione $recensione, int $idFilm)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $recensioneServiceImpl->insertRecensione($recensione, null, $idFilm);
    }

    public function insertRecensioneSerieTv(Recensione $recensione, int $idSerieTv)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $recensioneServiceImpl->insertRecensione($recensione, $idSerieTv);
    }

    public function updateRecensione(Recensione $recensione)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $recensioneServiceImpl->updateRecensione($recensione);
    }

    public function deleteRecensione(Recensione $recensione)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->deleteRecensione($recensione);
    }

    public function deleteRecensioneByIdFilm(int $idFilm)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->deleteRecensioneByIdFilm($idFilm);
    }

    public function getRecensioneById(int $idRecenzione)
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getRecensioneById($idRecenzione);
    }

    private function setUserAvatarIntoRecensione(Recensione $recensione): Recensione{
        $userService= new UserService();
        $recensione->setUtente($userService->setUltimiDieciUtenti($recensione->getUtente()));
        return $recensione;
    }

    public function getUltimiTrentaRecensioniRegistrate(): array{
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $array_recensioni = array();
        foreach ($recensioneServiceImpl->getUltimiTrentaRecensioniRegistrate() as $recensione){
            array_push($array_recensioni, self::setUserAvatarIntoRecensione($recensione));
        }
        return $array_recensioni;
    }

    public function getUltimiQuindiciRecensioniUtenteNormaleRegistrate(): array{
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $array_recensioni = array();
        foreach ($recensioneServiceImpl->getUltimiQuindiciRecensioniUtenteNormaleRegistrate() as $recensione){
            array_push($array_recensioni, self::setUserAvatarIntoRecensione($recensione));
        }
        return $array_recensioni;
    }

    public function getUltimiQuindiciRecensioniUtenteCriticoRegistrate(): array{
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        $array_recensioni = array();
        foreach ($recensioneServiceImpl->getUltimiQuindiciRecensioniUtenteCriticoRegistrate() as $recensione){
            array_push($array_recensioni, self::setUserAvatarIntoRecensione($recensione));
        }
        return $array_recensioni;
    }

    public function getFilmORSerietvFromRecensione(Recensione $recensione){
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->getFilmORSerietvFromRecensione($recensione);
    }

    public function countRecensioni(): int
    {
        $recensioneServiceImpl = new RecensioneServiceImplementation();
        return $recensioneServiceImpl->countRecensioni();
    }
}