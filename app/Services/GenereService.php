<?php


namespace App\Services;


use App\Model\Genere;
use App\Services\Implementation\GenereServiceImplementation;

class GenereService
{

    /**
     * GenereService constructor.
     */
    public function __construct()
    {
    }
    public function insertGenere(Genere $genere) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->insertGenere($genere);
    }

    public function insertGenereFilm(int $idGenere, int $idFilm) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->insertGenereFilm($idGenere, $idFilm);
    }

    public function insertGenereSerieTv(int $idGenere, int $idSerieTv) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->insertGenereSerieTv($idGenere, $idSerieTv);
    }

    public function updateGenere(Genere $genere) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->updateGenere($genere);

    }

    public function deleteGenere(Genere $genere) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->deleteGenere($genere);
    }

    public function deleteGenereFilmByIdFilm(int $idFilm) {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->deleteGenereFilmByIdFilm($idFilm);
    }

    public function getAllGenere() {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getAllGenere();
    }

    public function getAllGenereFilm() {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getAllGenereFilm();
    }

    public function getAllGenereSerieTv() {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getAllGenereSerieTv();
    }

    public function getGenereByIdFilm($idFilm): array {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getGenereByIdFilm($idFilm);
    }

    public function getGenereById(int $idGenere): Genere {
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getGenereById($idGenere);
    }

    public function getIDGenereNomeGenere(){
        $genereServiceImpl = new GenereServiceImplementation();
        return $genereServiceImpl->getIDGenereNomeGenere();
    }
}