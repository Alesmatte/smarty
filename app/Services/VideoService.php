<?php


namespace App\Services;


use App\Model\Film;
use App\Model\SerieTv;
use App\Model\Video;
use App\Services\Implementation\VideoServiceImplementation;

class VideoService
{

    /**
     * VideoService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $idVideo
     * @return Video
     */
    public function getVideoById($idVideo): Video {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->getVideoById($idVideo);
    }

    /**
     * @param $idFilm
     * @return Video
     */
    public function getVideoPrincipaleIdByFilm($idFilm): Video {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->getVideoByIdFilm($idFilm)[0];
    }

    /**
     * @param $idSerieTv
     * @return Video
     */
    public function getVideoPrincipaleByIdSerieTv($idSerieTv): Video {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->getVideoByIdSerieTv($idSerieTv)[0];
    }

    /**
     * @param $idFilm
     * @return array
     */
    public function getVideoByIdFilm($idFilm): array {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->getVideoByIdFilm($idFilm);
    }

    /**
     * @param $idserieTv
     * @return array
     */
    public function getVideosByIdSerieTv($idserieTv): array {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->getVideoByIdSerieTv($idserieTv);
    }

    /**
     * @param Video $video
     * @param $idFilm
     * @return mixed
     */
    public function insertVideoFilm(Video $video, $idFilm) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->insertVideo($video, null, $idFilm);
    }

    public function updateVideo(Video $video) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->updateVideo($video);
    }

    public function deleteOnlyVideo(Video $video) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->deleteVideo($video);
    }

    public function deleteVideo(Video $video) {
        $videoServiceImpl = new VideoServiceImplementation();
        $immagineService = new ImmagineService();
        $immagineService->deleteImgByIdVideo($video);
        return $videoServiceImpl->deleteVideo($video);
    }

    /**
     * @param Video $video
     * @param $idSerieTv
     * @return mixed
     */
    public function insertVideoSerieTv(Video $video, $idSerieTv) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->insertVideo($video, $idSerieTv);
    }

    public function updateVideoSerieTv(Video $video) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->updateVideo($video);
    }

    public function deleteVideoSerieTv(Video $video) {
        $videoServiceImpl = new VideoServiceImplementation();
        return $videoServiceImpl->deleteVideo($video);
    }
}